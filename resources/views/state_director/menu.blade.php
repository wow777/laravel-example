@php
    $base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
@endphp

<li class="{{ request()->segment(4) == 'dashboard' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/dashboard">
        <i class="fa fa-user" aria-hidden="true"></i>
        <span>*Dashboard</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.churches' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/churches">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Churches</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.contacts' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/contacts">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Contacts</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.agencies' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/agencies">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Agencies</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.requests' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/requests">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Requests</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.ips' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/implementing-partners">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>*IP's</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.communities' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/communities">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>*Community</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.events' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/events">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>*Events</span>
    </a>
</li>
<li class="{{ \Route::currentRouteName() == 'rm.finances' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/finances">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>*Finances</span>
    </a>
</li>

<li class="{{ \Route::currentRouteName() == 'rm.settings' ? 'active' : '' }}">
    <a class="link" href="{{$base_url}}/settings">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Settings</span>
    </a>
</li>

<li class="treeview">
    <!-- starting with Laravel 5.3  /logout is now a POST -->
    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                class="fa fa-sign-out "></i>
        Logout
    </a>
    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>
