<div id="addNoteDialog" class="modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Note</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('notes.store', [$roleCode, $roleId])}}" method="post" id="contact-note-form">
                    <input type="hidden" name="notable_id" value="{{$notable->id}}">
                    <input type="hidden" name="notable_type" value="{{get_class($notable)}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="control-label">
                            <span>Note</span>
                        </label>
                        <textarea name="body" class="form-control" id="" cols="30" rows="10" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="$('#contact-note-form').submit();">Save</button>
            </div>
        </div>

    </div>
</div>