@if($roles->count() > 1)
    <div style="padding:12px 5px 12px 15px;">
        <div style="color:white;">Role:</div>
        <select id="role-select">
            <option value=""></option>
            @foreach ($roles as $role)
                <option {{$role->selected ? 'selected' : ''}} value="{{ $role->route }}">{{ $role->name }}</option>
            @endforeach
        </select>
    </div>
@endif

@if($regions->count() > 0)
    <div style="padding:12px 5px 12px 15px;">
        <div style="color:white;">Regions:</div>
        <select id="region-select" onchange="location.href = $(this).val();">
            @foreach ($regions as $region)
                <option value="{{ route('role.dashboard', ['role' => 'regional_manager', 'role_id' => $region->id]) }}"
                        {{$region->id == $currentRole->id ? 'selected':''}}>
                    {{ $region->name }}
                </option>
            @endforeach
        </select>
    </div>
@endif

@if($states->count() > 0)
    <div style="padding:12px 5px 12px 15px;">
        <div style="color:white;">State:</div>
        <select id="state-select" onchange="location.href = $(this).val();">
            @foreach ($states as $state)
                <option value="{{ route('role.dashboard', ['role' => 'state_director', 'role_id' => $state->id]) }}"
                        {{$state->id == $currentRole->id ? 'selected':''}}>
                    {{ $state->name }}
                </option>
            @endforeach
        </select>
    </div>
@endif

@section('scripts')
    <script type="text/javascript">
        $(document).on('change', '#role-select', function () {
            if($(this).find('option:selected').val().length) {
                location.href = $(this).find('option:selected').val()
            }
        });
    </script>
@append