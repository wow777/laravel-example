<label class="control-label">
    <span>Notes</span>
    <a href="#" data-toggle="modal" data-target="#addNoteDialog">+</a>
</label>
<a href="#" class="pull-right">Export</a>
<div style="height: 200px; overflow-y: scroll; background-color: #fff;">
    <table class="table">
        @foreach($notable->notes as $note)
            <tr>
                <td>
                    {{$note->created_at->format('m/d/Y')}}
                </td>
                <td>
                    {{$note->type}}
                </td>
                <td>
                    {{$note->body}}
                </td>
                <td>
                    <a href="{{route('note.delete', [$roleCode, $roleId, $note])}}">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
</div>