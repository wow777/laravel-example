@extends('auth.layout')

@section('body')
    <div class="login-page">
        <div class="login-box">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <span>Sign in to start your session</span>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-md-6">
                        <input id="email" type="email" name="email" placeholder="example@gmail.com"
                               value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                             </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-md-6">
                        <input type="password" id="password" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                             </span>
                        @endif
                    </div>
                </div>
                <span class="rem">
                     <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                     Remember Me
                </span>
                <input class="sign-in" type="submit" value="Sign In">
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    I forgot my password
                </a>
                <a href="#">Register a new membership</a>
            </form>
        </div>
    </div>
@endsection