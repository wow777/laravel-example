@extends('church-enrollment.layout')
@section('title', 'Activate Your Community')

@section('body')
    @if (empty($church->orientation_date))
        <div class="content">
            <p>Church Orientation Details for:</p>
            <p class="c-name">{{ $church->name }}</p>

            <p>This church has not been activated. Please complete the activation process before you can proceed.</p>
        </div>
    @else
        <div class="content">
            <form action="{{route('storeActivation')}}" method="post">
                <input type="hidden" name="church_id" value="{{ $church->id }}">
                {{csrf_field()}}

                <p>Church Orientation Details for:</p>
                <p class="c-name">{{ $church->name }}</p>

                <h3>Submit a Plan</h3>
                <p>The greatest success with CarePortal comes from having a clear engagement plan.</p>

                <div class="form-block">
                    <div class="select-field">
                        <h4>Educate and Invite</h4>
                        <p>How will you engage your people with Care Portal ? <span class="important">*</span></p>
                        <select name="engage_congregation" required>
                            <option></option>
                            <option value="1">Pastor gives a vision-casting sermon that incorporates the CarePortal video and an individual sign-up process
                            </option>
                            <option value="2">Send email to congregation with the video link and opportunity to sign-up
                            </option>
                            <option value="3">Show video and provide a sign-up process</option>
                            <option value="4">A different way</option>
                        </select>
                    </div>
                    <div class="fill-field">
                        <h4>Share Your Ideas for Engagement <span class="important">*</span></h4>
                        <p>please share your ideas or questions regarding engaging your people with Care Portal</p>
                        <textarea class="enrollment-area" type="text" name="engage_share_your_ideas" required></textarea>
                    </div>
                </div>

                <div class="form-block">
                    <div class="select-field">
                        <h4>Share the Needs</h4>
                        <p>How will you share CarePortal requests with people that want to know</p>
                        <select name="how_will_you_share_requests" required>
                            <option value=""></option>
                            <option value="1">Call people you think could help</option>
                            <option value="2">Forward a portion of the emails and have them reply to the Point Person if they
                                can help
                            </option>
                            <option value="3">Forward the emails that come and let them respond directly</option>
                            <option value="4">Make the need known via Facebook or other social media</option>
                            <option value="5">Meet the need when you can using church resources</option>
                            <option value="6">Needs go out through small group leaders</option>
                            <option value="7">Send a text about the request</option>
                            <option value="8">A different way</option>
                        </select>
                    </div>

                    <div class="fill-field">
                        <h5>Share Your Ideas For Sharing CarePortal Needs<span class="important">*</span></h5>
                        <p>please share your ideas or questions regarding how you will share the
                            needs of children and families in crisis</p>
                        <textarea class="enrollment-area" name="share_needs_share_your_ideas" required></textarea>
                    </div>
                </div>


                <div class="form-block">
                    <div class="select-field">
                        <h4>Invest in Local children and families in crisis</h4>
                        <p>Our church's giving preference is</p>
                        <select name="giving_preference" id="giving_preference" required>
                            <option value=""></option>
                            <option value="1">We will give $100/month</option>
                            <option value="2">We will give $1,200/year</option>
                            <option value="3">We will give more than $100/month to provide access to financially
                                under-resourced churches.
                            </option>
                            <option value="4">We need to contribute at a lower amount this year.</option>
                            <option value="5">We plan to only collect a special offering for now</option>
                        </select>
                    </div>
                </div>

                <div class="form-block add-inputs" hidden="hidden" id="giv_freq_amount">
                    <div class="select-field">
                        <div class="form-sub-block">
                            <p>Giving Frequency<span class="important">*</span></p>
                            <select name="giving_frequency">
                                <option></option>
                                <option value="1">Monthly</option>
                                <option value="2">Quarterly</option>
                                <option value="3">Annually</option>
                                <option value="4">One-time</option>
                            </select>
                            <p>Giving Amount<span class="important">*</span></p>
                            <input name="giving_amount" type="text">
                        </div>
                    </div>
                </div>

                <div class="form-block add-inputs" id="datePic" hidden="hidden">
                    <div class="select-field">
                        <p>Special Offering Date <span class="important">*</span></p>
                        <input name="special_offering_date" id="datepicker">
                    </div>
                </div>

                <input type="submit" value="submit">
            </form>
        </div>
    @endif

    <script>
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,

        });
    </script>

@endsection


