@extends('church-enrollment.layouts.layout')
@section('title', 'Close Request')

@section('body')
    <div class="content">
        <p class="c-h-name p-details">Request Details</p>
        <div class="request-block">
            <ul>
                <li class="t-head">
                    <div class="u-data p-request-id">Request ID</div>
                    <div class="u-value">{{request()->segment(3)}}</div>
                </li>

                <li>
                    <div class="u-data">Agency ID</div>
                    <div class="u-value">{{$request->agency_id}}</div>
                </li>
                <li>
                    <div class="u-data">Grid Quadrant</div>
                    <div class="u-value">{{$request->tier}} - {{$request->role_of_worker}}</div>
                </li>
                <li>
                    <div class="u-data">Level of Urgency</div>
                    <div class="u-value">{{$request->level_of_urgency}}</div>
                </li>
                <li>
                    <div class="u-data">Children Served</div>
                    <div class="u-value">{{$request->children_served}}</div>
                </li>
                <li>
                    <div class="u-data">Estimated Need</div>
                    <div class="u-value">{{$request->estimated_value}}</div>
                </li>
                <li>
                    <div class="u-data">Needs List</div>
                    <div class="u-value">
                        @foreach ($request->needs as $needs)
                            {{$needs->value}}
                        @endforeach
                    </div>
                </li>
                <li>
                    <div class="u-data">Case Description</div>
                    <div class="u-value">
                        {{$request->case_description}}
                    </div>
                </li>
            </ul>
        </div>

        <hr>
        <p class="c-h-name">Previous Responses to This Request</p>
        <div class="p-request-block">
            <ul>
                <li class="p-r-head">
                    <div class="submitted-block">Submitted</div>
                    <div class="church-block">Church</div>
                    <div class="message-block">Message</div>
                </li>
                @foreach($request->responses as $response)
                    <li>
                        <div class="submitted-block">{{$response->created_at}}</div>
                        <div class="church-block">{{$response->church->name}}</div>
                        <div class="message-block">{{$response->message}}</div>
                    </li>
                @endforeach
            </ul>
        </div>

        <form action="{{route('storeCloseRequest')}}" method="post">
            {{csrf_field()}}
            <h2 class="c-h-name">Close This Request</h2>
            <h4 class="c-h-name">Why are you closing this request ? <span class="important">*</span></h4>
            <div class="close-request-block">
                <ul>
                    <li>
                        <input name="reason[]" class="checkbox-input" type="checkbox"
                               value="Church Responded and met the need">
                        <span>Church Responded and met the need</span>
                    </li>
                    <li>
                        <input name="reason[]" class="checkbox-input" type="checkbox"
                               value="Other resources came to meet the need">
                        <span>Other resources came to meet the need</span>
                    </li>
                    <li>
                        <input name="reason[]" class="checkbox-input" type="checkbox" value="Time has passed and no longer relevant">
                        <span>Time has passed and no longer relevant</span>
                    </li>
                    <li>
                        <input name="reason[]" class="checkbox-input" type="checkbox" value="Other reason">
                        <span>Other reason</span>
                    </li>
                    <li>
                        <input name="reason[]" class="checkbox-input" type="checkbox" value="I need to cancel this request">
                        <span>I need to cancel this request</span>
                    </li>

                </ul>
            </div>
            <div class="rating-box p-request-block">
                <p class="c-h-name sp-rating">CarePortal Rating <span class="important">*</span></p>
                <p class="u-data rating-question">How likely are you to use CarePortal again or tell a friend about
                    it?</p>
                <fieldset class="rating">
                    <input type="radio" id="star5" name="rating" value="5"/>
                    <label class="full" for="star5" title="Awesome - 5 stars"></label>
                    <input type="radio" id="star4half" name="rating" value="4.5"/>
                    <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="star4" name="rating" value="4"/>
                    <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="star3half" name="rating" value="3.5"/>
                    <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="star3" name="rating" value="3"/>
                    <label class="full" for="star3" title="Meh - 3 stars"></label>
                    <input type="radio" id="star2half" name="rating" value="2.5"/>
                    <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="star2" name="rating" value="2"/>
                    <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="star1half" name="rating" value="1.5"/>
                    <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="star1" name="rating" value="1"/>
                    <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="starhalf" name="rating" value="0.5"/>
                    <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <input type="hidden" name="rating_final" id="rating_final" value="">
            </div>


            <div class="note-box clearfull">
                <p class="c-h-name ">Please give the main reason you gave this rating</p>
                <textarea name="reason_text" id=""> </textarea>

                <p class="c-h-name">Please share how this experience personally impacted you or the family
                    (optional)</p>
                <p class="u-data">This field is optional, we might blog about your experience if you include your story
                    here.</p>
                <textarea name="experience_story" id=""> </textarea>
            </div>

            <input type="hidden" name="request_id" value="{{request()->segment(3)}}">
            <input type="submit" value="submit">
        </form>

    </div>

    <script>
        $(document).ready(function () {
            $('.rating input').click(function () {
                var rating = $(this).val();
                $('#rating_final').val(rating);
            });
        });
    </script>
@endsection