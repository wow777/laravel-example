@extends('church-enrollment.layouts.TiersLayout')
@section('body')

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif; max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('church-enrollment.layouts.TierHeader')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top'
                                   class="MainContainer">
                                <p>Tier 2 Application Training Update</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="content_data" id="i-padd" style="border: 0px; padding-top: 0px; position: relative;">
                <h3>Update The Training For This Application Record</h3>

                <h3>Service Type Training Dates</h3>
                <div class="train-block">
                    <div>
                        <h4>Training Completion Date</h4>
                        <ul>
                            <li>
                                <input class="hiddenDate" type="hidden" name="team_Meetings"/>
                                <a href="#" class="pickDate">click to enter date of training</a>
                            </li>
                            <li>
                                <input class="hiddenDate" type="hidden" name="supervision"/>
                                <a href="#" class="pickDate">click to enter date of training</a>
                            </li>
                            <li>
                                <input class="hiddenDate" type="hidden" name="transportation"/>
                                <a href="#" class="pickDate">click to enter date of training</a>
                            </li>
                        </ul>
                    </div>

                    <div>
                        <h4>Tier 2 Services Type</h4>
                        <ul>
                            <li><a href="">Family/Social Worker Team Meetings</a></li>
                            <li><a href="">Child Care/Supervision</a></li>
                            <li><a href="">Transportation</a></li>
                        </ul>
                    </div>
                </div>

                <div class="t-app-details">
                    <h3>Tier 2 Application Details</h3>
                    <h3>{Applicant FN LN}</h3>

                    <ul id="i-list">
                        <li>
                            <div>DOB</div>
                            <div>MM/DD/YY</div>
                        </li>
                        <li>
                            <div>Maiden</div>
                            <div>Maiden Name</div>
                        </li>
                        <li>
                            <div><p class="i-head">Contact information</p></div>
                        </li>
                        <li>
                            <div>Email</div>
                            <div>Email</div>
                        </li>
                        <li>
                            <div>Mobile Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Home Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Work Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Home Address</div>
                            <div>Home Address</div>
                        </li>
                        <li>
                            <div>Apt. No.</div>
                            <div>Apt. No.</div>
                        </li>
                        <li>
                            <div>City</div>
                            <div>City</div>
                        </li>
                        <li>
                            <div>State</div>
                            <div>State</div>
                        </li>
                        <li>
                            <div>Zip Code</div>
                            <div>XXXXX</div>
                        </li>
                        <li>
                            <div>Country</div>
                            <div>Country</div>
                        </li>
                        <li>
                            <div><p class="i-head">Demographics</p></div>
                        </li>
                        <li>
                            <div>Gender</div>
                        </li>
                        <li>
                            <div>Race</div>
                        </li>
                        <li>
                            <div>Ethnicity</div>
                        </li>
                        <li>
                            <div>Marital Status</div>
                        </li>
                        <li>
                            <div>Language(s) Spoken</div>
                        </li>
                        <li>
                            <div><p class="i-head">Employment</p></div>
                        </li>
                        `
                        <li>
                            <div>Employment Status</div>
                            <div>Employment Status</div>
                        </li>
                        <li>
                            <div>Occupation</div>
                            <div>Occupation</div>
                        </li>
                        <li>
                            <div>Employer Name</div>
                            <div>Employer Name</div>
                        </li>
                        <li>
                            <div>Employer Address</div>
                            <div>
                                <p>Employer Address line 1</p>
                                <p>Employer Address line 2</p>
                            </div>
                        </li>
                        <li>
                            <div>Employer Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Primary SSN</div>
                            <div>Primary SSN</div>
                        </li>
                        <li>
                            <div>Driver's License State</div>
                            <div>Driver's License State</div>
                        </li>
                        <li>
                            <div>Driver's License Number</div>
                            <div>Driver's License Number</div>
                        </li>
                    </ul>

                    <div class="miscel">
                        <p class="i-head">Miscellaneous</p>
                        <div>How did you hear about CarePortal?</div>
                        <div>How did you hear about CarePortal?</div>
                    </div>

                </div>

            </div>
        </td>
    </table>

    <script>
        $(function () {

            $('.hiddenDate').datepicker({
                changeYear: 'true',
                changeMonth: 'true',
                dateFormat: 'mm-dd-yy'
            });
            $('.pickDate').click(function (e) {
                $(e.target.previousElementSibling).datepicker("show");
                e.preventDefault();
            });
            // replace a tag text with a chosen date
            $('.hiddenDate').change(function () {
                let $this = $(this);
                $($this[0].nextElementSibling).text($this.val());
            });

        });

    </script>
@endsection