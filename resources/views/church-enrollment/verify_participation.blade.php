@extends('church-enrollment.layouts.layout')
@section('title', 'Verify Your Participation')
   
@section('body')
    <div class="content">

        <p class="c-h-name">Verify Your Participation</p>
        <input type="checkbox">
        <span>I verify that we helped meet this need</span>

        <div class="respond-form-section">
            <p class="c-h-name">Your Name</p>
            <p class="u-data">Please select your name from the drop-down below.
                if this is your first time responding click the + sign to add yourself to the system.
            </p>
            <select name="" id="">
                <option value="">Search for your name</option>
                <option value="">search</option>
                <option value="">your</option>
                <option value="">name</option>
            </select>

            <button type="button" class="plus-button">+</button>
        </div>

        <div class="rating-box">
            <p class="c-h-name">How likely are you to recommended CarePortal to friends at your church or another
                church </p>
            <fieldset class="rating">
                <input type="radio" id="star5" name="rating" value="5"/>
                <label class="full" for="star5" title="Awesome - 5 stars"></label>
                <input type="radio" id="star4half" name="rating" value="4 and a half"/>
                <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="star4" name="rating" value="4"/>
                <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="star3half" name="rating" value="3 and a half"/>
                <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="star3" name="rating" value="3"/>
                <label class="full" for="star3" title="Meh - 3 stars"></label>
                <input type="radio" id="star2half" name="rating" value="2 and a half"/>
                <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="star2" name="rating" value="2"/>
                <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="star1half" name="rating" value="1 and a half"/>
                <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="star1" name="rating" value="1"/>
                <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="starhalf" name="rating" value="half"/>
                <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset>
        </div>

        <div class="note-box clearfull">
            <p class="c-h-name">Please give the main reason you gave this rating</p>
            <textarea name="" id="#"> </textarea>

            <p class="c-h-name">Please share your story of how this experience personally
                impacted you or your church community(This is optional and we may blog about it)</p>
            <textarea name="" id="#"> </textarea>
        </div>


        <button class="verify-p">verify your participation</button>


        <div class="request-block">
            <ul>
                <li class="t-head">
                    <div class="u-data">Church Name</div>
                    <div class="u-value">{{$church->name}}</div>
                </li>

                <li class="t-head">
                    <div class="u-data">Request ID</div>
                    <div class="u-value">{{request()->segment(3)}}</div>
                </li>

                <li>
                    <div class="u-data">Grid Quadrant</div>
                    <div class="u-value">{{$request->tier}} - {{$request->role_of_worker}}</div>
                </li>
                <li>
                    <div class="u-data">Level of Urgency</div>
                    <div class="u-value">{{$request->level_of_urgency}}</div>
                </li>
                <li>
                    <div class="u-data">Children Served</div>
                    <div class="u-value">{{$request->children_served}}</div>
                </li>
                <li>
                    <div class="u-data">Estimated Need</div>
                    <div class="u-value">{{$request->estimated_value}}</div>
                </li>
                <li>
                    <div class="u-data">Needs List</div>
                    <div class="u-value">
                        @foreach ($request->needs as $needs)
                            {{$needs->value}}
                        @endforeach
                    </div>
                </li>
                <li>
                    <div class="u-data">Case Description</div>
                    <div class="u-value">
                        {{$request->case_description}}
                    </div>
                </li>
            </ul>
        </div>


    </div>
@endsection
