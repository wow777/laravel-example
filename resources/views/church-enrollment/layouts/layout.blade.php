<!DOCTYPE html>
<html lang="en">
<head>   
    <meta charset="utf-8">
    <title>@yield('title') | Care Portal</title>
    <meta name="viewport" content="width=560, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('css/style.css' )}}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


</head>
<body>
<div class="sign-up-wrapper">
    <div class="sign-up-container">
        <div class="head-panel">
            @if(request()->segment(2) !='close-request')
                <div class="ip-logo">
                    <img src="{{ Auth::user()->implementing_partner->logo_url ?? 'https://s3-us-west-2.amazonaws.com/careportal-ip-logo/logo_goproject1.png'}}" alt="">
                </div>
            @endif
            <div class="care-portal-logo">
                <img src="{{asset('img/connected-by-careportal-no-swooshes.png')}}" alt="connected-by-careportal">
            </div>

            <div style="clear:both"></div>

            <h1>@yield('title')</h1>
        </div>
        @yield('body')
    </div>
</div>

<script src="{{ asset('js/interactive.js') }}"></script>
</body>
</html>