@extends('church-enrollment.layouts.TiersLayout')
@section('body')

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif; max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('church-enrollment.layouts.TierHeader')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top'
                                   class="MainContainer">
                                <p>Tier 2 Application Background Check Approval</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div  class="content_data" id="i-padd" style="border: 0px; padding-top: 0px; position: relative;">
                <h3>Update This Background Check Application Record</h3>
                <p class="p-note">NOTE: Submitting "Approved" or "Not Approved" in the form below will send an email notification to the applicant.</p>

                <div class="up-status">
                    <div class="up-list">
                        <p>Update backtround Check Status</p>
                        <select name="check_status" id="check-status">
                            <option value="new">New</option>
                            <option value="approved">Approved</option>
                            <option value="not approved">Not Approved</option>
                            <option value="in progress">In Progress</option>
                        </select>
                    </div>
                    <div class="up-app">
                        <a href="" class="update-btn">UPDATE THIS APPLICATION</a>
                    </div>
                </div>

                <div class="t-app-details">
                    <h3>Tier 2 Application Details</h3>
                    <h3>{Applicant FN LN}</h3>

                    <ul id="i-list">
                        <li>
                            <div>DOB</div>
                            <div>MM/DD/YY</div>
                        </li>
                        <li>
                            <div>Maiden</div>
                            <div>Maiden Name</div>
                        </li>
                        <li>
                            <div><p class="i-head">Contact information</p></div>
                        </li>
                        <li>
                            <div>Email</div>
                            <div>Email</div>
                        </li>
                        <li>
                            <div>Mobile Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Home Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Work Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Home Address</div>
                            <div>Home Address</div>
                        </li>
                        <li>
                            <div>Apt. No.</div>
                            <div>Apt. No.</div>
                        </li>
                        <li>
                            <div>City</div>
                            <div>City</div>
                        </li>
                        <li>
                            <div>State</div>
                            <div>State</div>
                        </li>
                        <li>
                            <div>Zip Code</div>
                            <div>XXXXX</div>
                        </li>
                        <li>
                            <div>Country</div>
                            <div>Country</div>
                        </li>
                        <li>
                            <div><p class="i-head">Demographics</p></div>
                        </li>
                        <li>
                            <div>Gender</div>
                        </li>
                        <li>
                            <div>Race</div>
                        </li>
                        <li>
                            <div>Ethnicity</div>
                        </li>
                        <li>
                            <div>Marital Status</div>
                        </li>
                        <li>
                            <div>Language(s) Spoken</div>
                        </li>
                        <li>
                            <div><p class="i-head">Employment</p></div>
                        </li>
                        <li>
                            <div>Employment Status</div>
                            <div>Employment Status</div>
                        </li>
                        <li>
                            <div>Occupation</div>
                            <div>Occupation</div>
                        </li>
                        <li>
                            <div>Employer Name</div>
                            <div>Employer Name</div>
                        </li>
                        <li>
                            <div>Employer Address</div>
                            <div>
                                <p>Employer Address line 1</p>
                                <p>Employer Address line 2</p>
                            </div>
                        </li>
                        <li>
                            <div>Employer Phone</div>
                            <div>(xxx) xxx-xxxx</div>
                        </li>
                        <li>
                            <div>Primary SSN</div>
                            <div>Primary SSN</div>
                        </li>
                        <li>
                            <div>Driver's License State</div>
                            <div>Driver's License State</div>
                        </li>
                        <li>
                            <div>Driver's License Number</div>
                            <div>Driver's License Number</div>
                        </li>
                    </ul>

                    <div class="miscel">
                        <p class="i-head">Miscellaneous</p>
                        <div>How did you hear about CarePortal?</div>
                        <div>How did you hear about CarePortal?</div>
                    </div>

                </div>

            </div>
        </td>
    </table>


@endsection