@extends('church-enrollment.layouts.layout')
@section('title', 'Church To Church')

@section('body')
    <div class="content">
        <form action="" method="post">
            {{csrf_field()}}
            <p class="c-h-name">Request Details</p>
            <div class="request-block">
                <ul>
                    <li class="t-head">
                        <div class="u-data">Request ID</div>
                        <div class="u-value">{{request()->segment(3)}}</div>
                    </li>

                    <li>
                        <div class="u-data">Request Zip Code</div>
                        <div class="u-value">{{$request->zipCode->code}}</div>
                    </li>
                    <li>
                        <div class="u-data">Created On</div>
                        <div class="u-value">{{date('m-d-Y',strtotime($request->created_at))}}</div>
                    </li>
                    <li>
                        <div class="u-data">Level of urgency</div>
                        <div class="u-value">{{$request->level_of_urgency}}</div>
                    </li>
                    <li>
                        <div class="u-data">Children served</div>
                        <div class="u-value">{{$request->children_served}}</div>
                    </li>
                    <li>
                        <div class="u-data">Estimated Need</div>
                        <div class="u-value">{{$request->estimated_value}}</div>
                    </li>
                    <li>
                        <div class="u-data">Location</div>
                        <div class="u-value">{{$request->zipCode->city}}, {{$request->state->name}}
                            , {{$request->zipCode->code}}, {{$request->county->name}}</div>
                    </li>
                    <li>
                        <div class="u-data">Region</div>
                        <div class="u-value">{{$request->zipCode->region->name}}</div>
                    </li>
                    <li>
                        <div class="u-data">Needs List</div>
                        <div class="u-value">
                            @foreach ($request->needs as $needs)
                                {{$needs->value}}
                            @endforeach
                        </div>
                    </li>
                    <li>
                        <div class="u-data">Request Description</div>
                        <div class="u-value">
                            {{$request->case_description}}
                        </div>
                    </li>
                    <li>
                        <div class="u-data">Agency Contact</div>
                        <div class="u-value">{{$request->worker->full_name}}</div>
                    </li>
                </ul>
            </div>

            <hr>
            <p class="c-h-name">Previous Responses to This Request</p>
            <div class="p-request-block">
                <ul>
                    <li class="p-r-head">
                        <div class="submitted-block">Submitted</div>
                        <div class="church-block">Church</div>
                        <div class="message-block">Message</div>
                    </li>
                    @foreach($request->responses as $response)
                        <li>
                            <div class="submitted-block">{{$response->created_at}}</div>
                            <div class="church-block">{{$response->church->name}}</div>
                            <div class="message-block">{{$response->message}}</div>
                        </li>
                    @endforeach
                </ul>
            </div>


            <hr>
            <p class="c-h-name">Churches/Groups That Can Respond to This Request</p>
            <div class="p-request-block">
                <ul id="full-width" class="ch-link-color">
                    <li class="p-r-head">
                        <div id="submitted-block">Church Name</div>
                        <div class="church-block">Church Zip</div>
                        <div class="message-block">Miles From Request</div>
                        <div class="message-block">Responded</div>
                        <div class="message-block"></div>
                    </li>

                    @foreach($churches_groups as $churches_group)
                        <li>
                            <div id="submitted-block">{{$churches_group->churches->name}}</div>
                            <div class="church-block">{{$churches_group->churches->zipCode->code}}</div>
                            <div class="message-block">XX</div>

                            <div class="message-block">{{ in_array($churches_group->church_id, $respondedIds) ? 'Yes' : 'No' }}
                            </div>
                            <div class="message-block"><a
                                        href="{{route('getContact',['request_id'=>request()->segment(3),'church_id'=>$churches_group->church_id])}}">Contact
                                    Church</a></div>
                        </li>
                    @endforeach

                </ul>
            </div>
            <input type="submit" value="submit">
        </form>
    </div>

@endsection