@extends('church-enrollment.layouts.layout')
@section('title', 'Church To Church')
 
@section('body')
    <div class="content">
        <form action="" class="g-form" method="post">
            {{csrf_field()}}
            <div class="g-form-head">
                <p>Church to contact</p>
                <span>{{$church->name}}</span>
            </div>
            <span>Send a message to connect with this church.</span>

            <p>Your email address</p>
            <input type="text">

            <p>Your best phone number</p>
            <input type="number">

            <p>Message</p>

            <textarea name="" id="" ></textarea>


            <input type="submit" id="mt-50" value="submit">
        </form>


        <div class="request-block">
            <ul>
                <li class="t-head">
                    <div class="u-data">Request ID</div>
                    <div class="u-value">{{request()->segment(3)}}</div>
                </li>

                <li>
                    <div class="u-data">Request Zip Code</div>
                    <div class="u-value">{{$request->zipCode->code}}</div>
                </li>
                <li>
                    <div class="u-data">Created On</div>
                    <div class="u-value">{{date('m-d-Y',strtotime($request->created_at))}}</div>
                </li>
                <li>
                    <div class="u-data">Level of urgency</div>
                    <div class="u-value">{{$request->level_of_urgency}}</div>
                </li>
                <li>
                    <div class="u-data">Children served</div>
                    <div class="u-value">{{$request->children_served}}</div>
                </li>
                <li>
                    <div class="u-data">Estimated Need</div>
                    <div class="u-value">{{$request->estimated_value}}</div>
                </li>
                <li>
                    <div class="u-data">Location</div>
                    <div class="u-value">{{$request->zipCode->city}}, {{$request->state->name}}
                        , {{$request->zipCode->code}}, {{$request->county->name}}</div>
                </li>
                <li>
                    <div class="u-data">Region</div>
                    <div class="u-value">{{$request->zipCode->region->name}}</div>
                </li>
                <li>
                    <div class="u-data">Needs List</div>
                    <div class="u-value">
                        @foreach ($request->needs as $needs)
                            {{$needs->value}}
                        @endforeach
                    </div>
                </li>
                <li>
                    <div class="u-data">Request Description</div>
                    <div class="u-value">
                        {{$request->case_description}}
                    </div>
                </li>
                <li>
                    <div class="u-data">Agency Contact</div>
                    <div class="u-value">{{$request->worker->full_name}}</div>
                </li>
            </ul>
        </div>


    </div>

@endsection