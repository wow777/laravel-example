@extends('church-enrollment.layouts.layout')
@section('title', 'Request')

@section('body')
    <div class="content">
        <div class="name-section">
            <p class="c-h-name">Responding Church</p>
            <p class="c-name">{{$request->church}}</p>
        </div>
   
        <p class="c-h-name">Request Details</p>
        <div class="request-block">
            <ul>
                <li class="t-head">
                    <div class="u-data">Request ID</div>
                    <div class="u-value">{{request()->segment(3)}}</div>
                </li>

                <li>
                    <div class="u-data">Purpose of this Request</div>
                    <div class="u-value">{{$request->purpose_of_request}}</div>
                </li>
                <li>
                    <div class="u-data">Grid Quadrant</div>
                    <div class="u-value">{{$request->tier}} - {{$request->role_of_worker}}</div>
                </li>
                <li>
                    <div class="u-data">Level of Urgency</div>
                    <div class="u-value">{{$request->level_of_urgency}}</div>
                </li>
                <li>
                    <div class="u-data">Children Served</div>
                    <div class="u-value">{{$request->children_served}}</div>
                </li>
                <li>
                    <div class="u-data">Estimated Need</div>
                    <div class="u-value">{{$request->estimated_value}}</div>
                </li>
                <li>
                    <div class="u-data">Needs List</div>
                    <div class="u-value">
                        @foreach ($request->needs as $needs)
                            {{$needs->value}}
                        @endforeach
                    </div>
                </li>
                <li>
                    <div class="u-data">Case Description</div>
                    <div class="u-value">
                        {{$request->case_description}}
                    </div>
                </li>
            </ul>
        </div>


        <hr>
        <p class="c-h-name">Previous Responses to This Request</p>
        <div class="p-request-block">
            <ul>
                <li class="p-r-head">
                    <div class="submitted-block">Submitted</div>
                    <div class="church-block">Church</div>
                    <div class="message-block">Message</div>
                </li>
                @foreach($request->responses as $response)
                    <li>
                        <div class="submitted-block">{{$response->created_at}}</div>
                        <div class="church-block">{{$response->church->name}}</div>
                        <div class="message-block">{{$response->message}}</div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="respond-form">
            <p class="c-h-name">Respond to This Request</p>
            <p class="u-data">Complate the form below to respond to this request</p>
            <div class="respond-form-section">
                <p class="c-h-name">Your Name</p>
                <p class="u-data">Please select your name from the drop-down below.
                    if this is your first time responding click the + sign to add yourself to the system.
                </p>
                <select name="" id="">
                    <option value="">Search for your name</option>
                    <option value="">search</option>
                    <option value="">your</option>
                    <option value="">name</option>
                </select>

                <button type="button" class="plus-button">+</button>

                <p class="c-h-name">Church/Group Point Person's Email</p>
                <input type="email">
                <p class="c-h-name">Your Best Phone Number</p>
                <input type="number">

                <p class="c-h-name">How Can You Help</p>
                <p class="u-data">Enter a message for the agency worker reading how you can help</p>
                <textarea name="" id="#"> </textarea>
            </div>
        </div>

        <form action="">
            <input type="submit" value="submit">
        </form>

    </div>

@endsection