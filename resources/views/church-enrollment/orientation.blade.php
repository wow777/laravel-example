@extends('church-enrollment.layouts.layout')
@section('title', 'Church Orientation Details')

@section('body')  
    <div class="content">
        <p>Church Orientation Details for:</p>
        <p class="c-name">{{ $church->name }}</p>

        @if (!empty($church->orientation_date))
            <p>Orientation for this church was completed {{ date('F d, Y', strtotime($church->orientation_date)) }}.</p>
        @else
            <p>please enter the date you completed orientation for the church above</p>
            <form action="{{route('StoreOrientation')}}" method="post">
                {{csrf_field()}}
                <input name="church_id" type="hidden" value="{{request()->segment(4)}}">
                <input name="date" class="d-input middle" id="datepicker" type="text" required>
                <input type="submit" value="submit">
            </form>
        @endif
    </div>

    <script>
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
        });
    </script>
@endsection