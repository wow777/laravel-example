@extends('layouts.table-view')


@section('badges')
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-red number">?</h3>
            </div>
            <div class="small-box-footer bg-red inner font-weight-bold">No Responses<br>< 60 Days</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-aqua number">??</h3>
            </div>
            <div class="small-box-footer bg-aqua inner font-weight-bold">Responded in<br>Last 14 days</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-orange number"></h3>
            </div>
            <div class="small-box-footer bg-orange inner font-weight-bold">Pending-<br>Activation</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center">
                <h3 class="text-green number">?</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold">Activated<br>Churches</div>
        </div>
    </div>
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Submission Date</th>
                            <th>Applicant's name</th>
                            <th>Status</th>
                            <th>ST</th>
                            <th>County</th>
                            <th>Tier</th>
                            <th>Service Types</th>
                            <th>Background</th>
                            <th>Training</th>
                            <th>Walk Through</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($applications))
                            @foreach($applications as $application)
                                <tr data-id="{{ $application->id }}">
                                    <td>{{$application->submission_date}}</td>
                                    <td>{{$application->name}}</td>
                                    <td>{{$application->status}}</td>
                                    <td>{{$application->state}}</td>
                                    <td>{{$application->county}}</td>
                                    <td>{{$application->tier}}</td>
                                    <td>{{$application->service_types}}</td>
                                    <td>{{$application->background}}</td>
                                    <td>{{$application->training}}</td>
                                    <td>{{$application->walk_through}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
