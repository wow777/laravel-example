@extends('layouts.table-view')
@section('badges')
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-green number">{{ @$tier_1_cnt }}</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold">Number Tier 1 Agencies</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-aqua number">{{ @$tier_2_cnt }}</h3>
            </div>
            <div class="small-box-footer bg-aqua inner font-weight-bold">Number Tier 2 Agencies</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-orange number">{{ @$tier_3_cnt }}</h3>
            </div>
            <div class="small-box-footer bg-orange inner font-weight-bold">Number Tier 3 Agencies</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center">
                <h3 class="text-red number">{{ @$pending_cnt }}</h3>
            </div>
            <div class="small-box-footer bg-red inner font-weight-bold">Number of Agencies pending</div>
        </div>
    </div>
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
					<div class="row" style="margin:8px;">
						<div class="col-xs-11">
							<div class="row col-xs-3">
								<a href="javascript:void(0)" class="btn btn-default pull-center" onclick="showAllAgency();" style="padding: 5px 25px;">Show All</a>
							</div>
							<div class="col-xs-7">
								<div class="checkbox checkbox-info checkbox-circle" style="margin:0px;">
									{{ Form::checkbox('agency-check-deactivate', null, false, ['id' => 'agency-check-deactivate', 'class'=>'styled', 'style' => 'top: 5px;']) }}
									<label class="pl-0" for="agency-check-deactivate" style="line-height: 30px;">
										Include Deactivated
									</label>
								</div>
							</div>
						</div>
					</div>
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
							<tr>
								<th>Agency Name</th>
								<th>City</th>
								<th>ST</th>
								<th>Phone #</th>
								<th>Tier Approval</th>
								<th>Approval Option</th>
								<th>Open Requests</th>
								<th>Requests Made</th>
							</tr>
                        </thead>
                        <tbody>
							@if(isset($agencies) && count(@$agencies) != 0)
								@foreach($agencies as $agency)
									<tr data-id="{{ @$agency['id'] }}" onclick="editAgency('{{ @$agency['id'] }}');" id="agency{{@$agency['id']}}">
										<td>{{@$agency['agency_name']}}</td>
										<td>{{@$agency['city']}}</td>
										<td>{{@$agency['state']}}</td>
										<td>{{@$agency['phone_number']}}</td>
										<td>{{@$agency['tier_approval']}}</td>
										<td>{{@$agency['approval_option']}}</td>
										<td>{{@$agency['open_requests']}}</td>
										<td>{{@$agency['requests_made']}}</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="11">Nothing found.</td>
								</tr>
							@endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
