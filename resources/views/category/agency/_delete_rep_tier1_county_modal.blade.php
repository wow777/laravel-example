<div id="deleteAgencyTier1Dialog" class="modal" role="dialog">
    <div class="modal-dialog" style="width: 350px; top: 20%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h2 class="modal-title text-center" id="lblTitleConfirmYesNoTier1"></h2>
            </div>
			<!-- Modal Body -->
			
			<div class="modal-body text-center">
                <p id="lblMsgConfirmYesNoTier1"></p>
            </div>
			<div class="modal-footer">
				<button id="btnYesConfirmYesNoTier1" type="button" class="btn btn-primary">Yes</button>
				<button id="btnNoConfirmYesNoTier1" type="button" class="btn">No</button>
			</div>
        </div>
    </div>
</div>