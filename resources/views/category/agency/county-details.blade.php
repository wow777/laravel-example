@extends('layouts.table-view')
@section('badges')
     
@php
    $base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
@endphp

	<div class="col-xs-12">
		<div class="addCountyDetails">
			 <a href="javascript:void(0);" onclick="goBack();"> < &nbsp; BACK </a>
		</div>
		
		{!! Form::open(['url' => url('/').$base_url.'/agencies/addEditCounty', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'id' => 'county-form'] ) !!}
			
			{{csrf_field()}}
			<input type="hidden" id="county_id" name="county_id" value="{{@$countyData[0]['id']}}">
			<input type="hidden" id="agency_id" name="agency_id" value="{{@$agency_id}}">
			<div class="form-group">
				<div class="col-sm-2{{ $errors->has('state') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>State<span style="color:red;"> *</span></span>
					</label>
					{{ Form::select('state', ['' => '', 'NJ' => 'NJ', 'MT' => 'MT', 'MO' => 'MO'], (@$countyData[0]['state']), ['class' => 'selectpicker form-control', 'title' => 'Please select state', 'id' => 'state']) }}  
				</div>
				<div class="col-sm-4{{ $errors->has('county_name') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>County Name<span style="color:red;"> *</span></span>
					</label>
					{!! Form::text('county_name', @$countyData[0]['county_name'], ['class' => 'form-control', 'id' => 'county_name', 'title' => 'County Name',  'placeholder' => '']) !!}
				</div>
			</div>
			
			<br/>
			<h4>Tier 1 Approved</h4>
			<hr style="margin-top:10px;border-top: 2px solid gray;"/>
			
			<div class="col-xs-12">
				<div class="form-group">
					<div class="col-sm-2{{ $errors->has('approved_tier1') ? ' has-error' : '' }}">
						<label class="control-label">
							<span>Approved<span style="color:red;"> *</span></span>
						</label>
						{{ Form::select('approved_tier1', ['' => '', 'Yes' => 'Yes', 'No' => 'No'], (@$countyData[0]['approved_tier1']), ['class' => 'selectpicker form-control', 'title' => 'Please select approved', 'id' => 'approved_tier1']) }}  
					</div>
					<div class="col-sm-4{{ $errors->has('estimated_launch_date_tier1') ? ' has-error' : '' }}">
						<label class="control-label">
							<span>Estimated Launch Date<span style="color:red;"> *</span></span>
						</label>
						{!! Form::text('estimated_launch_date_tier1', @$countyData[0]['estimated_launch_date_tier1'], ['class' => 'form-control', 'id' => 'estimated_launch_date_tier1', 'title' => 'Estimated Launch Date',  'placeholder' => '']) !!}
					</div>
				</div>
				
				<div class="form-group" id="addNewAgencyTier1">
					@if(!empty(@$countyData[0]['rep_tier1']) && count(@$countyData[0]['rep_tier1']) != 0)
						@php $count_tier1 = 0; @endphp
						@foreach(@$countyData[0]['rep_tier1'] as $key => $rep_tier1)
							<div class="row col-xs-12" id="addNewRowAgencyTier1_{{@$count_tier1}}">
								<div class="col-sm-3">
									<label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label>
									{!! Form::email('agency_rep_email_tier1[]', @$rep_tier1['agency_rep_email_tier1'], ['class' => 'form-control', 'id' => 'agency_rep_email_tier1_'.@$count_tier1, 'title' => 'Agency Rep. Email',  'placeholder' => '']) !!}
								</div>
								<div class="col-sm-4">
									<label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label>
									<div class="form-group">
										<div class="col-sm-6">
											{!! Form::text('first_name_tier1[]', @$rep_tier1['first_name_tier1'], ['class' => 'form-control', 'id' => 'first_name_tier1_'.@$count_tier1, 'title' => 'First Name', 'placeholder' => 'First Name']) !!}
										</div>
										<div class="col-sm-6">
											{!! Form::text('last_name_tier1[]', @$rep_tier1['last_name_tier1'], ['class' => 'form-control', 'id' => 'last_name_tier1_'.@$count_tier1, 'title' => 'Last Name', 'placeholder' => 'Last Name']) !!}
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label>
									{!! Form::text('phone_number_tier1[]', @$rep_tier1['phone_number_tier1'], ['class' => 'form-control', 'id' => 'phone_number_tier1_'.@$count_tier1, 'title' => 'Agency Rep. Phone', 'placeholder' => '']) !!}
								</div>
								<div class="col-sm-2">
									<label class="control-label">&nbsp;</label>
									<div class="col-sm-12">
										<a href="javascript:void(0);" onClick="deleteAgencyTier1(`addNewRowAgencyTier1_{{@$count_tier1}}`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a>
									</div>
								</div>
							</div>
							@php $count_tier1++; @endphp
						@endforeach
					@endif
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						{!! Form::button('ADD REP', ['type' => 'button', 'class' => 'btn btn-primary', 'onClick' => 'addRepTier1Dialog()']) !!}
					</div>
				</div>
			</div>
			
			<br/>
			<h4>Tier 2 Approved</h4>
			<hr style="margin-top:10px;border-top: 2px solid gray;"/>
			
			<div class="col-xs-12">
				<div class="form-group">
					<div class="col-sm-2{{ $errors->has('approved_tier2') ? ' has-error' : '' }}">
						<label class="control-label">
							<span>Approved<span style="color:red;"> *</span></span>
						</label>
						{{ Form::select('approved_tier2', ['' => '', 'Yes' => 'Yes', 'No' => 'No'], (@$countyData[0]['approved_tier2']), ['class' => 'selectpicker form-control', 'title' => 'Please select approved', 'id' => 'approved_tier2']) }}  
					</div>
					<div class="col-sm-4{{ $errors->has('estimated_launch_date_tier2') ? ' has-error' : '' }}">
						<label class="control-label">
							<span>Estimated Launch Date<span style="color:red;"> *</span></span>
						</label>
						{!! Form::text('estimated_launch_date_tier2', @$countyData[0]['estimated_launch_date_tier2'], ['class' => 'form-control', 'id' => 'estimated_launch_date_tier2', 'title' => 'Estimated Launch Date',  'placeholder' => '']) !!}
					</div>
				</div>
				
				<div class="form-group" id="addNewAgencyTier2">
					@if(!empty(@$countyData[0]['rep_tier2']) && count(@$countyData[0]['rep_tier2']) != 0)
						@php $count_tier2 = 0; @endphp
						@foreach(@$countyData[0]['rep_tier2'] as $key => $rep_tier2)
							<div class="row col-xs-12" id="addNewRowAgencyTier2_{{@$count_tier2}}">
								<div class="col-sm-3">
									<label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label>
									{!! Form::email('agency_rep_email_tier2[]', @$rep_tier2['agency_rep_email_tier2'], ['class' => 'form-control', 'id' => 'agency_rep_email_tier2_'.@$count_tier2, 'title' => 'Agency Rep. Email', 'placeholder' => '']) !!}
								</div>
								<div class="col-sm-4">
									<label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label>
									<div class="form-group">
										<div class="col-sm-6">
											{!! Form::text('first_name_tier2[]', @$rep_tier2['first_name_tier2'], ['class' => 'form-control', 'id' => 'first_name_tier2_'.@$count_tier2, 'title' => 'First Name', 'placeholder' => 'First Name']) !!}
										</div>
										<div class="col-sm-6">
											{!! Form::text('last_name_tier2[]', @$rep_tier2['last_name_tier2'], ['class' => 'form-control', 'id' => 'last_name_tier2_'.@$count_tier2, 'title' => 'Last Name', 'placeholder' => 'Last Name']) !!}
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label>
									{!! Form::text('phone_number_tier2[]', @$rep_tier2['phone_number_tier2'], ['class' => 'form-control', 'id' => 'phone_number_tier2_'.@$count_tier2, 'title' => 'Agency Rep. Phone', 'placeholder' => '']) !!}
								</div>
								<div class="col-sm-2">
									<label class="control-label">&nbsp;</label>
									<div class="col-sm-12">
										<a href="javascript:void(0);" onClick="deleteAgencyTier2(`addNewRowAgencyTier2_{{@$count_tier2}}`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a>
									</div>
								</div>
							</div>
							@php $count_tier2++; @endphp
						@endforeach
					@endif
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						{!! Form::button('ADD REP', ['type' => 'button', 'class' => 'btn btn-primary', 'onClick' => 'addRepTier2Dialog()']) !!}
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						<h5 style="font-weight: bold;">Service Types</h5>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						@php $family_team_meeting_tier2 = false; @endphp
						@if(@$countyData[0]['service_tier2']['family_social']['family_team_meeting_tier2'] == 'Family Social')
							@php $family_team_meeting_tier2 = true; @endphp
						@elseif(@$countyData[0]['service_tier2']['family_social']['family_team_meeting_tier2'] != 'Family Social' || @$countyData[0]['service_tier2']['child_care']['child_care_tier2'] != 'Child Care' || @$countyData[0]['service_tier2']['transportation']['transportation_tier2'] != 'Transportation')
							@php $family_team_meeting_tier2 = true; @endphp
						@endif
						
						<div class="col-sm-12">
							{{ Form::checkbox('family_team_meeting_tier2', 'Family Social', @$family_team_meeting_tier2, ['id' => 'family_team_meeting_tier2', 'class'=>'styled']) }}
							<label class="pl-0" for="family_team_meeting_tier2" style="font-weight: 300; margin-left: 5px;">
								Family/Social Worker Team Meetings
							</label>
						</div>
						<div id="family_team_meeting_part_tier2">
							<div class="col-sm-12">
								<div class="col-sm-12">
									<div class="col-sm-12">
										<h5 style="font-weight: bold;">Are you the Background Agency</h5>
										<div class="col-sm-12 text-center">
											@php $background_agency_yes_tier2 = false; $background_agency_no_tier2 = false; @endphp
											@if(@$countyData[0]['service_tier2']['family_social']['background_agency_yes_no_tier2'] == 'Background Agency Yes')
												@php $background_agency_yes_tier2 = true; @endphp
											@elseif(@$countyData[0]['service_tier2']['family_social']['background_agency_yes_no_tier2'] == 'Background Agency No')
												@php $background_agency_no_tier2 = true; @endphp
											@else
												@php $background_agency_yes_tier2 = true; @endphp
											@endif
											
											<div class="col-sm-1">
												{{ Form::checkbox('background_agency_yes_no_tier2', 'Background Agency Yes', @$background_agency_yes_tier2, ['id' => 'background_agency_yes_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="background_agency_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
											</div>
											<div class="col-sm-1">
												{{ Form::checkbox('background_agency_yes_no_tier2', 'Background Agency No', @$background_agency_no_tier2, ['id' => 'background_agency_no_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="background_agency_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
											</div>
										</div>
										<div id="background_agency_yes_no_tier2">
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Email</label>
													<div class="col-sm-5">
														{!! Form::email('background_contact_email_tier2', @$countyData[0]['service_tier2']['family_social']['background_contact_email_tier2'], ['class' => 'form-control', 'id' => 'background_contact_email_tier2', 'title' => 'Contact Email',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Name</label>
													<div class="col-sm-3">
														{!! Form::text('background_contact_first_name_tier2', @$countyData[0]['service_tier2']['family_social']['background_contact_first_name_tier2'], ['class' => 'form-control', 'id' => 'background_contact_first_name_tier2', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
													</div>
													<div class="col-sm-3">
														{!! Form::text('background_contact_last_name_tier2', @$countyData[0]['service_tier2']['family_social']['background_contact_last_name_tier2'], ['class' => 'form-control', 'id' => 'background_contact_last_name_tier2', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
													</div>
													<label class="col-sm-1 control-label">Phone</label>
													<div class="col-sm-2">
														{!! Form::text('background_contact_phone_number_tier2', @$countyData[0]['service_tier2']['family_social']['background_contact_phone_number_tier2'], ['class' => 'form-control', 'id' => 'background_contact_phone_number_tier2', 'title' => 'Phone Number',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								@php $agencies_yes_tier2 = false; $agencies_no_tier2 = false; @endphp
								@if(@$countyData[0]['service_tier2']['family_social']['agencies_yes_no_tier2'] == 'Yes')
									@php $agencies_yes_tier2 = true; @endphp
								@elseif(@$countyData[0]['service_tier2']['family_social']['agencies_yes_no_tier2'] == 'No')
									@php $agencies_no_tier2 = true; @endphp
								@else
									@php $agencies_yes_tier2 = true; @endphp
								@endif
								
								<div class="row col-sm-4">
									<h5>Will you do this for other agencies?</h5>
								</div>
								<div class="col-sm-1" style="line-height: 40px; margin-left: -75px;">
									{{ Form::checkbox('agencies_yes_no_tier2', 'Yes', @$agencies_yes_tier2, ['id' => 'agencies_yes_tier2', 'class'=>'styled']) }}
									<label class="pl-0" for="agencies_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
								</div>
								<div class="row col-sm-1" style="line-height: 40px;">
									{{ Form::checkbox('agencies_yes_no_tier2', 'No', @$agencies_no_tier2, ['id' => 'agencies_no_tier2', 'class'=>'styled']) }}
									<label class="pl-0" for="agencies_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="col-sm-12">
									<div class="col-sm-12">
										<h5 style="font-weight: bold;">Are you the Training Agency</h5>
										<div class="col-sm-12 text-center">
											@php $training_agency_yes_tier2 = false; $training_agency_no_tier2 = false; @endphp
											@if(@$countyData[0]['service_tier2']['family_social']['training_agency_yes_no_tier2'] == 'Training Agency Yes')
												@php $training_agency_yes_tier2 = true; @endphp
											@elseif(@$countyData[0]['service_tier2']['family_social']['training_agency_yes_no_tier2'] == 'Training Agency No')
												@php $training_agency_no_tier2 = true; @endphp
											@else
												@php $training_agency_yes_tier2 = true; @endphp
											@endif
											
											<div class="col-sm-1">
												{{ Form::checkbox('training_agency_yes_no_tier2', 'Training Agency Yes', @$training_agency_yes_tier2, ['id' => 'training_agency_yes_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="training_agency_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
											</div>
											<div class="col-sm-1">
												{{ Form::checkbox('training_agency_yes_no_tier2', 'Training Agency No', @$training_agency_no_tier2, ['id' => 'training_agency_no_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="training_agency_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
											</div>
										</div>
										<div id="training_agency_yes_no_tier2">
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Email</label>
													<div class="col-sm-5">
														{!! Form::email('training_contact_email_tier2', @$countyData[0]['service_tier2']['family_social']['training_contact_email_tier2'], ['class' => 'form-control', 'id' => 'training_contact_email_tier2', 'title' => 'Contact Email',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Name</label>
													<div class="col-sm-3">
														{!! Form::text('training_contact_first_name_tier2', @$countyData[0]['service_tier2']['family_social']['training_contact_first_name_tier2'], ['class' => 'form-control', 'id' => 'training_contact_first_name_tier2', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
													</div>
													<div class="col-sm-3">
														{!! Form::text('training_contact_last_name_tier2', @$countyData[0]['service_tier2']['family_social']['training_contact_last_name_tier2'], ['class' => 'form-control', 'id' => 'training_contact_last_name_tier2', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
													</div>
													<label class="col-sm-1 control-label">Phone</label>
													<div class="col-sm-2">
														{!! Form::text('training_contact_phone_number_tier2', @$countyData[0]['service_tier2']['family_social']['training_contact_phone_number_tier2'], ['class' => 'form-control', 'id' => 'training_contact_phone_number_tier2', 'title' => 'Phone Number',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							@php $child_care_tier2 = false; @endphp
							@if(@$countyData[0]['service_tier2']['child_care']['child_care_tier2'] == 'Child Care')
								@php $child_care_tier2 = true; @endphp
							@endif
							
							{{ Form::checkbox('child_care_tier2', 'Child Care', @$child_care_tier2, ['id' => 'child_care_tier2', 'class'=>'styled']) }}
							<label class="pl-0" for="child_care_tier2" style="font-weight: 300; margin-left: 5px;">
								Child Care/Supervision
							</label>
						</div>
						<div id="child_care_part_tier2">
							<div class="col-sm-12">
								<div class="col-sm-12">
									<div class="col-sm-12">
										<h5 style="font-weight: bold;">Are you the Background Agency</h5>
										<div class="col-sm-12 text-center">
											@php $childcare_agency_yes_tier2 = false; $childcare_agency_no_tier2 = false; @endphp
											@if(@$countyData[0]['service_tier2']['child_care']['childcare_agency_yes_no_tier2'] == 'Background Agency Yes')
												@php $childcare_agency_yes_tier2 = true; @endphp
											@elseif(@$countyData[0]['service_tier2']['child_care']['childcare_agency_yes_no_tier2'] == 'Background Agency No')
												@php $childcare_agency_no_tier2 = true; @endphp
											@else
												@php $childcare_agency_yes_tier2 = true; @endphp
											@endif
											
											<div class="col-sm-1">
												{{ Form::checkbox('childcare_agency_yes_no_tier2', 'Background Agency Yes', @$childcare_agency_yes_tier2, ['id' => 'childcare_agency_yes_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="childcare_agency_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
											</div>
											<div class="col-sm-1">
												{{ Form::checkbox('childcare_agency_yes_no_tier2', 'Background Agency No', @$childcare_agency_no_tier2, ['id' => 'childcare_agency_no_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="childcare_agency_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
											</div>
										</div>
										<div id="childcare_agency_yes_no_tier2">
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Email</label>
													<div class="col-sm-5">
														{!! Form::email('childcare_contact_email_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_contact_email_tier2'], ['class' => 'form-control', 'id' => 'childcare_contact_email_tier2', 'title' => 'Contact Email',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Name</label>
													<div class="col-sm-3">
														{!! Form::text('childcare_contact_first_name_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_contact_first_name_tier2'], ['class' => 'form-control', 'id' => 'childcare_contact_first_name_tier2', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
													</div>
													<div class="col-sm-3">
														{!! Form::text('childcare_contact_last_name_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_contact_last_name_tier2'], ['class' => 'form-control', 'id' => 'childcare_contact_last_name_tier2', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
													</div>
													<label class="col-sm-1 control-label">Phone</label>
													<div class="col-sm-2">
														{!! Form::text('childcare_contact_phone_number_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_contact_phone_number_tier2'], ['class' => 'form-control', 'id' => 'childcare_contact_phone_number_tier2', 'title' => 'Phone Number',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								@php $childcare_agencies_yes_tier2 = false; $childcare_agencies_no_tier2 = false; @endphp
								@if(@$countyData[0]['service_tier2']['child_care']['childcare_agencies_yes_no_tier2'] == 'Yes')
									@php $childcare_agencies_yes_tier2 = true; @endphp
								@elseif(@$countyData[0]['service_tier2']['child_care']['childcare_agencies_yes_no_tier2'] == 'No')
									@php $childcare_agencies_no_tier2 = true; @endphp
								@else
									@php $childcare_agencies_yes_tier2 = true; @endphp
								@endif
								
								<div class="row col-sm-4">
									<h5>Will you do this for other agencies?</h5>
								</div>
								<div class="col-sm-1" style="line-height: 40px; margin-left: -75px;">
									{{ Form::checkbox('childcare_agencies_yes_no_tier2', 'Yes', @$childcare_agencies_yes_tier2, ['id' => 'childcare_agencies_yes_tier2', 'class'=>'styled']) }}
									<label class="pl-0" for="childcare_agencies_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
								</div>
								<div class="row col-sm-1" style="line-height: 40px;">
									{{ Form::checkbox('childcare_agencies_yes_no_tier2', 'No', @$childcare_agencies_no_tier2, ['id' => 'childcare_agencies_no_tier2', 'class'=>'styled']) }}
									<label class="pl-0" for="childcare_agencies_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="col-sm-12">
									<div class="col-sm-12">
										<h5 style="font-weight: bold;">Are you the Training Agency</h5>
										<div class="col-sm-12 text-center">
											@php $childcare_training_agency_yes_tier2 = false; $childcare_training_agency_no_tier2 = false; @endphp
											@if(@$countyData[0]['service_tier2']['child_care']['childcare_training_agency_yes_no_tier2'] == 'Training Agency Yes')
												@php $childcare_training_agency_yes_tier2 = true; @endphp
											@elseif(@$countyData[0]['service_tier2']['child_care']['childcare_training_agency_yes_no_tier2'] == 'Training Agency No')
												@php $childcare_training_agency_no_tier2 = true; @endphp
											@else
												@php $childcare_training_agency_yes_tier2 = true; @endphp
											@endif
											
											<div class="col-sm-1">
												{{ Form::checkbox('childcare_training_agency_yes_no_tier2', 'Training Agency Yes', @$childcare_training_agency_yes_tier2, ['id' => 'childcare_training_agency_yes_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="childcare_training_agency_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
											</div>
											<div class="col-sm-1">
												{{ Form::checkbox('childcare_training_agency_yes_no_tier2', 'Training Agency No', @$childcare_training_agency_no_tier2, ['id' => 'childcare_training_agency_no_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="childcare_training_agency_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
											</div>
										</div>
										<div id="childcare_training_agency_yes_no_tier2">
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Email</label>
													<div class="col-sm-5">
														{!! Form::email('childcare_training_contact_email_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_training_contact_email_tier2'], ['class' => 'form-control', 'id' => 'childcare_training_contact_email_tier2', 'title' => 'Contact Email',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Name</label>
													<div class="col-sm-3">
														{!! Form::text('childcare_training_contact_first_name_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_training_contact_first_name_tier2'], ['class' => 'form-control', 'id' => 'childcare_training_contact_first_name_tier2', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
													</div>
													<div class="col-sm-3">
														{!! Form::text('childcare_training_contact_last_name_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_training_contact_last_name_tier2'], ['class' => 'form-control', 'id' => 'childcare_training_contact_last_name_tier2', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
													</div>
													<label class="col-sm-1 control-label">Phone</label>
													<div class="col-sm-2">
														{!! Form::text('childcare_training_contact_phone_number_tier2', @$countyData[0]['service_tier2']['child_care']['childcare_training_contact_phone_number_tier2'], ['class' => 'form-control', 'id' => 'childcare_training_contact_phone_number_tier2', 'title' => 'Phone Number',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							@php $transportation_tier2 = false; @endphp
							@if(@$countyData[0]['service_tier2']['transportation']['transportation_tier2'] == 'Transportation')
								@php $transportation_tier2 = true; @endphp
							@endif
							
							{{ Form::checkbox('transportation_tier2', 'Transportation', @$transportation_tier2, ['id' => 'transportation_tier2', 'class'=>'styled']) }}
							<label class="pl-0" for="transportation_tier2" style="font-weight: 300; margin-left: 5px;">
								Transportation
							</label>
						</div>
						<div id="transportation_part_tier2">
							<div class="col-sm-12">
								<div class="col-sm-12">
									<div class="col-sm-12">
										<h5 style="font-weight: bold;">Are you the Background Agency</h5>
										<div class="col-sm-12 text-center">
											@php $transportation_agency_yes_tier2 = false; $transportation_agency_no_tier2 = false; @endphp
											@if(@$countyData[0]['service_tier2']['transportation']['transportation_agency_yes_no_tier2'] == 'Background Agency Yes')
												@php $transportation_agency_yes_tier2 = true; @endphp
											@elseif(@$countyData[0]['service_tier2']['transportation']['transportation_agency_yes_no_tier2'] == 'Background Agency No')
												@php $transportation_agency_no_tier2 = true; @endphp
											@else
												@php $transportation_agency_yes_tier2 = true; @endphp
											@endif
											
											<div class="col-sm-1">
												{{ Form::checkbox('transportation_agency_yes_no_tier2', 'Background Agency Yes', @$transportation_agency_yes_tier2, ['id' => 'transportation_agency_yes_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="transportation_agency_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
											</div>
											<div class="col-sm-1">
												{{ Form::checkbox('transportation_agency_yes_no_tier2', 'Background Agency No', @$transportation_agency_no_tier2, ['id' => 'transportation_agency_no_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="transportation_agency_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
											</div>
										</div>
										<div id="transportation_agency_yes_no_tier2">
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Email</label>
													<div class="col-sm-5">
														{!! Form::email('transportation_contact_email_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_contact_email_tier2'], ['class' => 'form-control', 'id' => 'transportation_contact_email_tier2', 'title' => 'Contact Email',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Name</label>
													<div class="col-sm-3">
														{!! Form::text('transportation_contact_first_name_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_contact_first_name_tier2'], ['class' => 'form-control', 'id' => 'transportation_contact_first_name_tier2', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
													</div>
													<div class="col-sm-3">
														{!! Form::text('transportation_contact_last_name_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_contact_last_name_tier2'], ['class' => 'form-control', 'id' => 'transportation_contact_last_name_tier2', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
													</div>
													<label class="col-sm-1 control-label">Phone</label>
													<div class="col-sm-2">
														{!! Form::text('transportation_contact_phone_number_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_contact_phone_number_tier2'], ['class' => 'form-control', 'id' => 'transportation_contact_phone_number_tier2', 'title' => 'Phone Number',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								@php $transportation_agencies_yes_tier2 = false; $transportation_agencies_no_tier2 = false; @endphp
								@if(@$countyData[0]['service_tier2']['transportation']['transportation_agencies_yes_no_tier2'] == 'Yes')
									@php $transportation_agencies_yes_tier2 = true; @endphp
								@elseif(@$countyData[0]['service_tier2']['transportation']['transportation_agencies_yes_no_tier2'] == 'No')
									@php $transportation_agencies_no_tier2 = true; @endphp
								@else
									@php $transportation_agencies_yes_tier2 = true; @endphp
								@endif
								
								<div class="row col-sm-4">
									<h5>Will you do this for other agencies?</h5>
								</div>
								<div class="col-sm-1" style="line-height: 40px; margin-left: -75px;">
									{{ Form::checkbox('transportation_agencies_yes_no_tier2', 'Yes', @$transportation_agencies_yes_tier2, ['id' => 'transportation_agencies_yes_tier2', 'class'=>'styled']) }}
									<label class="pl-0" for="transportation_agencies_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
								</div>
								<div class="row col-sm-1" style="line-height: 40px;">
									{{ Form::checkbox('transportation_agencies_yes_no_tier2', 'No', @$transportation_agencies_no_tier2, ['id' => 'transportation_agencies_no_tier2', 'class'=>'styled']) }}
									<label class="pl-0" for="transportation_agencies_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="col-sm-12">
									<div class="col-sm-12">
										<h5 style="font-weight: bold;">Are you the Training Agency</h5>
										<div class="col-sm-12 text-center">
											@php $transportation_training_agency_yes_tier2 = false; $transportation_training_agency_no_tier2 = false; @endphp
											@if(@$countyData[0]['service_tier2']['transportation']['transportation_training_agency_yes_no_tier2'] == 'Training Agency Yes')
												@php $transportation_training_agency_yes_tier2 = true; @endphp
											@elseif(@$countyData[0]['service_tier2']['transportation']['transportation_training_agency_yes_no_tier2'] == 'Training Agency No')
												@php $transportation_training_agency_no_tier2 = true; @endphp
											@else
												@php $transportation_training_agency_yes_tier2 = true; @endphp
											@endif
											
											<div class="col-sm-1">
												{{ Form::checkbox('transportation_training_agency_yes_no_tier2', 'Training Agency Yes', @$transportation_training_agency_yes_tier2, ['id' => 'transportation_training_agency_yes_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="transportation_training_agency_yes_tier2" style="font-weight: 300; margin-left: 5px;">Yes</label>
											</div>
											<div class="col-sm-1">
												{{ Form::checkbox('transportation_training_agency_yes_no_tier2', 'Training Agency No', @$transportation_training_agency_no_tier2, ['id' => 'transportation_training_agency_no_tier2', 'class'=>'styled']) }}
												<label class="pl-0" for="transportation_training_agency_no_tier2" style="font-weight: 300; margin-left: 5px;">No</label>
											</div>
										</div>
										<div id="transportation_training_agency_yes_no_tier2">
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Email</label>
													<div class="col-sm-5">
														{!! Form::email('transportation_training_contact_email_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_training_contact_email_tier2'], ['class' => 'form-control', 'id' => 'transportation_training_contact_email_tier2', 'title' => 'Contact Email',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">Contact Name</label>
													<div class="col-sm-3">
														{!! Form::text('transportation_training_contact_first_name_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_training_contact_first_name_tier2'], ['class' => 'form-control', 'id' => 'transportation_training_contact_first_name_tier2', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
													</div>
													<div class="col-sm-3">
														{!! Form::text('transportation_training_contact_last_name_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_training_contact_last_name_tier2'], ['class' => 'form-control', 'id' => 'transportation_training_contact_last_name_tier2', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
													</div>
													<label class="col-sm-1 control-label">Phone</label>
													<div class="col-sm-2">
														{!! Form::text('transportation_training_contact_phone_number_tier2', @$countyData[0]['service_tier2']['transportation']['transportation_training_contact_phone_number_tier2'], ['class' => 'form-control', 'id' => 'transportation_training_contact_phone_number_tier2', 'title' => 'Phone Number',  'placeholder' => '']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<br/>
			<h4>Tier 3 Approved</h4>
			<hr style="margin-top:10px;border-top: 2px solid gray;"/>
			
			<div class="col-xs-12">
				<div class="form-group">
					<div class="col-sm-2{{ $errors->has('approved_tier3') ? ' has-error' : '' }}">
						<label class="control-label">
							<span>Approved<span style="color:red;"> *</span></span>
						</label>
						{{ Form::select('approved_tier3', ['' => '', 'Yes' => 'Yes', 'No' => 'No'], (@$countyData[0]['approved_tier3']), ['class' => 'selectpicker form-control', 'title' => 'Please select approved', 'id' => 'approved_tier3']) }}  
					</div>
					<div class="col-sm-4{{ $errors->has('estimated_launch_date_tier3') ? ' has-error' : '' }}">
						<label class="control-label">
							<span>Estimated Launch Date<span style="color:red;"> *</span></span>
						</label>
						{!! Form::text('estimated_launch_date_tier3', @$countyData[0]['estimated_launch_date_tier3'], ['class' => 'form-control', 'id' => 'estimated_launch_date_tier3', 'title' => 'Estimated Launch Date',  'placeholder' => '']) !!}
					</div>
				</div>
				
				<div class="form-group" id="addNewAgencyTier3">
					@if(!empty(@$countyData[0]['rep_tier3']) && count(@$countyData[0]['rep_tier3']) != 0)
						@php $count_tier3 = 0; @endphp
						@foreach(@$countyData[0]['rep_tier3'] as $key => $rep_tier3)
							<div class="row col-xs-12" id="addNewRowAgencyTier3_{{@$count_tier3}}">
								<div class="col-sm-3">
									<label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label>
									{!! Form::email('agency_rep_email_tier3[]', @$rep_tier3['agency_rep_email_tier3'], ['class' => 'form-control', 'id' => 'agency_rep_email_tier3_'.@$count_tier3, 'title' => 'Agency Rep. Email', 'placeholder' => '']) !!}
								</div>
								<div class="col-sm-4">
									<label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label>
									<div class="form-group">
										<div class="col-sm-6">
											{!! Form::text('first_name_tier3[]', @$rep_tier3['first_name_tier3'], ['class' => 'form-control', 'id' => 'first_name_tier3_'.@$count_tier3, 'title' => 'First Name', 'placeholder' => 'First Name']) !!}
										</div>
										<div class="col-sm-6">
											{!! Form::text('last_name_tier3[]', @$rep_tier3['last_name_tier3'], ['class' => 'form-control', 'id' => 'last_name_tier3_'.@$count_tier3, 'title' => 'Last Name', 'placeholder' => 'Last Name']) !!}
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label>
									{!! Form::text('phone_number_tier3[]', @$rep_tier3['phone_number_tier3'], ['class' => 'form-control', 'id' => 'phone_number_tier3_'.@$count_tier3, 'title' => 'Agency Rep. Phone', 'placeholder' => '']) !!}
								</div>
								<div class="col-sm-2">
									<label class="control-label">&nbsp;</label>
									<div class="col-sm-12">
										<a href="javascript:void(0);" onClick="deleteAgencyTier3(`addNewRowAgencyTier3_{{@$count_tier3}}`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a>
									</div>
								</div>
							</div>
							@php $count_tier3++; @endphp
						@endforeach
					@endif
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						{!! Form::button('ADD REP', ['type' => 'button', 'class' => 'btn btn-primary', 'onClick' => 'addRepTier3Dialog()']) !!}
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						<h5 style="font-weight: bold;">Service Types</h5>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="col-sm-12">
							@php $NonLicensed = false; @endphp
							@if(@$countyData[0]['service_tier3']['non_licensed_home_tier3'] == 'Non-licensed')
								@php $NonLicensed = true; @endphp
							@else
								@php $NonLicensed = true; @endphp
							@endif
							
							{{ Form::checkbox('non_licensed_home_tier3', 'Non-licensed', @$NonLicensed, ['id' => 'non_licensed_home_tier3', 'class'=>'styled']) }}
							<label class="pl-0" for="non_licensed_home_tier3" style="font-weight: 300; margin-left: 5px;">
								Non-licensed Home
							</label>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-12">
								<div class="col-sm-12">
									<h5 style="font-weight: bold;">Are you the Background Agency</h5>
									<div class="col-sm-12 text-center">
										@php $background_agency_yes_tier3 = false; $background_agency_no_tier3 = false; @endphp
										@if(@$countyData[0]['service_tier3']['background_agency_yes_no_tier3'] == 'Background Agency Yes')
											@php $background_agency_yes_tier3 = true; @endphp
										@elseif(@$countyData[0]['service_tier3']['background_agency_yes_no_tier3'] == 'Background Agency No')
											@php $background_agency_no_tier3 = true; @endphp
										@else
											@php $background_agency_yes_tier3 = true; @endphp
										@endif
										
										<div class="col-sm-1">
											{{ Form::checkbox('background_agency_yes_no_tier3', 'Background Agency Yes', @$background_agency_yes_tier3, ['id' => 'background_agency_yes_tier3', 'class'=>'styled']) }}
											<label class="pl-0" for="background_agency_yes_tier3" style="font-weight: 300; margin-left: 5px;">Yes</label>
										</div>
										<div class="col-sm-1">
											{{ Form::checkbox('background_agency_yes_no_tier3', 'Background Agency No', @$background_agency_no_tier3, ['id' => 'background_agency_no_tier3', 'class'=>'styled']) }}
											<label class="pl-0" for="background_agency_no_tier3" style="font-weight: 300; margin-left: 5px;">No</label>
										</div>
									</div>
									<div id="background_agency_yes_no_tier3">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="col-sm-2 control-label">Contact Email</label>
												<div class="col-sm-5">
													{!! Form::email('background_contact_email_tier3', @$countyData[0]['service_tier3']['background_contact_email_tier3'], ['class' => 'form-control', 'id' => 'background_contact_email_tier3', 'title' => 'Contact Email', 'placeholder' => '']) !!}
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label class="col-sm-2 control-label">Contact Name</label>
												<div class="col-sm-3">
													{!! Form::text('background_contact_first_name_tier3', @$countyData[0]['service_tier3']['background_contact_first_name_tier3'], ['class' => 'form-control', 'id' => 'background_contact_first_name_tier3', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
												</div>
												<div class="col-sm-3">
													{!! Form::text('background_contact_last_name_tier3', @$countyData[0]['service_tier3']['background_contact_last_name_tier3'], ['class' => 'form-control', 'id' => 'background_contact_last_name_tier3', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
												</div>
												<label class="col-sm-1 control-label">Phone</label>
												<div class="col-sm-2">
													{!! Form::text('background_contact_phone_number_tier3', @$countyData[0]['service_tier3']['background_contact_phone_number_tier3'], ['class' => 'form-control', 'id' => 'background_contact_phone_number_tier3', 'title' => 'Phone Number',  'placeholder' => '']) !!}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							@php $agencies_yes_tier3 = false; $agencies_no_tier3 = false; @endphp
							@if(@$countyData[0]['service_tier3']['agencies_yes_no_tier3'] == 'Yes')
								@php $agencies_yes_tier3 = true; @endphp
							@elseif(@$countyData[0]['service_tier3']['agencies_yes_no_tier3'] == 'No')
								@php $agencies_no_tier3 = true; @endphp
							@else
								@php $agencies_yes_tier3 = true; @endphp
							@endif
							
							<div class="row col-sm-4">
								<h5>Will you do this for other agencies?</h5>
							</div>
							<div class="col-sm-1" style="line-height: 40px; margin-left: -75px;">
								{{ Form::checkbox('agencies_yes_no_tier3', 'Yes', @$agencies_yes_tier3, ['id' => 'agencies_yes_tier3', 'class'=>'styled']) }}
								<label class="pl-0" for="agencies_yes_tier3" style="font-weight: 300; margin-left: 5px;">Yes</label>
							</div>
							<div class="row col-sm-1" style="line-height: 40px;">
								{{ Form::checkbox('agencies_yes_no_tier3', 'No', @$agencies_no_tier3, ['id' => 'agencies_no_tier3', 'class'=>'styled']) }}
								<label class="pl-0" for="agencies_no_tier3" style="font-weight: 300; margin-left: 5px;">No</label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-12">
								<div class="col-sm-12">
									<h5 style="font-weight: bold;">Are you the Home walk-through Agency</h5>
									<div class="col-sm-12 text-center">
										@php $walk_through_agency_yes_tier3 = false; $walk_through_agency_no_tier3 = false; @endphp
										@if(@$countyData[0]['service_tier3']['walk_through_agency_yes_no_tier3'] == 'Walk-Through Agency Yes')
											@php $walk_through_agency_yes_tier3 = true; @endphp
										@elseif(@$countyData[0]['service_tier3']['walk_through_agency_yes_no_tier3'] == 'Walk-Through Agency No')
											@php $walk_through_agency_no_tier3 = true; @endphp
										@else
											@php $walk_through_agency_yes_tier3 = true; @endphp
										@endif
										
										<div class="col-sm-1">
											{{ Form::checkbox('walk_through_agency_yes_no_tier3', 'Walk-Through Agency Yes', @$walk_through_agency_yes_tier3, ['id' => 'walk_through_agency_yes_tier3', 'class'=>'styled']) }}
											<label class="pl-0" for="walk_through_agency_yes_tier3" style="font-weight: 300; margin-left: 5px;">Yes</label>
										</div>
										<div class="col-sm-1">
											{{ Form::checkbox('walk_through_agency_yes_no_tier3', 'Walk-Through Agency No', @$walk_through_agency_no_tier3, ['id' => 'walk_through_agency_no_tier3', 'class'=>'styled']) }}
											<label class="pl-0" for="walk_through_agency_no_tier3" style="font-weight: 300; margin-left: 5px;">No</label>
										</div>
									</div>
									<div id="walk_through_agency_yes_no_tier3">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="col-sm-2 control-label">Contact Email</label>
												<div class="col-sm-5">
													{!! Form::email('walk_through_contact_email_tier3', @$countyData[0]['service_tier3']['walk_through_contact_email_tier3'], ['class' => 'form-control', 'id' => 'walk_through_contact_email_tier3', 'title' => 'Contact Email',  'placeholder' => '']) !!}
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label class="col-sm-2 control-label">Contact Name</label>
												<div class="col-sm-3">
													{!! Form::text('walk_through_contact_first_name_tier3', @$countyData[0]['service_tier3']['walk_through_contact_first_name_tier3'], ['class' => 'form-control', 'id' => 'walk_through_contact_first_name_tier3', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
												</div>
												<div class="col-sm-3">
													{!! Form::text('walk_through_contact_last_name_tier3', @$countyData[0]['service_tier3']['walk_through_contact_last_name_tier3'], ['class' => 'form-control', 'id' => 'walk_through_contact_last_name_tier3', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
												</div>
												<label class="col-sm-1 control-label">Phone</label>
												<div class="col-sm-2">
													{!! Form::text('walk_through_contact_phone_number_tier3', @$countyData[0]['service_tier3']['walk_through_contact_phone_number_tier3'], ['class' => 'form-control', 'id' => 'walk_through_contact_phone_number_tier3', 'title' => 'Phone Number',  'placeholder' => '']) !!}
												</div>
											</div>
										</div>
									</div>
									<h5 style="font-weight: bold;">Are you the Training Agency</h5>
									<div class="col-sm-12 text-center">
										@php $training_agency_yes_tier3 = false; $training_agency_no_tier3 = false; @endphp
										@if(@$countyData[0]['service_tier3']['training_agency_yes_no_tier3'] == 'Training Agency Yes')
											@php $training_agency_yes_tier3 = true; @endphp
										@elseif(@$countyData[0]['service_tier3']['training_agency_yes_no_tier3'] == 'Training Agency No')
											@php $training_agency_no_tier3 = true; @endphp
										@else
											@php $training_agency_yes_tier3 = true; @endphp
										@endif
										
										<div class="col-sm-1">
											{{ Form::checkbox('training_agency_yes_no_tier3', 'Training Agency Yes', @$training_agency_yes_tier3, ['id' => 'training_agency_yes_tier3', 'class'=>'styled']) }}
											<label class="pl-0" for="training_agency_yes_tier3" style="font-weight: 300; margin-left: 5px;">Yes</label>
										</div>
										<div class="col-sm-1">
											{{ Form::checkbox('training_agency_yes_no_tier3', 'Training Agency No', @$training_agency_no_tier3, ['id' => 'training_agency_no_tier3', 'class'=>'styled']) }}
											<label class="pl-0" for="training_agency_no_tier3" style="font-weight: 300; margin-left: 5px;">No</label>
										</div>
									</div>
									<div id="training_agency_yes_no_tier3">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="col-sm-2 control-label">Contact Email</label>
												<div class="col-sm-5">
													{!! Form::email('training_contact_email_tier3', @$countyData[0]['service_tier3']['training_contact_email_tier3'], ['class' => 'form-control', 'id' => 'training_contact_email_tier3', 'title' => 'Contact Email',  'placeholder' => '']) !!}
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label class="col-sm-2 control-label">Contact Name</label>
												<div class="col-sm-3">
													{!! Form::text('training_contact_first_name_tier3', @$countyData[0]['service_tier3']['training_contact_first_name_tier3'], ['class' => 'form-control', 'id' => 'training_contact_first_name_tier3', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
												</div>
												<div class="col-sm-3">
													{!! Form::text('training_contact_last_name_tier3', @$countyData[0]['service_tier3']['training_contact_last_name_tier3'], ['class' => 'form-control', 'id' => 'training_contact_last_name_tier3', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
												</div>
												<label class="col-sm-1 control-label">Phone</label>
												<div class="col-sm-2">
													{!! Form::text('training_contact_phone_number_tier3', @$countyData[0]['service_tier3']['training_contact_phone_number_tier3'], ['class' => 'form-control', 'id' => 'training_contact_phone_number_tier3', 'title' => 'Phone Number',  'placeholder' => '']) !!}
												</div>
											</div>
										</div>
									</div>
									<h5 style="font-weight: bold;">Who is the agency contact for walk-through Agency</h5>
									<div class="col-sm-12">
										<label class="col-sm-3 control-label">Who is the agency</label>
										<div class="col-sm-5">
											{!! Form::text('walk_through_agency_contact_tier3', @$countyData[0]['service_tier3']['walk_through_agency_contact_tier3'], ['class' => 'form-control', 'id' => 'walk_through_agency_contact_tier3', 'title' => 'Who is the agency',  'placeholder' => '']) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		{!! Form::close() !!}
		
	</div>
    
@endsection

@section('modals')
    @include('category.agency._add_rep_tier1_modal')
	@include('category.agency._add_rep_tier2_modal')
	@include('category.agency._add_rep_tier3_modal')
	
	@include('category.agency._delete_rep_tier1_county_modal')
	@include('category.agency._delete_rep_tier2_county_modal')
	@include('category.agency._delete_rep_tier3_county_modal')
@append