@extends('layouts.table-view')
@section('badges')
    
@php
    $base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
@endphp

	<div class="col-xs-12">
		
		{!! Form::open(['url' => url('/').$base_url.'/agencies/addEditAgency', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'id' => 'agencies-form'] ) !!}
		
			{{csrf_field()}}
			<div class="form-group">
				<div class="col-sm-5{{ $errors->has('agency_name') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>Agency Name<span style="color:red;"> *</span></span>
					</label>
					{!! Form::text('agency_name', @$agencyData[0]['agency_name'], ['class' => 'form-control', 'id' => 'agency_name', 'title' => 'Agency Name',  'placeholder' => '']) !!}
					<input type="hidden" id="agency_id" name="agency_id" value="{{@$agencyData[0]['id']}}">
				</div>
				<div class="col-sm-5{{ $errors->has('agency_status') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>Agency Status<span style="color:red;"> *</span></span>
					</label>
					{{ Form::select('agency_status', ['' => '', 'Active' => 'Active', 'Deactivated' => 'Deactivated'], (@$agencyData[0]['status']), ['class' => 'selectpicker form-control', 'title' => 'Select agency status', 'id' => 'agency_status']) }}  
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-5{{ $errors->has('designated_agency_email') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>Designated Agency Email(copied on all CP communications)<span style="color:red;"> *</span></span>
					</label>
					{!! Form::email('designated_agency_email', @$agencyData[0]['designated_agency_email'], ['class' => 'form-control', 'id' => 'designated_agency_email', 'title' => 'Designated Agency Email',  'placeholder' => '']) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-5{{ $errors->has('agency_website') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>Agency Website<span style="color:red;"> *</span></span>
					</label>
					{!! Form::text('agency_website', @$agencyData[0]['agency_website'], ['class' => 'form-control', 'id' => 'agency_website', 'title' => 'Agency Website',  'placeholder' => '']) !!}
					<p id="resource_link_msg" style="display:none;color:red;"></p>
				</div>
				<div class="col-sm-5{{ $errors->has('agency_email_domain') ? ' has-error' : '' }}">
					<label class="control-label">
						<span>Agency Email Domain<span style="color:red;"> *</span></span>
					</label>
					{!! Form::email('agency_email_domain', @$agencyData[0]['agency_email_domain'], ['class' => 'form-control', 'id' => 'agency_email_domain', 'title' => 'Agency Email Domain',  'placeholder' => '']) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-5">
					<label class="control-label">
						<span>Director<span style="color:red;"> *</span></span>
					</label>
					<div class="form-group">
						<div class="col-sm-6{{ $errors->has('first_name') ? ' has-error' : '' }}">
							{!! Form::text('first_name', @$agencyData[0]['first_name'], ['class' => 'form-control', 'id' => 'first_name', 'title' => 'First Name',  'placeholder' => 'First Name']) !!}
						</div>
						<div class="col-sm-6{{ $errors->has('last_name') ? ' has-error' : '' }}">
							{!! Form::text('last_name', @$agencyData[0]['last_name'], ['class' => 'form-control', 'id' => 'last_name', 'title' => 'Last Name',  'placeholder' => 'Last Name']) !!}
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="col-sm-6{{ $errors->has('agency_title') ? ' has-error' : '' }}">
							<label class="control-label">
								<span>Title<span style="color:red;"> *</span></span>
							</label>
							{!! Form::text('agency_title', @$agencyData[0]['agency_title'], ['class' => 'form-control', 'id' => 'agency_title', 'title' => 'Title',  'placeholder' => '']) !!}
						</div>
						<div class="col-sm-6{{ $errors->has('phone_number') ? ' has-error' : '' }}">
							<label class="control-label">
								<span>Phone<span style="color:red;"> *</span></span>
							</label>
							{!! Form::text('phone_number', @$agencyData[0]['phone_number'], ['class' => 'form-control', 'id' => 'phone_number', 'title' => 'Phone Number',  'placeholder' => '']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-5">
					<div class="form-group">
						<div class="col-sm-12{{ $errors->has('director_email') ? ' has-error' : '' }}">
							<label class="control-label">
								<span>Director's Email<span style="color:red;"> *</span></span>
							</label>
							{!! Form::text('director_email', @$agencyData[0]['director_email'], ['class' => 'form-control', 'id' => 'director_email', 'title' => 'Director`s Email', 'placeholder' => '']) !!}
						</div>
						
						<div class="col-sm-12" style="margin-top:47px;"></div>
						<div class="col-sm-12{{ $errors->has('agencies_approval_option') ? ' has-error' : '' }}">
							<label class="control-label">
								<span>Agencies Approval Option<span style="color:red;"> *</span></span>
							</label>
							{{ Form::select('agencies_approval_option', ['' => '', 'Approval Option 1' => 'Approval Option 1', 'Approval Option 2' => 'Approval Option 2', 'Approval Option 3' => 'Approval Option 3', 'Approval Option 4' => 'Approval Option 4', 'Approval Option 5' => 'Approval Option 5', 'Approval Option 6' => 'Approval Option 6'], (@$agencyData[0]['agencies_approval_option']), ['class' => 'selectpicker form-control', 'title' => 'Select agencies approval option', 'id' => 'agencies_approval_option']) }}  
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="col-sm-12{{ $errors->has('address1') ? ' has-error' : '' }}">
							<label class="control-label">
								<span>Address<span style="color:red;"> *</span></span>
							</label>
							{!! Form::text('address1', @$agencyData[0]['address1'], ['class' => 'form-control', 'id' => 'address1', 'title' => 'Address 1',  'placeholder' => '']) !!}
						</div>
						<div class="col-sm-12{{ $errors->has('address2') ? ' has-error' : '' }}">
							<label class="control-label"></label>
							{!! Form::text('address2', @$agencyData[0]['address2'], ['class' => 'form-control', 'id' => 'address2', 'title' => 'Address 2',  'placeholder' => '']) !!}
						</div>
						<div class="col-sm-12">
							<label class="control-label"></label>
							<div class="form-group">
								<div class="col-sm-6{{ $errors->has('city') ? ' has-error' : '' }}">
									{!! Form::text('city', @$agencyData[0]['city'], ['class' => 'form-control', 'id' => 'city', 'title' => 'City',  'placeholder' => 'City']) !!}
								</div>
								<div class="col-sm-2{{ $errors->has('state') ? ' has-error' : '' }}">
									{!! Form::text('state', @$agencyData[0]['state'], ['class' => 'form-control', 'id' => 'state', 'title' => 'State',  'placeholder' => 'ST']) !!}
								</div>
								<div class="col-sm-4{{ $errors->has('zip') ? ' has-error' : '' }}">
									{!! Form::text('zip', @$agencyData[0]['zip'], ['class' => 'form-control', 'id' => 'zip', 'title' => 'ZIP',  'placeholder' => 'ZIP']) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		{!! Form::close() !!}
		
	</div>
    
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
			<div class="addCountyDetails">
				Counties <a href="@if(@$agency_id){{$base_url}}/agencies/county-details?agency_id={{@$agency_id}} @else javascript:void(0); @endif">&nbsp; + </a>
			</div>
            <div class="box table-container">
                <div class="box-body table-responsive">
					<div class="row" style="margin:8px;">
						<div class="col-xs-11">
							<div class="col-xs-7">
								<div class="checkbox checkbox-info checkbox-circle" style="margin:0px 0px 0px 20%;">
									{{ Form::checkbox('county-check-deactivate', null, false, ['id' => 'county-check-deactivate', 'class'=>'styled', 'style' => 'top: 5px;']) }}
									<label class="pl-0" for="county-check-deactivate" style="line-height: 30px;">
										Include Deactivated
									</label>
								</div>
							</div>
						</div>
					</div>
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
							<tr>
								<th>County Name</th>
								<th>Status</th>
								<th>ST</th>
								<th>Tier 1</th>
								<th>Tier 2</th>
								<th>Tier 3</th>
								<th>Agency Rep.</th>
								<th>Rep. Email/s</th>
								<th>Rep. Phone/s</th>
								<th>Activation Date</th>
							</tr>
                        </thead>
                        <tbody>
							@if(isset($countyData) && count(@$countyData) != 0 && !empty(@$agency_id))
								@foreach($countyData as $counties)
									<tr data-id="{{@$counties['id']}}" id="county{{@$counties['id']}}" onclick="editCounty('{{ @$counties['id'] }}, {{ @$agency_id }}');">
										<td>{{@$counties['county_name']}}</td>
										<td>{{@$counties['status']}}</td>
										<td>{{@$counties['state']}}</td>
										<td>{{@$counties['approved_tier1']}}</td>
										<td>{{@$counties['approved_tier2']}}</td>
										<td>{{@$counties['approved_tier3']}}</td>
										<td>{{@$counties['rep_tier1'][0]['first_name_tier1']}} {{@$counties['rep_tier1'][0]['last_name_tier1']}}</td>
										<td>{{@$counties['rep_tier1'][0]['agency_rep_email_tier1']}}</td>
										<td>{{@$counties['rep_tier1'][0]['phone_number_tier1']}}</td>
										<td>{{@$counties['activation_date']}}</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="11">Nothing found.</td>
								</tr>
							@endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
