<div id="addRepTier1Dialog" class="modal" role="dialog">
    <div class="modal-dialog" style="width: 575px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h2 class="modal-title text-center" id="model_title">Tier 1</h2>
            </div>
			<!-- Modal Body -->
			
			{!! Form::open(['url' => url('/').$base_url.'/agencies/repTier1AutoPopulateEmail', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'id' => 'repTier1AutoPopulateEmail-form'] ) !!}
			
				{{csrf_field()}}
				<div class="modal-body">
					<div class="text-center" id="successMeg"></div>
					<div id="inputContentPannel">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8 text-center">
								<span style="font-size: 20px;">Do you want to auto populate an email?</span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="text-align: center;">
					
					{!! Form::button('Yes', ['type' => 'button', 'class' => 'btn btn-success addAgencyTier1', 'style' => 'padding: 5px 25px; margin-left:20px;', 'onclick' => 'addAgencyTier1();']) !!}
					
					{!! Form::button('No', ['type' => 'button', 'class' => 'btn btn-default addAgencyTier1', 'data-dismiss' => 'modal', 'style' => 'padding: 5px 25px; margin-right:20px;', 'onclick' => 'addAgencyTier1();']) !!}
					
				</div>
			
			{!! Form::close() !!}
        </div>
    </div>
</div>