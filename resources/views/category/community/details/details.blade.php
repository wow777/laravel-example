@extends('category.community.details.layout')


@section('badges')
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 id="bbudget" class="text-green number">15877777</h3>
            </div>

            <div class="small-box-footer bg-green inner font-weight-bold" id="h-45">Communities</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-aqua number">00</h3>
            </div>
            <div class="small-box-footer bg-aqua inner font-weight-bold" id="h-45">Activations</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-orange number">?</h3>
            </div>
            <div class="small-box-footer bg-orange inner font-weight-bold" id="h-45">Partners</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-green number">00</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold" id="h-45">Partner Responses</div>
        </div>
    </div>

@endsection


@section('buttons')

    <div class="buttons">
        <a href="#" class="button-sett c-cancel">CANCEL</a>
        <a href="#" class="button-sett c-delete">DELETE</a>
        <a href="#" class="button-sett c-edit">EDIT</a>
        <a href="#" class="button-sett c-save">SAVE</a>
    </div>

@endsection

@section('comm-content')

    <div class="comm-content">
        <div class="c-name-block">
            <h4>Community Name</h4>
            <input type="text" class="comm-name" value="{{$communityDetail->name}}">
            <span>Area of Critical Need:</span>
            <input type="radio" name="yes_no" {{$communityDetail->area_of_critical_need ? 'checked' : ''}}> Yes</input>
            <input type="radio" name="yes_no" {{$communityDetail->area_of_critical_need ? '' : 'checked'}}> No</input>
        </div>
        <div class="c-cord-block">
            <h4>Cordinator's Name</h4>
            <p>{{$communityDetail->coordinator->full_name}}</p>
            <div>
                <label for="c-region">CarePortal Region</label>
                <input type="text" id="c-region">
            </div>
            <div>
                <label for="c-state">State/Providence</label>
                <input type="text" id="c-state">
            </div>
        </div>
        <div class="zip-block">
            <span class="zip">ZIP Codes.</span>
            <span class="z-plus">+</span>
            <span class="county">or.  County</span>
            <textarea name="zip-text" id="zip-text"></textarea>
        </div>

        <div class="comm-table">
            <div class="t-contacts">
                <h4>Community Contacts</h4>
                <div class="tab-icon-block">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <h4>Export</h4>
                </div>
                <div class="tab-contacts">
                    <ul>
                        <li class="tab-head">
                            <span class="f-box">All</span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role"> </span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role">role</span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role">role</span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role">role</span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role">role</span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role">role</span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-name">First Name</span>
                            <span class="l-name">Last Name</span>
                            <span class="email-a">email address</span>
                            <span class="role">role</span>
                            <span class="b-role">Role</span>
                            <span class="org">Organization</span>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="t-contacts m-30" id="half-width">
                <h4>Activation History</h4>
                <div class="tab-icon-block">
                    <h4>Export</h4>
                </div>

                <div class="tab-contacts">
                    <ul>
                        <li class="tab-head">
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-date">Date</span>
                            <span class="act-by">Activated By</span>
                            <span class="orgg">Organization</span>
                            <span class="request-id">Request ID#</span>
                        </li>
                        <li>
                            <span class="f-box"><input type="checkbox"></span>
                            <span class="f-date">Date</span>
                            <span class="act-by">Activated By</span>
                            <span class="orgg">Organization</span>
                            <span class="request-id">Request ID#</span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="t-contacts" id="half-width">
                <h4>Churches</h4>
                <div class="tab-icon-block">
                    <h4>Export</h4>
                </div>

                <div class="tab-contacts">
                    <ul>
                        <li class="tab-head">
                            <span class="c-small">Church/Small</span>
                            <span class="group-n">Group Name</span>
                            <span class="s-group">Small Group</span>
                            <span class="t-city">City</span>
                        </li>
                        <li>
                            <span class="c-small">Church/Small</span>
                            <span class="group-n">Group Name</span>
                            <span class="s-group">Small Group</span>
                            <span class="t-city">City</span>
                        </li>
                        <li>
                            <span class="c-small">Church/Small</span>
                            <span class="group-n">Group Name</span>
                            <span class="s-group">Small Group</span>
                            <span class="t-city">City</span>
                        </li>
                        <li>
                            <span class="c-small">Church/Small</span>
                            <span class="group-n">Group Name</span>
                            <span class="s-group">Small Group</span>
                            <span class="t-city">City</span>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="t-contacts m-30" >
                <h4>Notes</h4>
                <span class="z-plus">+</span>
                <div class="tab-icon-block">
                    <h4>Export</h4>
                </div>

                <div class="tab-contacts " id="h-100">
                    <ul>
                        <li class="tab-head">
                            <span class="k-date">Date</span>
                            <span class="k-user">User</span>
                            <span class="k-note">Note</span>
                        </li>
                        <li>
                            <span class="k-date">Date</span>
                            <span class="k-user">User FN LN.</span>
                            <span class="k-note">Note text goes here</span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    </div>

@endsection