@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('sidebar')
    @include('layouts.sidebar')
@endsection
@section('content-title')
    <h1>Community Detail</h1>

    {{-- attention buttons --}}
    @yield('buttons')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @elseif(session('error'))
                    <div class="alert alert-success">{{ session('error') }}</div>
                @endif
            </div>


            {{-- Badges --}}
            <div class="row members-content-numbers pb-5">
                @yield('badges')
            </div>


            {{-- community details content --}}
            @yield('comm-content')


        </div>

    </section>

@endsection


@section('scripts')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.flash.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.js') }}"></script>
    <script src="{{ asset('js/datatable/input.js') }}"></script>
    <script src="{{ asset('js/datatable/datatable.js') }}"></script>
    <script src="{{ asset('js/datatable/table.js') }}"></script>
    <script src="{{ asset('js/interactive.js') }}"></script>
    {{--<script src="{{ asset('js/churches.js') }}"></script>--}}
@append