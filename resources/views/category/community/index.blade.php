@extends('layouts.table-view')


@section('badges')
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-green number">{{$totalCommunities}}</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold" id="h-45">Communities</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-red number">{{$communitiesOfCriticalNeed}}</h3>
            </div>
            <div class="small-box-footer bg-red inner font-weight-bold" id="h-45">Communities of Critical Need</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-aqua number">??</h3>
            </div>
            <div class="small-box-footer bg-aqua inner font-weight-bold" id="h-45">Communities Activations</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-orange number">?</h3>
            </div>
            <div class="small-box-footer bg-orange inner font-weight-bold" id="h-45">Community<br>Partners</div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3 width-20">
        <div class="small-box f-header center-block f-header-small-box" id="abs-width">
            <div class="small-box-bg-white text-center">
                <h3 class="text-green number">?</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold" id="h-45">Partner<br>Responses</div>
        </div>
    </div>
@endsection




@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Critical</th>
                            <th>Coordinator</th>
                            <th>Coordinator Email</th>
                            <th># Activations</th>
                            <th># Partners</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($communities))
                            @foreach($communities as $community)

                                <tr data-id="{{ $community->id }}">
                                    <td>
                                        <a href="{{route('communityDetail',['role'=>request()->segment(2),'role_id'=>request()->segment(3),'id'=>$community->id])}}">{{ $community->name }}</a>
                                    </td>
                                    <td>{{ ($community->area_of_critical_need == 1) ? 'X' :'' }}</td>
                                    <td>{{ $community->coordinator->full_name }}</td>
                                    <td>{{ $community->coordinator->email }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <style>

        @media all and (min-width:1240px) {
            .width-20 {
                width: 20%;
            }
        }

    </style>

    {{--Font Size For badges--}}
    <script>
        let slides = (document.getElementsByClassName("badge-number"));
        for (let i = 0; i < slides.length; i++) {
            let number = (slides.item(i).childNodes[1]);
            let length = number.innerText.length;

            if (length >= 3 && length < 5) {
                number.style.fontSize = '68px';
            }
            else if (length >= 5 && length < 7) {
                number.style.fontSize = '50px';
            }
            else if (length >= 7 && length < 10) {
                number.style.fontSize = '37px';
            }
            else if (length >= 10 && length < 15) {
                number.style.fontSize = '21px';
            }

        }
    </script>
@endsection
