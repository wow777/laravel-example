@extends('layouts.table-view')

@section('badges')
    @include('layouts.badges')
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>All</th>
                            <th>State</th>
                            <th>State Director</th>
                            <th>Total Children Served</th>
                            <th>Avg Children Served Per Month</th>
                            <th>Last 12 Month</th>
                            <th>Number of Churches Giving</th>
                            <th>Requests Past 30 Days</th>
                            <th>Economic Impact 1yr</th>
                            <th>Number of Agencies</th>
                            <th>Pending Churches</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($states))
                            @foreach($states as $state)
                                @php
                                    $yearlyStats = $state->getYearlyStats();
                                @endphp
                                <tr data-id="{{ $state->id }}">
                                    <td><input type="checkbox" name="{{ $state->id }}" value="{{ $state->id }}"></td>
                                    <td>{{$state->name}}</td>
                                    <td>{{ $state->stateDirector->full_name }}</td>
                                    <td>{{$state->total_children_served}}</td>
                                    <td>{{number_format($state->average_children_served_per_month, 0)}}</td>
                                    <td>{{$yearlyStats['children_served']}} {{$state->children_served_last_12_months}}</td>
                                    <td>?</td>
                                    <td>{{$state->old_request_count}}</td>
                                    <td>{{$state->economic_impact_last_1_year}}</td>
                                    <td>{{$state->agencies->count()}}</td>
                                    <td>{{$state->pendingActivation()->count()}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
