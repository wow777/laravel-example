@extends('layouts.table-view')
<style>
    .icon-buttons, .dt-buttons {
        display: flex;
        justify-content: flex-end;
        margin-right: 20px;
    }

    .fa {
        padding: 5px;
    }

    .modal-title, .modal-footer {
        display:flex;
        justify-content: center;
    }


</style>


@section('badges')
    @include('layouts.badges')
@endsection

@section('table')
    <div class="row">
        <div class="icon-buttons">
            <a href="#"><i id="calendar" style="color:teal" class="fa fa-calendar fa-2x"></i></a>
            <a href="#"><i id="functionList" style="color:green" class="fa fa-envelope fa-2x"></i></a>
            <a href="#"><i id="mailto" style="color:red" class="fa fa-envelope-o fa-2x"></i></a>
        </div>
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>All</th>
                            <th>Date Activated</th>
                            <th>Status</th>
                            <th>Church Name</th>
                            <th>Tier Approval</th>
                            <th>Primary Point Person</th>
                            <th>Phone</th>
                            <th>City</th>
                            <th>ST</th>
                            <th>Last Login</th>
                            <th>Responses</th>
                            <th>Requests Met</th>
                            <th style="display:none">Email</th>
					@if(count(@$columns) != 0)
						@foreach ($columns as $column)
							<th style="display:none">{{ $column }}</th>

						@endforeach
					@endif
                        </tr>
                        </thead>
                        <tbody>
                        @if(count(@$churches) != 0)

                            @foreach($churches as $church)
                                <tr data-id="{{ $church->id }}">
                                    <td><input type="checkbox" name="{{ $church->id }}" value="{{ $church->id }}"></td>
                                    <td class="foo">{{ empty($church->tier_1_enrollment_complete_date) ? '' : $church->tier_1_enrollment_complete_date->format('Y-m-d') }}</td>
                                    <td class="{{'status-'.$church->status()}}">{{$church->status()}}</td>
                                    <td>{{ $church->name }}</td>
                                    <td>{{ $church->getTierApproval() }}</td>
                                    <td>{{$church->primary_point_person ? $church->primary_point_person->full_name : ''}}</td>
                                    <td>{{$church->primary_point_person ? $church->primary_point_person->formatted_phone : ''}}</td>
                                    <td>{{$church->city}}</td>
                                    <td>{{$church->state->abbreviation}}</td>
                                    <td>{{$church->primary_point_person ? $church->primary_point_person->last_login : ''}}</td>
                                    <td>{{$church->response_count}}</td>
                                    <td>{{$church->requests_met}}</td>
                                    <td style="display:none">{{$church->primary_point_person ? $church->primary_point_person->email: ''}}</td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">No churches found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    {{$churches->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection





<!-- Modal -->
<div id="functionListModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select the function from the drop-down below.</h4>
            </div>
            <div class="modal-body">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Example function 1</a></li>
                        <li><a href="#">Example function 2</a></li>
                        <li><a href="#">Example function 3</a></li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>

    </div>
</div>