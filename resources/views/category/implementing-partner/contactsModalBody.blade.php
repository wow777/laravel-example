<form action="{{route('implementingPartner.manageContacts', [$roleCode, $roleId, $implementingPartner])}}" method="post" id="ip-contacts-form">
    {{csrf_field()}}
    <div class="form-group" style="height: 200px; overflow-y: scroll;">
        <table class="table">
            @foreach(\App\User::get() as $user)
                <tr>
                    <td>
                        {{$user->displayName}}
                    </td>
                    <td>
                        <input type="checkbox" name="admins[]" value="{{$user->id}}"
                            {{$implementingPartner->admins->where('id', $user->id)->first() ? 'checked' : ''}}>
                        Admin
                    </td>
                    <td>
                        <input type="checkbox" name="ambassadors[]" value="{{$user->id}}"
                        {{$implementingPartner->ambassadors->where('id', $user->id)->first() ? 'checked' : ''}}>
                        Ambassador
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</form>