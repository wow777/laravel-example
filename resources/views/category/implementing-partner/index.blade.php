@extends('layouts.table-view')


@section('badges')
    {{--No Badges Here--}}
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>All</th>
                            <th>IP Name</th>
                            <th>Active Churches</th>
                            <th>Ambassadors</th>
                            <th>Primary IP Admin</th>
                            <th>Phone</th>
                            <th>Email</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($implementingPartners))
                            @foreach($implementingPartners as $implementingPartner)
                                <tr data-id="{{ $implementingPartner->id }}" style="cursor:pointer;"
                                    onclick="location.href='{{route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner])}}'">
                                    <td><input type="checkbox" name="check-{{ $implementingPartner->id }}" value="{{ $implementingPartner->id }}"></td>
                                    <td>{{$implementingPartner->name}}</td>
                                    <td>{{$implementingPartner->churches->count()}}</td>
                                    <td>{{$implementingPartner->ambassadors->count()}}</td>
                                    <td>??</td>
                                    <td>{{$implementingPartner->phone}}</td>
                                    <td>??</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    {{$implementingPartners->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
