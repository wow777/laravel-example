@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('content-title')
    Contact detail
@endsection

@section('content')
    <form action="{{route('implementingPartner.update', [$roleCode, $roleId, $implementingPartner])}}" method="post" enctype="multipart/form-data" id="edit-form">
        {{method_field('PUT')}}
        {{csrf_field()}}
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    @if(session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @elseif(session('error'))
                        <div class="alert alert-success">{{ session('error') }}</div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <a href="{{route('role.implementingPartners', [$roleCode, $roleId])}}">< BACK</a>
                    </div>
                    <div class="col-md-9">
                        <a href="#" onclick="$(this).closest('form').submit()" class="btn btn-success pull-right">SAVE</a>
                        <a href="#" class="btn btn-edit btn-info pull-right" style="margin-right: 15px;">EDIT</a>
                        @if($implementingPartner->status == $implementingPartner::STATUS_ACTIVE)
                            <a href="{{route('implementingPartner.deactivate', [$roleCode, $roleId, $implementingPartner])}}" style="margin: 0 15px;" class="btn btn-danger pull-right">DEACTIVATE</a>
                        @else
                            <a href="{{route('implementingPartner.activate', [$roleCode, $roleId, $implementingPartner])}}" style="margin: 0 15px;" class="btn btn-success pull-right">ACTIVATE</a>
                        @endif
                        <a href="{{route('role.implementingPartners', [$roleCode, $roleId])}}" class="btn btn-default pull-right">CANCEL</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">&nbsp;</div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
                        <div class="small-box f-header center-block f-header-small-box">
                            <div class="small-box-bg-white text-center badge-number">
                                <h3 class="text-red number">{{$implementingPartner->churchesWithoutNewResponses()->count()}}</h3>
                            </div>
                            <div class="small-box-footer bg-red inner font-weight-bold">No Engagement<br>> 60 Days</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
                        <div class="small-box f-header center-block f-header-small-box">
                            <div class="small-box-bg-white text-center badge-number">
                                <h3 class="text-aqua number">{{$implementingPartner->churchesWithNewResponses()->count()}}</h3>
                            </div>
                            <div class="small-box-footer bg-aqua inner font-weight-bold">Active in <br>Last 14 days</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
                        <div class="small-box f-header center-block f-header-small-box">
                            <div class="small-box-bg-white text-center badge-number">
                                <h3 class="text-orange number">{{$responses}}</h3>
                            </div>
                            <div class="small-box-footer bg-orange inner font-weight-bold">Responses</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
                        <div class="small-box f-header center-block f-header-small-box">
                            <div class="small-box-bg-white text-center">
                                <h3 class="text-green number">
                                    {{$implementingPartner->newChurches()->count()}}
                                </h3>
                            </div>
                            <div class="small-box-footer bg-green inner font-weight-bold">New Churches <br>Last 60 days</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Implementing Partner Name</span>
                                    </label>
                                    <input type="text" disabled name="name" value="{{$implementingPartner->name}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Address</span>
                                    </label>
                                    <input type="text" disabled name="address_line1" value="{{$implementingPartner->address_line1}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Country</span>
                                    </label>
                                    <input type="text" disabled name="country" value="{{$implementingPartner->country}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>City/Town</span>
                                    </label>
                                    <input type="text" disabled name="city" value="{{$implementingPartner->city}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>State/Providence/Region</span>
                                    </label>
                                    <input type="text" disabled name="state" value="{{$implementingPartner->state}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>ZIP/Postal Code</span>
                                    </label>
                                    <input type="text" disabled name="zip_code" value="{{$implementingPartner->zip_code}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Office Phone</span>
                                    </label>
                                    <input type="text" disabled name="phone" value="{{$implementingPartner->phone}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>IP Website</span>
                                    </label>
                                    <input type="text" disabled name="website_url" value="{{$implementingPartner->website_url}}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Churches</span>
                                    </label>
                                    <a href="{{route('implementingPartner.churchesExport', [$roleCode, $roleId, $implementingPartner])}}" class="pull-right">Export</a>
                                    <div style="height: 200px; overflow-y: scroll; background-color: #fff;">
                                        <table class="table">
                                            @foreach($implementingPartner->churches as $church)
                                            <tr>
                                                <td>
                                                    {{$church->name}}
                                                </td>
                                                <td>
                                                    {{$church->initial_enrollment_email}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Regions</span>
                                        <a href="#" data-toggle="modal" data-target="#implementingPartnerAddRegionsModal">+</a>
                                    </label>
                                    <a href="{{route('implementingPartner.regionsExport', [$roleCode, $roleId, $implementingPartner])}}" class="pull-right">Export</a>
                                    <div style="height: 200px; overflow-y: scroll; background-color: #fff;">
                                        <table class="table">
                                            @foreach($implementingPartner->regions as $region)
                                                <tr>
                                                    <td>
                                                        {{$region->name}}
                                                    </td>
                                                    <td>
                                                        {{$region->regionalManager->displayName}}
                                                    </td>
                                                    <td>
                                                        <a href="javascript:;" onclick="if(confirm('Do you really want to detach this item?')) {location.href='{{route('implementingPartner.regionDetach', [$roleCode, $roleId, $implementingPartner, $region])}}';}" style="cursor: pointer;">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    @component('components.notesList', ['notable' => $implementingPartner, 'roleCode' => $roleCode, 'roleId' => $roleId])
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">
                                <span>Contacts</span>
                                <a href="{{route('implementingPartner.contactsAdd', [$roleCode, $roleId, $implementingPartner])}}" data-toggle="modal" data-target="#implementingPartnerAddContactsModal">+</a>
                            </label>
                            <a href="{{route('implementingPartner.contactsExport', [$roleCode, $roleId, $implementingPartner])}}" class="pull-right">Export</a>
                            <div style="height: 200px; overflow-y: scroll;  background-color: #fff;">
                                <table class="table">
                                    @foreach($contacts as $user)
                                        <tr>
                                            <td onclick="location.href='{{route('contact.view', [$roleCode, $roleId, $user])}}'" style="cursor: pointer;">
                                                @if($user->pivot->admin)
                                                    Admin
                                                    @if($user->pivot->ambassador)
                                                        | Ambassador
                                                    @endif
                                                @elseif($user->pivot->ambassador)
                                                    Ambassador
                                                @else
                                                    Role is not defined
                                                @endif
                                            </td>
                                            <td onclick="location.href='{{route('contact.view', [$roleCode, $roleId, $user])}}'" style="cursor: pointer;">
                                                {{$user->displayName}}
                                            </td>
                                            <td onclick="location.href='{{route('contact.view', [$roleCode, $roleId, $user])}}'" style="cursor: pointer;">
                                                {{$user->phone}}
                                            </td>
                                            <td onclick="location.href='{{route('contact.view', [$roleCode, $roleId, $user])}}'" style="cursor: pointer;">
                                                {{$user->email}}
                                            </td>
                                            <td>
                                                <a href="#" onclick="if(confirm('Do you really want to detach this item?')) {location.href='{{route('implementingPartner.contactDetach', [$roleCode, $roleId, $implementingPartner, $user])}}';}">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
@endsection
@section('modals')
    @component('components.noteAdd', ['notable' => $implementingPartner, 'roleCode' => $roleCode, 'roleId' => $roleId])
    @endcomponent
    @include('category.implementing-partner.addRegionsModal')
    @include('category.implementing-partner.contactsModal')
@append
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#edit-form input').each(function() {
                $(this).prop('disabled', true);
            });
            $('#edit-form .btn-edit').on('click', function() {
                $('#edit-form input:disabled').prop('disabled', false);
            });
            // Fill modal with content from link href
            $("#implementingPartnerAddContactsModal").on("show.bs.modal", function(e) {
                var link = $(e.relatedTarget);
                $(this).find(".modal-body").load(link.attr("href"));
            });
        })
    </script>
@append