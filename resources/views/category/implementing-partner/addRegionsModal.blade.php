<div id="implementingPartnerAddRegionsModal" class="modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Implementing Partner Regions</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('implementingPartner.manageRegions', [$roleCode, $roleId, $implementingPartner])}}" method="post" id="contact-note-form">
                    {{csrf_field()}}
                    <div class="form-group" style="height: 200px; overflow-y: scroll;">
                        <table class="table">
                            @foreach(\App\Models\Region::get() as $region)
                                <tr>
                                    <td>
                                        <input type="checkbox" name="regions[]" value="{{$region->id}}"
                                        {{in_array($region->id, $implementingPartner->regions->pluck('id')->toArray()) ? 'checked' : ''}}>
                                    </td>
                                    <td>
                                        {{$region->name}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="$('#contact-note-form').submit();">Save</button>
            </div>
        </div>

    </div>
</div>