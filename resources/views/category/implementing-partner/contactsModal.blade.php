<div id="implementingPartnerAddContactsModal" class="modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Implementing Partner Contacts</h4>
            </div>
            <div class="modal-body">
                <p style="text-align: center">
                    <img src="/bower_components/ckeditor/plugins/mathjax/images/loader.gif" alt="">
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="$('#ip-contacts-form').submit();">Save</button>
            </div>
        </div>

    </div>
</div>