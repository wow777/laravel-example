@extends('layouts.table-view')

@php
    $base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
@endphp

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
					<div class="row" style="margin:8px;">
						<div class="col-xs-11">
							<div class="row col-xs-2">
								<a href="javascript:void(0)" class="btn btn-default pull-center" onclick="showAllResources();" style="padding: 5px 25px;">Show All</a>
							</div>
							<div class="row col-xs-3">
								<a href="javascript:void(0)" onClick="addCategoriesDialog();" class="btn btn-default pull-center categories">Categories</a>
							</div>
							<div class="col-xs-6">
								<div class="checkbox checkbox-info checkbox-circle" style="margin:0px;">
									{{ Form::checkbox('check-deactivate', null, false, ['id' => 'check-deactivate', 'class'=>'styled', 'style' => 'top: 5px;']) }}
									
									<label for="check-deactivate" style="line-height: 30px;">
										Show Deactivated
									</label>
								</div>
							</div>
						</div>
					</div>
                    <table id="datatable" class="table table-bordered table-striped mainTable" width="100%">
                        <thead>
                        <tr>
                            <th>Date Added</th>
							<th>Status</th>
							<th>Title</th>
							<th>Category</th>
							<th>Role/s</th>
                        </tr>
                        </thead>
                        <tbody>
						
							@if(isset($getResourceData) && count($getResourceData) != 0)
								@foreach($getResourceData as $resources)
									@if($resources['resource_type'] == 'Video')
										@php 
											$resourceLinkArr = explode("=", $resources['resource_link']); 
										@endphp
									@endif
									
									<tr data-id="{{ $resources['id'] }}" id="resources{{ $resources['id'] }}">
										<td onClick="editResource('{{ $resources['id'] }}');">{{$resources['added_date']}}</td>
										<td onClick="editResource('{{ $resources['id'] }}');">{{$resources['status']}}</td>
										<td onClick="editResource('{{ $resources['id'] }}');">{{$resources['title']}}</td>
										<td onClick="editResource('{{ $resources['id'] }}');">{{$resources['category']}}</td>
										<td onClick="@if($resources['resource_type'] == 'Video') openVideosImage('{{ $resources['resource_link'] }}'); @else editResource('{{ $resources['id'] }}'); @endif">
											@if($resources['resource_type'] == 'PDF') 
												<img src="{{asset('img/pdf-icons.png')}}" title="PDF Image" alt="PDF Image" width="22" />
											@elseif($resources['resource_type'] == 'PowerPoint')  
												<img src="{{asset('img/powerPoint-icon.png')}}" title="PowerPoint Image" alt="PowerPoint Image" width="22"/>
											@elseif($resources['resource_type'] == 'Video')  
												<img src="https://img.youtube.com/vi/{{@$resourceLinkArr[1]}}/0.jpg" title="Click Here Get Video First Screen" alt="Video Image" width="22" />
											@endif
											{{$resources['role']}}
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="5">Nothing found.</td>
								</tr>
							@endif
							
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('category.resource._add_resources_modal')
	@include('category.resource._add_categories_modal')
	@include('category.resource._show_video_screen_modal')
	@include('category.resource._delete_resources_modal')
@append