<div id="addCategoriesDialog" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h2 class="modal-title text-center">CATEGORIES</h2>
            </div>
			<!-- Modal Body -->
			<div class="modal-body">
				
				{!! Form::open(['url' => '', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'id' => 'category-form'] ) !!}
			
					{{csrf_field()}}
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8 category-main-content">
							
							<table class="table table-striped deleteCategory">
								<thead>
									<tr>
										<th>Category Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(isset($getCategoryData) && count($getCategoryData) != 0)
										@foreach($getCategoryData as $categories)
											<tr id="deleteCategory_{{$categories['id']}}">
												<td>
													{{$categories['category']}}
												</td>
												<td>
													<a href="javascript:void(0);" onclick="deleteCategory('{{$categories['id']}}');">
														<span class="glyphicon glyphicon-trash pull-right" aria-hidden="true" style="color:red;"></span>
													</a>
												<td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="2">Nothing found.</td>
										</tr>
									@endif
								</tbody>
							</table>
							
							<div class="col-xs-12" id="addNextCategoryButton">
								<p class='alignCenter'>
									<a href="javascript:void(0);"  onclick="addNextCategory();" class="addNextCategory">Click here to add next category</a>
								</p>
							</div>
							<div class="col-xs-12" id="addNextCategoryPart">
								<p>
									<label for="usrname">Category Name<span style="color:red;"> *</span></label>
									{!! Form::text('categoryname', null, ['class' => 'form-control', 'id' => 'categoryname', 'title' => 'Category Name']) !!}
									<input type="hidden" id="category_hidden_id">
								</p>
								<p style="margin-bottom:60px;">
									{!! Form::button('Save', ['type' => 'button', 'class' => 'btn btn-primary pull-right btn-block', 'onclick' => 'addNextCategorySubmit();']) !!}
								</p>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
            <div class="modal-footer" style="text-align: center;">
				{!! Form::button('Close', ['type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal', 'style' => 'padding: 5px 25px;']) !!}
            </div>
        </div>
    </div>
</div>