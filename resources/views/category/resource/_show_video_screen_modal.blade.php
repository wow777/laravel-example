<div id="videoScreenDialog" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h2 class="modal-title text-center">Video Play Screen</h2>
            </div>
			<!-- Modal Body -->
			<div class="modal-body">
				<div class="row form-group">
					<div class="col-sm-12 text-center" id="videoScreenImage"></div>
				</div>
			</div>
            <div class="modal-footer" style="text-align: center;">
				{!! Form::button('Close', ['type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal', 'style' => 'padding: 5px 25px;']) !!}
            </div>
        </div>
    </div>
</div>