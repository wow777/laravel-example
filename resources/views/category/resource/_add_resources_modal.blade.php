<div id="addResourcesDialog" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h2 class="modal-title text-center" id="model_title">Add Resources</h2>
            </div>
			<!-- Modal Body -->
			
			{!! Form::open(['url' => url('/').$base_url.'/settings/addEditResources', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'id' => 'resources-form'] ) !!}
			
				{{csrf_field()}}
				<div class="modal-body">
					<div class="text-center" id="successMeg"></div>
					<div id="inputContentPannel">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<label class="control-label">
									<span>Title<span style="color:red;"> *</span></span>
								</label>
								{!! Form::text('title', null, ['class' => 'form-control', 'id' => 'title', 'title' => 'Title',  'placeholder' => 'Title Here']) !!}
								<input type="hidden" id="resource_id" name="resource_id"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<label class="control-label">
									<span>Category<span style="color:red;"> *</span></span>
								</label>

								<select class="selectpicker form-control" name="category_name" id="category_name">
									<option value="">Select Category</option>
									@if(isset($getCategoryData) && count($getCategoryData) != 0)
										@foreach($getCategoryData as $categories)
											<option value="{{$categories['category']}}">{{$categories['category']}}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label">
											<span>Resource Type<span style="color:red;"> *</span></span>
										</label>
										{{ Form::select('resource_type', ['PDF' => 'PDF', 'PowerPoint' => 'PowerPoint', 'Video' => 'Video'], null, ['class' => 'selectpicker form-control', 'title' => 'Select resource type', 'id' => 'resource_type']) }}  
									</div>
									<div class="col-sm-8">
										<label class="control-label">
											<span>Resource Link<span style="color:red;"> *</span></span>
										</label>
										{!! Form::text('resource_link', null, ['class' => 'form-control', 'id' => 'resource_link', 'title' => 'Enter Link Here',  'placeholder' => 'Enter Link Here']) !!}
										<p id="resource_link_msg" style="display:none;color:red;"></p>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<label class="control-label">
									<span>Select Role/s<span style="color:red;"> *</span></span>
								</label>
								<div class="col-sm-12 category-main-content">
									<div class="checkbox">
										@if(isset($getRolesData) && count($getRolesData) != 0)
											@php $count = 1; @endphp
											@foreach($getRolesData as $roles)
												<p>
													<label>
														{{ Form::checkbox('roles[]', @$roles['role'], false, ['id' => 'roles_checkbox.$count', 'class'=>'roles_checkbox']) }}
														{{@$roles['role']}}
													</label>
												</p>
											@endforeach
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="text-align: center;">
				
					{!! Form::button('Cancel', ['type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal', 'style' => 'padding: 5px 25px; margin-right:20px;']) !!}
					
					{!! Form::button('Delete', ['type' => 'button', 'class' => 'btn btn-danger', 'style' => 'padding: 5px 25px;display:none; margin:20px;']) !!}
					
					{!! Form::button('Save', ['type' => 'button', 'class' => 'btn btn-success', 'style' => 'padding: 5px 25px; margin-left:20px;', 'onclick' => 'addEditResources();']) !!}
					
				</div>
			
			{!! Form::close() !!}
        </div>
    </div>
</div>