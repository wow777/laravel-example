@extends('layouts.table-view')

@section('badges')
    @include('layouts.badges')
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>All</th>
                            <th>ID</th>
                            <th>Date Added</th>
                            <th>Status</th>
                            <th>Role(s)</th>
                            <th>Organization</th>
                            <th>Name</th>
                            <th>Phone #</th>
                            <th>City</th>
                            <th>County</th>
                            <th>Active Community</th>
                            <th>ST</th>
                            <th>Last Login</th>
                            <th>Requests Met</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($contacts))
                            @foreach($contacts as $contact)
                                <tr data-id="{{ $contact->id }}">
                                    <td><input type="checkbox" name="check-{{ $contact->id }}" value="{{ $contact->id }}"></td>
                                    <td>
                                        <a href="{{route('contact.view', [$roleCode, $roleId, $contact->id])}}">{{$contact->id}}</a>
                                    </td>
                                    <td>{{!empty($contact->created_at)? $contact->created_at->format('d/m/Y'): ''}}</td>
                                    <td>{{$contact->status}}</td>
                                    <td>{{$contact->rolesAsString()}}</td>
                                    <td>{{$contact->organization}}</td>
                                    <td>{{$contact->full_name}}</td>
                                    <td>{{ $contact->formatted_phone }}</td>
                                    <td>??</td>
                                    <td>??</td>
                                    <td>??</td>
                                    <td>??</td>
                                    <td>{{ $contact->last_login }}</td>
                                    <td>??</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="14">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    @if(isset($contacts)){{$contacts->links()}}@endif
                </div>
            </div>
        </div>
    </div>
@endsection
