@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('content-title')
    Contact detail
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @elseif(session('error'))
                    <div class="alert alert-success">{{ session('error') }}</div>
                @endif
            </div>
            
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('role.contacts', [$roleCode, $roleId])}}">< BACK</a>
                </div>
                <div class="col-md-9">
                    <a href="{{route('contact.edit', [$roleCode, $roleId, $user])}}" class="btn btn-info pull-right">EDIT</a>
                    @if($user->status == $user::STATUS_ACTIVE)
                        <a href="{{route('contact.deactivate', [$roleCode, $roleId, $user])}}" style="margin: 0 15px;" class="btn btn-danger pull-right">DEACTIVATE</a>
                    @else
                        <a href="{{route('contact.activate', [$roleCode, $roleId, $user])}}" style="margin: 0 15px;" class="btn btn-success pull-right">ACTIVATE</a>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <span>First Name</span>
                                        </label>
                                        <input type="text" readonly value="{{$user->first_name}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <span>Last Name</span>
                                        </label>
                                        <input type="text" readonly value="{{$user->last_name}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <span>Phone</span>
                                        </label>
                                        <input type="text" readonly value="{{$user->phone}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <span>Email</span>
                                        </label>
                                        <input type="text" readonly value="{{$user->email}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <span>Profile Photo</span>
                                </label><br>
                                <img src="{{$user->user_photo_url}}" alt="" style="max-width: 300px; max-height: 300px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                @component('components.notesList', ['notable' => $user, 'roleCode' => $roleCode, 'roleId' => $roleId])
                                @endcomponent
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <span>CarePortal Status</span>
                        </label>
                        <input type="text" readonly value="{{$user->status}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <span>CarePortal Roles</span>
                        </label>
                        <textarea class="form-control" readonly>{{implode(PHP_EOL, $user->roles())}}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div  style="background-color: #fff; padding: 1px 15px 10px 15px;">
                                <h3>Address</h3>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label">
                                                <span>Address Type</span>
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="" id="" class="form-control" readonly="">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Country</span>
                                    </label>
                                    <select name="" id="" class="form-control" readonly="">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">
                                                <span>City/Town</span>
                                            </label>
                                            <input type="text" readonly class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">
                                                <span>State/Providence/Region</span>
                                            </label>
                                            <input type="text" readonly class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">
                                                <span>ZIP/Postal Code</span>
                                            </label>
                                            <input type="text" readonly class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal -->
@endsection

@section('modals')
    @component('components.noteAdd', ['notable' => $user, 'roleCode' => $roleCode, 'roleId' => $roleId])
    @endcomponent
@append