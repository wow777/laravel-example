@extends('layouts.table-view')


@section('badges')
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-red number">?</h3>
            </div>
            <div class="small-box-footer bg-red inner font-weight-bold">Open Requests</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-aqua number">??</h3>
            </div>
            <div class="small-box-footer bg-aqua inner font-weight-bold">Requests Met To-Date</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-orange number">%</h3>
            </div>
            <div class="small-box-footer bg-orange inner font-weight-bold">Percent Met By Churches</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center">
                <h3 class="text-green number">?</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold">Total Children Served To-Date</div>
        </div>
    </div>
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Date Submitted</th>
                            <th>Request Status</th>
                            <th>Request ID#</th>
                            <th>Responses</th>
                            <th>Request Tier</th>
                            <th>Days Open</th>
                            <th>Churches Notified</th>
                            <th>Children Served</th>
                            <th>Reason Closed</th>
                            <th>Agency Name</th>
                            <th>Agency Contact</th>
                            <th>Agency Email</th>
                            <th>ZIP</th>
                            <th>County</th>
                            <th>Economic Impact</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($requests))
                            @foreach($requests as $request)
                                <tr data-id="{{ $request->id }}">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
