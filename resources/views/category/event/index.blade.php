@extends('layouts.table-view')

@section('badges')
    @include('layouts.badges')
@endsection

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Creator</th>
                            <th>Registrants</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>ST</th>
                            <th>ZIP</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($events))
                            @foreach($events as $event)
                                <tr data-id="{{ $event->id }}">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">Nothing found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
