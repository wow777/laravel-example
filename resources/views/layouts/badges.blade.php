<div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
    <div class="small-box f-header center-block f-header-small-box">
        <div class="small-box-bg-white text-center badge-number">
            <h3 class="text-red number">{{$badges['one']['number']}}</h3>
        </div>
        <div class="small-box-footer bg-red inner font-weight-bold">{{$badges['one']['title']}}
            <br>{{$badges['one']['title_br'] or ''}}</div>
    </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
    <div class="small-box f-header center-block f-header-small-box">
        <div class="small-box-bg-white text-center badge-number">
            <h3 class="text-aqua number">{{$badges['two']['number']}}</h3>
        </div>
        <div class="small-box-footer bg-aqua inner font-weight-bold">{{$badges['two']['title']}}
            <br>{{$badges['two']['title_br'] or ''}}</div>
    </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
    <div class="small-box f-header center-block f-header-small-box">
        <div class="small-box-bg-white text-center badge-number">
            <h3 class="text-orange number">{{$badges['three']['number']}}</h3>
        </div>
        <div class="small-box-footer bg-orange inner font-weight-bold">{{$badges['three']['title']}}
            <br>{{$badges['three']['title_br'] or ''}}</div>
    </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
    <div class="small-box f-header center-block f-header-small-box">
        <div class="small-box-bg-white text-center">
            <h3 class="text-green number">{{$badges['four']['number']}}</h3>
        </div>
        <div class="small-box-footer bg-green inner font-weight-bold">{{$badges['four']['title']}}
            <br>{{$badges['four']['title_br'] or ''}}</div>
    </div>
</div>

@if(isset($badges['five']))
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-red number">{{$badges['five']['number']}}</h3>
            </div>
            <div class="small-box-footer bg-red inner font-weight-bold">{{$badges['five']['title']}}
                <br>{{$badges['five']['title_br'] or ''}}</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-aqua number">{{$badges['six']['number']}}</h3>
            </div>
            <div class="small-box-footer bg-aqua inner font-weight-bold">{{$badges['six']['title']}}
                <br>{{$badges['six']['title_br'] or ''}}</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center badge-number">
                <h3 class="text-orange number">{{$badges['seven']['number']}}</h3>
            </div>
            <div class="small-box-footer bg-orange inner font-weight-bold">{{$badges['seven']['title']}}
                <br>{{$badges['seven']['title_br'] or ''}}</div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-3">
        <div class="small-box f-header center-block f-header-small-box">
            <div class="small-box-bg-white text-center">
                <h3 class="text-green number">{{$badges['eight']['number']}}</h3>
            </div>
            <div class="small-box-footer bg-green inner font-weight-bold">{{$badges['eight']['title']}}
                <br>{{$badges['eight']['title_br'] or ''}}</div>
        </div>
    </div>
@endif