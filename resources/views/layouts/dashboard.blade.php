@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('content-title')
    Dashboard
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @elseif(session('error'))
                    <div class="alert alert-success">{{ session('error') }}</div>
                @endif
            </div>

            <div class="row">
                @include('common.lineGraph', [
                    'id' => uniqid('total-activated-churches-canvas-'),
                    'title' => 'Total Activated Churches',
                    'xAxisLabels' => $lineGraphXAxisLabels,
                    'dataSets' => $activatedChurchesDataSets,
                    'exportUrl' => $activatedChurchesExportUrl,
                ])
            </div>

            <div class="row">
                @include('common.lineGraph', [
                    'id' => uniqid('total-activated-groups-canvas-'),
                    'title' => 'Total Activated Groups',
                    'xAxisLabels' => $lineGraphXAxisLabels,
                    'dataSets' => $activatedGroupsDataSets,
                    'exportUrl' => $activatedGroupsExportUrl,
                ])
            </div>

            <div class="row">
                @include('common.lineGraph', [
                    'id' => uniqid('total-requests-canvas-'),
                    'title' => 'Total Requests',
                    'xAxisLabels' => $lineGraphXAxisLabels,
                    'dataSets' => $requestsDataSets,
                    'exportUrl' => $requestsExportUrl,
                ])
            </div>

            <div class="row">
                @include('common.lineGraph', [
                    'id' => uniqid('total-children-served-canvas-'),
                    'title' => 'Total Children Served',
                    'xAxisLabels' => $lineGraphXAxisLabels,
                    'dataSets' => $childrenServedDataSets,
                    'exportUrl' => $childrenServedExportUrl,
                ])
            </div>

            <div class="row">
                @include('common.lineGraph', [
                    'id' => uniqid('total-income-canvas-'),
                    'title' => 'Total Income',
                    'xAxisLabels' => $lineGraphXAxisLabels,
                    'dataSets' => $incomeDataSets,
                    'exportUrl' => $incomeExportUrl,
                ])
            </div>

            <div class="row">
                @include('common.lineGraph', [
                    'id' => uniqid('economic-impact-canvas-'),
                    'title' => 'Economic Impact',
                    'xAxisLabels' => $lineGraphXAxisLabels,
                    'dataSets' => $economicImpactDataSets,
                    'exportUrl' => $economicImpactExportUrl,
                ])
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="{{ asset('js/advanced-search/config.js') }}"></script>
    <script src="{{ asset('js/datatable/input.js') }}"></script>
    <script src="{{ asset('js/datatable/datatable.js') }}"></script>
    <script src="{{asset('js/charts.js')}}"></script>
    {{--<script src="{{ asset('js/campaigns/campaigns.table.js') }}"></script>--}}
@append
