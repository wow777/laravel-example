@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	@if(request()->segment(4) == 'settings' || request()->segment(4) == 'agencies')
		@if(request()->segment(5) == 'resources' || request()->segment(4) == 'agencies')
			<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
		@endif
	@endif
@endsection

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@php
    $base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
@endphp

@section('content-title')
	@if(request()->segment(4) == 'settings')
		{{ ucfirst( request()->segment(5) ) }}
		@if(request()->segment(5) == 'resources')
			<a href="javascript:void(0)" onclick="addResourcesDialog();">&nbsp; + </a>
		@endif
	@elseif(request()->segment(4) == 'agencies')
		@if(request()->segment(5) == 'agency-details' || request()->segment(5) == 'county-details')
			{{ ucwords( str_replace("-"," ",request()->segment(5)) ) }}
		@else
			{{ ucfirst( request()->segment(4) ) }}
			<a href="{{$base_url}}/agencies/agency-details">&nbsp; + </a>
		@endif
	@else
		{{ ucfirst( request()->segment(4) ) }}
	@endif

@endsection

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @elseif(session('error'))
                    <div class="alert alert-success">{{ session('error') }}</div>
                @endif
            </div>
            {{-- Badges --}}
            <div class="row members-content-numbers pb-5">
                @yield('badges')
            </div>

			@if(request()->segment(5) != 'agency-details' && request()->segment(5) != 'county-details')
				{{-- Search --}}
				<div class="container mb-2" id="search-body">
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3 message"></div>
					</div>
					<div class="row">
						<form action="">
							@if(request()->segment(4) == 'settings' || request()->segment(4) == 'agencies')
								<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">
							@else
								<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-2 col-sm-offset-1">
							@endif
								<label for="search" class="mr-1 pull-left"><h3 class="m-0">SEARCH</h3></label>
								<input type="search" id="search"
									   class="form-control input-search input-sm mr-1 pull-left" name="q" value="{{request('q')}}"
								onchange="$(this).closest('form').submit();">
								@if(request()->segment(4) == 'settings' || request()->segment(4) == 'agencies')
									@if(request()->segment(5) == 'resources' || request()->segment(4) == 'agencies')
										<button type="submit" class="btn btn-primary" style="padding: 4px 20px;">Advanced</button>
									@endif
								@endif
							</div>
						</form>
					</div>
				</div>
			@endif
			<!--
			@if(request()->segment(4) == 'agencies' && request()->segment(5) != 'agency-details' && request()->segment(5) != 'county-details')
				<div class="row">
					<div class="col-lg-1 col-lg-offset-11" id="export-dest">
						<strong>Export</strong>
					</div>
				</div>
			@endif
			-->

            {{-- Table --}}
            @yield('table')

        </div>

    </section>

@endsection



@section('scripts')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.flash.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.js') }}"></script>
    <script src="{{ asset('js/datatable/input.js') }}"></script>
    <script src="{{ asset('js/datatable/datatable.js') }}"></script>
    <script src="{{ asset('js/datatable/table.js') }}"></script>
    @if(request()->segment(4) !='communities')
        <script src="{{ asset('js/interactive.js') }}"></script>
    @endif
    <script src="{{ asset('js/churches.js') }}"></script>
	@if(request()->segment(4) == 'settings' || request()->segment(4) == 'agencies')
		@if(request()->segment(5) == 'resources' || request()->segment(4) == 'agencies')
			<script src="{{ asset('js/custom.js') }}"></script>
			<script>
				var base_url = "{{url('/')}}/role/{{request()->segment(2)}}/{{request()->segment(3)}}";
				var agency_id = "{{@$agency_id}}";
				if(agency_id != ''){
					$("form#agencies-form :input").prop("disabled", true);
					$("form#agencies-form input#agency_id").prop("disabled", false);
					$("form#agencies-form input[name='_token']").prop("disabled", false);
				}
			</script>
		@endif
	@endif
@append