<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CarePortal') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <link rel="stylesheet" href="{{ asset('css/helper.css') }}">
  
    @yield('css')
</head>
<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">

        <!-- Left side column. contains the logo and sidebar -->
    @yield('sidebar')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content-header bg-light pb-1">
                <div class="row">
                    <div class="col-sm-9"><h1>@yield('content-title')</h1></div>
                    <div class="col-sm-1"><h4>Help</h4></div>
                    <div class="col-sm-2 pr-2" style="text-align:right;">
                        <img src="{{asset('img/connected-by-careportal-no-swooshes.png')}}"
                             alt="Connected by CarePortal"
                             style="max-height: 30px!important;line-height: 100%;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;height: auto!important;border:none;margin:0">
                        <div style="padding-top: 5px;">Contact CarePortal</div>
                    </div>
                </div>
				@if(request()->segment(5) == 'agency-details')
					<div class="row text-right" style="margin-bottom: -11px;">
						{!! Form::button('CANCEL', ['type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal', 'onclick' => 'goBack()']) !!}
						
						@if(!empty(@$agency_id))
							
							{!! Form::button('DEACTIVATE', ['type' => 'button', 'class' => 'btn btn-danger', 'style' => 'margin-left:20px;', 'onclick' => 'deactivateAgency();']) !!}
								
							{!! Form::button('EDIT', ['type' => 'button', 'class' => 'btn btn-primary', 'style' => 'margin-left:20px;', 'onclick' => 'agencyEditable();']) !!}
							
						@endif
						
						{!! Form::button('SAVE', ['type' => 'button', 'class' => 'btn btn-success', 'style' => 'margin-left:20px;margin-right:10px;', 'onclick' => 'addEditAgency();']) !!}
					</div>
				@elseif(request()->segment(5) == 'county-details')
					<div class="row text-right" style="margin-bottom: -11px;">
						{!! Form::button('SAVE', ['type' => 'button', 'class' => 'btn btn-success', 'style' => 'margin-right:20px;', 'onclick' => 'addEditCounty();']) !!}
					</div>
				@endif
            </section>
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © 2018 <a href="#">The Global Orphan Project</a>.</strong> All rights reserved.
        </footer>
    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{!! url('/') !!}">
                    InfyOm Generator
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{!! url('/home') !!}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="{!! url('/login') !!}">Login</a></li>
                    <li><a href="{!! url('/register') !!}">Register</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@endif

@yield('modals')

<!-- Scripts -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/admin-lte/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/shared.js') }}"></script>
<!--  <script src="{{ asset('js/churches.js') }}"></script>  -->


@yield('scripts')
</body>
</html>
