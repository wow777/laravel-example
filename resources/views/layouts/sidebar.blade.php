<aside class="main-sidebar" id="sidebar-wrapper">

    <a href="#" class="logo">
        <img src="{{ Auth::user()->implementing_partner->logo_url ?? 'https://s3-us-west-2.amazonaws.com/careportal-ip-logo/logo_goproject1.png'}}" class="img-responsive">
    </a>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form (Optional)
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form>
         Sidebar Menu -->

        <ul class="sidebar-menu" data-widget="tree">


            @if(!empty(Auth::user()->roles()) )
                <li>
                    @component('components.roleDropDown')
                    @endcomponent
                </li>
            @endif

            {{-- gets role type of url /role/gms/317938/ --}}
            @include(Request::segment(2).'.menu')
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>