<script type="text/javascript">

    var thEvt = function (pId) {
        if (!document.getElementById(pId)) {
            return;
        }
        var el = document.getElementById(pId);
        el['onchange'] = function () {
            window.location.href = el.value;
        }
    };


    window['onload'] = function () {
        thEvt('region-select');
        thEvt('role-select');
    }


</script>


@php
    $base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
@endphp


<li class="">
    <a class="link" href="{{$base_url}}/dashboard">
        <i class="fa fa-user" aria-hidden="true"></i>
        <span>Dashboard</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/states">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>States</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/regions">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Regions</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/churches">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Churches</span>
    </a>
</li>

<li class="">
    <a class="link" href="{{$base_url}}/applications">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Applications</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/contacts">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Contacts</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/agencies">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Agencies</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/implementing-partners">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>IP's</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/communities">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Community</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/events">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Events</span>
    </a>
</li>
<li class="">
    <a class="link" href="{{$base_url}}/finances">
        <i class="fa fa-circle-o" aria-hidden="true"></i>
        <span>Finances</span>
    </a>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-cogs" aria-hidden="true"></i> <span>Settings</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu" style="display: none;">
        <li><a href="{{$base_url}}/settings/messaging-center"><i></i> Messaging Center</a></li>
        <li><a href="{{$base_url}}/settings/states"><i></i> States</a></li>
        <li><a href="{{$base_url}}/settings/regions"><i></i> Regions</a></li>
        <li><a href="{{$base_url}}/settings/communities"><i></i> Communities</a></li>
        <li><a href="{{$base_url}}/settings/counties"><i></i> Counties</a></li>
        <li><a href="{{$base_url}}/settings/implementing-partners"><i></i> IP's</a></li>
        <li><a href="{{$base_url}}/settings/churches"><i></i> Churches</a></li>
        <li><a href="{{$base_url}}/settings/groups"><i></i> Groups</a></li>
        <li><a href="{{$base_url}}/settings/community-partners"><i></i> Community Partners</a></li>
        <li><a href="{{$base_url}}/settings/agencies"><i></i> Agencies</a></li>
        <li><a href="{{$base_url}}/settings/resources"><i></i> Resources</a></li>
        <li><a href="{{$base_url}}/settings/users"><i></i> Users</a></li>
    </ul>
</li>



<li class="treeview">
    <!-- starting with Laravel 5.3  /logout is now a POST -->
    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                class="fa fa-sign-out "></i>
        Logout
    </a>
    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>

