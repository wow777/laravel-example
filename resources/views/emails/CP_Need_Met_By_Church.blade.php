@extends('emails.layout.layout')
@section('title', 'CP Need Met By Church')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent cls_church">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>PULPIT ROCK CHURCH HAS Met This Need!</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="reunifyContent req_church">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Request {#12467} - {Normal; Within 7-10 days}</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>{Help Prevent a Child From Entering Foster Care}</h1></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>{EI Paso County, Colorado (80910)}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Urgency: {Normal; Within 7-10 days}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Children Served: {4}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">Estimated Need: {Primarily goods & services} -
                                see detailed description of need</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;"><b>Items Needed</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">{Item 1, item 2, item 3}</h3></td>
                    </tr>
                </table>
            </div>
            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Description of the Need</h1></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p>{SECOND REQUEST Large request WOULD BE GREAT FOR SMALL GROUP.}<br>
                                    This family has 4 children ages 3(F), 8(M), 9(M), 11(M). The family is in need
                                    of laundry detergent, shampoo, conditioner and body wash. The family also needs
                                    help with groceries to lst through the end of the month. The children are in
                                    need of clothes including underwear, shoes and winter coats.</p>
                            </h6>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <ul class="cp_point">
                                    <li>{3yo: 2T, shoes: 5}</li>
                                    <li>{8yo: 8, shoes: 3}</li>
                                    <li>{9yo: 8, shoes: 3.5}</li>
                                    <li>{11yo: 12/14, shoes: 4.5}</li>
                                </ul>
                            </h6>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <span class="thnk">Thanks for helping.</span>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection