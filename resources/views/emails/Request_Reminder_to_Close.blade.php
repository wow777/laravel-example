@extends('emails.layout.layout')
@section('title', 'Request Reminder to Close')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
            <td class='reunify'>
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>Help Reunify a Bio Family</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Reminder to Close</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>{He Beverly,}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>You submitted request {#14079} {28} days ago
                                    and it is still open. Please do one of the following.</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;">
                                <ul class="col_point">
                                    <li>If the need has been met by a church or another way please click below to
                                        close.
                                    </li>
                                    <li>If time has passed and it's no longer a need please click below to close.</li>
                                    <li>If you still have the need, please delete this email.</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                        </tr>
                    </table>
                </div>
                <div class="text_btnss">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25' style="text-align: center; padding: 0 20px;"><h1>Need to Close This
                                    Request?</h1></td>
                        </tr>
                        <tr>
                            <td height='20'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">Close Request
                                    {#14079}</a></td>
                        </tr>
                    </table>
                </div>

                <div class="req_content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Request Details</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Careportal Request: <span class="req_des">{#14079}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Agency Case ID: <span class="req_des">{42804045}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / Country: <span class="req_des">{70410 / Nueces County, Texas}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Request Radius: <span class="req_des">{30 miles}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Grid Quardant: <span class="req_des">{Tier 1 - Physical Needs - prevention}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Request Urgency: <span>{Normal; Within 7-10 days}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Estimated need: <span class="req_des">{Primarily goods & services - sec detailed description of need.}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Estimated Value: <span
                                            class="req_des">{$80.00}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                            class="req_des">{2}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{Two parents household has two children that are now walking. The family is in need of one two very study baby gales. The older elder child is able to adjust the wooden baby gales and finding ways to take it down. parents and children would highly benefit from having one-two baby galse that mount to the wall and have the swing door, as long as the gale stands about waist high to an adult so the child can not reach it. This family lives in a two story home and so a baby gale at the top and bollen would be ideal.}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2><span class="req_des">{Updated on 01/10/2018 9:31am by Beverly Davis (Agency Worker) Request is being updated due to pending supervisor approval.}</span>
                                </h2></td>
                        </tr>
                    </table>
                </div>

                <div class="text_btn">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25' style="text-align: center; padding: 0 20px;"><h1>Need to Update This
                                    Request?</h1></td>
                        </tr>
                        <tr>
                            <td height='10'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center;"><a href="">Update Request {#14079}</a></td>
                        </tr>
                        <tr>
                            <td height='20'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center;"><p>See How This Works [Video]</p></td>
                        </tr>
                    </table>
                </div>
            </td>
    </table>

@endsection
