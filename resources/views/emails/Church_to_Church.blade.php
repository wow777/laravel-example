@extends('emails.layout.layout')
@section('title', 'Church to Church')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif; max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'
                                   class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h2 style="color: #626262">Let's Partner in Meeting This need</h2></td>
                    </tr>
                    <tr>
                        <td height='25'><h3><b>The message below in form:</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 class="church_link">{Dawns Poe} on behalf of {Hoover Valley Bapist
                                Church}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Please call or email to connect about this.</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 class="church_link" style="padding: 0 80px;">{dawnapoe0117@gmail.com}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25'><h3 class="church_link" style="padding: 0 80px;">{(210) 215-8637}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 20px;">{"Good Afternoon, I just spoke to Lawana with CPS
                                and she asked me to reach out to you to coordinate what all we will have as far as the
                                bunk
                                beds and dresser. I can pick up the bed and look at the dresser Sat morning if that is
                                an ok
                                time frame. If you think we should do it sooner please let me know and I'll get with our
                                member who has the stuff and see if I can come tonight. You can call, text or email me.
                                I
                                look forward to working with you on this and am so excited to see this system take off
                                and
                                help families. I hope you have a blesses day!! Dawna Poe"}</h3></td>
                    </tr>
                </table>
            </div>
            <div class="req_content">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Request Details</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Careportal Request: <span
                                        class="req_des">{#14776}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / Country: <span class="req_des">{78654 / Burnet County, Texas}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Purpose: <span class="req_des">{Help Prevent a Child From Entering Foster Care}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Grid Quardant: <span class="req_des">Grid Quardant: {Tier 1 - Physical Needs _ Prevention}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request Urgency:
                                <span>{Normal; Within 7-10 days}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Estimated need: <span class="req_des">{Primarily goods & services} - see detailed description of need</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Children Served: <span class="req_des">{3}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{A mother with a 5 year old child is in nedd of household items. This will be the first time the mother has lived on her own with her son, She has made a lot of positive changes in her life and needs all the help and support she can get. She has worked very hard to change things around for hreself and her son. he has absolutely nothing to start out with. She is in need of furniture, kitchen supplies and everyday household items. The items do not have to be brand new but could be things that are no longer needed. She is moving into her apartment on Tuesday 2/13/18. We do have a bed frame for her but will be needing a mattress and box spiring for a twin bed.}</span>
                            </h2></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
