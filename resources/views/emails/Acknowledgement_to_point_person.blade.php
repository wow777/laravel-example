@extends('emails.layout.layout')
@section('title', 'Church Enrollment')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Church Enrollment</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {{ $recipient->first_name }}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><strong>{{ $churchName }}</strong> was recently enrolled on the CarePortal platform and you were indicated as the Primary Point for your church. We are so excited fo your church to be involved and grateful for your leadership.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3></h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>You will be contacted soon to discuss the details of your church's orientation and steps to get your church active and serving more children in crisis. If you have any concerns please don't hesitate to reply to this email.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3></h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>CarePortal team</h3>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
