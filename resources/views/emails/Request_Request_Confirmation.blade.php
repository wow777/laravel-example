@extends('emails.layout.layout')
@section('title', 'Request Request Confirmation')

@section('body')

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        <tbody>
        {{--Header Logos--}}
        @include('emails.layout.header')
        <tr>
            <td class="reunify">
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td valign="middle" colspan="3"><p>Help Reunify a Bio Family</p>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign="top"
                                       class="MainContainer">

                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height="25"><h1>Portal Request Confirmation</h1></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>{Hi Natasha,}</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>The email is reference to CarePortal Request
                                    {#14715}.</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>This email is your confirmation that your
                                    request
                                    was sent to {6} churches within a {30} mile radius of this need.</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>Thank you for using CarePortal to change the
                                    church
                                    to care for children and families in need.</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="req_content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height="25"><h1>Request Details</h1></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Careportal Request: <span
                                            class="req_des">{#14715}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Agency Case ID: <span
                                            class="req_des">{17752384}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Case Zip Code / Country: <span class="req_des">{31021 / Laurens County, Georgia}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Request Radius: <span
                                            class="req_des">{30 miles}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Grid Quadrant: <span class="req_des">{Tier 1 - Physical Needs - Prevention}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Request Urgency: <span class="req_des">{Critical; Needed Within 24 hrs}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Estimated need: <span class="req_des">{Primarily goods &amp; services} - see detailed description of need</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Estimated Value: <span
                                            class="req_des">{$600.00}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Children Served: <span
                                            class="req_des">{6}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h2>Description: <span class="req_des">{Request description goes here}</span>
                                </h2></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text_btn text_btn1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height="25" style="text-align: center; padding: 0 20px;"><h1>Need to Update This
                                    Request?</h1></td>
                        </tr>
                        <tr>
                            <td height="10">
                            </td>
                        </tr>
                        <tr>
                            <td height="25" style="text-align: center;"><a href="">Update Request {#14715}</a></td>
                        </tr>
                        <tr>
                            <td height="20">
                            </td>
                        </tr>
                        <tr>
                            <td height="25" style="text-align: center;"><p>See How This Works [53 Second Video]</p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text_btns text_btn1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height="25" style="text-align: center; padding: 0 20px;"><h1>Need to Close This
                                    Request?</h1></td>
                        </tr>
                        <tr>
                            <td height="20">
                            </td>
                        </tr>
                        <tr>
                            <td height="25" style="text-align: center;padding-bottom: 20px;"><a href="">Close Request
                                    {#14715}</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </td>
        </tr>
        </tbody>
    </table>
@endsection