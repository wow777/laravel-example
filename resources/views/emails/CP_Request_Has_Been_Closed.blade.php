@extends('emails.layout.layout')
@section('title', 'CP Request Has Been Closed')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <tr>
            <td class='reunify'>
                <div class="reunifyContent re-closed">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>This Request Has Been Closed</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-------Reason Closed------>
                <div class="content_data" style="text-align:center;margin-bottom: 18px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h3 style="padding: 0 80px;"><strong>Reason Closed:</strong> {Church
                                    Responded and Met the Need}</h3></td>
                        </tr>
                    </table>
                </div>
                <!-------req-closed------>
                <div class="reunifyContent re-closed1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>Request {#12314} - {Normal; Within 7-10 days}</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-------------------->
                <div class="content_data" style="text-align:center;margin-bottom:0px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h2>{Help Reunify a Bio Family}</h2></td>
                        </tr>
                        <tr>
                            <td height='25'><h3>{Barton County, Kansas (67530)}</h3></td>
                        </tr>
                        <tr>
                            <td height='25'><h3>Children Served: {2}</h3></td>
                        </tr>
                        <tr>
                            <td height='25'><h3 style="padding: 0 80px;">Estimated Need: {Primarly goods & services} -
                                    see detailed description of need</h3></td>
                        </tr>
                        <tr>
                            <td height='25'><h3 style="padding: 0 80px;"><b>Items Needed</b></h3></td>
                        </tr>
                        <tr>
                            <td height='25'><h3 style="padding: 0 80px;">{Item 1, Item 2, Item 3}</h3></td>
                        </tr>
                    </table>
                </div>
                <div class="movabledes movabledes-req-closed">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Description of the Need</h1></td>
                        </tr>
                        <tr>
                            <td style="padding: 0px 20px;">
                                <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                    <p>{Items Requested: 3 new/used beds for single mom and 2 children.</br><br>
                                        A family of three is in need of assistance with beds. A single mom recently
                                        moved to a new home, to help her family have a fresh start. Sadly, when the
                                        family was in between homes, they did not have storage, and they were unable to
                                        find a place to store their beds and lost them.
                                        <br>
                                        The family's agency worker reports that mom is able to meet the families needs
                                        with their limited budget, and that mom has been completing everything asked of
                                        her. Currently, this mom is having overright visits four nights a week. The
                                        children and mom are in need of beds, to ensure that the children are well
                                        rested for school. Mom does have a disability and providing her with a bed would
                                        help ease stress in her body, allowing her to spend energy and focus on the
                                        children. The agency worker reports that this family is strongly bondes.
                                        <br>
                                        Any assistance with new/used beds would be a blessing to his mom and her two
                                        children.
                                        <br><br>
                                        Thank you in advance for your consideration!}
                                    </p>
                                </h6>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
@endsection
