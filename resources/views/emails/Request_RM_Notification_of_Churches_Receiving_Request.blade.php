@extends('emails.layout.layout')
@section('title', 'Request Request Confirmation')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data btn-styling" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Regional Manager's Report - Request #{16025}</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>The email contains a report of all churches who
                                have received Careportal request #{16025}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><strong>NOTE:</strong> The full list of
                                churches could take from several minutes to a few
                                hours to populate in the Regional Manager interface.
                            </h3></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;padding-top: 15px;" height="55"><a href="">View Churches on
                                CarePortal.org</a></td>
                    </tr>
                </table>
            </div>
            <div class="req_content rm-req">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Request Details</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request ID: <span
                                        class="req_des">{#16025}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / County: <span class="req_des">{63736/Scott County, Missouri}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request Radius: <span class="req_des">{30} Miles from Zip Code</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Grid Quadrant: <span class="req_des">{Tier 1 - Physical Needs-Prevention}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request Urgency: <span class="req_des">{High; Needed Within 72 hrs}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Estimated Need: <span class="req_des">{Primarily goods & services - see detailed description of need}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                        class="req_des">{1}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{We are working with a special needs family. This family has a new born daughter who was premature. The baby is still
			hospitalized and is foreseeing a discharge date early next week. the family does not an infant carseat with a base. We are requesting our communities
			support in meeting this need for this family.}</span></h2></td>
                    </tr>
                </table>
            </div>
            <div class="text_btn text_btn1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Need to Close This
                                Request?</h1></td>
                    </tr>
                    <tr>
                        <td height='10'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Close Request {#16025}</a></td>
                    </tr>
                    <tr>
                        <td height='20'>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="text_btns text_btn1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Request date: {April 06.
                                2018 12:37pm}</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h5>{20} Records Found within
                                {30} Miles</h5></td>
                    </tr>
                    <tr>
                        <td height='20'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h3>Churches</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church Benton}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Benton - Scott County,
                                Missouri, 63736 (MO-6=SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{First Bapist Scott City}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{SCOTT CITY - Scott County,
                                Missouri, 63780 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Eisleben Luthern Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Scott City - Scott County,
                                Missouri, 63780 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Trinity Bapist Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Sikeston - Scott County,
                                Missouri, 63801 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Miner Bapist Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Sikeston - Scott County,
                                Missouri, 63801 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Fellowship Bapist Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Sikeston - Scott County,
                                Missouri, 63801 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{First United Methodist Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Sikeston - Scott County,
                                Missouri, 63801 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Anchor Church Sikeston}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Sikeston - Scott County,
                                Missouri, 63801 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Community Bapist Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{SIkeston - Scott County,
                                Missouri, 63801 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Cape Community Church of God}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Cape Giradeau - Cape Giradeau
                                County, Missouri, 63703 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Charleston United Methodist Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Charleston - Mississippi
                                County, Missouri, 63834 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Trinity Luthern Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE GIRARDEAU - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE GIRARDEAU - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='20'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h3>Small Groups</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church@Connie Lape}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE Girardeau - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church@Matt U small group}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE Girardeau - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church@Brandi Knight}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE Girardeau - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church@Chantelle Becking}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE GIRARDEAU - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church@Amanda Flinn}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE Girardeau - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{La Croix Church@Dana Glass}</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{CAPE GIRARDEAU - Cape
                                Girardeau County, Missouri, 63701 (MO-6-SE)}</h4></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
