@extends('emails.layout.layout')
@section('title', 'Request Church is ready to help')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
            <td class='reunify'>
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>Help Reunify a Bio Family</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>The Church is Ready to help</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>{Hi Melisa,}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>{April Hill} from {Union Valley Baptist Church}
                                    has responded to CarePortal Request {#14679}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>{April Hill} said {"Union Valley Baptist
                                    church} would like to meet this need"</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>We have informed {April Hill} via email you
                                    will be reaching out ASAP.</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                        </tr>
                    </table>
                </div>
                <div class="col_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr style="padding: 0 20px;">
                            <td height='25'
                                style="background: #f56600;border-radius: 4px; padding: 17px 26px;text-align: center;">
                                <a class="mid_btn" href="">Action Step: Contact {April Hill}</a></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4>{April Hill's} Email Address & Phone
                                    Number</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4><a style="color: #f56600;"
                                                                            href="mailto: april.hill1521@gmail.com">{april.hill1521@gmail.com}</a>
                                </h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4><a style="color: #f56600;"
                                                                            href="tel: (580) 281-0387">{(580)
                                        281-0387}</a></h4></td>
                        </tr>
                    </table>
                </div>
                <div class="req_content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Request Details</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Careportal Request: <span class="req_des">{#14679}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / Country: <span class="req_des">{73533 / Stephens County, Oklahoma}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Purpose: <span class="req_des">{Help Reunify a Bio Family}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Grid Quardant: <span class="req_des">{Tier 1 - Physical Needs - Foster Care}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Request Urgency: <span>{Tier 1 - Physical Needs - Foster Care}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Estimated need: <span class="req_des">{Primarily goods & services - see detailed description of need}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                            class="req_des">{1}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{A mother with a 5 year old child is in nedd of household items. This will be the first time the mother has lived on her own with her son, She has made a lot of positive changes in her life and needs all the help and support she can get. She has worked very hard to change things around for hreself and her son. he has absolutely nothing to start out with. She is in need of furniture, kitchen supplies and everyday household items. The items do not have to be brand new but could be things that are no longer needed. She is moving into her apartment on Tuesday 2/13/18. We do have a bed frame for her but will be needing a mattress and box spiring for a twin bed.}</span>
                                </h2></td>
                        </tr>
                    </table>
                </div>
                <div class="close_btn">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25' style="text-align: center; padding: 0 20px;"><h1>Need to Close This
                                    Request?</h1></td>
                        </tr>
                        <tr>
                            <td height='40'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">Close Request
                                    #14679</a></td>
                        </tr>
                    </table>
                </div>
            </td>
    </table>
@endsection