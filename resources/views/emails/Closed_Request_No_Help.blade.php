@extends('emails.layout.layout')
@section('title', 'Closed Request No Help')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent cls_church">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>{Outreach Christian Church} Has Met This Need!</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="reunifyContent req_church">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Request {#14512} - {Normal Within 7-10 days}</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>{Help Reunify a Bio Family}</h1></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>{Clay County, Missouri (64068)}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>{Urgency; Normal; Within 7-10 days}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Children Served: {2}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">Estimated Need: {Primarily goods & services} -
                                see detailed description of need</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;"><b>Items Needed</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">{Item 1, item 2, item 3}</h3></td>
                    </tr>
                </table>
            </div>
            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Description of the Need</h1></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p>{The Children's Division is currently working with a father in hopes to helo him
                                    reunify with his children. he has just acquired low income housing and he has
                                    employement now. He is visiting with his daughter and step daughter in hopes to
                                    have them both placed in his home. He is in need of a table and chairs for his
                                    home. This will help with home visits and the family can sit down for dinners
                                    etc. He is currently on a limited income and any help would be greatly
                                    appreciate. Thank you!}</p>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection