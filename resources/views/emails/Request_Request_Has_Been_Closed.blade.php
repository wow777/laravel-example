@extends('emails.layout.layout')
@section('title', 'Request Request Has Been Closed')

@section('body')

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <tr>
            <td class='reunify'>
                <div class="reunifyContent re-closed">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top'
                                       class="MainContainer">
                                    <p>This Request Has Been Closed</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-------Reason Closed------>
                <div class="content_data" style="text-align:center;margin-bottom: 18px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h3 style="padding: 0 80px;"><strong>Reason Closed:</strong> Church
                                    Responded
                                    and Met the Need</h3></td>
                        </tr>
                    </table>
                </div>
                <!-------req-closed------>
                <div class="reunifyContent re-closed1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top'
                                       class="MainContainer">
                                    <p>Request {#14077} - {Normal}, {Within 7-10 days}</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-------------------->
                <div class="content_data" style="text-align:center;margin-bottom:0px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h2>{Help Strengthen a Bio Family}</h2></td>
                        </tr>
                        <tr>
                            <td height='25'><h3>{Tarrant County, Texas (76179)}</h3></td>
                        </tr>
                        <tr>
                            <td height='25'><h3>Children Served: {2}</h3></td>
                        </tr>
                        <tr>
                            <td height='25'><h3 style="padding: 0 80px;">Estimated Need: {Primarly goods & services} -
                                    see
                                    detailed description of need</h3></td>
                        </tr>
                    </table>
                </div>
                <div class="movabledes movabledes-req-closed">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Description of the Need</h1></td>
                        </tr>
                        <tr>
                            <td style="padding: 0px 20px;">
                                <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                    <p>{A Loving hardworking mother has found her way! Through perseverance and hard
                                        work a
                                        mother is able to reconnect with her two energetic sweet and loving boys.
                                        They are full of energy understandably being 8 years and 6 months old. This
                                        mother
                                        is working two jobs, attending classes and some how always has the energy to
                                        chase
                                        her boys around
                                        the house with a smile. She has recently been able to secure her first apartment
                                        in
                                        her name and now the boys have a place to call home.</br>
                                        I am requesting help to get her oldest boy his own bed. He went from being an
                                        only
                                        child to a wonderful big brother who is always willing to help
                                        out anyway he can.</br>
                                        The family is in need of a twin bed, mattress, bedding and a pillow. This
                                        precious
                                        boy is currently sleeping in a sleeping bag on the floor.</br>
                                        If at all possible, the family is also in need of clothing/shoes. The 8 year old
                                        could greatly benefit from size 8/9 warm clothes. He wears a size 4 in Big
                                        kid shoes. The six month old is in need of 9 month warm clothing. This family
                                        would
                                        be grateful from any assistance that they receive.}
                                    </p>
                                </h6>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

    </table>

@endsection
