@extends('emails.layout.layout')
@section('title', 'CP Request Tier 1')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Request #12537 - Tier 1- Physical Needs - Prevention</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h2>Help Strengthen a Bio Family</h2></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Maricopa Country, Arizona (85207)</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Urgency: Normal; Within 7-10 days</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Children Served: 5</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">Estimated Need: Primarly goods & services - see
                                detailed description of need</h3></td>
                    </tr>
                </table>
            </div>
            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Description of the Need</h1></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p>This family consists of a single mother and her 5 children. Mother is very
                                    attentive to her children and has worked hard to provide what she can do for
                                    them.She has not been able to recently work due to a new diagnosis of a
                                    medical condition of one of the children.<br/>
                                    Children: Twin boys age 3, female age 5, twin boys age 7<br/>
                                    Needs: 2 twin beds with bedding, help with painting walls in 2 bedrooms,<br/>
                                    ceiling fan replaced(fan is missing blades and light is broken).<br/>
                                    <br/>
                                    Thank You!<br/>
                                    Pam - DCS

                                </p>

                                <h3 class="aut_name">Child Welfare Woker: <span class="main_name">Pam Harris</span>
                                </h3>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="main_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Respond to Request</a></td>
                    </tr>
                </table>
            </div>
            <div class="text_btn">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Engage Your Active
                                Community</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p><b>Does your church need assistance meeting this
                                    need?</b> Simply click the button below to send a message to the churches in
                                your active community. </p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Send a Message</a></td>
                    </tr>
                </table>
            </div>
            <div class="text_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Help another church meet
                                this need</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p><b><span class="new">NEW</span> See a list of
                                    churches that are closer to this need and collaborate with them to meet it.</b>
                            </p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">See Churches</a>
                        </td>
                    </tr>
                </table>
            </div>
    </table>
@endsection
