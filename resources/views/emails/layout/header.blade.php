<tr>
    <td class="movableContentContainer">
        <div class="movableContent">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                <tbody>
                <tr>
                    <td valign="top" colspan="3">
                        <table style="padding: 0 20px;" width="600" border="0" cellspacing="0" cellpadding="0"
                               align="center" valign="top" class="MainContainer">
                            <tbody>
                            <tr>
                                <td align="left" valign="middle" width="200">
                                    <div class="contentEditableContainer contentImageEditable">
                                        <div class="contentEditable"><a href="#"><img class="logo"
                                                                                      src="{{asset('img/logo.png')}}"
                                                                                      alt="logo"
                                                                                      data-default="placeholder"
                                                                                      data-max-width="300"></a>


                                        </div>
                                    </div>
                                </td>
                                <td align="right" valign="bottom" width="200">
                                    <div class="contentEditableContainer contentImageEditable">
                                        <div class="contentEditable"><a href="#"><img
                                                        src="{{asset('img/care_portal.png')}}"
                                                        alt="portal"
                                                        data-default="placeholder"
                                                        data-max-width="300"
                                                        width="103" height="32"
                                                        class="responsive-logo"></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </td>
</tr>