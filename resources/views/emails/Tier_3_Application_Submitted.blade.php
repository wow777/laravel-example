@extends('emails.layout.layout')
@section('title', 'Tier 3 Application Submitted')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Tier 3 Application Submitted to Agency</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {Derek!}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>This email confirms that you have submitted
                                your Tier 3 Application to {TEST ALASKA}. If you have questions about your
                                background check, you may contact the agency using the information listed
                                below.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>{TEST ALASKA}</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Scott Platter}</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{scott@gorproject.org}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>If you have any questions, don't hesitate to
                                reply to this email and send me a message.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>I'm here to help!</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Derek Nelson</h3></td>
                    </tr>
                </table>
            </div>

        </td>
    </table>
@endsection
