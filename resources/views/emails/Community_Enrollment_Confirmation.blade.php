@extends('emails.layout.layout')
@section('title', 'Community Enrollment Confirmation')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif; max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <tr>
            <td class='reunify'>
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top' class="MainContainer">
                                    <p>Community Enrollment Confirmation</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content_data" style="border: 0px; padding-top: 30px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25' style="padding: 0 20px;" ><h3><b>Hi {{ $recipient->first_name }},</b></h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;" >
                                <h3>Thank you for joining a CarePortal community! We're excited and appreciate your willingness to help meet tangible needs of children and families in crisis.</h3>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;" ><h3><b>Your Information:</b></h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;" ><h3>Name: {{ $recipient->first_name." ".$recipient->last_name }}</h3></td>
                        </tr>
                        @if ($recipient->organization)
                            <tr>
                                <td height='5' style="padding: 0 20px;" ><h3>Organization: {{ $recipient->organization }}</h3></td>
                            </tr>
                        @endif
                        <tr>
                            <td height='25' style="padding: 0 20px;" ><h3>Email: {{ $recipient->email }}</h3></td>
                        </tr>
                        @if ($recipient->phone)
                            <tr>
                                <td height='5' style="padding: 0 20px;" ><h3>Phone: {{ $recipient->phone }}</h3></td>
                            </tr>
                        @endif
                        <tr>
                            <td height='25' style="padding: 0 20px;" ><h3><b>Your selected communities:</b><h3></td>
                        </tr>
                    </table>
                </div>
                @foreach($communities as $community)
                    <div class="text_btn"  style="padding-bottom: 20px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                            <tbody>
                            <tr>
                                <td height='25' style="text-align: center; padding: 0 20px;" >
                                    @if(!empty($community->counties))
                                        <h1>{{ $community->name.", ".$community->counties[0]->state->abbreviation }}</h1>
                                    @else
                                        <h1>{{ $community->name }}</h1>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td height='25' style="padding: 0 20px;" >
                                    <p class="con_down">{{ $community->coordinator->first_name." ".$community->coordinator->last_name }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td height='25' style="padding: 0 20px;" >
                                    <p class="con_down" style="color: #16bd6d;">{{ $community->coordinator->email }}</p>
                                </td>
                            </tr>
                            @if($community->coordinator->phone)
                                <tr>
                                    <td height='25' style="padding: 0 20px;" >
                                        <p class="con_down" style="color: #16bd6d;">{{ $community->coordinator->phone }}</p>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            </td>
        </tr>

    </table>
@endsection
