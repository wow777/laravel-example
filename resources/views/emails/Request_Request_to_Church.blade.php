@extends('emails.layout.layout')
@section('title', 'Request Request to Church')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top'
                                   class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height=""></td>
                    </tr>
                    <tr>
                        <td height="259" class="bgItem">
                            <table align="center" width="600" border="0" cellspacing="0" cellpadding="0"
                                   class="MainContainer">
                                <tbody>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="310" valign="top" class="specbundle4">
                                                    <table width="310" border="0" cellspacing="0" cellpadding="0"
                                                           align="center" valign="top">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="3" height="10"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10"></td>
                                                            <td width="270" valign="top">
                                                                <table width="270" border="0" cellspacing="0"
                                                                       cellpadding="0" align="center" valign="top">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable">
                                                                                    <h2>Request #{41788}</h2>
                                                                                    <h2>{Tier 1 | Physical
                                                                                        Needs}</h2>
                                                                                    <p>{Foster care}</p>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable">
                                                                                    <h2>Urgency: {High}</h2>
                                                                                    <p>{Needed within 72 hrs}</p>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable">
                                                                                    <h2>{Cleveland Country, OK
                                                                                        (73051)}</h2>
                                                                                    <h2>Children served: {2}</h2>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="10"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="310" valign="top" class="specbundle4">
                                                    <table width="310" border="0" cellspacing="0" cellpadding="0"
                                                           align="center" valign="top">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="3" height="10"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10"></td>
                                                            <td width="270" valign="top">
                                                                <table width="270" border="0" cellspacing="0"
                                                                       cellpadding="0" align="center" valign="top">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable glob_need">
                                                                                    <h2>Needed</h2>
                                                                                    <p>{Money ($200) or temporary or
                                                                                        permanent placement for 4
                                                                                        dogs &
                                                                                        2 puppies}</p>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="10"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="movabledes req-church">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Description of the Need</h1></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px 12px; border-bottom: solid 1px #b3b3b3;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p>{Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.
                                    It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged. It was popularised in the 1960s
                                    with
                                    the release of Letraset sheets containing Lorem Ipsum passages, and more
                                    recently
                                    with desktop publishing software like Aldus PageMaker including versions of
                                    Lorem
                                    Ipsum.}</p>
                                <h3 class="aut_name">Child Welfare Woker: <span
                                            class="main_name">{Jordan Baker}</span>
                                </h3>
                                <h3 class="aut_name">Submitting Agency: <span class="main_name">{OK-DHS}</span></h3>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height=""></td>
                    </tr>
                    <tr>
                        <td height="259" class="btnItem">
                            <table align="center" width="600" border="0" cellspacing="0" cellpadding="0"
                                   class="MainContainer">
                                <tbody>
                                <tr>
                                    <td height="40"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="310" valign="top" class="specbundle4">
                                                    <table width="310" border="0" cellspacing="0" cellpadding="0"
                                                           align="center" valign="top">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="3" height="10"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10"></td>
                                                            <td width="270" valign="top">
                                                                <table width="270" border="0" cellspacing="0"
                                                                       cellpadding="0" align="center" valign="top">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="content_btn">
                                                                                    <a href="#">Activate My
                                                                                        Church</a>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="content_btn">
                                                                                    <a href="#">Activate
                                                                                        Community</a>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="content_btn">
                                                                                    <a href="#">Church to Church</a>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="10"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="310" valign="top" class="specbundle4">
                                                    <table width="310" border="0" cellspacing="0" cellpadding="0"
                                                           align="center" valign="top">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="3" height="10"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10"></td>
                                                            <td width="270" valign="top">
                                                                <table width="270" border="0" cellspacing="0"
                                                                       cellpadding="0" align="center" valign="top">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="content_btns">
                                                                                    <a href="#">Respond </a>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="content_btns">
                                                                                    <a href="#">Commit To Pray</a>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="content_btns">
                                                                                    <a href="#">Flag</a>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="10"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
    </table>
@endsection
