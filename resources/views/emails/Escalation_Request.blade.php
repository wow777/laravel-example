@extends('emails.layout.layout')
@section('title', 'Escalation Request')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Request #{14788 - Tier 1- Physical Needs - Foster Care}</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h2>{Help Reunify a Bio Family}</h2></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>{Cleveland Country, Oklahoma (73051)}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Urgency: {High; Needed Within 72 hrs}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Children Served:{2}</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">Estimated Need: {Primarly goods & services} -
                                see detailed description of need</h3></td>
                    </tr>
                </table>
            </div>
            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Description of the Need</h1></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p>{I am working with a biological father who is wishing to go
                                    into rehab/treatment. His wife is currently already in treatement, which
                                    has left him at their home and with 4 grown dogs and 2 newborn puppies.
                                    The dad has had his bags ready to go and paked since
                                    Monday; however, he is having no luck finding someone to take the
                                    family dogs. He is willing to surrender them; however animal control will
                                    charge him about $60 per dog to surrender them. He has contacted the
                                    OK Human Society and has not heard back. The Bella Foundation will
                                    allow him to surrender them ; however they cannot take them until they
                                    have located a place for them to go. The dad does not have reliable
                                    transportation and has a limit support system, making this difficult for him
                                    as well. I called Second Chance and they do not take animals from the public.
                                    Inpatient treatement is recommended for this parent to be successful, but he
                                    cannot leave his pets uncared for and desire to re-home them or surrender them
                                    responsibility.
                                    We are requesting assistance to meet the financial cost of surrendering the pets
                                    to the animal shelter, or help finding temporary or permanency homes for the
                                    dogs. The father is willing to accept any help he can find at this pont so he
                                    can go to treatment and also ensure the animal are reponsibly taken care of.}
                                    <br/>
                                    <br/>
                                </p>

                                <h3 class="aut_name">Child Welfare Woker: <span
                                            class="main_name">{Jordan Baker}</span></h3>
                                <h3 class="aut_name">Submitting Agency: <span class="main_name">{OK-DHS}</span></h3>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="main_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Respond to Request</a></td>
                    </tr>
                </table>
            </div>
            <div class="text_btn">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Engage Your Active
                                Community</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p><b>Does your church need assistance meeting this
                                    need?</b> Simply click the button below to send a message to the churches in
                                your active community. </p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Send a Message</a></td>
                    </tr>
                </table>
            </div>
            <div class="text_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Help another church meet
                                this need</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p><b><span class="new">NEW</span> See a list of
                                    churches that are closer to this need and collaborate with them to meet it.</b>
                            </p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">See Churches</a>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
