@extends('emails.layout.layout')
@section('title', 'Church Enrollment – Final Step')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Church Enrollment &mdash; Final Step</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {{ $recipient->first_name }}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;">
                            <h3>Thank you for completing your orientation. You're one-step away from <strong>{{ $church->name }}</strong> becoming active on the CarePortal platform. Please complete your Church Action Plan. Once complete, your church will be ready to serve children in your community through the CarePortal platform.</h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3></h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;">
                            <h3>If you have any additional questions please contact {{ $regionalManager->first_name." ".$regionalManager->last_name }} at <a href="mailto:{{ $regionalManager->email }}" style="color: #f56600">{{ $regionalManager->email }}</a> or {{ $regionalManager->phone }}.
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3></h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>We are excited to help {{ $church->name }} serve more children in crisis.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3></h3>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="main_btns" style="text-align: center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr style="padding: 0 20px;">
                        <td height='25' style="padding-bottom: 20px;"><a href="{{ action('ChurchEnrollmentController@activate', ['church' => $church->id]) }}">Complete Action Plan</a></td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>CarePortal team</h3>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
