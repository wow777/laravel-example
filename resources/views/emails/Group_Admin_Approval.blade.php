@extends('emails.layout.layout')
@section('title', 'Group Admin Approval')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p style="border:none;">Hi {Jerimiah},<br>
                                    Great news!!!<br>
                                    {Sharon Gritz} has enrolled their small group ({Univercity Baptist Church@Adult}
                                    6) in the CarePortal Network as a part of {Univercity Baptist Church.}
                                    <br>
                                    This is a fantastic opportunity for your church body to be the hands and feet of
                                    Christ in your local community.
                                    <br>
                                    If this small group is associated with {Univercity Baptist Church,} and you
                                    approve of their enrollment in CarePortal, please let us know by clicking the
                                    button below and completing a one (1) question form.


                                </p>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="main_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Approval small Group Enrollment</a>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p style="border:none;">P.S If {Sharon Gritz's} small group is not associated with
                                    your church, disregard this email and the enrolled small group will not be
                                    allowed to continue the enrollment process.


                                </p>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="spread_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Spread the World!</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p>Tell the small group leaders at {University
                                Baptist Church} how they can enroll their group, just like {University Baptist
                                Church@Adult 6} did on the CarePortal Network. Click the button below to access your
                                church's small group enrollement page.</p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">Group Enrollment
                                Page</a></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
