@extends('emails.layout.layout')
@section('title', 'Tier 2 Background Check Approved')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>New Tier 2 Background Check Approved</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {Derek!}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>We are excited to inform you that Tier 2
                                background check has been approved!</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you again for your heart for the least of
                                these.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Derek Nelson</h3></td>
                    </tr>
                </table>
            </div>
            <div class="main_btns" style="text-align: center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr style="padding: 0 20px;">
                        <td height='25' style="padding-bottom: 20px;"><a href="">View Open Tier 2 Requests in My
                                County
                            </a></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>

@endsection
