@extends('emails.layout.layout')
@section('title', 'Escalation Community Digest')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data btn-styling" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Community Digest - Request #{14788}</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>This email contains a report of churches who
                                have recieved CarePortal request {#14788} for the {Central OKC} Active
                                Community.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><strong>NOTE:</strong>The full list of churched
                                could take from several minutes to a few hours to populate in your interface.
                            </h3></td>
                    </tr>
                </table>
            </div>
            <div class="req_content rm-req">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Request Details</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request ID: <span
                                        class="req_des">{#14788}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / County: <span class="req_des">{73051 / Cleveland County, Oklahoma}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Purpose: <span class="req_des">{Help Reunify a Bio Family}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Grid Quadrant: <span class="req_des">{Tier 1 - Physical Needs - Foster Care</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request Urgency: <span class="req_des">{High; Needed Within 72 hrs}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Estimated Need: <span class="req_des">{Primarily goods & services - see detailed description of need}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                        class="req_des">{2}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>

                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{I am working with a biological father who is wishing to go
                                into rehab/treatment. His wife is currently already in treatement, which
                                has left him at their home and with 4 grown dogs and 2 newborn puppies.
                                The dad has had his bags ready to go and paked since
                                Monday; however, he is having no luck finding someone to take the
                                family dogs. He is willing to surrender them; however animal control will
                                charge him about $60 per dog to surrender them. He has contacted the
                                OK Human Society and has not heard back. The Bella Foundation will
                                allow him to surrender them ; however they cannot take them until they
                                have located a place for them to go. The dad does not have reliable
                                transportation and has a limit support system, making this difficult for him
                                as well. I called Second Chance and they do not take animals from the public.
                                Inpatient treatement is recommended for this parent to be successful, but he
                                cannot leave his pets uncared for and desire to re-home them or surrender them responsibility.
                                We are requesting assistance to meet the financial cost of surrendering the pets to the animal shelter, or help finding temporary or permanency homes for the dogs. The father is willing to accept any help he can find at this pont so he can go to treatment and also ensure the animal are reponsibly taken care of }</span>
                            </h2></td>
                    </tr>
                </table>
            </div>
            <div class="text_btns text_btn1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Request Date: {02/16/2018
                                4:02pm}</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h5>{1} Records Found within
                                {30} Miles of {73051}</h5></td>
                    </tr>
                    <tr>
                        <td height='20'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>{Life.church SOK@Nerds}, Pastors and
                                Engineers</h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{Midwest City - Oklahoma
                                country, Oklahoma, 73110 (OK-3 OKC)}</h4></td>
                    </tr>

                </table>
            </div>
            <div class="close_btn">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">Engage This
                                Active Community</a></td>
                    </tr>
                </table>
            </div>
            <div class="engage_btn">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">View All
                                Churches for this Request</a></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
