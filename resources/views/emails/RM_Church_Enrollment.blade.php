@extends('emails.layout.layout')
@section('title', 'New Church Orientation Request')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>New Church Orientation Request</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {{ $recipient->first_name." ".$recipient->last_name }}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{{ $pastor->first_name." ".$pastor->last_name }} from {{ $church->name }} has indicated they need to schedule a carePortal orientation session with you.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Contact info for: {{ $pastor->first_name." ".$pastor->last_name }}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Phone: {{ $pastor->phone }}</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Church Info:</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>{{ $church->name }}</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;">
                            <h3>
                                {{ $church->address_line1 }}
                                @if($church->address_line2)
                                    <br />{{ $church->address_line2 }}
                                @endif
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>{{ $church->city.", ".$church->state->abbreviation." ".$church->zipCode->code }}</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    {{--
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>==How they would like to meet==</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>I can meet either in person or via webinar</h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    --}}
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3><b>NOTE:</b> After meeting with them, you will
                                have to <a href="{{ action('ChurchEnrollmentController@orientation', ['church' => $church->id]) }}" target="_blank" style="font-weight: bold;">click here</a> to complete the enrollment from for them so they receive the
                                email tp begin the next step.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Have a great day!</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Central Office</h3>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
