@extends('emails.layout.layout')
@section('title', 'Tier 3 Background Check Status')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Background Check Status</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {Derek!}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you so much for your willingness to serve
                                the least of these in our community.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Unfortunalety, your background check was not
                                approved by {TEST ALASKA}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>If you have questions about the result of your
                                background check, you may contact the agency (listed below) that conducted the
                                background check for your home.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>{TEST ALASKA}</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Scott Platter}</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{scott@gorproject.org}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Derek Nelson</h3></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>

@endsection
