@extends('emails.layout.layout')
@section('title', 'Tier 3 Home Walk Through Request')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

            <td class='reunify'>
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>Help Reunify a Bio Family</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Tier 3 Home Walk-Through Request</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Hi {Scott Platter!}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Please schedule a home walk-through for the
                                    following applicant:</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3><b>{Test Test}</b></h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>{derekrichardnelson+test1@gmail.com}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Mobile: {(111) 111-1111}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Home: {test}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Work: {test}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>A background check has been <b>APPROVED</b> for
                                    this applicant by {Scott Platter (TEST ALASKA)}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>If you have any questions or concerns about
                                    this application, simply reply to this email and send me a message.</h3></td>
                        </tr>
                        <tr>
                            <td height='15'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                        </tr>
                        <tr>
                            <td height='5' style="padding: 0 20px;"><h3>Derek Nelson</h3></td>
                        </tr>
                    </table>
                </div>
                <div class="main_btns" style="text-align: center;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr style="padding: 0 20px;">
                            <td height='25' style="padding-bottom: 20px;"><a href="">Access Tier 3 Application</a></td>
                        </tr>
                    </table>
                </div>
            </td>
    </table>
@endsection

