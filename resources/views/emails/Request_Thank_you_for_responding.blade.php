@extends('emails.layout.layout')
@section('title', 'Thank you for helping')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Thank you for Responding!</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {Howard Boyd},</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you for responding to CarePortal Request
                                {#14730} on behalf of {Branson Hills Assembly} with his message. {"Have access to
                                excellent clothing in a thift shop and can provide for all of these children."}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>We have emailed {Emily Hurst}, the social
                                worker handling this request, to let them know you have request responded to this
                                request.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>If you have not heard from the worker in a
                                timely manner, you can reach them using the contact information below.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Emily Hurst's} Contact Info</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Email: <span class="team_sign">{emily.hurst@dss.mo.gov}</span>
                            </h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Phone: <span
                                        class="team_sign">{(870) 716-2012}</span></h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you for using the CarePortal!}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                    </tr>
                </table>
            </div>
            <div class="req_content">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Closed Request Details</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Careportal Request: <span
                                        class="req_des">{#14730}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / Country: <span class="req_des">{65616 / Taney County, Missouri}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Grid Quardant: <span class="req_des">{Tier 1 - Physical Needs}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Request Urgency:
                                <span>{Normal; Within 7-10 days}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Estimated need: <span class="req_des">{Primarily goods & services} - see detailed description of need</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                        class="req_des">{5}</span></h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height="2"></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{Children were living with their natural parents but became homeless in Oklahoma and theie grandparents came and picked them up. The children have been residing with their grandparents for several months and it is planned for them to continue to stay with them for an extented period of the time. The grandparents are not getting any finacial support from the children's parents at this time. The grandfather is a disabled veteran and the grandmother is disabled and not working. The children are desperately in need of clothing at this time and the family has called asking for any support possible. The children's sizes are as follows}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2><span class="req_des">{Men's size Large}</span>
                            </h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2><span
                                        class="req_des">{2 Girls sized 1-16}</span></h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2><span
                                        class="req_des">{1 Girls sized 10-12}</span></h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2><span
                                        class="req_des">{1 Boys sized 10-12}</span></h2></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h2><span class="req_des">{There is also a newborn who has just come to live at the residence and his mother reports that she has only three outfits for him. Therefore, any newborn or 3-4 months boys clothes would be very helpful if possible.}</span>
                            </h2></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>

@endsection
