@extends('emails.layout.layout')
@section('title', 'New Request Notification')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>New Request Notification</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you for working with CarePortal. {Bryan
                                Allen} has submitted Case {#188029} that requires your approval prior to being made
                                available to churches to respond.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Please click below to approve or decline this
                                request.</h3></td>
                    </tr>
                </table>
            </div>
            <div class="main_btns" style="text-align: center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr style="padding: 0 20px;">
                        <td height='25' style="padding-bottom: 20px;"><a href="">Approve/Decline Case #188029</a>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4>You may copy/paste this link into your browser
                                to approve or decline this request.</h4></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h4 style="color: #f56600;">
                                {https://careportal.org/partners/review-request/#pending-requests/review-request/5a53cf23ee21265571853a7c/}</h4>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>What happend next?</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>This request will NOT be shared with churches
                                until you take the action to approve it.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Once approved, we will email this request to
                                each appropriate CarePortal church and send confirmation to your agency staff that
                                submitted the request.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>If you have any questions, feel free to contact
                                the CarePortal team..</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Ream</h3></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>
@endsection
