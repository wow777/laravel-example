@extends('emails.layout.layout')
@section('title', 'Thank you for helping')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

            <td class='reunify'>
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>Help Reunify a Bio Family</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Thank You for Helping</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Dear {12 Stone Church - Lawrenceville
                                    -30043,}</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Request {#14696} has been closed, and the child
                                    welfare worker who submitted this request says that you assisted with this
                                    need.</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>It's important that you verify your involvement
                                    with meeting this CarePortal request.</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>Thank you in advance for your assistance!</h3>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                        </tr>
                    </table>
                </div>
                <div class="main_btns" style="text-align: center;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr style="padding: 0 20px;">
                            <td height='25' style="padding-bottom: 20px;"><a href="">Verify Your Participation</a></td>
                        </tr>
                    </table>
                </div>
                <div class="req_content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Closed Request Details</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Careportal Request: <span class="req_des">{#14696}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / Country: <span class="req_des">{30046 / Gwinnett County, Georgia}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Grid Quardant: <span class="req_des">{Tier 1 - Physical Needs - Transition (Aging Out)}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Request Urgency: <span>{High; Needed Within 72 hrs}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Estimated need: <span class="req_des">{Primarily goods & services} - see detailed description of need.</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                            class="req_des">{1}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{item1, item2, item3}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{The youth has obtained employement with UPS and he is in need off a pair of size 11, steel toe boots. He is faithfull working toward a smooth transition into the independent Living Program. This assistance will help him to be successful at his new job as a package handler. he is very excited to begin working and is anxious about whether he is going to}</span>
                                </h2></td>
                        </tr>
                    </table>
                </div>
            </td>
    </table>

@endsection
