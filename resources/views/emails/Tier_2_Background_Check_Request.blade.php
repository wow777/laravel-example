@extends('emails.layout.layout')
@section('title', 'Tier 2 Background Check Request')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>New Tier 2 Background Check Request</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {Scott Platter!}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Test Test} has submitted a new Tier 2
                                background check request for {TEST ALASKA in Juneau County, Alsaka.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>NOTE:</b> You have untill {01/11/2018} at
                                {5:00 PM} to download the applicant's data for background check purposes. To
                                maintain the privacy of applications, all personal sensitive information will be
                                deleted at that time.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>If you have any questions, don't hesitate to
                                reply to this email and send me a message.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Derek Nelson</h3></td>
                    </tr>
                </table>
            </div>
            <div class="main_btns" style="text-align: center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr style="padding: 0 20px;">
                        <td height='25' style="padding-bottom: 20px;"><a href="">View Tier 2 Application</a></td>
                    </tr>
                </table>
            </div>

        </td>
    </table>

@endsection
