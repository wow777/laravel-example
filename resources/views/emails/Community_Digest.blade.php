@extends('emails.layout.layout')
@section('title', 'Community_Digest')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
            <td class='reunify'>
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                            <td valign='middle' colspan='3'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign='top' class="MainContainer">
                                    <p>Help Reunify a Bio Family</p>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content_data btn-styling" style="border: 0px; padding-top: 0px; position: relative;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Community Digest - Request #14850</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3>This email contains a report of churches who
                                    have recieved CarePortal request {#14580} for the {EI Paso West} Active
                                    Community.</h3></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h3><strong>NOTE:</strong>The full list of churched
                                    could take from several minutes to a few hours to polulated in your interface.
                                </h3></td>
                        </tr>
                    </table>
                </div>
                <div class="req_content rm-req">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25'><h1>Request Details</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Request ID: <span
                                            class="req_des">{#14580}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Case Zip Code / County: <span class="req_des">{79928 / EI Passp County, Texas}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Purpose: <span class="req_des">{Help Improve a child's Well-being}e</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Grid Quadrant: <span class="req_des">{Tier 1 - Physical Needs - Prevention</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Request Urgency: <span class="req_des">{Normal Within 7-10 days}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Estimated Need: <span class="req_des">{Primarily goods & services - see detailed description of need}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Children Served: <span
                                            class="req_des">{1}</span></h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Items Needed: <span class="req_des">{Item 1, Item 2, Item 3}</span>
                                </h2></td>
                        </tr>
                        <tr>
                            <td height="2"></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>Description: <span class="req_des">{A 10 year old child lives with her grandparents. the family has been battling bed bugs and all efforts of the grandmother to get rid of them have gone unsuccesful. The family is going through financial strains and unable to obtain fumigating services. Any help with his issues would be greatly appreciated. thank you in advance for your kind assistance.}</span>
                                </h2></td>
                        </tr>
                    </table>
                </div>
                <div class="text_btns text_btn1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='25' style="text-align: center; padding: 0 20px;"><h1>Request date: {02/15/2018
                                    11:05am}}</h1></td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center; padding: 0 20px;"><h5>{10} Records Found within
                                    {30} Miles of 79928</h5></td>
                        </tr>
                        <tr>
                            <td height='20'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Mesa Place Church}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{CHURCH OF SAINT CLEMENT}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{First Bapist Church EI Paso}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Primera lglesia Bautusta Mexicana}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{St. Francis Cathedral}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Bethel F.O.R.C.E. Church of God in
                                    Christ}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Life Gate Church}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Abundant Grace Church}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Northeast Bible Fellowship Church}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h2>{Destiny Family Christian Center}</h2></td>
                        </tr>
                        <tr>
                            <td height='25' style="padding: 0 20px;"><h4 class="col-bord">{EI Paso - EI Paso County,
                                    Texas, 79902 (TX-10)}</h4></td>
                        </tr>
                    </table>
                </div>
                <div class="close_btn">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='40'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">Engage This
                                    Active Community</a></td>
                        </tr>
                    </table>
                </div>
                <div class="engage_btn">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height='40'>
                            </td>
                        </tr>
                        <tr>
                            <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">View All
                                    Churches for this Request</a></td>
                        </tr>
                    </table>
                </div>
    </table>
@endsection
