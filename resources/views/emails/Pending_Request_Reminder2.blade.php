@extends('emails.layout.layout')
@section('title', 'Request Declined')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        <tbody>
        {{--Header Logos--}}
        @include('emails.layout.header')
        <tr>
            <td class="reunify">
                <div class="reunifyContent">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td valign="middle" colspan="3"><p>Help Reunify a Bio Family</p>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                       valign="top" class="MainContainer">

                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="content_data">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                        <tbody>
                        <tr>
                            <td height="25"><h1>Pending Request Reminder</h1></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>{Dear Jennifer,}</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>Thank you for submitting Case
                                    {#1602-7967-7759-0039758} on {Febraury 09, 2018 6:44pm}. This request is over 48
                                    hours old and needs immediate attention by your approver before it can be sent to
                                    churches.</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>We sent a reminder to your approver, however it
                                    might be useful for you to reach our and make sure they review this request ASAP. If
                                    it is not approved in the next 48 hours. It will automatically expire.</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3>Thank you for serving children in crisis, it's
                                    an honor to serve with you.</h3></td>
                        </tr>
                        <tr>
                            <td height="25" style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </td>
        </tr>
        </tbody>
    </table>

@endsection