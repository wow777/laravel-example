@extends('emails.layout.layout')
@section('title', 'Request Church')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent church">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Request #14458 - Tier 1- Physical Needs - Foster Care</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h2>Help Reunify a Bio Family</h2></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Tulsa County, Oklahoma (74011)</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Urgency: High: Needed Within 72 hrs</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3>Children Served: 1</h3></td>
                    </tr>
                    <tr>
                        <td height='25'><h3 style="padding: 0 80px;">Estimated Need: Primarly goods & services - see
                                detailed description of need</h3></td>
                    </tr>
                </table>
            </div>
            <div class="movabledes">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Description of the Need</h1></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 20px;">
                            <h6 style="margin: 0;line-height: 27px;font-size: 15px;">
                                <p>Mother and daughter are starting family counseling next week. NM has a car, but
                                    she has no money for gas. Counseling will be twice a week. So I am asking for
                                    five 10.00 gas cards if possible. They really need these counseling sessions to
                                    work on their relationship. I will meet her each with one to get her gas so that
                                    can make it to the session and back home. Thank you so very much for helping our
                                    clients. Sometimes all they need is a heliping hand ti lift them to that next
                                    level.</p>
                                <h3 class="aut_name">Child Welfare Woker: <span
                                            class="main_name">Cynthin Jankins</span></h3>
                                <h3 class="aut_name">Submitting Agency: <span class="main_name">OK-DHS</span></h3>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="main_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Respond to Request</a></td>
                    </tr>
                </table>
            </div>
            <div class="text_btn">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Engage Your Active
                                Community</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p><b>Does your church need assistance meeting this
                                    need?</b> Simply click the button below to send a message to the churches in
                                your active community. </p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;"><a href="">Send a Message</a></td>
                    </tr>
                </table>
            </div>
            <div class="text_btns">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25' style="text-align: center; padding: 0 20px;"><h1>Help another church meet
                                this need</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><p><b><span class="new">NEW</span> See a list of
                                    churches that are closer to this need and collaborate with them to meet it.</b>
                            </p></td>
                    </tr>
                    <tr>
                        <td height='40'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="text-align: center;padding-bottom: 40px;"><a href="">See Churches</a>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </table>

@endsection
