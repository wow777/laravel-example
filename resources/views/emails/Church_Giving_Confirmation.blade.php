@extends('emails.layout.layout')
@section('title', 'Church Giving Confirmation')

@section('body')

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif; max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')
        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top'
                                   class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Church Giving Confirmation</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you for generous commitment to
                                financially
                                support CarePortal in the days ahead. As you clearly recognize, church giving is
                                essential
                                to accelerate your local ministry to children and families in crisis.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>Here is a confirmation of the giving
                                    intentions
                                    we received:</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Council Road Bapist Church}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Giving Amount: {$100.00}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Giving Frequency: {Monthly}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Giving Preference: {Email Invoices}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Special Offering Date: {12/10/2017}</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>Finance Person:</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Client Chamberiain}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{chamberiain@councilroad.org}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{(405) 789-3175}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you again for your financial support. We
                                are
                                excited to help you love your neighbour.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3 class="team_sign">CarePortal Team</h3></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>


@endsection