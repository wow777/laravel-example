@extends('emails.layout.layout')
@section('title', 'Tier 3 Background Check Approved')

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"
           style="font-family: 'Quicksand', sans-serif;  max-width:600px;    border: 1px solid #eaeaea;">

        {{--Header Logos--}}
        @include('emails.layout.header')

        <td class='reunify'>
            <div class="reunifyContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                    <tr>
                        <td valign='middle' colspan='3'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center"
                                   valign='top' class="MainContainer">
                                <p>Help Reunify a Bio Family</p>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content_data" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                    <tbody>
                    <tr>
                        <td height='25'><h1>Tier 3 Background Check Approved!</h1></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Hi {Test Test,}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you so much for your heart to serve the
                                least of these in our community.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>We are excited to inform you that your
                                background check has been approved!</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>The next step is a home insepction. You should
                                except a call or email soon from the agency (listed below) that will complete the
                                inspection. They will schedule a time with you to come to your home. This should not
                                take more than 30-45 minutes.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>{TEST ALASKA}</b></h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{Scott Platter}</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>{scott@gorproject.org}</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Once the inspection is completed and approved
                                you will be notified again to confirm your spot in the next training class. Please
                                don't hesitate to reply to this email with any questions.</h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3><b>To prepare for the home inspection, please
                                    make sure you have the list below in place:</b></h3></td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0 20px;"><h4 class="appr_head">Common Spaces</h4></td>
                    </tr>
                    <tr>
                        <td>
                            <ul class="col_point">
                                <li>All firearms locked; ammunition locked seprately.</li>
                                <li>Working smoke detectors in each room.</li>
                                <li>Working carbon monoxide detector near bedrooms</li>
                                <li>Home is free of significant clutter/debris</li>
                                <li>Home is in good repair</li>
                                <li>In-ground pool is fenced (if applicable)</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0 20px;"><h4 class="appr_head">Sleeping Spaces</h4></td>
                    </tr>
                    <tr>
                        <td>
                            <ul class="col_point">
                                <li>Availbale sleeping space for the number of children you are willing to take. Alr
                                    mattress, pull out bed, or mattress on the floor are all options.
                                </li>
                                <li>Sleeping area must have two exists in case of emergency</li>
                                <li>Sleeping space is finished (cannot be an unfinished basement, or attic, closet,
                                    garage, laundry room etc.)
                                </li>
                                <li>If children are sharing a room, the children must be biological siblings or
                                    within 3 years of age
                                </li>
                                <li>For children 18 months and under you will need to have access to a crib (no drop
                                    side cribs)
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0 20px;"><h4 class="appr_head">Resources</h4></td>
                    </tr>
                    <tr>
                        <td>
                            <ul class="col_point">
                                <li>Home has access to telephone (cell phone) at all times</li>
                                <li>List of emergency contacts is clearly posted</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Thank you again for your heart for the least of
                                these.</h3></td>
                    </tr>
                    <tr>
                        <td height='25' style="padding: 0 20px;"><h3>Sincerely,</h3></td>
                    </tr>
                    <tr>
                        <td height='5' style="padding: 0 20px;"><h3>Derek Nelson</h3></td>
                    </tr>
                </table>
            </div>
        </td>
    </table>

@endsection