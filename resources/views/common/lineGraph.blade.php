<div style="margin: 0 0 50px;" class="chart-container">
    <div>
        <h3>{{$title}}</h3>
        <div style="position: relative; top: -30px;">
            <a href="{{$exportUrl}}" class="pull-right" style="margin: 0 30px;"><b>Export</b></a>
            <a href="#" class="pull-right chart-print-btn"><b>Print</b></a>
        </div>
    </div>
    <div style="height: 300px">
        <canvas id="{{$id}}"></canvas>
    </div>
</div>
<script type="text/javascript">
    var charts = charts || [];
    charts.push(function() {
        new Chart(document.getElementById('{{$id}}').getContext('2d'), {
            type: 'line',
            data: {
                labels: @json($xAxisLabels),
                datasets: @json($dataSets)
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display:false
                        }
                    }]
                }
            }
        })
    });
</script>