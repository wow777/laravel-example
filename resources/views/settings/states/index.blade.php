@extends('layouts.table-view')
@section('content-title', 'States')

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>All</th>
                            <th>Date Created</th>
                            <th>State</th>
                            <th>State Director</th>
                            <th>Total Children Served</th>
                            <th>Avg. Children Server Per Month</th>
                            <th>Last 12 Month Income</th>
                            <th>Number of churches giving</th>
                            <th>Requests Past 30 Days</th>
                            <th>Economic Impact 1 yr</th>
                            <th>Number of Agencies</th>
                            <th>Pending Churches</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($states as $state)
                            <tr data-id="{{ $state->id }}">
                                <td><input type="checkbox" name="check-{{ $state->id }}" value="{{ $state->id }}"></td>
                                <td>{{$state->created_at}}</td>
                                <td>{{$state->name}}</td>
                                <td>{{$state->stateDirector->displayName}}</td>
                                <td>{{$state->totalChildrenServed}}</td>
                                <td>{{number_format($state->average_children_served_per_month, 0)}}</td>
                                <td>{{$state->getYearlyStats()['children_served']}}</td>
                                <td>{{$state->givingChurches->count()}}</td>
                                <td>{{$state->requestsLastMonth->count()}}</td>
                                <td>{{$state->agencies->count()}}</td>
                                <td>??</td>
                                <td>{{$state->pendingChurches->count()}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="12">Nothing found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{$states->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
