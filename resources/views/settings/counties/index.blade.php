@extends('layouts.table-view')
@section('content-title', 'Counties')

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>County Name</th>
                            <th>Launch date</th>
                            <th>ST</th>
                            <th>Status</th>
                            <th>Max Churches In Radius</th>
                            <th>Max Radius</th>
                            <th>Escalation Churches</th>
                            <th>Escalation Radius</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($counties as $county)
                            <tr data-id="{{ $county->id }}">
                                <td>
                                    {{$county->id}}
                                </td>
                                <td>{{$county->name}}</td>
                                <td>{{$county->launch_date}}</td>
                                <td>{{$county->state->name}}</td>
                                <td>{{$county->status}}</td>
                                <td>{{$county->max_churches_in_radius}}</td>
                                <td>{{$county->max_radius}}</td>
                                <td>{{$county->esc_max_churches_in_radius}}</td>
                                <td>{{$county->esc_max_radius}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">Nothing found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{$counties->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
