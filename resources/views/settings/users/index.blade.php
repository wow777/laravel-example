@extends('layouts.table-view')
@section('content-title', 'Users')

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date Added</th>
                            <th>Status</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Role(s)</th>
                            <th>Email Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($users as $user)
                            <tr data-id="{{ $user->id }}">
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>{{!empty($user->created_at) ? $user->created_at->format('d/m/Y'): ''}}</td>
                                <td>{{$user->status}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{ $user->last_name }}</td>
                                <td>{{$user->rolesAsString()}}</td>
                                <td>{{$user->email}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7">Nothing found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{$users->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
