@extends('layouts.table-view')
@section('content-title', 'Regions')

@section('table')
    <div class="row">
        <div class="col-xs-12">
            <div class="box table-container">
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>All</th>
                            <th>Date Created</th>
                            <th>Region</th>
                            <th>Regional Manager</th>
                            <th>Total Children Served</th>
                            <th>Avg. Children Server Per Month</th>
                            <th>Last 12 Month Income</th>
                            <th>Number of churches giving</th>
                            <th>Requests Past 30 Days</th>
                            <th>Economic Impact 1 yr</th>
                            <th>Number of Agencies</th>
                            <th>Pending Churches</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($regions as $region)
                            <tr data-id="{{ $region->id }}">
                                <td><input type="checkbox" name="check-{{ $region->id }}" value="{{ $region->id }}"></td>
                                <td>{{$region->created_at ? $region->created_at->format('m/d/Y') : ''}}</td>
                                <td>{{$region->name}}</td>
                                <td>{{$region->regionalManager->displayName}}</td>
                                <td>{{$region->totalChildrenServed}}</td>
                                <td>{{number_format($region->average_children_served_per_month, 0)}}</td>
                                <td>{{$region->getYearlyStats()['children_served']}}</td>
                                <td>{{$region->givingChurches()->count()}}</td>
                                <td>{{$region->requestsLastMonth()->count()}}</td>
                                <td>{{'??'}}</td>
                                <td>{{$region->agencies()->count()}}</td>
                                <td>{{$region->pendingChurches()->count()}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="12">Nothing found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{$regions->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
