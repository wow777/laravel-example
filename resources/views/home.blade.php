@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">
                    @if( Auth::user()->isGMS() )
                        <div class="panel-heading">GMS View</div>
                        <div class="panel-content" style="padding-left:30px;"><a
                                    href="/role/gms/{{Auth::user()->id}}/dashboard">GMS</a></div>
                    @else
                        <div class="panel-heading">! GMS</div>
                    @endif
                </div>

                <div class="panel panel-default">
                    @if(count(Auth::user()->regions))
                        <div class="panel-heading">Regional Manager View</div>
                        @foreach(Auth::user()->regions as $region)
                            <div class="panel-content" style="padding-left:30px;"><a
                                        href="/role/regional_manager/{{$region->id}}/dashboard">{{$region->name}}</a></div>
                        @endforeach
                    @else
                        <div class="panel-heading">! Regional Manager</div>
                    @endif
                </div>
                <div class="panel panel-default">
                    @if(count(Auth::user()->states))
                        <div class="panel-heading">State Director View</div>
                        @foreach(Auth::user()->states as $state)
                            <div class="panel-content" style="padding-left:30px;"><a
                                        href="/role/state_director/{{$state->id}}/dashboard">{{$state->name}}</a></div>
                        @endforeach
                    @else
                        <div class="panel-heading">! State Director</div>
                    @endif
                </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{Auth::user()->first_name}} {{Auth::user()->last_name}} is logged in!
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
