<?php

namespace App\Listeners;

use App\Events\ChurchEnrolled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendChurchEnrollmentRequestConfirmationMailable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChurchEnrolled  $event
     * @return void
     */
    public function handle(ChurchEnrolled $event)
    {
        //
    }
}
