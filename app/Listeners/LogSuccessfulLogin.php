<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

  public function handle(Login $event)
  {

    $event->user->last_login = date('Y-m-d H:i:s');
    $event->user->save();
  }
}
