<?php

namespace App\Listeners;

use App\Events\OrientationCompleted;
use App\Mail\PointPersonOrientationCompletedNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPointPersonOrientationCompletedMailable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrientationCompleted  $event
     * @return void
     */
    public function handle(OrientationCompleted $event)
    {
        $church = $event->church->load(['region', 'users']);

        /**
         * @var \App\User $regionalManager
         */
        $regionalManager = $church->region->regionalManager;

        /**
         * @var \App\User $recipient
         */
        $recipient = $church->primaryPointPerson();

        \Mail::to($recipient)
            ->send(new PointPersonOrientationCompletedNotification($recipient, $church, $regionalManager));
    }
}
