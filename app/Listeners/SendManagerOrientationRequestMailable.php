<?php

namespace App\Listeners;

use App\Events\ChurchEnrolled;
use App\Mail\RegionalManagerEnrollmentNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendManagerOrientationRequestMailable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChurchEnrolled  $event
     * @return void
     */
    public function handle(ChurchEnrolled $event)
    {
        $church = $event->church->load(['region', 'state', 'users', 'zipCode']);
        $region = $church->region;

        /**
         * @var \App\User $recipient
         */
        $recipient = $region->regionalManager;

        /**
         * @var \App\User $pastor
         */
        $pastor = $church->pastor;

        \Mail::to($recipient)
            ->send(new RegionalManagerEnrollmentNotification($recipient, $church, $pastor));
    }
}
