<?php

namespace App\Listeners;

use App\Events\UserCommunitiesUpdated;
use App\Mail\CommunityEnrollmentNotification;
use App\Models\Community;
use function foo\func;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserCommunityEnrollmentMailable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCommunitiesUpdated  $event
     * @return void
     */
    public function handle(UserCommunitiesUpdated $event)
    {
        $recipient = $event->recipient;

        $communities = Community::whereIn('id', $event->communityIds)
            ->with([
                'coordinator' => function($query) {
                    $query->select(
                        'users.id',
                        'first_name',
                        'last_name',
                        'email',
                        'phone'
                    );
                },
                'counties' => function($query) {
                    $query->select(
                        'counties.id',
                        'name',
                        'state_id'
                    )->with([
                        'state' => function($query) {
                            $query->select('states.id', 'abbreviation');
                        }
                    ]);
                },
            ])
            ->get();

        \Mail::to($recipient)
            ->send(new CommunityEnrollmentNotification($recipient, $communities));
    }
}
