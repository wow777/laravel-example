<?php

namespace App\Listeners;

use App\Events\ChurchEnrolled;
use App\Mail\PointPersonEnrollmentAcknowledgement;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPointPersonAcknowledgementMailable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChurchEnrolled  $event
     * @return void
     */
    public function handle(ChurchEnrolled $event)
    {
        $church = $event->church;

        /**
         * @var \App\User $recipient
         */
        $recipient = $church->primaryPointPerson();

        \Mail::to($recipient)
            ->send(new PointPersonEnrollmentAcknowledgement($recipient, $church->name));
    }
}
