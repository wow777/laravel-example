<?php

namespace App\Listeners\Request;

use App\Events\Laravel\Passport\Events\AccessTokenCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestSubmitted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestSubmitted  $event
     * @return void
     */
    public function handle(RequestSubmitted $event)
    {
        //
    }
}
