<?php

namespace App\Rules;

use App\TeamApiClient;
use Illuminate\Contracts\Validation\Rule;

class UniqueTeamGoProject implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = app(TeamApiClient::class)->request('post', '/api/user/email', [
            'email' => $value
        ])->data;
        return empty($user);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email is already taken.';
    }
}
