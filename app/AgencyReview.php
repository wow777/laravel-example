<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyReview extends Model
{
    protected $table = 'agency_reviews';

    protected $guarded = [];
}
