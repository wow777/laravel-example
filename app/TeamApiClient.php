<?php

namespace App;

use Carbon\Carbon;
use GuzzleHttp\Client;

/**
 * Class TeamApiClient
 * @package App
 */
class TeamApiClient
{
    protected $client;
    protected $accessToken;

    /**
     * TeamApiClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('services.team.baseUri'),
        ]);

        $this->accessToken = $this->getAccessToken();
    }

    /**
     * @param $type
     * @param $endPoint
     * @param array $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($type, $endPoint, array $data = [])
    {
        $params = [];
        if(!empty($data)) {
            $params['form_params'] = $data;
        }
        if(!empty($this->accessToken)) {
            if($this->isTokenExpired()) {
                $this->accessToken = $this->getAccessToken();
            }
            $params['headers'] = [
                'Authorization' => 'Bearer ' . $this->accessToken['token'],
                'Accept' => 'application/json'
            ];
        }

        $response = $this->client->request($type, $endPoint, $params);

        return json_decode($response->getBody()->__toString());
    }

    /**
     * @return bool
     */
    private function isTokenExpired()
    {
        return $this->accessToken['expires'] < Carbon::now();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getAccessToken()
    {
        $data = $this->request('post', '/oauth/token', [
            'client_id' => config('services.team.client_id'),
            'client_secret' => config('services.team.client_secret'),
            'grant_type' => 'client_credentials',
        ]);

        return [
            'token' => $data->access_token,
            'expires' => Carbon::now()->addSeconds($data->expires_in)
        ];
    }
}