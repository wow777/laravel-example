<?php


// Helper function that can be accessed anywhere go here

function searchAllColumns($table, $keyword, $select = '*')
{

    $columns = Schema::getColumnListing($table);
    $query = DB::table($table)->select($select);

    foreach ($columns as $column) {

        $query->orWhere($column, 'like', '%' . $keyword . '%');

    }

    return $query;
}

/**
 * @param int|string $num
 * @return array
 */
function getColor($num)
{
    $hash = md5('color' . $num);
    return array(
        hexdec(substr($hash, 0, 2)), // r
        hexdec(substr($hash, 2, 2)), // g
        hexdec(substr($hash, 4, 2))); //b
}

if (!function_exists('currentRole')) {
    /**
     * @return \App\Interfaces\RoleInterface|null
     */
    function currentRole()
    {
        return request()->attributes->get('role', null);
    }
}

if (!function_exists('coordinateDistance')) {
    function coordinateDistance($lat1, $lon1, $lat2, $lon2, $unit = 'miles')
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

function distanceBetweenZipcodes($zip1, $zip2)
{
    if( !is_numeric($zip1)|| is_numeric($zip2)){
        return false;
    }

    $coords = \App\Models\ZipCode::whereIn('code', [$zip1, $zip2])->limit(2)->get();

    if(!empty($coords) && count($coords) == 2){
        return round(coordinateDistance($coords[0]->lat, $coords[0]->lon, $coords[1]->lat, $coords[1]->lon), 1);
    }

    return false;

}