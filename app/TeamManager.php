<?php

namespace App;

use App\Models\ZipCode;
use App\Models\Region;

class TeamManager
{
    public function getAllManagersAndDirectors()
    {
        // TODO ambassadors, implementing partner,

        // Get all unique user ID's for regional managers, state directors and combine
        $result = collect(\DB::select('
            SELECT DISTINCT user_id from (
            SELECT regional_manager_id AS user_id
            FROM regions
            
            UNION ALL
            
            SELECT s.state_director_id AS user_id
            FROM regions AS r
            JOIN states AS s ON r.state_id=s.id
            ) AS tmp
            WHERE user_id IS NOT NULL
                    '));

        // Load the users, along with eager loading their states and regions relationships (since these are used to determine their roles)
        $users = User::with(['states', 'regions'])
                     ->whereIn('id', $result->pluck('user_id'))
                     ->get();

        // Assuming we want to sort by name
        return $users->sortBy(function (User $user) {
            return $user->getFullNameAttribute();
        })->values()
          ->map(function (User $user) {
            $title = [];
            $regions = [];
            $stateRegions = [];

            if ($user->regions->count()) {
                $title[] = 'Regional Manager';
                $regions = $user->regions->pluck('name');
            }

            if ($user->states->count()) {
                $title[] = 'State Director';
                // Regions for state director
                $stateRegions = Region::where('state_id', $user->states->first()->id)
                                      ->get()
                                      ->pluck('name');
            }

              return [
                'name' => $user->first_name . ' ' . $user->last_name,
                'title' => $title,
                'regions' => $regions,
                'implementing_partner' => @$user->implementing_partners->first()->name,
                //'state_regions' => $stateRegions,  // do we need the regions of each state?
                'email' => $user->email,
                'user_photo_url' => $user->user_photo_url,
            ];
        });
    }

    public function getStateDirectorByZip(ZipCode $zip)
    {
        $zip->load('state.director', 'region');

        if (!$zip->state || !$zip->state->director) {
            return ['role' => 'State Director', 'exists' => false];
        }

        return [
            'role' => 'State Director',
            'name' => "{$zip->state->director->first_name} {$zip->state->director->last_name}",
            'email' => $zip->state->director->email,
            'region' => @$zip->region->name,
            'implementing_partner' => @$zip->state->implementingPartners()->first()->name, //could be multiple?  just fetching first
            'image' => $zip->state->director->user_photo_url,
            'exists' => true
        ];
    }

    public function getRegionalManagerByZip(ZipCode $zip)
    {
        $zip->load('region.manager', 'region');

        if (!$zip->region || !$zip->region->manager) {
            return ['role' => 'Regional Manager', 'exists' => false];
        }

        return [
            'role' => 'Regional Manager',
            'name' => "{$zip->region->manager->first_name} {$zip->region->manager->last_name}",
            'email' => $zip->region->manager->email,
            'region' => $zip->region->name,
            'implementing_partner' => @$zip->region->implementingPartners()->first()->name, // could be multiple?  just fetching first
            'image' => $zip->region->manager->user_photo_url,
            'exists' => true
        ];
    }

    public function getCommunityCoordinator(ZipCode $zip)
    {
        $zip->load('region', 'community');

        if (!$zip->community || !$zip->community->coordinator) {
            return ['exists' => false];
        }

        return [
            'role' => 'Community Coordinator',
            'name' => '',
            'email' => '',
            'region' => '',
            'implementing_partner' => '',// todo
            'image' => '',
            'exists' => true
        ];
    }
}