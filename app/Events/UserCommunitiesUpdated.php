<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserCommunitiesUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User $recipient
     */
    public $recipient;

    /**
     * @var string[]|int[] $communityIds
     */
    public $communityIds;

    /**
     * Create a new event instance.
     *
     * @param User $recipient
     * @param string[]|int[] $communityIds
     * @return void
     */
    public function __construct(User $recipient, $communityIds)
    {
        $this->recipient = $recipient;
        $this->communityIds = $communityIds;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
//        return new PrivateChannel('channel-name');
    }
}
