<?php

namespace App\Events;

use App\Models\Church;
use App\Models\EnrollmentRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChurchEnrolled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Church $church
     */
    public $church;

    /**
     * Create a new event instance.
     *
     * @param Church $church
     * @return void
     */
    public function __construct(Church $church)
    {
        $this->church = $church;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
//        return new PrivateChannel('channel-name');
    }
}
