<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class CommunityEnrollmentNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User $recipient
     */
    protected $recipient;

    /**
     * @var Collection $communities
     */
    protected $communities;

    /**
     * Create a new message instance.
     *
     * @param User $recipient
     * @param Collection $communities
     * @return void
     */
    public function __construct(User $recipient, Collection $communities)
    {
        $this->recipient = $recipient;
        $this->communities = $communities;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.Community_Enrollment_Confirmation')
            ->subject('Care Portal Community Enrollment Confirmation')
            ->with([
                'recipient' => $this->recipient,
                'communities' => $this->communities
            ]);
    }
}
