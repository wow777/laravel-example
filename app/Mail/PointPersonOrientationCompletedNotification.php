<?php

namespace App\Mail;

use App\User;
use App\Models\Church;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PointPersonOrientationCompletedNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User $recipient
     */
    protected $recipient;

    /**
     * @var Church $church
     */
    protected $church;

    /**
     * @var User $regionalManager
     */
    protected $regionalManager;

    /**
     * Create a new message instance.
     *
     * @param User $recipient
     * @param Church $church
     * @param User $regionalManager
     * @return void
     */
    public function __construct(User $recipient, Church $church, User $regionalManager)
    {
        $this->recipient = $recipient;
        $this->church = $church;
        $this->regionalManager = $regionalManager;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.Orientation_completed')
            ->subject('Care Portal Church Enrollment Final Step')
            ->with([
                'recipient' => $this->recipient,
                'church' => $this->church,
                'regionalManager' => $this->regionalManager
            ]);
    }
}
