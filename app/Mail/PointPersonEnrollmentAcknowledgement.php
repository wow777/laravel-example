<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PointPersonEnrollmentAcknowledgement extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User $recipient
     */
    protected $recipient;

    /**
     * @var string $churchName
     */
    protected $churchName;

    /**
     * Create a new message instance.
     *
     * @param User $recipient
     * @param string $churchName
     * @return void
     */
    public function __construct(User $recipient, $churchName)
    {
        $this->recipient = $recipient;
        $this->churchName = $churchName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.Acknowledgement_to_point_person')
            ->subject('Care Portal Church Enrollment')
            ->with([
                'recipient' => $this->recipient,
                'churchName' => $this->churchName
            ]);
    }
}
