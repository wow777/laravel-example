<?php

namespace App\Mail;

use App\Models\Church;
use App\Models\EnrollmentRequest;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegionalManagerEnrollmentNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User $recipient
     */
    public $recipient;

    /**
     * @var Church $church
     */
    public $church;

    /**
     * @var User $pastor
     */
    public $pastor;

    /**
     * Create a new message instance.
     *
     * @param User $recipient
     * @param Church $church
     * @param User $pastor
     * @return void
     */
    public function __construct(User $recipient, Church $church, User $pastor)
    {
        $this->recipient = $recipient;
        $this->church = $church;
        $this->pastor = $pastor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.RM_Church_Enrollment')
            ->subject('Care Portal New Church Orientation Request')
            ->with([
                'recipient' => $this->recipient,
                'church' => $this->church,
                'pastor' => $this->pastor
            ]);
    }
}
