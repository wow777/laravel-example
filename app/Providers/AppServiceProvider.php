<?php

namespace App\Providers;


use App\TeamApiClient;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container singletons that should be registered.
     *
     * @var array
     */
    public $singletons = [
        TeamApiClient::class => TeamApiClient::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Relation::morphMap([
            'requests' => 'App\Models\Requests',
            'applications' => 'App\Models\Applications',
            'churches' => 'App\Models\Church',
            'implementing_partners' => 'App\Models\ImplementingPartner',
            'users' => 'App\User',
            'communities' => 'App\Models\Community',
            'groups' => 'App\Models\Group',
            'agencies' => 'App\Models\Agency',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
