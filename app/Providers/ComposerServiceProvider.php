<?php

namespace App\Providers;

use App\Http\ViewComposers\RoleDropDownComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $view = view();
        $view->composer('components.roleDropDown', RoleDropDownComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
