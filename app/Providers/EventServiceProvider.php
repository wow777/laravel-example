<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\LogSuccessfulLogin',
        ],
        'Laravel\Passport\Events\AccessTokenCreated' => [
            'App\Listeners\APILogin',
        ],
        'App\Events\RequestSubmitted'=> [
            'App\Listeners\Request\RequestSubmitted',
        ],
        'App\Events\RequestApproved'=> [
            'App\Listeners\Request\RequestApproved',
        ],
        'App\Events\RequestResponse'=> [
            'App\Listeners\Request\RequestResponse',
        ],
        'App\Events\ChurchEnrolled' => [
            'App\Listeners\SendChurchEnrollmentRequestConfirmationMailable',
            'App\Listeners\SendPointPersonAcknowledgementMailable',
            'App\Listeners\SendManagerOrientationRequestMailable',
        ],
        'App\Events\OrientationCompleted' => [
            'App\Listeners\SendPointPersonOrientationCompletedMailable',
        ],
        'App\Events\UserCommunitiesUpdated' => [
            'App\Listeners\SendUserCommunityEnrollmentMailable'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
