<?php
/**
 * Created by PhpStorm.
 * User: dougniccum
 * Date: 5/3/18
 * Time: 1:59 PM
 */

namespace Library;


use App\Models\ChurchUser;

class ChurchUserService
{
    public function __construct()
    {

    }

    /**
     * Creates a church user
     * @param int $churchId
     * @param int $userId
     * @param bool $isPastor
     * @param bool $t1PointPerson
     * @return ChurchUser|\Illuminate\Database\Eloquent\Model
     */
    public static function createUser($churchId, $userId, $isPastor=false, $t1PointPerson=false)
    {
        $churchUser = ChurchUser::create([
            'church_id' => $churchId,
            'user_id' => $userId,
            'pastor' => $isPastor,
            't1_point_person' => $t1PointPerson
        ]);

        return $churchUser;
    }
}