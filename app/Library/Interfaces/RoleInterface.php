<?php

namespace App\Interfaces;

use App\User;

/**
 * Interface RoleInterface
 * @package App\Interfaces
 */
interface RoleInterface
{
    public function contacts();

    public function hasContact(User $contact);

    public function implementingPartners();
}