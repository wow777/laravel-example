<?php
/**
 * Created by PhpStorm.
 * User: dougniccum
 * Date: 5/11/18
 * Time: 10:54 AM
 */

namespace App\Library;


class AgencyReviewSeedingService
{
    /**
     * Because the agency reviews are present in the requests table,
     * the agency reviews must be migrated to their own table.
     * This seeder allows this function to be used in both the migration and seeder.
     *
     * @return void
     */
    public static function migrateAgencyReviews()
    {
        \App\Models\Request::chunk(200, function ($requests) {
            $requestsToImport = [];

            foreach ($requests as $request) {
                if (!empty($request->rating)) {
                    $newReview = [
                        'rating' => $request->rating,
                        'comments_questions' => $request->reason_for_rating ? $request->reason_for_rating : null,
                        'your_story' => $request->worker_story ? $request->worker_story : null,
                        'agency_id' => $request->agency_id,
                        'agency_worker_id' => $request->worker_id,
                        'created_at' => $request->created_at,
                        'updated_at' => $request->updated_at
                    ];

                    array_push($requestsToImport, $newReview);
                }
            }

            \DB::table('agency_reviews')->insert($requestsToImport);
        });
    }
}