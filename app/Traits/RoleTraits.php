<?php

namespace App\Traits;

use App\Models\Church;
use App\Models\Community;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

trait RoleTraits{

    /**
     * @return Collection
     */
    public function activatedChurches()
    {
        return $this->churches()->activated()->get();
    }

    /**
     * @return Collection
     */
    public function pendingChurches()
    {
        return $this->churches()->pendingActivation()->get();
    }

    /**
     * @deprecated
     * @return Collection
     */
    public function pendingActivation()
    {
        return $this->pendingChurches();
    }

    public function activatedGroups()
    {
        return $this->groups()->activated()->get();
    }

    /**
     * @return mixed
     */
    public function contacts()
    {
        return User::withTrashed()->whereIn('id', $this->activatedChurches()->pluck('id'));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function contactsOld()
    {
        $this->pastorCnt = 0;
        $this->ppCnt = 0;
        $this->fpCnt = 0;
        $this->sgaCnt = 0;

        dd($this->churches()->count());
        $contacts = array();
        $churches = $this->churches;
        foreach ($churches as $church) {
            foreach ($church->users as $user) {
                $contacts[] = $user;

                $userChurch = $user->church;
                if ($userChurch->pivot->pastor) {
                    $this->pastorCnt++;
                }
                if ($userChurch->pivot->t1_point_person) {
                    $this->ppCnt++;
                }
                if ($userChurch->pivot->finance_person) {
                    $this->fpCnt++;
                }
                if ($userChurch->pivot->small_group_admin) {
                    $this->sgaCnt++;
                }
            }
        }

        return collect($contacts);
    }

    /**
     * @param User $contact
     * @return bool
     */
    public function hasContact(User $contact)
    {
        return $this->contacts()->pluck('id')->contains($contact->id);
    }

    /**
     * @return Builder
     */
    public function communities()
    {
        return Community::query();
    }

}