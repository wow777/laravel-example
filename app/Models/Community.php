<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Community extends Model
{

    public function counties()
    {
        return $this->belongsToMany(\App\Models\County::class, 'community_locations', 'community_id', 'county_id')->distinct('id');
    }

    public function zipCodes()
    {
        return $this->belongsToMany(\App\Models\ZipCode::class, 'community_locations', 'community_id', 'zip_code_id')->distinct('id');
    }

    public function coordinator()
    {
        return $this->belongsTo(\App\User::class, 'coordinator_id');
    }

    /**
     * @param Builder $builder
     * @return $this
     */
    public function scopeOfCriticalNeed(Builder $builder)
    {
        return $builder->where('area_of_critical_need',1);
    }

}
