<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class RepCounty extends Pivot
{
    protected $table = 'rep_counties';

    public function county(){

        return $this->belongsTo('App\Models\County');

    }
}
