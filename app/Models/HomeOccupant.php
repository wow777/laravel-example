<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeOccupant extends Model
{

    public function applications(){
        return $this->belongsTo('App\Models\Application');
    }

}
