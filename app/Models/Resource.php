<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public static function resources()
	{
		return [
			'0' => ['id' => '1', 'added_date' => '2017-02-08', 'status' => 'Active', 'title' => 'CMG Templates', 'category' => 'Handbook (LDS Church)', 'resource_type' => 'PowerPoint', 'resource_link' => 'https://www.youtube.com/watch?v=j685NaMDVYE', 'role' => 'Role 1, Role 3'], 
			'1' => ['id' => '2', 'added_date' => '2017-04-06', 'status' => 'Active', 'title' => 'Seeds Church On The Move Resources', 'category' => 'Consistory (Protestantism)', 'resource_type' => 'PDF', 'resource_link' => 'https://www.youtube.com/watch?v=FrZtShb1-8E', 'role' => 'Role 2'],
			'2' => ['id' => '3', 'added_date' => '2017-06-03', 'status' => 'Deactivated', 'title' => 'One Church Resource', 'category' => 'Congregationalist polity & Ecclesiastical polity', 'resource_type' => 'Video', 'resource_link' => 'https://www.youtube.com/watch?v=g8a4F6mVX64', 'role' => 'Role 3'],
			'3' => ['id' => '4', 'added_date' => '2017-07-05', 'status' => 'Active', 'title' => 'Open by LifeChurch.tv', 'category' => 'Communion (religion)', 'resource_type' => 'PowerPoint', 'resource_link' => 'https://www.youtube.com/watch?v=qUXtCFpybvQ', 'role' => 'Role 1, Role 2'],
			'4' => ['id' => '5', 'added_date' => '2018-01-09', 'status' => 'Deactivated', 'title' => 'Elevation Church Resources', 'category' => 'Church union', 'resource_type' => 'Video', 'resource_link' => 'https://www.youtube.com/watch?v=_sV9JS3sjTU', 'role' => 'Role 1, Role 2, Role 3'], 
			'5' => ['id' => '6', 'added_date' => '2018-04-06', 'status' => 'Deactivated', 'title' => 'New Life Church Creative', 'category' => 'Church planting', 'resource_type' => 'Video', 'resource_link' => 'https://www.youtube.com/watch?v=rZP0qKVJOlc', 'role' => 'Role 2, Role 3']
		];
	}
	
	public static function getResourceById($id){
		$getResourceData = Resource::resources();
		foreach($getResourceData as $resources){
			if($resources['id'] === $id){
				$getResourceData = $resources;
			}
		}
		return $getResourceData;
	}
	
	public static function deleteResource($id){
		// Logic to delete the resource goes here
		return true;
	}
}
