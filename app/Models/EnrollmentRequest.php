<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnrollmentRequest extends Model
{
    protected $table = 'enrollment_requests';

    protected $casts = [];

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = ['church_id'];

    public function church()
    {
        return $this->belongsTo(Church::class, 'church_id');
    }
}
