<?php

namespace App\Models;

use App\Traits\RoleTraits;
use App\Interfaces\RoleInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Gms extends Model implements RoleInterface
{
    protected $primaryKey = 'user_id';
    use RoleTraits;

    public function user(){
        $this->belongsTo('App\User');
    }

    /**
     * @return Builder
     */
    public function requests()
    {
        return Request::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getRequestsAttribute()
    {
        return $this->requests()->get();
    }

    /**
     * @return Builder
     */
    public function churches()
    {
        return Church::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getChurchesAttribute()
    {
        return $this->churches()->get();
    }

    /**
     * @return Builder
     */
    public function groups()
    {
        return Group::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getGroupsAttribute()
    {
        return $this->groups()->get();
    }

    /**
     * @return Builder
     */
    public function states()
    {
        return State::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getStatesAttribute()
    {
        return $this->states()->get();
    }

    /**
     * @return Builder
     */
    public function communities()
    {
        return Community::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getCommunitiesAttribute()
    {
        return $this->communities()->get();
    }

    /**
     * @return Builder
     */
    public function regions()
    {
        return Region::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getRegionsAttribute()
    {
        return $this->regions()->get();
    }

    /**
     * @return Builder
     */
    public function agencies()
    {
        return Agency::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getAgenciesAttribute()
    {
        return $this->agencies()->get();
    }

    /**
     * @return Builder
     */
    public function implementingPartners()
    {
        return ImplementingPartner::query();
    }

    /**
     * @return Collection|static[]
     */
    public function getImplementingPartnersAttribute()
    {
        return $this->implementingPartners()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function activatedChurches($forLastTwoYears = false)
    {
        $query = Church::query()
            ->where('tier_1_enrollment_complete', true)
            ->where('has_enrolled', true);

        if($forLastTwoYears) {
            $query->whereYear('tier_1_enrollment_complete_date', '>=', date('Y', strtotime('-1 year')));
        }

        return $query->get();
    }
}
