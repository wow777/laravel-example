<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function homeOccupants(){
        return $this->hasMany('App\Models\HomeOccupant');
    }

    public function applicationsChecks(){

        return $this->hasMany('App\Models\ApplicationCheck');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }

}
