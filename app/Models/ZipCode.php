<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    public function churches()
    {
        return $this->hasMany('App\Models\Church');
    }

    public function counties()
    {
        return $this->hasManyThrough('App\Models\County', 'App\Models\CountyZipCode',
            'zip_code_id', 'id', '', 'county_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region', 'region_id', 'id');
    }

    public function community()
    {
        return $this->belongsTo(Community::class, 'community_id', 'id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'knack_state_id', 'id');
    }

}
