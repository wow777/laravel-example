<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AgencyWorker extends Pivot
{

    public function counties(){
        return $this->hasMany('App\Models\RepCounty', 'rep_id', 'id')->with('county');

    }

    public function agency(){

        return $this->belongsTo('App\Models\Agency');

    }

    public function user(){
        return $this->belongsTo('App\User');

    }

}
