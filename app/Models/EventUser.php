<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventUser extends Model
{
    protected function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
}
