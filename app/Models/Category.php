<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public static function getCategories()
	{
		return [
			'0' => ['id' => '1', 'category' => 'Handbook (LDS Church)'], 
			'1' => ['id' => '2', 'category' => 'Consistory (Protestantism)'],
			'2' => ['id' => '3', 'category' => 'Congregationalist polity & Ecclesiastical polity'],
			'3' => ['id' => '4', 'category' => 'Communion (religion)'],
			'4' => ['id' => '5', 'category' => 'Church union'], 
			'5' => ['id' => '6', 'category' => 'Church planting']
		];
	}
	
	public static function deleteCategories($id){
		// Logic to delete the category goes here
		return true;
	}
}
