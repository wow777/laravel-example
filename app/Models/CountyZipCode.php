<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CountyZipCode extends Pivot
{
    //
    protected $table = 'county_zip_code';
}
