<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ImplementingPartner
 * @package App\Models
 */
class ImplementingPartner extends Model
{
    const STATUS_ACTIVE = 'Active';

    const STATUS_INACTIVE = 'Inactive';

    use SoftDeletes;

    protected $fillable = ['name', 'address_line1', 'city', 'state', 'zip_code', 'phone', 'website_url'];

    /**
     * @return BelongsToMany
     */
    public function regions()
    {
        return $this->belongstoMany('App\Models\Region');
    }

    /**
     * @return HasMany
     */
    public function churches()
    {
        return $this->hasMany('App\Models\Church');
    }

    /**
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function newChurches()
    {
        return $this->churches()->whereDate('orientation_date', '>=', Carbon::now()->subDays(60));
    }

    /**
     * @return Builder|static
     */
    public function churchesWithNewResponses()
    {
        return $this->churches()->whereHas('responses', function(Builder $builder) {
            return $builder->whereDate('created_at', '>=', Carbon::now()->subDays(14));
        });
    }

    /**
     * @return Builder|static
     */
    public function churchesWithoutNewResponses()
    {
        return $this->churches()->whereDoesntHave('responses', function(Builder $builder) {
            return $builder->whereDate('created_at', '>=', Carbon::now()->subDays(60));
        });
    }

    /**
     * @return Builder
     */
    public function activeChurches()
    {
        return $this->churches()->activated();
    }

    /**
     * @return int
     */
    public function getActiveChurchCountAttribute() {
        return $this->activeChurches->count();
    }

    /**
     * @return BelongsToMany
     */
    public function users()
    {
        return $this
            ->belongsToMany(User::class, 'implementing_partner_user', 'implementing_partner_id', 'user_id')
            ->withPivot('admin', 'ambassador');
    }


    /**
     * @return BelongsToMany
     */
    public function ambassadors()
    {
        return $this->users()->wherePivot('ambassador', 1);
    }

    /**
     * @return BelongsToMany
     */
    public function admins()
    {
        return $this->users()->wherePivot('admin', 1);
    }

    /**
     * @return int
     */
    public function getAmbassadorCountAttribute() {
        return $this->ambassadors()->count();
    }

    /**
     * @param Builder $builder
     * @param string $q
     * @return mixed
     */
    public function scopeFiltered(Builder $builder, $q)
    {
        return $builder
            ->when(!empty($q), function (Builder $builder) use ($q) {
                return $builder
                    ->orWhere('name', $q)
                    ->orWhere('phone', $q);
            });
    }

    /**
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->deleted_at ? static::STATUS_INACTIVE : static::STATUS_ACTIVE;
    }

    /**
     * Get all of the IP's notes.
     */
    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }
}
