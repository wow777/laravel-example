<?php

namespace App\Models;

use App\Traits\RoleTraits;
use App\Interfaces\RoleInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class State extends Model implements RoleInterface
{
    use RoleTraits;

    protected $hidden = ['polygon_data'];

    public function regions()
    {
        return $this->hasMany('App\Models\Region', 'state_id');
    }

    public function counties()
    {
        return $this->hasMany('App\Models\County');
    }

    /**
     * @return HasMany
     */
    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }

    /**
     * @return HasMany
     */
    public function requestsLastMonth()
    {
        return $this->requests()->whereDate('created_at', '>=', Carbon::now()->subDays(30));
    }

    public function requestsMet()
    {
        $requests = $this->relationLoaded('requests') ? $this->requests : $this->requests();
        return $requests->where('reason_closed', 'Church Responded and Met the Need');
    }

    public function getOldRequestCountAttribute()
    {
        $requests = $this->relationLoaded('requests') ? $this->requests : $this->requests();
        return $requests
            ->where('status', 'Open')
            ->where('created_at', '<', Carbon::now()->subDays(30))
            ->count();
    }

    /**
     * @return float|int
     */
    public function getAverageChildrenServedPerMonthAttribute()
    {
        $months = $this->completedRequests
            ->groupBy(function($request) {
                return $request->created_at->format('Y-m');
            })
            ->map(function($row) {
                return $row->sum('children_served');
            });

        $monthCount = $months->count();

        if($monthCount < 1) {
            return 0;
        }


        $sum = array_reduce($months->toArray(), function($sum, $monthSum) {
            return $sum + $monthSum;
        });

        return $sum/$monthCount;
    }

    /**
     * @return HasMany
     */
    public function completedRequests()
    {
        return $this->requests()
            ->completed();
    }

    /**
     * @return HasMany
     */
    public function completedRequestsLastYear()
    {
        return $this->completedRequests()
            ->where('created_at', '>', Carbon::now()->subYear());
    }

    public function getYearlyStats()
    {
        $children_served = $this->completedRequestsLastYear->sum('children_served');
        $estimated_value = $this->completedRequestsLastYear->sum('estimated_value');
        $cost_avoidance = $this->completedRequestsLastYear->sum('cost_avoidance');

        return compact('children_served', 'estimated_value', 'cost_avoidance', 'economic_impact');
    }

    // TEMP TEMP TEMP - this is another way of getting the YearlyStats, but is considerably slower
//    public function getChildrenServedLast12MonthsAttribute()
//    {
//        return $this->requestsMet->where('created_at', '>', Carbon::now()->subYear())->sum('children_served');
//    }
//
//    public function getEconomicImpactLast1YearAttribute()
//    {
//        return $this->requestsMet->where('created_at', '>', Carbon::now()->subYear())->sum('estimated_value') +
//            $this->requestsMet->where('created_at', '>', Carbon::now()->subYear())->sum('cost_avoidance');
//    }

    /**
     * @return mixed
     */
    public function getTotalChildrenServedAttribute()
    {
        return $this->completedRequests->sum('children_served');
    }

    /**
     * @return HasMany
     */
    public function churches()
    {
        return $this->hasMany('App\Models\Church');
    }

    /**
     * @return HasMany
     */
    public function givingChurches()
    {
        return $this->churches()->giving();
    }

    /**
     * @return HasMany
     */
    public function pendingChurches()
    {
        return $this->churches()->pendingActivation();
    }

    public function groups(){
        return $this->hasMany('App\Models\Group');
    }

    public function stateDirector()
    {
        return $this->hasOne('App\User', 'id', 'state_director_id')
            ->withDefault(['last_name' => '', 'first_name' => '']);
    }

    // TEMP TEMP TEMP - Not sure if this is even needed, but if so, would you do it this way?
    public function getImplementingPartnerAttribute()
    {
        return $this->stateDirector->implementing_partner;
    }

    public function implementingPartners()
    {
        return ImplementingPartner::fromQuery(
            DB::table('implementing_partners')
                ->select('implementing_partners.*')->distinct()
                ->join('implementing_partner_region', 'implementing_partner_region.implementing_partner_id', 'implementing_partners.id')
                ->join('regions', 'regions.id', 'implementing_partner_region.region_id')
                ->whereRaw('regions.state_id='.$this->id)->toSql());
    }

    public function agencies()
    {
        return $this->hasMany('App\Models\Agency');
    }

    // TEMP TEMP TEMP - this does not account for all contacts
    public function contacts()
    {
        $this->pastorCnt = 0;
        $this->ppCnt = 0;
        $this->fpCnt = 0;
        $this->sgaCnt = 0;

        $contacts = array();
        $churches = $this->churches;
        foreach ($churches as $church) {
            foreach ($church->users as $user) {
                $contacts[] = $user;

                $usersChurch = $user->church;
                if ($usersChurch->pivot->pastor) {
                    $this->pastorCnt++;
                }
                if ($usersChurch->pivot->t1_point_person) {
                    $this->ppCnt++;
                }
                if ($usersChurch->pivot->finance_person) {
                    $this->fpCnt++;
                }
                if ($usersChurch->pivot->small_group_admin) {
                    $this->sgaCnt++;
                }
            }
        }

        return $contacts;
    }

    public function getContactTypeCounts()
    {
        return array(
            'pastorCnt' => $this->pastorCnt,
            'ppCnt' => $this->ppCnt,
            'fpCnt' => $this->fpCnt,
            'sgaCnt' => $this->sgaCnt
        );
    }

    public function activatedChurchCnt()
    {
        return $this->churches
            ->where('tier_1_enrollment_complete', true)
            ->where('has_enrolled', true)
            ->count();
    }

    public function pendingActivationCnt()
    {
        return $this->churches
            ->where('tier_1_enrollment_complete', false)
            ->where('has_enrolled', true)
            ->count();
    }

    public function director()
    {
        return $this->belongsTo('App\User', 'state_director_id', 'id');
    }

    public function scopeDirectors()
    {
        $ids = DB::table('states')->pluck('state_director_id');
        return $ids;

    }


    private $pastorCnt = -1;
    private $ppCnt = -1;
    private $fpCnt = -1;
    private $sgaCnt = -1;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFiltered(Builder $builder, $q)
    {
        return $builder->when(!empty($q), function($builder) use($q) {
            return $builder
                ->orWhere('name', $q)
                ->orWhereHas('stateDirector', function($builder) use($q) {
                    $names = explode(' ', $q);
                    return $builder
                        ->whereIn('first_name', $names)
                        ->orWhere(function ($query) use ($names) {
                            $query->whereIn('last_name', $names);
                        });
                });
        });
    }

    /**
     * @return Builder
     */
    public function communities()
    {
        return Community::whereIn('id',
            DB::table('community_locations')
                ->select('community_locations.community_id')
                ->join('counties', 'counties.id', 'community_locations.county_id')
                ->join('states', 'states.id', 'counties.state_id')
                ->where('states.id', $this->id)
                ->distinct('community_locations.community_id')
        );
    }
}
