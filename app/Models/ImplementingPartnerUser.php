<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ImplementingPartnerUser extends Pivot
{
    //
    protected $table = 'implementing_partner_user';
}
