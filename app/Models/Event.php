<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function users(){
        return $this->hasMany(EventUser::class)->with('users');
    }


}
