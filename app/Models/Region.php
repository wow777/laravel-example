<?php

namespace App\Models;

use App\Interfaces\RoleInterface;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use App\Traits\RoleTraits;

class Region extends Model implements RoleInterface
{
    use RoleTraits;

    private $completedRequestsCalculated;

    public function zipCodes()
    {
        return $this->hasMany('App\Models\ZipCode', 'region_id');
    }

    public function counties()
    {
        return $this->hasManyThrough('App\Models\County', 'App\Models\RegionLocation',
            'region_id', 'id', 'id', 'county_id')
            ->distinct('region_locations.county_id');

    }

    public function requests()
    {
        return $this->hasManyThrough('App\Models\Request', 'App\Models\RegionLocation',
            'region_id', 'zip_code_id', 'id', 'zip_code_id')
            ->whereRaw('requests.county_id = region_locations.county_id');
    }

    public function numOldRequests($days = null)
    {
        if (count($this->zipCodes)) {
            return $this->hasManyThrough('App\Models\Request', 'App\Models\ZipCode')
                ->where('requests.created_at', '<', Carbon::now()->subDays($days))
                ->where('requests.status', 'Open')
                ->whereRaw('requests.county_id not in (select id from counties where region_id is not null)')
                ->count();
        } else {
            return $this->hasManyThrough('App\Models\Request', 'App\Models\County')
                ->where('requests.created_at', '<', Carbon::now()->subDays($days))
                ->where('requests.status', 'Open')
                ->count();
        }
    }

    // TEMP TEMP TEMP - best way to get children served.
    public function childrenServed()
    {
        return -1;
    }

    public function churches()
    {
        return $this->hasManyThrough('App\Models\Church', 'App\Models\RegionLocation',
            'region_id', 'zip_code_id', 'id', 'zip_code_id')
            ->whereRaw('churches.county_id = region_locations.county_id');

    }

    public function groups()
    {

        if (count($this->zipCodes)) {
            $groups = $this->hasManyThrough('App\Models\Group', 'App\Models\ZipCode')
                ->whereRaw('groups.county_id not in (select id from counties where region_id is not null)');
        } else {
            $groups = $this->hasManyThrough('App\Models\Group', 'App\Models\County');
        }

        return $groups;
    }

    public function regionalManager()
    {
        return $this->hasOne('App\User', 'id', 'regional_manager_id')
            ->withDefault(['last_name' => '', 'first_name' => '']);
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'regional_manager_id', 'id');
    }

    public function scopeManagers()
    {
        $ids = DB::table('regions')->pluck('regional_manager_id');
        return $ids;

    }

    // TEMP TEMP TEMP - Not sure if this is even needed, but if so, would you do it this way?
    public function getImplementingPartnerAttribute()
    {
        return $this->regionalManager->implementing_partner;
    }

    public function implementingPartners()
    {
        return $this->belongsToMany('App\Models\ImplementingPartner');
    }

    // TEMP TEMP TEMP - is there a better way
    public function agencies()
    {


        return Agency::whereIn('id',
            DB::table('agency_counties')
                ->select('agency_counties.agency_id')
                ->join('region_locations', 'region_locations.county_id', 'agency_counties.county_id')
                ->where('region_locations.region_id', $this->id)
                ->distinct('agency_counties.agency_id')
        );

        /* if (count($this->zipCodes)) {
              return Agency::fromQuery(
                  DB::table('agencies')
                  ->select('agencies.*')->distinct()
                  ->join('agency_counties', 'agency_counties.agency_id', 'agencies.id')
                  ->join('county_zip_code', 'agency_counties.county_id', 'county_zip_code.county_id')
                  ->join('zip_codes', 'county_zip_code.zip_code_id', 'zip_codes.id')
                  ->whereRaw('zip_codes.region_id='.$this->id)->toSql());
          }
          else {
              return Agency::fromQuery(
                  DB::table('agencies')
                      ->select('agencies.*')->distinct()
                      ->join('agency_counties', 'agency_counties.agency_id', 'agencies.id')
                      ->join('counties', 'agency_counties.county_id', 'counties.id')
                      ->whereRaw('counties.region_id='.$this->id)->toSql());
          }*/
    }


    public function getContactTypeCounts()
    {
        return array(
            'pastorCnt' => $this->pastorCnt,
            'ppCnt' => $this->ppCnt,
            'fpCnt' => $this->fpCnt,
            'sgaCnt' => $this->sgaCnt
        );
    }



    // If the county has the region, that's the one
    // If the county isn't in a region, then check the zip (with state)
    public static function findWithAddress($state_id, $county_id, $zip_code_id)
    {
        $county = County::with('region')->find($county_id);
        if (!empty($county->region)) {
            return $county->region;
        }

        $zipCode = ZipCode::with('region')->find($zip_code_id);
        if (!empty($zipCode->region) && $zipCode->region->state_id === $state_id) {
            return $zipCode->region;
        }

        return null;
    }

    /**
     * @param Builder $builder
     * @param $q
     * @return Builder
     */
    public function scopeFiltered(Builder $builder, $q)
    {
        return $builder->when(!empty($q), function ($builder) use ($q) {
            return $builder
                ->orWhere('name', $q)
                ->orWhereHas('regionalManager', function ($builder) use ($q) {
                    $names = explode(' ', $q);
                    return $builder
                        ->whereIn('first_name', $names)
                        ->orWhere(function ($query) use ($names) {
                            $query->whereIn('last_name', $names);
                        });
                });
        });
    }

    /**
     * @return HasMany
     */
    public function givingChurches()
    {
        return $this->churches()->giving();
    }

    /**
     * @return HasMany
     */
    public function requestsLastMonth()
    {
        return $this->requests()->whereDate('requests.created_at', '>=', Carbon::now()->subDays(30));
    }

    /**
     * @return HasMany
     */
    public function completedRequests()
    {
        return $this->requests()
            ->completed();
    }

    /**
     * @return mixed
     */
    public function getCompletedRequestsAttribute()
    {
        if (is_null($this->completedRequestsCalculated)) {
            $this->completedRequestsCalculated = $this->completedRequests()->get();
        }

        return $this->completedRequestsCalculated;
    }


    /**
     * @return HasMany
     */
    public function completedRequestsLastYear()
    {
        return $this->completedRequests()
            ->where('requests.created_at', '>', Carbon::now()->subYear());
    }


    /**
     * @return array
     */
    public function getYearlyStats()
    {
        $children_served = $this->completedRequestsLastYear->sum('children_served');
        $estimated_value = $this->completedRequestsLastYear->sum('estimated_value');
        $cost_avoidance = $this->completedRequestsLastYear->sum('cost_avoidance');

        return compact('children_served', 'estimated_value', 'cost_avoidance', 'economic_impact');
    }


    /**
     * @return float|int
     */
    public function getAverageChildrenServedPerMonthAttribute()
    {
        $months = $this->completedRequests
            ->groupBy(function ($request) {
                return $request->created_at->format('Y-m');
            })
            ->map(function ($row) {
                return $row->sum('children_served');
            });

        $monthCount = $months->count();

        if ($monthCount < 1) {
            return 0;
        }


        $sum = array_reduce($months->toArray(), function ($sum, $monthSum) {
            return $sum + $monthSum;
        });

        return $sum / $monthCount;
    }

    /**
     * @return Community
     */
    public function communities()
    {
        return Community::whereIn('id',
            DB::table('community_locations')
                ->select('community_locations.community_id')
                ->join('region_locations', 'region_locations.county_id', 'community_locations.county_id')
                ->where('region_locations.region_id', $this->id)
                ->whereRaw('region_locations.zip_code_id = community_locations.zip_code_id')
                ->distinct('community_locations.community_id')
        );
    }

    /**
     * @return mixed
     */
    public function getTotalChildrenServedAttribute()
    {
        return $this->completedRequests->sum('children_served');
    }

    private $pastorCnt = -1;
    private $ppCnt = -1;
    private $fpCnt = -1;
    private $sgaCnt = -1;

}
