<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestNeed extends Model
{
    protected $guarded = [];

    protected $casts = [
        'met' => 'boolean'
    ];

    protected $attributes = [
        'met' => false
    ];

    //
    public function request(){
        return $this->belongsTo('App\Models\Request');
    }
}
