<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ImplementingPartnerRegion extends Pivot
{
    //
    protected $table = 'implementing_partner_region';
}
