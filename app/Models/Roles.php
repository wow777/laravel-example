<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public static function getRoles()
	{
		return [
			'0' => ['id' => '1', 'role' => 'Role 1'], 
			'1' => ['id' => '2', 'role' => 'Role 2'],
			'2' => ['id' => '3', 'role' => 'Role 3']
		];
	}
}
