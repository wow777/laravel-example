<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use DB;

class AgencyCounty extends Pivot
{
    protected $table = 'agency_counties';

    public function county()
    {
        return $this->belongsTo('App\Models\County');
    }

    public function reps()
    {
        return DB::table('rep_counties')
            ->select('users.first_name', 'users.last_name', 'users.email', 'users.phone')
            ->join('agency_worker', 'agency_worker.id', 'rep_counties.rep_id')
            ->join('users', 'users.id', 'agency_worker.user_id')
            ->where('rep_counties.agency_id', $this->agency_id)
            ->distinct('users.id')
            ->get()
            ;
//        return $this->belongsToMany('App\Models\RepCounty', 'null', 'agency_id')
//            ->whereRaw('rep_counties.county_id = agency_counties.county_id');
    }
}
