<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    public function zipCode()
    {
        return $this->belongsTo('App\Models\ZipCode');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function counties()
    {
        return $this->hasManyThrough('App\Models\County', 'App\Models\AgencyCounty', 'agency_id',
            'id', 'id', 'county_id');
    }

    public function agency_counties(){
        return $this->hasMany('App\Models\AgencyCounty')->with('county');
    }

    public function getTierApproval()
    {
        $tierApproval = array();
        if ($this->tier_1_approval) {
            $tierApproval[] = '1';
        }

        if ($this->tier_2_approval) {
            $tierApproval[] = '2';
        }

        if ($this->tier_3_approval) {
            $tierApproval[] = '3';
        }

        return implode(',', $tierApproval);
    }

    public function requests() {
        return $this->hasMany('App\Models\Request');
    }

    public function getRequestCountAttribute() {

        return $this->requests()
            ->where('status', '!=', 'Pending')
            ->where('status', '!=', 'Declined')
            ->count();
    }

    public function getOpenRequestCountAttribute() {
        return $this->requests()
            ->where('status', '=', 'Open')
            ->count();
    }
	
    public static function getAgencies() {
        return [
			'0' => ['id' => '1', 'agency_name' => 'Youth for Christ', 'city' => 'New York', 'state' => 'Alaska', 'phone_number' => '1234567890', 'tier_approval' => '1', 'approval_option' => '2', 'open_requests' => '0', 'requests_made' => '444', 'status' => 'Active', 'designated_agency_email' => 'agency1@www.com', 'agency_website' => 'http://www.example1.com', 'agency_email_domain' => 'abcd1@www.com', 'first_name' => 'First Name 1', 'last_name' => 'Last Name 1', 'agency_title' => 'New Agency 1', 'director_email' => 'xyz1@www.com', 'address1' => '11 abc india', 'address2' => '123 abc india', 'zip' => '12000', 'agencies_approval_option' => 'Approval Option 1'],
			'1' => ['id' => '2', 'agency_name' => 'Young Life', 'city' => 'Los Angeles', 'state' => 'Kansas', 'phone_number' => '2345678901', 'tier_approval' => '2', 'approval_option' => '3', 'open_requests' => '0', 'requests_made' => '333', 'status' => 'Deactivated', 'designated_agency_email' => 'agency2@www.com', 'agency_website' => 'http://www.example2.com', 'agency_email_domain' => 'abcd2@www.com', 'first_name' => 'First Name 2', 'last_name' => 'Last Name 2', 'agency_title' => 'New Agency 2', 'director_email' => 'xyz2@www.com', 'address1' => '12 abc india', 'address2' => '124 abc india', 'zip' => '12111', 'agencies_approval_option' => 'Approval Option 2'],
			'2' => ['id' => '3', 'agency_name' => 'Wycliffe Bible Translators', 'city' => 'Chicago', 'state' => 'Arizona', 'phone_number' => '3456789012', 'tier_approval' => '3', 'approval_option' => '4', 'open_requests' => '0', 'requests_made' => '222', 'status' => 'Deactivated', 'designated_agency_email' => 'agency3@www.com', 'agency_website' => 'http://www.example3.com', 'agency_email_domain' => 'abcd3@www.com', 'first_name' => 'First Name 3', 'last_name' => 'Last Name 3', 'agency_title' => 'New Agency 3', 'director_email' => 'xyz3@www.com', 'address1' => '13 abc india', 'address2' => '125 abc india', 'zip' => '12222', 'agencies_approval_option' => 'Approval Option 3'],
			'3' => ['id' => '4', 'agency_name' => 'Scripture Union', 'city' => 'Houston', 'state' => 'Montana', 'phone_number' => '4567890123', 'tier_approval' => '1,3', 'approval_option' => '1', 'open_requests' => '0', 'requests_made' => '111', 'status' => 'Active', 'designated_agency_email' => 'agency4@www.com', 'agency_website' => 'http://www.example4.com', 'agency_email_domain' => 'abcd4@www.com', 'first_name' => 'First Name 4', 'last_name' => 'Last Name 4', 'agency_title' => 'New Agency 4', 'director_email' => 'xyz4@www.com', 'address1' => '14 abc india', 'address2' => '125 abc india', 'zip' => '12333', 'agencies_approval_option' => 'Approval Option 4'],
			'4' => ['id' => '5', 'agency_name' => 'Prison Fellowship', 'city' => 'San Antonio', 'state' => 'Georgia', 'phone_number' => '5678901234', 'tier_approval' => '1,2', 'approval_option' => '5', 'open_requests' => '0', 'requests_made' => '115', 'status' => 'Deactivated', 'designated_agency_email' => 'agency5@www.com', 'agency_website' => 'http://www.example5.com', 'agency_email_domain' => 'abcd5@www.com', 'first_name' => 'First Name 5', 'last_name' => 'Last Name 5', 'agency_title' => 'New Agency 5', 'director_email' => 'xyz5@www.com', 'address1' => '15 abc india', 'address2' => '125 abc india', 'zip' => '12444', 'agencies_approval_option' => 'Approval Option 5'], 
			'5' => ['id' => '6', 'agency_name' => 'Christian Research Institute', 'city' => 'Jacksonville', 'state' => 'Missouri', 'phone_number' => '6789012345', 'tier_approval' => '2', 'approval_option' => '6', 'open_requests' => '0', 'requests_made' => '214', 'status' => 'Active', 'designated_agency_email' => 'agency6@www.com', 'agency_website' => 'http://www.example6.com', 'agency_email_domain' => 'abcd6@www.com', 'first_name' => 'First Name 6', 'last_name' => 'Last Name 6', 'agency_title' => 'New Agency 6', 'director_email' => 'xyz2@www.com', 'address1' => '16 abc india', 'address2' => '126 abc india', 'zip' => '12555', 'agencies_approval_option' => 'Approval Option 6']
		];
    }
	
	public static function agencyTierApproval() {
		return ['tier_1_approval' => '10', 'tier_2_approval' => '20', 'tier_3_approval' => '7', 'pending_cnt' => '40'];
	}
	
    public static function getCounties() {
        return [
			'0' => ['id' => '1', 'state' => 'NJ', 'county_name' => 'New Jersey', 'approved_tier1' => 'No', 'estimated_launch_date_tier1' => '04/03/2018', 'rep_tier1' => ['0' => ['agency_rep_email_tier1' => 'a1@gmail.com', 'first_name_tier1' => 'Test', 'last_name_tier1' => 'Demo', 'phone_number_tier1' => '1234567890']],
			'approved_tier2' => 'Yes', 'estimated_launch_date_tier2' => '03/03/2018', 'rep_tier2' => ['0' => ['agency_rep_email_tier2' => 'example21@gmail.com', 'first_name_tier2' => 'First Name 21', 'last_name_tier2' => 'Last Name 21', 'phone_number_tier2' => '1234567890'], '1' => ['agency_rep_email_tier2' => 'example22@gmail.com', 'first_name_tier2' => 'First Name 22', 'last_name_tier2' => 'Last Name 22', 'phone_number_tier2' => '1234567890']], 'service_tier2' => ['family_social' => ['family_team_meeting_tier2' => 'Family Social', 'background_agency_yes_no_tier2' => 'Background Agency Yes', 'background_contact_email_tier2' => 'example221@gmail.com', 'background_contact_first_name_tier2' => 'First Name 221', 'background_contact_last_name_tier2' => 'Last Name 221', 'background_contact_phone_number_tier2' => '1234567890', 'agencies_yes_no_tier2' => 'Yes', 'training_agency_yes_no_tier2' => 'Training Agency Yes', 'training_contact_email_tier2' => 'example222@gmail.com', 'training_contact_first_name_tier2' => 'First Name 222', 'training_contact_last_name_tier2' => 'Last Name 222', 'training_contact_phone_number_tier2' => '1234567890'], 'child_care' => ['child_care_tier2' => 'Child Care', 'childcare_agency_yes_no_tier2' => 'Background Agency Yes', 'childcare_contact_email_tier2' => 'example223@gmail.com', 'childcare_contact_first_name_tier2' => 'First Name 223', 'childcare_contact_last_name_tier2' => 'Last Name 223', 'childcare_contact_phone_number_tier2' => '1234567890', 'childcare_agencies_yes_no_tier2' => 'Yes', 'childcare_training_agency_yes_no_tier2' => 'Training Agency Yes', 'childcare_training_contact_email_tier2' => 'example224@gmail.com', 'childcare_training_contact_first_name_tier2' => 'First Name 224', 'childcare_training_contact_last_name_tier2' => 'Last Name 224', 'childcare_training_contact_phone_number_tier2' => '1234567890'], 'transportation' => ['transportation_tier2' => 'Transportation', 'transportation_agency_yes_no_tier2' => 'Background Agency Yes', 'transportation_contact_email_tier2' => 'example225@gmail.com', 'transportation_contact_first_name_tier2' => 'First Name 225', 'transportation_contact_last_name_tier2' => 'Last Name 225', 'transportation_contact_phone_number_tier2' => '1234567890', 'transportation_agencies_yes_no_tier2' => 'Yes', 'transportation_training_agency_yes_no_tier2' => 'Training Agency Yes', 'transportation_training_contact_email_tier2' => 'example226@gmail.com', 'transportation_training_contact_first_name_tier2' => 'First Name 226', 'transportation_training_contact_last_name_tier2' => 'Last Name 226', 'transportation_training_contact_phone_number_tier2' => '1234567890']], 
			'approved_tier3' => 'Yes', 'estimated_launch_date_tier3' => '02/03/2018', 'rep_tier3' => ['0' => ['agency_rep_email_tier3' => 'example31@gmail.com', 'first_name_tier3' => 'First Name 31', 'last_name_tier3' => 'Last Name 31', 'phone_number_tier3' => '1234567890'], '1' => ['agency_rep_email_tier3' => 'example1@gmail.com', 'first_name_tier3' => 'First Name 32', 'last_name_tier3' => 'Last Name 32', 'phone_number_tier3' => '1234567890'], '2' => ['agency_rep_email_tier3' => 'example33@gmail.com', 'first_name_tier3' => 'First Name 33', 'last_name_tier3' => 'Last Name 33', 'phone_number_tier3' => '1234567890']], 'service_tier3' => ['non_licensed_home_tier3' => 'Non-licensed', 'background_agency_yes_no_tier3' => 'Background Agency Yes', 'background_contact_email_tier3' => 'example331@gmail.com', 'background_contact_first_name_tier3' => 'First Name 331', 'background_contact_last_name_tier3' => 'Last Name 331', 'background_contact_phone_number_tier3' => '1234567890', 'agencies_yes_no_tier3' => 'Yes', 'walk_through_agency_yes_no_tier3' => 'Walk-Through Agency Yes', 'walk_through_contact_email_tier3' => 'example332@gmail.com', 'walk_through_contact_first_name_tier3' => 'First Name 332', 'walk_through_contact_last_name_tier3' => 'Last Name 332', 'walk_through_contact_phone_number_tier3' => '1234567890', 'training_agency_yes_no_tier3' => 'Training Agency Yes', 'training_contact_email_tier3' => 'example333@gmail.com', 'training_contact_first_name_tier3' => 'First Name 333', 'training_contact_last_name_tier3' => 'Last Name 333', 'training_contact_phone_number_tier3' => '1234567890', 'walk_through_agency_contact_tier3' => 'Churche walk-through Agency'], 'status' => 'Active', 'activation_date' => '02/07/2018'],
			
			'1' => ['id' => '2', 'state' => 'MT', 'county_name' => 'Montana', 'approved_tier1' => 'Yes', 'estimated_launch_date_tier1' => '04/03/2018', 'rep_tier1' => ['0' => ['agency_rep_email_tier1' => 'a2@gmail.com', 'first_name_tier1' => 'Union', 'last_name_tier1' => 'Demo', 'phone_number_tier1' => '5454567890']],
			'approved_tier2' => 'Yes', 'estimated_launch_date_tier2' => '03/03/2018', 'rep_tier2' => ['0' => ['agency_rep_email_tier2' => 'example21@gmail.com', 'first_name_tier2' => 'First Name 21', 'last_name_tier2' => 'Last Name 21', 'phone_number_tier2' => '1234567890'], '1' => ['agency_rep_email_tier2' => 'example22@gmail.com', 'first_name_tier2' => 'First Name 22', 'last_name_tier2' => 'Last Name 22', 'phone_number_tier2' => '1234567890']], 'service_tier2' => ['family_social' => ['family_team_meeting_tier2' => 'Family Social', 'background_agency_yes_no_tier2' => 'Background Agency Yes', 'background_contact_email_tier2' => 'example221@gmail.com', 'background_contact_first_name_tier2' => 'First Name 221', 'background_contact_last_name_tier2' => 'Last Name 221', 'background_contact_phone_number_tier2' => '1234567890', 'agencies_yes_no_tier2' => 'Yes', 'training_agency_yes_no_tier2' => 'Training Agency Yes', 'training_contact_email_tier2' => 'example222@gmail.com', 'training_contact_first_name_tier2' => 'First Name 222', 'training_contact_last_name_tier2' => 'Last Name 222', 'training_contact_phone_number_tier2' => '1234567890'], 'child_care' => ['child_care_tier2' => 'Child Care', 'childcare_agency_yes_no_tier2' => 'Background Agency Yes', 'childcare_contact_email_tier2' => 'example223@gmail.com', 'childcare_contact_first_name_tier2' => 'First Name 223', 'childcare_contact_last_name_tier2' => 'Last Name 223', 'childcare_contact_phone_number_tier2' => '1234567890', 'childcare_agencies_yes_no_tier2' => 'Yes', 'childcare_training_agency_yes_no_tier2' => 'Training Agency Yes', 'childcare_training_contact_email_tier2' => 'example224@gmail.com', 'childcare_training_contact_first_name_tier2' => 'First Name 224', 'childcare_training_contact_last_name_tier2' => 'Last Name 224', 'childcare_training_contact_phone_number_tier2' => '1234567890']], 
			'approved_tier3' => 'Yes', 'estimated_launch_date_tier3' => '02/03/2018', 'rep_tier3' => ['0' => ['agency_rep_email_tier3' => 'example31@gmail.com', 'first_name_tier3' => 'First Name 31', 'last_name_tier3' => 'Last Name 31', 'phone_number_tier3' => '1234567890'], '1' => ['agency_rep_email_tier3' => 'example1@gmail.com', 'first_name_tier3' => 'First Name 32', 'last_name_tier3' => 'Last Name 32', 'phone_number_tier3' => '1234567890'], '2' => ['agency_rep_email_tier3' => 'example33@gmail.com', 'first_name_tier3' => 'First Name 33', 'last_name_tier3' => 'Last Name 33', 'phone_number_tier3' => '1234567890']], 'service_tier3' => ['non_licensed_home_tier3' => 'Non-licensed', 'background_agency_yes_no_tier3' => 'Background Agency Yes', 'background_contact_email_tier3' => 'example331@gmail.com', 'background_contact_first_name_tier3' => 'First Name 331', 'background_contact_last_name_tier3' => 'Last Name 331', 'background_contact_phone_number_tier3' => '1234567890', 'agencies_yes_no_tier3' => 'Yes', 'walk_through_agency_yes_no_tier3' => 'Walk-Through Agency Yes', 'walk_through_contact_email_tier3' => 'example332@gmail.com', 'walk_through_contact_first_name_tier3' => 'First Name 332', 'walk_through_contact_last_name_tier3' => 'Last Name 332', 'walk_through_contact_phone_number_tier3' => '1234567890', 'training_agency_yes_no_tier3' => 'Training Agency Yes', 'training_contact_email_tier3' => 'example333@gmail.com', 'training_contact_first_name_tier3' => 'First Name 333', 'training_contact_last_name_tier3' => 'Last Name 333', 'training_contact_phone_number_tier3' => '1234567890', 'walk_through_agency_contact_tier3' => 'Churche walk-through Agency'], 'status' => 'Active', 'activation_date' => '03/07/2018'],
			
			'2' => ['id' => '3', 'state' => 'MO', 'county_name' => 'Missouri', 'approved_tier1' => 'No', 'estimated_launch_date_tier1' => '04/03/2018', 'rep_tier1' => ['0' => ['agency_rep_email_tier1' => 'a3@gmail.com', 'first_name_tier1' => 'Group', 'last_name_tier1' => 'Demo', 'phone_number_tier1' => '3223567890']],
			'approved_tier2' => 'Yes', 'estimated_launch_date_tier2' => '03/03/2018', 'rep_tier2' => ['0' => ['agency_rep_email_tier2' => 'example21@gmail.com', 'first_name_tier2' => 'First Name 21', 'last_name_tier2' => 'Last Name 21', 'phone_number_tier2' => '1234567890'], '1' => ['agency_rep_email_tier2' => 'example22@gmail.com', 'first_name_tier2' => 'First Name 22', 'last_name_tier2' => 'Last Name 22', 'phone_number_tier2' => '1234567890']], 'service_tier2' => ['family_social' => ['family_team_meeting_tier2' => 'Family Social', 'background_agency_yes_no_tier2' => 'Background Agency Yes', 'background_contact_email_tier2' => 'example221@gmail.com', 'background_contact_first_name_tier2' => 'First Name 221', 'background_contact_last_name_tier2' => 'Last Name 221', 'background_contact_phone_number_tier2' => '1234567890', 'agencies_yes_no_tier2' => 'Yes', 'training_agency_yes_no_tier2' => 'Training Agency Yes', 'training_contact_email_tier2' => 'example222@gmail.com', 'training_contact_first_name_tier2' => 'First Name 222', 'training_contact_last_name_tier2' => 'Last Name 222', 'training_contact_phone_number_tier2' => '1234567890']], 
			'approved_tier3' => 'Yes', 'estimated_launch_date_tier3' => '02/03/2018', 'rep_tier3' => ['0' => ['agency_rep_email_tier3' => 'example31@gmail.com', 'first_name_tier3' => 'First Name 31', 'last_name_tier3' => 'Last Name 31', 'phone_number_tier3' => '1234567890'], '1' => ['agency_rep_email_tier3' => 'example1@gmail.com', 'first_name_tier3' => 'First Name 32', 'last_name_tier3' => 'Last Name 32', 'phone_number_tier3' => '1234567890'], '2' => ['agency_rep_email_tier3' => 'example33@gmail.com', 'first_name_tier3' => 'First Name 33', 'last_name_tier3' => 'Last Name 33', 'phone_number_tier3' => '1234567890']], 'service_tier3' => ['non_licensed_home_tier3' => 'Non-licensed', 'background_agency_yes_no_tier3' => 'Background Agency Yes', 'background_contact_email_tier3' => 'example331@gmail.com', 'background_contact_first_name_tier3' => 'First Name 331', 'background_contact_last_name_tier3' => 'Last Name 331', 'background_contact_phone_number_tier3' => '1234567890', 'agencies_yes_no_tier3' => 'Yes', 'walk_through_agency_yes_no_tier3' => 'Walk-Through Agency Yes', 'walk_through_contact_email_tier3' => 'example332@gmail.com', 'walk_through_contact_first_name_tier3' => 'First Name 332', 'walk_through_contact_last_name_tier3' => 'Last Name 332', 'walk_through_contact_phone_number_tier3' => '1234567890', 'training_agency_yes_no_tier3' => 'Training Agency Yes', 'training_contact_email_tier3' => 'example333@gmail.com', 'training_contact_first_name_tier3' => 'First Name 333', 'training_contact_last_name_tier3' => 'Last Name 333', 'training_contact_phone_number_tier3' => '1234567890', 'walk_through_agency_contact_tier3' => 'Churche walk-through Agency'], 'status' => 'Deactivated', 'activation_date' => '01/07/2018'],
		];
    }
	
	public static function deleteAgencyTier($id){
		// Logic to delete the Agency County Tier goes here
		return true;
	}
}
