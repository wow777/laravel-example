<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{

    public function church()
    {
        return $this->belongsTo('App\Models\Church');
    }

    public function request(){
        return $this->belongsTo('App\Models\Request');
    }



}
