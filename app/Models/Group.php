<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    public $dates = ['created_at', 'updated_at', 'tier_1_enrollment_complete_date', 'deleted_at'];
    protected $guarded = [];

    public function church()
    {
        return $this->belongsTo('App\Models\Church');
    }

    public function zipCode()
    {
        return $this->belongsTo('App\Models\ZipCode');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function county()
    {
        return $this->belongsTo('App\Models\County');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot(
            't1_point_person',
            'primary_point_person',
            't2_point_person',
            't3_point_person'
        );
    }

    //
    public function getTierApproval()
    {
        $tierApproval = array();
        if ($this->tier_1_enrollment_complete) {
            $tierApproval[] = '1';
        }

        if ($this->tier_2_enrollment_complete) {
            $tierApproval[] = '2';
        }

        if ($this->tier_3_enrollment_complete) {
            $tierApproval[] = '3';
        }

        return implode(',', $tierApproval);
    }

    public function status()
    {
        return $this->tier_1_enrollment_complete ? 'Active' : 'N/A';
    }

    // TEMP TEMP TEMP - we should import the data so that there IS a primary point person
    // and going forward it should be required, don't just grab the first one if the flag isn't set
    public function getPrimaryPointPersonAttribute()
    {

        $users = $this->users;
        foreach($users as $user) {
            if($user->pivot->primary_point_person) {
                return $user;
            }
        }

        return $users->first();
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActivated(Builder $builder)
    {
        return $builder
            ->where('tier_1_enrollment_complete', true);
    }

    /**
     * @param Builder $builder
     * @param int $years
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeForYears(Builder $builder, $years = 0)
    {
        $minYear = $years > 0
            ? date('Y', strtotime('-' . ($years - 1) .' year'))
            : date('Y');
        return $builder->whereYear('tier_1_enrollment_complete_date', '>=', $minYear);
    }
}
