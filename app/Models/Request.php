<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

class Request extends Model
{
    protected $guarded = [];

    public $dates = ['created_at', 'updated_at', 'date_closed', 'approved_at'];

    protected $casts = [
        'responding_churches_not_listed' => 'boolean',
        'strictly_financial' => 'boolean',
    ];

    protected $attributes = [
        'status' => 'Open',
        'estimated_value' => 0,
        'cost_avoidance' => 0,
    ];

    public function agency()
    {
        return $this->belongsTo('App\Models\Agency');
    }

    public function worker()
    {
        return $this->belongsTo('App\User', 'worker_id');
    }

    public function zipCode()
    {
        return $this->belongsTo('App\Models\ZipCode');
    }

    public function county()
    {
        return $this->belongsTo('App\Models\County');
    }
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function needs(){
        return $this->hasMany('App\Models\RequestNeed');
    }

    public function recipients(){
        return $this->hasManyThrough('App\Models\RequestRecipient', 'App\Models\Church');
    }

    public function responses(){
        return $this->hasMany('App\Models\Response');
    }

    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }

    public function churchReview(){

        // TODO add relation has one responses -> church_reviews
        return DB::table('church_reviews')
            ->join('responses', 'responses.id', 'church_reviews.request_response_id')
            ->join('requests', 'requests.id', 'responses.request_id')
            ->where('requests.id', $this->id )
            ->first();

    }

    public function economicImpact()
    {
        return $this->estimated_value + $this->cost_avoidance;
    }

    public function daysOpen()
    {
        if ($this->status === 'Open') {
            return Carbon::now()->diffInDays($this->created_at);
        } else {
            if (empty($this->date_closed)) {
                return -1;
            } // TEMP TEMP TEMP - this shouldn't happen
            return $this->date_closed->diffInDays($this->created_at);
        }
    }

    /**
     * @param Builder $builder
     * @param int $years
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeForYears(Builder $builder, $years = 0)
    {
        $minYear = $years > 0
            ? date('Y', strtotime('-' . ($years - 1) .' year'))
            : date('Y');
        return $builder->whereYear('requests.created_at', '>=', $minYear);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeHasServedChildren(Builder $builder)
    {
        return $builder->where('children_served', '>', 0);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCompleted(Builder $builder)
    {
        return $builder->where('reason_closed', 'Church Responded and Met the Need');
    }
}
