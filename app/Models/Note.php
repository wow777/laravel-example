<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Note
 * @package App\Models
 */
class Note extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['type', 'body', 'notable_id', 'notable_type', 'author_id', 'author_type'];

    /**
     * Get all of the owning notable models.
     */
    public function notable()
    {
        return $this->morphTo();
    }

    /**
     * Get author.
     */
    public function author()
    {
        return $this->morphTo();
    }
}
