<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ChurchUser extends Pivot
{
    protected $table = 'church_user';

    protected $casts = [
        't1_point_person' => 'boolean',
        't2_point_person' => 'boolean',
        't3_point_person' => 'boolean',
        'pastor' => 'boolean',
        'finance_person' => 'boolean',
        'small_group_admin' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function church()
    {
        return $this->belongsTo(Church::class, 'church_id');
    }
}
