<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Church extends Model
{
    use SoftDeletes;

    public $dates = ['created_at', 'updated_at', 'tier_1_enrollment_complete_date', 'last_invoice_date', 'deleted_at'];

    protected $guarded = [];

    // TEMP TEMP TEMP - we should import the data so that there IS a primary point person
    // and going forward it should be required, don't just grab the first one if the flag isn't set
    public function primaryPointPerson()
    {
        $users = $this->users;
        foreach($users as $user) {
            if($user->pivot->primary_point_person) {
                return $user;
            }
        }

        return $users->first();
    }

    public function requests()
    {
        return $this->hasManyThrough('App\Models\Request', 'App\Models\RequestRecipient', null, 'id', null, 'request_id');
    }

    public function responses()
    {
        return $this->hasMany('App\Models\Response');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\ChurchReview');
    }

    public function zipCode()
    {
        return $this->belongsTo('App\Models\ZipCode');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function county()
    {
        return $this->belongsTo('App\Models\County');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region', 'state_id', 'state_id');
    }

    public function enrollmentRequest()
    {
        return $this->hasOne(EnrollmentRequest::class, 'church_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot(
            't1_point_person',
            'primary_point_person',
            't2_point_person',
            't3_point_person',
            'pastor',
            'finance_person'
        );
    }

    //
    public function getTierApproval()
    {
        $tierApproval = array();
        if ($this->tier_1_enrollment_complete) {
            $tierApproval[] = '1';
        }

        if ($this->tier_2_enrollment_complete) {
            $tierApproval[] = '2';
        }

        if ($this->tier_3_enrollment_complete) {
            $tierApproval[] = '3';
        }

        return implode(',', $tierApproval);
    }

    public function status()
    {
        if ($this->tier_1_enrollment_complete) {
            return 'Active';
        } else {
            if ($this->activation_step_2_complete) {
                return 'Step 3';
            } else {
                if ($this->activation_step_1_complete) {
                    return 'Step 2';
                } else {
                    if ($this->has_enrolled) {
                        return 'Step 1';
                    } else {
                        return 'N/A';
                    }
                }
            }
        }
    }

    public function getResponseCountAttribute() {
        return $this->responses->count();
    }

    public function getRequestsMetAttribute() {
        return $this->reviews->where('verify_did_you_help', true)->count();
    }

    /**
     * Returns the church's regional manager
     *
     * @return \App\User
     */
    public function getRegionalManagerAttribute()
    {
        $region = $this->load('region')->region;

        return $region->regionalManager;
    }

    /**
     * Returns the church's primary point person
     *
     * @return \App\User
     */
    public function getPrimaryPointPersonAttribute()
    {

        $users = $this->users;
        foreach($users as $user) {
            if($user->pivot->primary_point_person) {
                return $user;
            }
        }

        return $users->first();
    }

    /**
     * Returns the church's pastor
     *
     * @return \App\User
     */
    public function getPastorAttribute()
    {
        /**
         * @var Collection $users
         */
        $users = $this->users;

        /**
         * @var \App\User $pastor
         */
        $pastor = $users->first(function ($value) {
            return $value->pivot->pastor;
        });

        return $pastor;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActivated(Builder $builder)
    {
        return $builder
            ->where('tier_1_enrollment_complete', true)
            ->where('has_enrolled', true);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopePendingActivation(Builder $builder)
    {
        return $builder
            ->where('tier_1_enrollment_complete', false)
            ->where('has_enrolled', true)
            ;//->where('attended_orientation', 1)
    }

    /**
     * @param Builder $builder
     * @param int $years
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeForYears(Builder $builder, $years = 0)
    {
        $minYear = $years > 0
            ? date('Y', strtotime('-' . ($years - 1) .' year'))
            : date('Y');
        return $builder->whereYear('tier_1_enrollment_complete_date', '>=', $minYear);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeGiving(Builder $builder)
    {
        return $builder->where('giving_amount', '>', 0);
    }
}
