<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityLocation extends Model
{
    public function community(){
        return $this->belongsTo('App\Models\Community');
    }


}
