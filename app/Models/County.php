<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $hidden = ['polygon_data'];

    //protected $with = ['state'];

    protected $dates = ['launch_date'];

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function zipCodes()
    {
        return $this->hasManyThrough('App\Models\ZipCode', 'App\Models\CountyZipCode',
            'county_id', 'id', '', 'zip_code_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFiltered(Builder $builder, $q)
    {
        return $builder->when(!empty($q), function (Builder $query) use ($q) {
                return $query
                    ->orWhere('name', $q)
                    ->orWhere('status', $q);
            });
    }
}
