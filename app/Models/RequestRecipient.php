<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestRecipient extends Model
{

    protected $table = 'request_recipients';


    public function churches()
    {
        return $this->belongsTo('App\Models\Church','church_id','id');
    }

}
