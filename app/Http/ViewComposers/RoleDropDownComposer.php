<?php

namespace App\Http\ViewComposers;


use App\Models\Gms;
use App\Models\Region;
use App\Models\State;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class RoleDropDownComposer
 * @package App\Http\ViewComposers
 */
class RoleDropDownComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $roles = collect([]);

        $currentRole = request()->attributes->get('role');
        $currentUser = Auth::user();

        foreach ($currentUser->roles() as $role) {
            $stdRole = new \stdClass();
            $stdRole->name = $role;
            if($role == 'Regional Manager') {
                $region = $currentUser->regions->first();
                $stdRole->route = route('role.dashboard', ['role' => 'regional_manager', 'role_id' => $region->id]) ;
            } elseif($role == 'State Director') {
                $state = $currentUser->states->first();
                $stdRole->route = route('role.dashboard', ['role' => 'state_director', 'role_id' => $state->id]);
            } elseif($role == 'GMS') {
                $stdRole->route = route('role.dashboard', ['gms', Auth::id()]);
            } else{
                $stdRole->route = '#';
            }

            $stdRole->selected = ($role == 'Regional Manager' && $currentRole instanceof Region)
                || ($role == 'State Director' && $currentRole instanceof State)
                || ($role == 'GMS' && $currentRole instanceof Gms);

            $roles->push($stdRole);
        }


        $states = collect([]);
        $regions = collect([]);

        if($currentRole instanceof Region) {
            $regions = $currentUser->regions;
        } elseif ($currentRole instanceof State) {
            $states = $currentUser->states;
        }

        $view->with(compact('currentRole', 'roles', 'states', 'regions'));
    }
}