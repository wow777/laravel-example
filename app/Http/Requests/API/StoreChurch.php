<?php

namespace App\Http\Requests\API;

class StoreChurch extends APIFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => 'required',
            "address_line1" => 'required',
            "city" => 'required',
            "state" => 'required',
            "county" => 'required',
            "zip_code" => 'required',

        ];
    }

}
