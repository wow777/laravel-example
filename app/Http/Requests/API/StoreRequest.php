<?php

namespace App\Http\Requests\API;

class StoreRequest extends APIFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "status" => 'required',
            "guid" => 'required',
            "role_of_worker" => 'required',
            "tier" => 'required',
            "zip_code" => 'required',
            "agency_case_id" => 'required',
            "purpose_of_request" => 'required',
            "case_description" => 'required',
        ];
    }

}