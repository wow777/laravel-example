<?php

namespace App\Http\Requests\API;

class StoreGroup extends APIFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => 'required',
            "address_line1" => 'required',
            "state" => 'required',
            "city" => 'required',
            "zip_code" => 'required',
            "church_id" => 'required',
            "initial_enrollment_phone" => 'required',
            "initial_enrollment_email" => 'required',
            "initial_enrollment_name" => 'required',
        ];
    }

}
