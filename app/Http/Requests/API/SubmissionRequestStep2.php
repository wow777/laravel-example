<?php

namespace App\Http\Requests\API;

class SubmissionRequestStep2 extends APIFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_id' => 'required|numeric|exists:requests,id',
            'tier' => 'required|integer|min:1|max:3',
            'case_description' => 'required|string|max:10000',

            // tier 1
            'level_of_urgency' => 'required_if:tier,1|integer|min:0|max:2',
            'children_served' => 'required_if:tier,1|integer|min:1|max:20',
            'estimated_value' => 'required_if:tier,1|integer|min:1|max:10000',
            'strictly_financial' => 'required_if:tier,1|boolean',

            // tier 2
            'tier2_service_type_id' => 'required_if:tier,2|numeric|exists:tier2_service_types,id',
            'service_hours_hours_per' => 'required_if:tier,2|integer|min:1',
            'service_hours_times_per_week' => 'required_if:tier,2|integer|min:1',
            'service_hours_total_weeks' => 'required_if:tier,2|integer|min:1',

            // tier 3
            'tier3_service_type_id' => 'required_if:tier,2|numeric|exists:tier3_service_types,id',
        ];
    }
}
