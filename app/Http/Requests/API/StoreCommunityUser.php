<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommunityUser extends APIFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'exists:users,id',
            'communities' => 'array|nullable',
            'communities.id' => 'exists:communities,id',

            'email' => 'bail|email|required_without:user_id|unique:users,email',
            'first_name' => 'required_with:email|string|max:100',
            'last_name' => 'required_with:email|string|max:100',

            'organization' => 'required|string|max:100',
            'phone' => 'required|phone:US',
            'zip_code' => 'required|string|max:5'
        ];
    }
}
