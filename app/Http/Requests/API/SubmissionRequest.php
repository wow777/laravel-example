<?php

namespace App\Http\Requests\API;

class SubmissionRequest extends APIFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required_without:email|integer|exists:users,id',

            'email' => 'required_without:user_id|email|unique:users,email',
            'first_name' => 'required_with:email|string|max:100',
            'last_name' => 'required_with:email|string|max:100',
            'phone' => 'required_with:email|phone:US',
            'county_id' => 'required_with:email|numeric|exists:counties,id',
            'agency_id' => 'required_with:email|numeric|exists:agencies,id',
            'office_zip_code' => 'string|max:5',

            'role_of_worker' => 'required|string',
            'case_zip_code' => 'required|string|max:5',
            'agency_case_id' => 'required|numeric',
            'purpose_of_request' => 'required|string',
            'tier' => 'required|numeric|max:3|min:1',
        ];
    }
}
