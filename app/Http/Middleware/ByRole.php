<?php

namespace App\Http\Middleware;

use App\Interfaces\RoleInterface;
use App\Models\Gms;
use App\Models\Region;
use App\Models\State;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class ByRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = $request->route()->parameter('role');
        $roleId = $request->route()->parameter('role_id');

        if( in_array($role , ['rm', 'regional_manager']) ) {
            $role = Region::find($roleId);
        } elseif(in_array($role, ['sd', 'state_director'] ) ){
            $role = State::find($roleId);
        } elseif ($role =='gms'){
            $role = Gms::find($roleId);
        }

        if(!$role instanceof RoleInterface) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $request->attributes->set('role', $role);

        return $next($request);
    }
}
