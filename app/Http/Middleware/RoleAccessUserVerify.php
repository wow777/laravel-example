<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class RoleAccessUserVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $user */
        $user = $request->route()->parameter('user');
        if(!currentRole()->hasContact($user)) {
            abort(Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }
}
