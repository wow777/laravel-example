<?php

namespace App\Http\Middleware;

use App\Models\Region;
use App\Models\State;
use Closure;
use Auth;

class CheckRole
{
    /*
     * Roles
     * regional manager
     * state director
     * gms
     * coordinator
     *
     * Implementing partner
     * ambassador
     *
     * */


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $path = explode('/', trim($request->getPathInfo(), '/') );

        // route prefix
        $type = $path[0];

        // id of instance
        $instance = $path[1];

        switch ($type){
            case 'rm':
                $this->region($request, $instance);
                break;
            case 'sd':
                $this->state($request, $instance);
                break;
            default:
                dd('Incorrect Type');
        }

        return $next($request);
    }

    // todo move permission rules to custom gate
    protected function region($request, $region_id){

        $region = Region::find($region_id);


        $user = $request->user();

        if($region->regional_manager_id == $user->id)  {

            return true;

        }
        else{
            dd('Unauthorized');
        }
    }

    protected function state(Request $request, $state_id){

        $state = State::find($state_id);

        $user = $request->user();

        if($state->state_director_id == $user->id)  {
            return true;
        }
        else{
            dd('Unauthorized');
        }
    }
}
