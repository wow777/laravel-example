<?php

namespace App\Http\Resources\Graph;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LineChart extends ResourceCollection
{

    /**
     * Predefined colors rgb
     * @var array
     */
    private static $predefinedColors = [
        [51, 122, 183],
        [0, 166, 90]
    ];

    /**
     * @param string $dateFieldKey
     * @param null|string $sumField
     * @return array
     */
    public function toArray($dateFieldKey, $sumField = null)
    {
        $predefinedColors = static::$predefinedColors;
        $dataSets = [];
        foreach ($this->collection as $collectionItem) {
            if(empty($collectionItem->{$dateFieldKey})) {
                continue;
            }
            $date = $collectionItem->{$dateFieldKey};
            $year = $date->format('Y');
            $month = $date->format('n');

            if(!isset($dataSets[$year])) {
                $color = array_shift($predefinedColors);
                $color = 'rgb('.implode(',', ($color ? $color : getColor($year))) .')';

                $dataSets[$year] = [
                    'label' => $year,
                    'fill' => false,
                    'backgroundColor' => $color,
                    'borderColor' => $color,
                    'data' => []
                ];
                for($i = 0; $i < 12; $i++) {
                    $dataSets[$year]['data'][$i] = 0;
                }
            }
            $dataSets[$year]['data'][$month - 1] += is_null($sumField) ? 1 : $collectionItem->{$sumField};
        }

        ksort($dataSets);
        return array_values($dataSets);
    }

    /**
     * @param string $dateFieldKey
     * @param string|null $sumField
     * @return array
     */
    public function toCSVArray($dateFieldKey, $sumField = null)
    {
        $data = [];
        foreach ($this->collection as $collectionItem) {
            if(empty($collectionItem->{$dateFieldKey})) {
                continue;
            }
            $date = $collectionItem->{$dateFieldKey}->format('F Y');

            if(!isset($data[$date])) {
                $data[$date] = [
                    $date,
                    0
                ];
            }
            $data[$date][1] += is_null($sumField) ? 1 : $collectionItem->{$sumField};
        }

        return array_values($data);
    }
}
