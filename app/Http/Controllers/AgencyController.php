<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Agency;
use App\Models\AgencyCounty;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class AgencyController extends Controller
{
    
    /**
     * Responsible to return the agency list
	 * param, q, string, searchKeyword
     */
    public function index(Request $request){
		// Retrieving all agency
		$agencies = Agency::getAgencies();
		// Retrieving all agency tier approval
        $agencyTierApproval = Agency::agencyTierApproval();
		$tier_1_cnt = $agencyTierApproval['tier_1_approval'];
        $tier_2_cnt = $agencyTierApproval['tier_2_approval'];
        $tier_3_cnt = $agencyTierApproval['tier_3_approval'];
        $pending_cnt = $agencyTierApproval['pending_cnt'];
		
		$searchKeyword   = $request->get('q');
        $agencyData    = array();
		
        if(!empty($searchKeyword)){
            foreach($agencies as $agency){
				// Check if keyword exist in the current agency
                if (strpos(strtoupper($agency['agency_name']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['city']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['state']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['phone_number']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['tier_approval']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['approval_option']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['open_requests']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($agency['requests_made']), strtoupper($searchKeyword)) !== false) {
                    $agencyData[] = $agency;
                }
            }
            $agencies = $agencyData;
        }
		
		$agency_id   = $request->get('agency_id');
		if(!empty($agency_id)){
			session()->flash('success', 'Agency has been successfully deactivated!');
		}
        return view('category.agency.index',
            compact('role', 'agencies', 'tier_1_cnt', 'tier_2_cnt', 'tier_3_cnt', 'pending_cnt'));
    }
	
    /**
     * Responsible to return the deactivated agency list
	 * return all agency if deactivated is not set
	 * return only deactivated agency if deactivated is set
     */
    public function deactivatedAgency(Request $request) {
        $deactivate = $request->get('deactivate');
        if(!empty($deactivate)){
            $agencyData = array();
			// Retrieving all agency
            $getAgencyData = Agency::getAgencies();
            if($deactivate == 'Deactivated'){
                foreach($getAgencyData as $agency){
                    if(trim($agency['status']) == 'Deactivated' && trim($agency['status']) != 'Active'){
                        $agencyData[] = $agency;
                    }
                }
                return $agencyData;
            }else{
                return $getAgencyData;
            }
        }
    }
	
	/**
     * Responsible to return the agency details
     */
	public function agenciesDetails(Request $request){
		$agency_id = $request->get('agency_id');
		$agencyData = array();
		if(!empty($agency_id)){
			// Retrieving all agency
			$getAgencyData = Agency::getAgencies();
			foreach($getAgencyData as $agency){
				if(trim($agency['id']) == $agency_id){
					$agencyData[] = $agency;
				}
			}
		}
		$countyData = Agency::getCounties();
        return view('category.agency.agency-details', compact('agency_id', 'agencyData', 'countyData'));
    }
	
    /**
     * Responsible to add or edit Agency
	 * param, id, int, Agency id
     */
	public function addEditAgency(Request $request){
		// Validate Agency Fields
		$validator = $this->validate($request, [
						'agency_name' 				=> 'required',
						'agency_status' 			=> 'required',
						'designated_agency_email' 	=> 'required|email',
						'agency_website' 			=> 'required',
						'agency_email_domain' 		=> 'required|email',
						'first_name' 				=> 'required',
						'last_name' 				=> 'required',
						'agency_title' 				=> 'required',
						'phone_number' 				=> 'required',
						'director_email' 			=> 'required|email',
						'address1' 					=> 'required',
						'city' 						=> 'required',
						'state' 					=> 'required',
						'zip' 						=> 'required',
						'agencies_approval_option' 	=> 'required',
					]);
		if ($validator){
			$base_url 					= '/role/'.request()->segment(2).'/'.request()->segment(3);
			$id 						= $request->get('agency_id');
			$agency_name 				= $request->get('agency_name');
			$agency_status 				= $request->get('agency_status');
			$designated_agency_email 	= $request->get('designated_agency_email');
			$agency_website 			= $request->get('agency_website');
			$agency_email_domain 		= $request->get('agency_email_domain');
			$first_name 				= $request->get('first_name');
			$last_name 					= $request->get('last_name');
			$agency_title 				= $request->get('agency_title');
			$phone_number 				= $request->get('phone_number');
			$director_email 			= $request->get('director_email');
			$address1 					= $request->get('address1');
			$address2 					= $request->get('address2');
			$city 						= $request->get('city');
			$state 						= $request->get('state');
			$zip 						= $request->get('zip');
			$agencies_approval_option 	= $request->get('agencies_approval_option');
			
			if(!empty($id)){
				// Agency edit time code here //
				session()->flash('success', 'Agency has been successfully edited!');
			}else{
				// Agency edit time code here //
				session()->flash('success', 'Agency has been successfully added!');
			}
			return redirect($base_url.'/agencies');
		}
    }
	
    /**
     * Responsible to load edit Agency screen
	 * param, id, int, Agency id
     */
	public function editAgency(Request $request){
		$base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
		$id = $request->get('id');
        return $id;
    }
	
	/**
     * Responsible to return the county details
     */
	public function countyDetails(Request $request){
		$county_id = $request->get('county_id');
		$agency_id = $request->get('agency_id');
		$countyData = array();
		if(!empty($county_id)){
			// Retrieving all county
			$getCountyData = Agency::getCounties();
			foreach($getCountyData as $county){
				if(trim($county['id']) == $county_id){
					$countyData[] = $county;
				}
			}
		}
        return view('category.agency.county-details', compact('agency_id', 'county_id', 'countyData'));
    }
	
    /**
     * Responsible to add or edit County
	 * param, id, int, County id
     */
	public function addEditCounty(Request $request){
		// Validate County Fields
		$validator = $this->validate($request, [
						'state' 						=> 'required',
						'county_name' 					=> 'required',
						'approved_tier1' 				=> 'required',
						'estimated_launch_date_tier1' 	=> 'required',
						'approved_tier2' 				=> 'required',
						'estimated_launch_date_tier2' 	=> 'required',
						'approved_tier3' 				=> 'required',
						'estimated_launch_date_tier3' 	=> 'required',
					]);
		if ($validator){
			$county_id = $request->get('county_id');
			$agency_id = $request->get('agency_id');
			$base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
			if(!empty($county_id)){
				// County edit time code here //
				session()->flash('success', 'County has been successfully edited!');
			}else{
				// County add time code here //
				session()->flash('success', 'County has been successfully added!');
			}
			return redirect($base_url.'/agencies/agency-details?agency_id='.$agency_id);
		}
	}
	
    /**
     * Responsible to return the deactivated county list
	 * return all county if deactivated is not set
	 * return only deactivated county if deactivated is set
     */
    public function deactivatedCounty(Request $request) {
        $deactivate = $request->get('deactivate');
        if(!empty($deactivate)){
            $countyData = array();
			// Retrieving all county
            $getCountyData = Agency::getCounties();
            if($deactivate == 'Deactivated'){
                foreach($getCountyData as $county){
                    if(trim($county['status']) == 'Deactivated' && trim($county['status']) != 'Active'){
                        $countyData[] = $county;
                    }
                }
                return $countyData;
            }else{
                return $getCountyData;
            }
        }
    }
	
	/**
     * Responsible to return the auto populate email.
     */
	public function autoPopulateEmailCounty(Request $request){
		$encripted_code = substr(md5(uniqid(rand(1,6))), 0, 8);
		$data = 'example'.$encripted_code.'@gmail.com';
		return $data;
	}
	
    /**
     * Responsible to load edit County screen
	 * param, id, int, County id
     */
	public function editCounty(Request $request){
		$base_url = '/role/'.request()->segment(2).'/'.request()->segment(3);
		$agency_id = $request->get('agency_id');
		$county_id = $request->get('county_id');
		$data = ['agency_id' => $agency_id, 'county_id' => $county_id];
        return $data;
	}
	
    /**
     * Responsible to delete the Agency County Tier1
	 * param, id, int, Agency County Tier1 id
	 * return, true if deleted
     */
    public function deleteAgencyTier1(Request $request) {
        $id = $request->get('id');
        if(!empty($id) && Agency::deleteAgencyTier($id)){
            echo 'true';
        }
    }
    
    /**
     * Responsible to delete the Agency County Tier2
	 * param, id, int, Agency County Tier2 id
	 * return, true if deleted
     */
    public function deleteAgencyTier2(Request $request) {
        $id = $request->get('id');
        if(!empty($id) && Agency::deleteAgencyTier($id)){
            echo 'true';
        }
    }
    
    /**
     * Responsible to delete the Agency County Tier3
	 * param, id, int, Agency County Tier3 id
	 * return, true if deleted
     */
    public function deleteAgencyTier3(Request $request) {
        $id = $request->get('id');
        if(!empty($id) && Agency::deleteAgencyTier($id)){
            echo 'true';
        }
    }
    
}
