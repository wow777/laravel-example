<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Resource;
use App\Models\Category;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class ContactsController
 * @package App\Http\Controllers
 */
class ResourceController extends Controller {

    /**
     * Responsible to return the resource list
	 * param, q, string, searchKeyword
     */
    public function resources(Request $request) {
        $searchKeyword   = $request->get('q');
        $resourceData    = array();
		// Retrieving all resources
        $getResourceData = Resource::resources();
        if(!empty($searchKeyword)){
            foreach($getResourceData as $resources){
				// Check if keyword exist in the current resource
                if (strpos(strtoupper($resources['title']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($resources['status']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($resources['category']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($resources['resource_type']), strtoupper($searchKeyword)) !== false || strpos(strtoupper($resources['role']), strtoupper($searchKeyword)) !== false) {
                    $resourceData[] = $resources;
                }
            }
            $getResourceData = $resourceData;
        }
		
		// Retrieving all categories and roles
		$getCategoryData = Category::getCategories();
		$getRolesData = Roles::getRoles();
		
        return view('category.resource.resources', compact('getResourceData', 'getCategoryData', 'getRolesData'));
    }
    
    /**
     * Responsible to add or edit resource
	 * param, id, int, resource id
	 * param, title, string, resource title
	 * param, category_name, string, category name
	 * param, resource_type, string, resource type (pdf/power point/video)
	 * param, resource_link, string, resource url
	 * param, roles, array, Selected roles array
     */
    public function addEditResources(Request $request) {
        $id              = $request->get('id');
        $title           = $request->get('title');
        $category        = $request->get('category_name');
        $resource_type   = $request->get('resource_type');
        $resource_link   = $request->get('resource_link');
        $roles           = $request->get('roles');
        $getResourceData = array();
        if(!empty($id)){
            $getResourceData = [ 'id' => $id, 'title' => $title, 'category' => $category, 'resource_type' => $resource_type, 'resource_link' => $resource_link, 'roles' => $roles];
        }
        return $getResourceData;
    }

    /**
     * Responsible to load edit resource screen
	 * param, id, int, resource id
     */
    public function editResources(Request $request) {
        $id = $request->get('id');
        if(!empty($id)){
			// Retrieving resource by id
            $getResourceData = Resource::getResourceById($id);
            return $getResourceData;
        }
    }
    
    /**
     * Responsible to delete the resource
	 * param, id, int, resource id
	 * return, true if deleted
     */
    public function deleteResources(Request $request) {
        $id = $request->get('id');
        if(!empty($id) && Resource::deleteResource($id)){
            echo 'true';
        }
    }
    
    /**
     * Responsible to return the deactivated resources list
	 * return all resources if deactivated is not set
	 * return only deactivated resources if deactivated is set
     */
    public function deactivatedResources(Request $request) {
        $deactivate = $request->get('deactivate');
        if(!empty($deactivate)){
            $resourceData = array();
			// Retrieving all resources
            $getResourceData = Resource::resources();
            if($deactivate == 'Deactivated'){
                foreach($getResourceData as $resources){
                    if(trim($resources['status']) == 'Deactivated' && trim($resources['status']) != 'Active'){
                        $resourceData[] = $resources;
                    }
                }
                return $resourceData;
            }else{
                return $getResourceData;
            }
        }
    }
	
    /**
     * Responsible to delete the category
	 * param, id, int, resource id
	 * return, true if deleted
     */
	public function deleteCategory(Request $request){
		$id = $request->get('id');
        if(!empty($id) && Category::deleteCategories($id)){
            echo 'true';
        }
	}
}