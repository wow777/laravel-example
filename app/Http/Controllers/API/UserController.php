<?php

namespace App\Http\Controllers\API;

use App\Events\UserCommunitiesUpdated;
use App\Http\Requests\API\StoreCommunityUser;
use App\Models\CommunityUser;
use App\Models\ZipCode;
use App\TeamManager;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;


class UserController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response|\App\User
     */
    public function show($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'error' => 'User not found.'
            ], 404);
        }

        return response()->json([
            'user' => $user
        ], 200);
    }

    public function team($zipcode)
    {
        // todo need community coordinator
        // todo need implementing_partner

        $teamManager = new TeamManager();

        // to fetch all Regional Managers/State Directors go to /users/team/all instead of /users/team/{zipcode}
        if($zipcode == 'all') return $this->showAll();

        $zip = ZipCode::with(['state.stateDirector', 'region', 'region.manager', 'community'])
            ->where('code', $zipcode)
            ->first();

        if (!$zip) {
            return response()->json(['error' => 'No team in this zipcode'], 422);
        }


        return response()->json([
            $teamManager->getStateDirectorByZip($zip),
            $teamManager->getRegionalManagerByZip($zip),
            //$teamManager->getCommunityCoordinator($zip)  todo still need logic for this
        ], 201);

    }

    public function showAll()
    {
        $teamManager = new TeamManager();
        return response()->json([$teamManager->getAllManagersAndDirectors()], 201);
    }


    public function showCommunities(User $user)
    {


        $communities = $user->communities;

        return response()->json(['data' => $communities], 201);

    }

    /**
     * @param StoreCommunityUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function addCommunities(StoreCommunityUser $request)
    {
        $userCommunities = [];

        if ($request->user_id) {
            $user = User::where('id', $request->user_id)
                ->with(['communities' => function($query) {
                    $query->select('communities.id');
                }])
                ->first();

            $userCommunities = $user->communities->toArray();

            if (empty($user)) {
                return response()->json([
                    'error' => 'User not found'
                ], 404);
            }

            $user->phone = $request->phone;
            $user->organization = $request->organization;
            $user->zip_code = $request->zip_code;
            $user->save();
        } else if ($request->email) {
            $user = User::create([
                'email' => $request->email,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'organization' => $request->organization,
                'phone' => $request->phone,
                'zip_code' => $request->zip_code
            ]);
        }

        /**
         * @var int[]|string[] $communities
         */
        $communities = $request->communities;

        $user->communities()->sync($communities);

        // for email
        $newCommunities = [];

        if (!empty($communities)) {
            foreach ($communities as $community) {
                if (!in_array($community, $userCommunities)) {
                    array_push($newCommunities, $community);
                }
            }

            if (count($newCommunities) > 0) {
                event(new UserCommunitiesUpdated($user, $newCommunities));
            }
        }


        return response()->json([
            'user' => $user,
            'communities' => $user->communities
        ], 200);
    }

    public function getByEmail(Request $request)
    {
        $user = User::getByEmail($request->get('email'));

        if (empty($user)) {
            return response()->json([
                'error' => 'User not found.'
            ], 404);
        } else {
            return response()->json([
                'user' => $user->makeHidden(['implementing_partners', 'gms', 'churches', 'states', 'regions'] )
            ], 200);
        }
    }
}
