<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\StoreGroup;
use App\Models\Group;
use App\Models\Church;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\County;
use App\Models\State;
use App\Models\ZipCode;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // search only active groups

        $select = ['church_id', 'name', 'group_size', 'how_did_you_hear', 'address_line1', 'address_line2', 'city', 'state_id', 'zip_code_id', 'initial_enrollment_name', 'initial_enrollment_email', 'initial_enrollment_phone', 'questions_comments'];
        if ($request->keyword) {
            $groups = searchAllColumns('groups', $request->keyword, $select)
                ->where('tier_1_enrollment_complete', true)->get();

            foreach($groups as $group) {
                $id = $group->state_id;
                $stateName = State::where('id', $id)->pluck('name')->first();
                $group->state = $stateName;
            }

        } else {
            $groups = Group::with('state')->where('tier_1_enrollment_complete', true)->get()
                           ->map(function ($group) {
                               $array = $group->makeHidden('state')->toArray();
                               $array['state'] = $group->state->name;

                               return $array;
                           });
        }

        return response()->json(['data' => $groups], 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroup $request)
    {
        $state = State::where('name', $request->state)
                      ->orWhere('abbreviation', $request->state)
                      ->first();
        if (!$state) return response()->json(['error' => 'State not in database'], 422);
        $state_id = $state->id;


        $zipcode = ZipCode::where('code', $request->zip_code)
                          ->first();
        if (!$zipcode) return response()->json(['error' => 'Zip code is not in database'], 422);
        $zipcode_id = $zipcode->id;


        $county = County::where('id', $zipcode->knack_county_id)
                        ->first();
        if (!$county) return response()->json(['error' => 'County not in database'], 422);
        $county_id = $county->id;


        $church = Church::where('id', $request->church_id)
                        ->first();
        if (!$church) return response()->json(['error' => 'No church found'], 422);
        $church_id = $church->id;

        // check if name and address match
        if (Group::where('name', $request->name)->where('address_line1', $request->address_line1)->first()) {
            return response()->json(['error' => 'Group Already Exists'], 422);
        }

        $group = Group::create([

            "name" => $request->name,
            "address_line1" => $request->address_line1,
            "address_line2" => $request->address_line2,
            "city" => $request->city,
            "state_id" => $state_id,
            "county_id" => $county_id,
            "zip_code_id" => $zipcode_id,
            "church_id" => $church_id,
            "group_size" => $request->group_size,
            "country" => 'United States of America',
            "initial_enrollment_name" => $request->initial_enrollment_name,
            "initial_enrollment_email" => $request->initial_enrollment_email,
            "initial_enrollment_phone" => $request->initial_enrollment_phone,
            "questions_comments" => $request->questions_comments
        ]);

        return response()->json(['data' => $group], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
