<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\ChurchReview;
use App\Models\Request;

class HomeController extends Controller
{
    public function index()
    {
        $reason = 'Church Responded and Met the Need';

        $data = \DB::select('SELECT
              (
                SELECT COUNT(churches.id)
                FROM churches
                WHERE churches.deleted_at IS NULL
              ) AS "number_of_churches",
              (
                SELECT COUNT(church_reviews.id)
                FROM church_reviews
              ) AS "number_of_church_reviews",
              (
                SELECT ROUND(AVG(church_reviews.rating), 2)
                FROM church_reviews
              ) AS "average_church_rating",
              (
                SELECT CAST(SUM(requests.children_served) AS UNSIGNED)
                FROM requests
                WHERE requests.reason_closed = "'.$reason.'"
              ) AS "number_of_children",
              (
                SELECT COUNT(DISTINCT requests.state_id)
                FROM requests
                WHERE requests.reason_closed = "'.$reason.'"
              ) AS "number_of_states_helping",
              (
                SELECT COUNT(id)
                FROM agency_reviews
              ) AS "number_of_agency_reviews",
              (
                SELECT ROUND(AVG(agency_reviews.rating), 2)
                FROM agency_reviews
              ) AS "average_agency_rating";');

        return response()->json([
            'data' => [
                'number_of_churches' => $data[0]->number_of_churches,
                'number_of_church_reviews' => $data[0]->number_of_church_reviews,
                'average_church_rating' => $data[0]->average_church_rating,
                'number_of_children' => $data[0]->number_of_children,
                'number_of_states_helping' => $data[0]->number_of_states_helping,
                'number_of_agency_reviews' => $data[0]->number_of_agency_reviews,
                'average_agency_rating' => $data[0]->average_agency_rating
            ]
        ], 200);
    }


    public function impact()
    {
        $collection = Request::selectRaw('purpose_of_request, reason_closed, SUM(children_served) AS sum_children_served, SUM(estimated_value) AS sum_estimated_value')
                             ->whereRaw('reason_closed = "Church Responded and Met the Need"')
                             ->groupBy('purpose_of_request', 'reason_closed')
                             ->get();

        $data = $collection->map(function ($row) {
                return [
                    $row->purpose_of_request => [
                        'sum_children_served' => number_format($row->sum_children_served),
                        'sum_estimated_value' => number_format($row->sum_estimated_value),
                    ]
                ];
        })->toArray();


        $numberChildrenTotal = Request::where('reason_closed', 'Church Responded and Met the Need')->sum('children_served');
        $economicImpactTotal = Request::where('reason_closed', 'Church Responded and Met the Need')->sum('estimated_value');
        $numberChurchesTotal = Church::whereNull('deleted_at')->count();

        $total = [
            'number of children served' => number_format($numberChildrenTotal),
            'number of churches' => number_format($numberChurchesTotal),
            'economic impact' => number_format($economicImpactTotal),
        ];

        return response()->json(['data' => $data, 'totals' => $total], 201);

    }
}