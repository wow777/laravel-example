<?php

namespace App\Http\Controllers\API;

use App\Models\Agency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agencies = Agency::all()
        ->map(function ($agency) {
            $array['name'] = $agency->name;
            $array['email_domain'] = $agency->email_domain;
        return $array;
    });

        return response()->json(['data' => $agencies], 201);

    }

}