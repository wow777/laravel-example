<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\SubmissionRequest;
use App\Http\Requests\API\SubmissionRequestStep2;
use App\Models\RequestNeed;
use App\User;
use App\Models\Agency;
use App\Models\Request;
use App\Models\ZipCode;
use Illuminate\Support\Collection;
use App\Models\AgencyWorker;
use Illuminate\Http\Request as HTTPRequest;

class RequestController extends Controller
{

    /**
     * If possible, finds the user's email in the users table
     * and returns the agencies that the user could belong to based on
     * the domain of their email address.
     *
     * @param HTTPRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateAgencyEmail(HTTPRequest $request)
    {
        $email = $request->get('email');

        if (!$this->validateEmail($email)) {
            return response()->json([
                'error' => 'Please submit a valid email address.'
            ], 400);
        }

        $user = User::getByEmail($email);

        if (empty($user)) {
            return response()->json([
                'error' => 'User does not exist.'
            ], 404);
        }

        if (!$this->isValidAgencyEmail($email)) {
            return response()->json([
                'error' => 'There has been an error. Please contact us with what you were trying to do.'
            ], 400);
        }

        $agencies = $this->getAgenciesByEmail($email);
        $agencyList = $agencies->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        })->all();

        /**
         * @var Collection $agencyCounties
         */
        $agencyCounties = collect([]);

        foreach($agencies as $agency) {
            $countyList = $agency->counties->map(function($item) {
                $countyName = $item->name;
                $countyId = $item->id;
                $stateAbbreviation = $item->state->abbreviation;

                return [
                    'id' => $countyId,
                    'name' => $countyName.' ('.$stateAbbreviation.')'
                ];
            });

            $agencyCounties = $agencyCounties->unique()->merge($countyList);
        }

        return response()->json([
            'user' => $user,
            'agencies' => $agencyList,
            'counties' => $agencyCounties
        ], 200);
    }

    /**
     * Creates and populates the information for the initial request
     *
     * @param SubmissionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRequestStep1(SubmissionRequest $request)
    {
        $email = $request->get('email');
        $user_id = $request->get('user_id');

        if (!$this->isValidAgencyEmail($email)) {
            return response()->json([
                'error' => 'There has been an error. Please contact us with what you were trying to do.'
            ], 400);
        }

        $caseZipCode = ZipCode::where('code', $request->get('case_zip_code'))
            ->select('id')
            ->first();

        if (empty($caseZipCode)) {
            return response()->json([
                'error' => 'Zip code is currently not activated within the system.'
            ], 422);
        }

        if (!empty($user_id)) {
            $user = User::findOrFail($user_id);
        } else {
            $user = User::create([
                'email' => $email,
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'phone' => $request->get('phone')
            ]);
        }

        $agency_id = $request->get('agency_id');

        $agencyWorker = AgencyWorker::with('counties', 'user', 'agency')
            ->where('user_id', $user->id)
            ->first();

        if (empty($agencyWorker)) {
            AgencyWorker::create([
                'user_id' => $user->id,
                'agency_id' => $agency_id,
            ]);
        }

        $requestData = Request::create([
            'zip_code_id' => $caseZipCode->id,
            'role_of_worker' => $request->role_of_worker,
            'purpose_of_request' => $request->purpose_of_request,
            'tier' => $request->tier,
            'agency_case_id' => $request->agency_case_id
        ]);

        return response()->json([
            'user' => $user,
            'request' => $requestData
        ], 201);
    }

    /**
     * The second part of the create a request form
     *
     * @param SubmissionRequestStep2 $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRequestStep2(SubmissionRequestStep2 $request)
    {
        $agencyRequest = Request::find($request->get('request_id'));

        if ($request->get('tier') != $agencyRequest->tier) {
            return response()->json([
                'error' => 'The tier that you entered does not match what we have on file.'
            ], 422);
        }

        $httpRequestArray = $request->except('request_id');

        $agencyRequest->update($httpRequestArray);

        return response()->json([
            'request' => $agencyRequest
        ], 200);
    }

    /**
     * For the user to be able to create a tier 1 "request need"
     *
     * @param HTTPRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRequestNeed(HTTPRequest $request)
    {
        $formData = $request->all('request_id', 'name');

        $validator = \Validator::make($formData, [
            'request_id' => 'required|exists:requests,id',
            'name' => 'required|string|max:190'
        ]);

        if ($validator->passes()) {
            $requestSubmission = Request::find($formData['request_id']);

            if (intval($requestSubmission->tier) != 1) {
                return response()->json([
                    'error' => 'The request that you are attempting to add a need to is not a tier 1 request.'
                ], 422);
            }

            $newRequest = new RequestNeed($formData);
            $newRequest->save();

            return response()->json([
                'request_need' => $newRequest
            ], 201);
        }

        return response()->json($validator->errors(), 422);
    }

    /**
     * Validates the email address
     *
     * @param string $emailAddress
     * @return bool
     */
    private function validateEmail($emailAddress)
    {
        $validator = \Validator::make([
            'email' => $emailAddress
        ], [
            'email' => 'required|email'
        ]);

        if ($validator->passes()) {
            return true;
        }

        return false;
    }

    /**
     * Determines if the user's email address belongs to a valid agency
     *
     * @param string $emailAddress
     * @return bool
     */
    private function isValidAgencyEmail($emailAddress)
    {
        $agencies = $this->getAgenciesByEmail($emailAddress);

        if (empty($agencies)) {
            return false;
        }

        return true;
    }

    /**
     * Gets a list of agencies based on the users email address
     *
     * @param string $emailAddress
     * @return Agency[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getAgenciesByEmail($emailAddress)
    {
        $userDomain = substr(strrchr($emailAddress, "@"), 0);

        $agencies = Agency::where('email_domain', $userDomain)
            ->select('agencies.name', 'agencies.id')
            ->with(['counties' => function($query) {
                $query->select('counties.id', 'counties.name', 'state_id')
                    ->with(['state' => function($query) {
                        $query->select('states.id', 'states.name', 'abbreviation');
                    }])
                    ->orderBy('name', 'ASC');
            }])
            ->orderBy('name', 'ASC')
            ->get();

        return $agencies;
    }
}
