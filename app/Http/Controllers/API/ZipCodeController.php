<?php

namespace App\Http\Controllers\API;

use App\Models\ZipCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZipCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zipcodes = ZipCode::select('code as zipcode', 'city', 'states.name')
            ->join('states', 'zip_codes.knack_state_id', 'states.id')
            ->get();

        return response()->json(['data' => $zipcodes], 201);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $zipcode = ZipCode::select('code as zipcode', 'city', 'states.name as state', 'counties.name as county')->where('code', $id)
            ->join('states', 'zip_codes.knack_state_id', 'states.id')
            ->join('counties', 'zip_codes.knack_county_id', 'counties.id')
            ->first();

        return response()->json(['data' => $zipcode], 201);

    }




}
