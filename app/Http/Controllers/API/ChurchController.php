<?php

namespace App\Http\Controllers\API;

use App\Events\ChurchEnrolled;
use App\Http\Requests\API\StoreChurch;
use App\Models\Church;
use App\Models\ChurchUser;
use App\Models\County;
use App\Models\EnrollmentRequest;
use App\Models\State;
use App\Models\ZipCode;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use DB;
use Library\ChurchUserService;

class ChurchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // search only active churches
        $select = ['id', 'name', 'state_id'];
        if ($request->keyword) {

            $churches = searchAllColumns('churches', $request->keyword, $select)
                ->where('tier_1_enrollment_complete', true)->get();

        } else {

            $churches = Church::select($select)->where('tier_1_enrollment_complete', true)->get();

        }

        return response()->json(['data' => $churches], 201);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChurch $request)
    {
        $state = State::where('name', $request->state)
                      ->orWhere('abbreviation', $request->state)
                      ->first();
        if (!$state) return response()->json(['error' => 'State not in database'], 422);
        $state_id = $state->id;

        $zipcode = ZipCode::where('code', $request->zip_code)
                          ->first();
        if (!$zipcode) return response()->json(['error' => 'No zip code'], 422);
        $zipcode_id = $zipcode->id;

        $county = County::where('id', $zipcode->knack_county_id)
                        ->first();
        if (!$county) return response()->json(['error' => 'County not in database'], 422);
        $county_id = $county->id;


        // check if name and address match
        if (Church::where('name', $request->name)->where('address_line1', $request->address_line1)->first()) {
            return response()->json(['error' => 'Church Already Exists'], 422);
        }

        /**
         * @var Church $church
         */
        $church = Church::create([
            "name" => $request->name,
            "address_line1" => $request->address_line1,
            "address_line2" => $request->address_line2,
            "city" => $request->city,
            "state_id" => $state_id,
            "county_id" => $county_id,
            "zip_code_id" => $zipcode_id,
            "country" => 'United States of America',
        ]);

        /**
         * Client wants to store this information. Currently is not being
         * referenced within the application.
         *
         * @author Doug Niccum <doug@ontargetinteractive.com>
         */
        EnrollmentRequest::create([
            'first_name' => $request->get('fname'),
            'last_name' => $request->get('lname'),
            'hear_about' => $request->get('hear_about'),
            'email' => $request->get('email'),
            'ministry_implement' => $request->get('ministry_implement'),
            'phone_number' => $request->get('phone_number'),
            'comments' => $request->get('comments'),
            'church_id' => $church->id
        ]);

        if (!$request->pastor) {
            return response()->json(['error' => 'Please include at least one pastor for this church.'], 422);
        }

        // Add Pastor
        foreach ($request->pastor as $pastor) {
            // add pastor
            // check if user exists
            $user = User::getByEmail($pastor['email']);

            if (!$user) {
                //create user

                $newPastor = new User();
                if (!$newPastor->validate($pastor)) {
                    return response()->json(['error' => 'Please include a first name, last name, valid phone number, and valid email for this pastor.'], 422);
                    break;
                }

                $user = User::create([
                    'first_name' => $pastor['first_name'],
                    'last_name' => $pastor['last_name'],
                    'phone' => $pastor['phone'],
                    'email' => $pastor['email']
                ]);

                // create church user
                ChurchUserService::createUser($church->id, $user->id, true);
            } else {

                if (!ChurchUser::where([['church_id', $church->id], ['user_id', $user->id]])
                    ->update(['pastor' => true])){
                    ChurchUserService::createUser($church->id, $user->id, true);
                }
            }
        }

        if (!$request->point_people) {
            return response()->json(['error' => 'Please include at least one point person for this church.'], 422);
        }

        // Add Point People
        foreach ($request->point_people as $person) {
            $user = User::getByEmail($person['email']);
            if (!$user) {
                //create user
                $user = User::create([
                    'first_name' => $person['first_name'],
                    'last_name' => $person['last_name'],
                    'phone' => $person['phone'],
                    'email' => $person['email']
                ]);
                // create church user
                $church_user = ChurchUserService::createUser($church->id, $user->id, false, true);

            } else {
                // a user exists, but not necessarily a church user
                $church_user = ChurchUser::where([['church_id', $church->id], ['user_id', $user->id]])->first();
                if ($church_user) {
                    $church_user = ChurchUser::where([['church_id', $church->id], ['user_id', $user->id]])
                        ->update(['t1_point_person' => 1]);
                } else {
                    // create church user
                    $church_user = ChurchUserService::createUser($church->id, $user->id, false, true);
                    if (!$church_user) return response()->json(['error' => 'cannot find church user'], 422);
                }
            }


            // Add point people check if primary
            if (isset($person['primary'])) {
                // Should only have one
                $existingPrimary = ChurchUser::where([['church_id', $church->id], ['primary_point_person', 1]])
                    ->first();
                if ($existingPrimary) return response()->json(['error' => 'Can only have 1 primary person'], 422);

                $church_user->primary_point_person = true;
                $church_user->save();
            }
        }

        event(new ChurchEnrolled($church));

        return response()->json(['data' => $church], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $church = Church::find($id)->load('state');

        return response()->json(['data' => $church], 201);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function churchZip($zipcode)
    {
        $zip = ZipCode::with(['churches'])
                      ->where('code', $zipcode)
                      ->first();

        if (!$zip) return response()->json(['error' => 'Zip code is not in database'], 422);

        if (!$zip->churches) return response()->json(['error' => 'No church found in zipcode']);

        $churches = $zip->churches()->get()
        ->map(function ($church){
            $array['id'] = $church->id;
            $array['name'] = $church->name;

            return $array;
        });

        if(!$churches) return response()->json(['error' => 'No church found in zipcode']);

        return response()->json(['data' => $churches], 201);

    }

}