<?php

namespace App\Http\Controllers;

use App\Events\OrientationCompleted;
use App\Models\Church;
use App\Models\RequestRecipient;
use Illuminate\Http\Request;
use App\Models\Request as RequestModel;
use App\Models\Response;


class ChurchEnrollmentController extends Controller
{
    public function orientation(Church $church)
    {
        $region = $church->region;

        return view('church-enrollment.orientation', compact('region', 'church', 'title'));
    }

    protected function storeOrientation(Request $request)
    {

        $church = Church::findOrFail($request->church_id);

        $date = date('Y-m-d', strtotime($request->date));
        $church->orientation_date = $date;

        $church->save();

        event(new OrientationCompleted($church));

        return redirect()->back()
            ->with('success', 'Orientation for this church has been completed');
    }

    protected function activate(Church $church)
    {
        return view('church-enrollment.activate', compact('church'));
    }

    protected function storeActivation(Request $request)
    {
        $engage_congregation = $this->engage_congregation($request->engage_congregation);
        $engage_share_your_ideas = $request->engage_share_your_ideas;
        $how_will_you_share_requests = $this->how_will_you_share_requests($request->how_will_you_share_requests);
        $share_needs_share_your_ideas = $request->share_needs_share_your_ideas;
        $giving_preference = $this->giving_preference($request->giving_preference);
        $giving_frequency = $this->giving_frequency($request->giving_frequency);
        $giving_amount = $request->giving_amount;
        $special_offering_date = $request->special_offering_date ? date('Y-m-d', strtotime($request->special_offering_date)) : null;

        $church = Church::find($request->church_id);

        $church->engage_congregation = $engage_congregation;
        $church->engage_share_your_ideas = $engage_share_your_ideas;
        $church->how_will_you_share_requests = $how_will_you_share_requests;
        $church->share_needs_share_your_ideas = $share_needs_share_your_ideas;
        $church->giving_options = $giving_preference;
        $church->giving_frequency = $giving_frequency;
        $church->giving_amount = $giving_amount;
        $church->special_offering_date = $special_offering_date;

        $church->save();

        exit('Saved !');
    }


    protected function churchToChurch(Request $request)
    {

        $reqId = $request->request_id;

        $request = RequestModel::find($reqId);
        $churches_groups = RequestRecipient::where('request_id', $reqId)->get();
        $respondedIds = Response::where('request_id', $reqId)->pluck('church_id')->toArray();


        return view('church-enrollment.churchToChurch', compact('region', 'request', 'churches_groups', 'respondedIds', 'title'));

    }


    public function contactToChurch(Request $request)
    {

        $reqId = $request->request_id;
        $church = Church::find($request->church_id);

        $request = RequestModel::find($reqId);
        return view('church-enrollment.contactToChurch', compact('region', 'request','church'));
    }

    protected function closeRequest(Request $request)
    {
        $request_id = $request->request_id;

        $request = RequestModel::find($request_id);

        return view('church-enrollment.close_request', compact('request'));

    }

    protected function storeCloseRequest(Request $request)
    {
        $reason = $request->reason;
        $reasons = implode(', ', $reason);

        $request_id = $request->request_id;
        $rating_final = $request->rating_final;
        $reason_text = $request->reason_text;
        $experience_story = $request->experience_story;

        $request = RequestModel::find($request_id);
        $request->reason_closed = $reasons;
        $request->rating = $rating_final;
        $request->reason_for_rating = $reason_text;
        $request->worker_story = $experience_story;

        $request->save();

        exit('Saved !');
    }


    protected function responseToRequest(Request $request)
    {
        $reqId = $request->request_id;
        $churchId = $request->church_id;

        $request = RequestModel::find($reqId);
        $responses = Response::where('church_id', $churchId)->get();


        return view('church-enrollment.respond_to_request', compact('request', 'responses'));
    }

    protected function verifyParticipation(Request $request)
    {
        $reqId = $request->request_id;
        $churchId = $request->church_id;

        $request = RequestModel::find($reqId);
        $church = Church::find($churchId);

        return view('church-enrollment.verify_participation', compact('request', 'church'));
    }

    //new 3contactToChurch
    public function tier2AppBgCheckApproval()
    {
        return view('church-enrollment.Tier_2_Application_Background_Check_Approval');
    }
    public function tier2AppTrainingUpdate()
    {
        return view('church-enrollment.Tier_2_Application_Training_Update');
    }
    public function tier3BgCheckApproval()
    {
        return view('church-enrollment.Tier_3_Background_Check_Approval');
    }

    protected function engage_congregation($key)
    {
        $engage_congregation = [
            1 => 'Pastor gives a vision-casting sermon that incorporates the CarePortal video and an individual sign-up process',
            2 => 'Send email to congregation with the video link and opportunity to sign-up',
            3 => 'Show video and provide a sign-up process',
            4 => 'A different way',
        ];

        return $key ? $engage_congregation[$key] : '';
    }

    protected function how_will_you_share_requests($key)
    {

        $how_will_you_share_requests = [
            1 => 'Call people you think could help',
            2 => 'Forward a portion of the emails and have them reply to the Point Person if they can help',
            3 => 'Forward the emails that come and let them respond directly',
            4 => 'Make the need known via Facebook or other social media',
            5 => 'Meet the need when you can using church resources',
            6 => 'Needs go out through small group leaders',
            7 => 'Send a text about the request',
            8 => 'A different way',
        ];

        return $key ? $how_will_you_share_requests[$key] : '';

    }

    protected function giving_preference($key)
    {

        $giving_preference = [
            1 => 'We will give $100/month',
            2 => 'We will give $1,200/year',
            3 => 'We will give more than $100/month to provide access to financially under-resourced churches.',
            4 => 'We need to contribute at a lower amount this year.',
            5 => 'We plan to only collect a special offering for now',
        ];

        return $key ? $giving_preference[$key] : '';

    }

    protected function giving_frequency($key)
    {
        $giving_frequency = [
            1 => 'Monthly',
            2 => 'Quarterly',
            3 => 'Annually',
            4 => 'One-time',
        ];

        return $key ? $giving_frequency[$key] : '';
    }

}
