<?php

namespace App\Http\Controllers;

use App\Http\Resources\Graph\LineChart;
use App\Interfaces\RoleInterface;
use App\Models\Community;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Schema;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class RoleController extends Controller
{

    /**
     * @param Request $request
     * @param $roleCode
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function dashboard($roleCode, $roleId)
    {
        $role = $this->getRole($roleCode, $roleId);
        $message = "To be determined";

        $activated_churches = $role->churches()->activated()->forYears(2)->get();
        $activated_groups = $role->groups()->activated()->forYears(2)->get();
        $requests = $role->requests()->forYears(2)->get();
        $children_served = $role->requests()->hasServedChildren()->forYears(2)->get();

        $income = collect([]); // TODO determine what this needs to be
        $impact = collect([]); // TODO determine what this needs to be

        $activatedChurchesDataSets = (new LineChart($activated_churches))->toArray('tier_1_enrollment_complete_date');
        $activatedGroupsDataSets = (new LineChart($activated_groups))->toArray('tier_1_enrollment_complete_date');
        $requestsDataSets = (new LineChart($requests))->toArray('created_at');
        $childrenServedDataSets = (new LineChart($children_served))->toArray('created_at', 'children_served');
        $incomeDataSets = (new LineChart($income))->toArray('created_at');
        $economicImpactDataSets = (new LineChart($impact))->toArray('created_at');

        $lineGraphXAxisLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        $activatedChurchesExportUrl = route('roleDashboardDataExport', [
            'role' => $roleCode,
            'role_id' => $roleId,
            'dataSource' => 'activated_churches'
        ]);
        $activatedGroupsExportUrl = route('roleDashboardDataExport', [
            'role' => $roleCode,
            'role_id' => $roleId,
            'dataSource' => 'activated_groups'
        ]);
        $requestsExportUrl = route('roleDashboardDataExport', [
            'role' => $roleCode,
            'role_id' => $roleId,
            'dataSource' => 'requests'
        ]);
        $childrenServedExportUrl = route('roleDashboardDataExport', [
            'role' => $roleCode,
            'role_id' => $roleId,
            'dataSource' => 'children_served'
        ]);
        $incomeExportUrl = '#';
        $economicImpactExportUrl = '#';

        return view('layouts.dashboard',
            compact('role', 'message', 'activated_churches', 'activated_groups', 'requests', 'children_served',
                'income', 'impact', 'activatedChurchesDataSets', 'requestsDataSets', 'childrenServedDataSets',
                'activatedGroupsDataSets', 'incomeDataSets', 'economicImpactDataSets', 'lineGraphXAxisLabels',
                'activatedChurchesExportUrl', 'activatedGroupsExportUrl', 'requestsExportUrl', 'childrenServedExportUrl',
                'incomeExportUrl', 'economicImpactExportUrl'));
    }

    /**
     * @param Request $request
     * @param $role
     * @param $roleId
     * @param $dataSource
     * @return StreamedResponse
     */
    public function dashboardDataExport(Request $request, $role, $roleId, $dataSource)
    {
        $role = $request->attributes->get('role');
        switch ($dataSource) {
            case 'activated_churches':
                $name = 'Activated Churches';
                $collection = $role->churches()->activated()->forYears(2)->orderBy('tier_1_enrollment_complete_date')->get();
                $data = array_merge(
                    [['Date', 'Activated Churches']],
                    (new LineChart($collection))
                        ->toCSVArray('tier_1_enrollment_complete_date')
                );
                break;
            case 'activated_groups':
                $name = 'Activated Groups';
                $collection = $role->groups()->activated()->forYears(2)->orderBy('created_at')->get();
                $data = array_merge(
                    [['Date', 'Activated Groups']],
                    (new LineChart($collection))
                        ->toCSVArray('created_at')
                );
                break;
            case 'requests':
                $name = 'Requests';
                $collection = $role->requests()->forYears(2)->orderBy('created_at')->get();
                $data = array_merge(
                    [['Date', 'Requests']],
                    (new LineChart($collection))
                        ->toCSVArray('created_at')
                );
                break;
            case 'children_served':
                $name = 'Children Served';
                $collection = $role->requests()->hasServedChildren()->forYears(2)->orderBy('created_at')->get();
                $data = array_merge(
                    [['Date', 'Children Served']],
                    (new LineChart($collection))
                        ->toCSVArray('created_at', 'children_served')
                );
                break;
            default:
                abort(Response::HTTP_NOT_FOUND);
        }

        return new StreamedResponse(
            function () use ($data) {
                // A resource pointer to the output stream for writing the CSV to
                $handle = fopen('php://output', 'w');
                foreach ($data as $row) {
                    // Loop through the data and write each entry as a new row in the csv
                    fputcsv($handle, $row);
                }

                fclose($handle);
            },
            200,
            [
                'Content-type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename=' . $name . '.csv'
            ]
        );
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function regions($roleCode, $roleId)
    {
        $role = $this->getRole($roleCode, $roleId);
        $regions = $role->regions()->with(['regionalManager', 'churches'])->paginate(30);
        $childrenServedTotal = $regions->sum(function ($region) {
            return $region->total_children_served;
        });
        $childrenServedAverage = $regions->count() > 0
            ? number_format($regions->sum(function ($region) {
                    return $region->average_children_served_per_month;
                }) / $regions->count(), 0)
            : 0;

        $requestLastMonth = $role->requests()
            ->whereIn('state_id', $regions->pluck('state_id')->unique())
            ->whereDate('created_at', '>=', date('Y-m-d', strtotime('-30 days')))
            ->count();

        $activeChurches = $role->churches()->activated()->count();
        $pendingChurches = $role->churches()->pendingActivation()->count();
        $givingChurches = $role->churches()->giving()->count();


        //test
        $MonthIncome = 123;
        $EconomicImpact1YR = 123;

        $badges = $this->regionsBadges($childrenServedTotal,$childrenServedAverage,$MonthIncome,$givingChurches,$requestLastMonth,$EconomicImpact1YR,$activeChurches,$pendingChurches);

        return view('category.region.index', compact(
            'role',
            'regions',
            'badges'
        ));
    }


    protected function regionsBadges($childrenServedTotal,$childrenServedAverage,$MonthIncome,$givingChurches,$requestLastMonth,$EconomicImpact1YR,$activeChurches,$pendingChurches)
    {
        return $badges = [
            'one' => [
                'title' => 'Total Children Served',
                'number' => $childrenServedTotal
            ],
            'two' => [
                'title' => 'Avg Children Served Per Month',
                'number' => $childrenServedAverage
            ],
            'three' => [
                'title' => '12 Month Income',
                'number' => $MonthIncome
            ],
            'four' => [
                'title' => 'Number of Churches Giving',
                'number' => $givingChurches
            ],
            'five' => [
                'title' => '>Requests Past 30 Days',
                'number' => $requestLastMonth
            ],
            'six' => [
                'title' => 'Economic Impact 1YR',
                'number' => $EconomicImpact1YR
            ],
            'seven' => [
                'title' => 'Active Churches',
                'number' => $activeChurches
            ],
            'eight' => [
                'title' => 'Pending Churches',
                'number' => $pendingChurches
            ],
        ];
    }

    protected function states($role, $role_id)
    {
        $role = $this->getRole($role, $role_id);

        $states = $role->states->load('stateDirector', 'agencies', 'churches');

        $activated_churches_count = $role->activatedChurches()->count();
        $pending_count = $role->pendingActivation()->count();

        //test
        $TotalChildrenServed = 123;
        $AvgChildrenServedPerMonth = 123;
        $MonthIncome = 123;
        $NumberOfChurchesGivingnthIncome = 123;
        $RequestsPast30Days = 123;
        $EconomicImpact1YR = 123;

        $badges = $this->statesBadges($TotalChildrenServed, $AvgChildrenServedPerMonth, $MonthIncome, $NumberOfChurchesGivingnthIncome, $RequestsPast30Days,$EconomicImpact1YR,$activated_churches_count,$pending_count);

        return view('category.state.index',
            compact('role', 'states','badges'));
    }

    protected function statesBadges($TotalChildrenServed, $AvgChildrenServedPerMonth, $MonthIncome, $NumberOfChurchesGivingnthIncome, $RequestsPast30Days,$EconomicImpact1YR,$activated_churches_count,$pending_count)
    {
        return $badges = [
            'one' => [
                'title' => 'Total Children Served',
                'number' => $TotalChildrenServed
            ],
            'two' => [
                'title' => 'Avg Children Served Per Month',
                'number' => $AvgChildrenServedPerMonth
            ],
            'three' => [
                'title' => '12 Month Income',
                'number' => $MonthIncome
            ],
            'four' => [
                'title' => 'Number of Churches Giving',
                'number' => $NumberOfChurchesGivingnthIncome
            ],
            'five' => [
                'title' => 'Requests Past 30 Days',
                'number' => $RequestsPast30Days
            ],
            'six' => [
                'title' => 'Economic Impact 1YR',
                'number' => $EconomicImpact1YR
            ],
            'seven' => [
                'title' => 'Active Churches',
                'number' => $activated_churches_count
            ],
            'eight' => [
                'title' => 'Pending Churches',
                'number' => $pending_count
            ],

        ];
    }

    /**
     * @param $role
     * @param $role_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function communities($role, $role_id)
    {

        $role = $this->getRole($role, $role_id);
        $totalCommunities = $role->communities()->count();
        $communitiesOfCriticalNeed = $role->communities()->ofCriticalNeed()->count();
        $communities = $role->communities()->with('coordinator')->get();

        return view('category.community.index',
            compact('role', 'totalCommunities', 'communitiesOfCriticalNeed', 'communities'));
    }


    protected function churches($role, $role_id)
    {

        $role = $this->getRole($role, $role_id);
        $churches = $role->churches()->with(['users', 'state', 'responses', 'reviews'])->paginate(30);

        $pending_activation = $role->pendingActivation()->count();
        $activated_churches = $role->activatedChurches()->count();

        //test
        $noResponses = 123;
        $respondedIn = 123;

        $columns = Schema::getColumnListing('churches');

        $badges = $this->badgesChurches($pending_activation, $activated_churches, $noResponses, $respondedIn);

        return view('category.church.index',
            compact('role', 'churches', 'states', 'columns', 'badges'));

    }

    protected function badgesChurches($pending_activation, $activated_churches, $noResponses, $respondedIn)
    {

        return $badges = [
            'one' => [
                'title' => 'No Responses',
                'title_br' => '60 Days',
                'number' => $noResponses
            ],
            'two' => [
                'title' => 'Responded in',
                'title_br' => 'Last 14 days',
                'number' => $respondedIn
            ],
            'three' => [
                'title' => 'Pending-',
                'title_br' => 'Activation',
                'number' => $pending_activation
            ],
            'four' => [
                'title' => 'Activated',
                'title_br' => 'Churches',
                'number' => $activated_churches
            ]
        ];
    }

    protected function applications($role, $role_id)
    {
        $role = $this->getRole($role, $role_id);

        $applications = []; // TODO add this

        //test
        $pastorsCount = 123;
        $pointPeopleCount = 123;
        $coordinatorsCount = 123;
        $ambassadorsCount = 123;

        $badges = $this->badgesApplication($pastorsCount, $pointPeopleCount, $coordinatorsCount, $ambassadorsCount);

        return view('category.contact.index',
            compact('role', 'applications', 'badges'));
    }

    protected function badgesApplication($pastorsCount, $pointPeopleCount, $coordinatorsCount, $ambassadorsCount)
    {

        return $badges = [
            'one' => [
                'title' => 'Pastors',
                'number' => $pastorsCount
            ],
            'two' => [
                'title' => 'Point People',
                'number' => $pointPeopleCount
            ],
            'three' => [
                'title' => 'Coordinators',
                'number' => $coordinatorsCount
            ],
            'four' => [
                'title' => 'Ambassadors',
                'number' => $ambassadorsCount
            ]
        ];

    }


    /**
     * @param $roleCode
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function contacts($roleCode, $roleId)
    {

        $role = $this->getRole($roleCode, $roleId);

        $contacts = $role->contacts()->filtered(request('q'))->paginate(30);
        $counts = null;

        $pastorsCount = 0;
        $pointPeopleCount = 0;

        //test
        $coordinatorsCount = 123;
        $ambassadorsCount = 123;

        foreach ($contacts as $contact) {
            $roles = $contact->roles();
            if (in_array('Pastor', $roles)) {
                $pastorsCount++;
            } elseif (in_array('PrimaryPP', $roles) || in_array('T1PP', $roles)) {
                $pointPeopleCount++;
            }
        }

        $badges = $this->contactsBadges($pointPeopleCount, $pastorsCount, $coordinatorsCount, $ambassadorsCount);

        return view('category.contact.index', compact(
            'roleCode',
            'roleId',
            'role',
            'contacts',
            'counts',
            'badges'
        ));
    }


    protected function contactsBadges($pointPeopleCount, $pastorsCount, $coordinatorsCount, $ambassadorsCount)
    {
        return $badges = [
            'one' => [
                'title' => 'Pastors',
                'number' => $pastorsCount
            ],
            'two' => [
                'title' => 'Point People',
                'number' => $pointPeopleCount
            ],
            'three' => [
                'title' => 'Coordinators',
                'number' => $coordinatorsCount
            ],
            'four' => [
                'title' => 'Ambassadors',
                'number' => $ambassadorsCount
            ]
        ];

    }

    protected function agencies($role, $role_id)
    {

        $role = $this->getRole($role, $role_id);


        $agencies = $role->agencies->load('state');

        $tier_1_cnt = $agencies->where('tier_1_approval', true)->count();
        $tier_2_cnt = $agencies->where('tier_2_approval', true)->count();
        $tier_3_cnt = $agencies->where('tier_3_approval', true)->count();
        $pending_cnt = $agencies->where('tier_1_approval', false)
            ->where('tier_2_approval', false)
            ->where('tier_3_approval', false)
            ->count();

        $badges = $this->agenciesBadges($tier_1_cnt, $tier_2_cnt, $tier_3_cnt, $pending_cnt);

        return view('category.agency.index',
            compact('role', 'agencies', 'badges'));
    }

    protected function agenciesBadges($tier_1_cnt, $tier_2_cnt, $tier_3_cnt, $pending_cnt)
    {
        return $badges = [

            'one' => [
                'title' => 'Number Tier 1 Agencies',
                'number' => $tier_1_cnt
            ],
            'two' => [
                'title' => 'Number Tier 2 Agencies',
                'number' => $tier_2_cnt
            ],
            'three' => [
                'title' => 'Number Tier 3 Agencies',
                'number' => $tier_3_cnt
            ],
            'four' => [
                'title' => 'Number of Agencies pending',
                'number' => $pending_cnt
            ]

        ];
    }

    protected function requests($role, $role_id)
    {
        $role = $this->getRole($role, $role_id);

        $requests = $role->requests;
        $num_open = $requests->where('status', 'Open')->count();
        $num_closed = $requests->count() - $num_open;
        $met_by_church = $requests->where('reason_closed', 'Church Responded and Met the Need')->count();
        $percent_met_by_church = $num_closed > 0 ? $met_by_church * 100 / $num_closed : 0;

        return view('category.request.index',
            compact('role', 'requests', 'num_open', 'num_closed', 'met_by_church', 'percent_met_by_church'));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function implementingPartner($roleCode, $roleId)
    {
        $role = $this->getRole($roleCode, $roleId);
        $implementingPartners = $role->implementingPartners()->withTrashed()
            ->with(['churches', 'ambassadors'])
            ->filtered(request('q'))
            ->paginate(30);

        return view('category.implementing-partner.index',
            compact('roleCode', 'roleId', 'role', 'implementingPartners'));
    }

    public function events($role, $role_id)
    {

        $role = $this->getRole($role, $role_id);

        //test
        $myEvents = 123;
        $regionEvents = 123;
        $stateEvents = 123;
        $nationalEvents = 123;

        $badges = $this->eventsBadges($myEvents, $regionEvents, $stateEvents, $nationalEvents);


        return view('category.event.index', compact('badges'));
    }

    public function eventsBadges($myEvents, $regionEvents, $stateEvents, $nationalEvents)
    {
        return $badges = [
            'one' => [
                'title' => 'My Events',
                'number' => $myEvents
            ],
            'two' => [
                'title' => 'Region Evetns',
                'number' => $regionEvents
            ],
            'three' => [
                'title' => 'State Events',
                'number' => $stateEvents
            ],
            'four' => [
                'title' => 'National Events',
                'number' => $nationalEvents
            ]
        ];
    }

    public function finances($role, $role_id)
    {
        $role = $this->getRole($role, $role_id);

        //test
        $numberOfChurchesGiving = 123;
        $monthTotalGiving = 123;
        $percentGivingFromChurches = 123;
        $percentGivingFromOther = 123;

        $badges = $this->financesBadges($numberOfChurchesGiving, $monthTotalGiving, $percentGivingFromChurches, $percentGivingFromOther);

        return view('category.finance.index', compact('badges'));
    }

    public function financesBadges($numberOfChurchesGiving, $monthTotalGiving, $percentGivingFromChurches, $percentGivingFromOther)
    {
        return $badges = [
            'one' => [
                'title' => 'Number of Churches Giving',
                'number' => $numberOfChurchesGiving
            ],
            'two' => [
                'title' => '12 Month Total Giving',
                'number' => $monthTotalGiving
            ],
            'three' => [
                'title' => 'Percent Giving From Churches',
                'number' => $percentGivingFromChurches
            ],
            'four' => [
                'title' => 'Percent Giving From Other',
                'number' => $percentGivingFromOther
            ]


        ];
    }

    public function settings($role, $role_id)
    {
        $role = $this->getRole($role, $role_id);

        return view('category.setting.index');

    }


    /**
     * @param $role
     * @param $role_id
     * @return RoleInterface
     */
    protected function getRole($role, $role_id)
    {
        return request()->attributes->get('role');
    }

}
