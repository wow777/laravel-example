<?php

namespace App\Http\Controllers;

use App\Models\Community;
use Illuminate\Http\Request;

class CommunityController extends Controller
{
    public function show(Request $request)
    {
        $communityDetail = Community::find($request->id);

        return view('category.community.details.details',compact('communityDetail'));

    }



}
