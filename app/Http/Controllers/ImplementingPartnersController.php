<?php

namespace App\Http\Controllers;

use App\Models\ImplementingPartner;
use App\Models\Region;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class ImplementingPartnersController
 * @package App\Http\Controllers
 */
class ImplementingPartnersController extends Controller
{
    /**
     * @param Request $request
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $contacts = $implementingPartner->users()->orderBy('admin', 'desc')->get();
        $responses = $implementingPartner->churches()->with(['responses'])->get()->sum(function($church) {
            return $church->responses->count();
        });
        return view('category.implementing-partner.show', compact(
            'implementingPartner',
            'roleCode',
            'roleId',
            'contacts',
            'responses'
        ));
    }

    /**
     * @param Request $request
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $validatedInput = $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'address_line1' => 'required',
            'state' => 'required',
            'zip_code' => 'required',
            'website_url' => 'required|url',
        ]);

        $implementingPartner->update($validatedInput);


        session()->flash('success', 'Implementing Partner details updated');
        return redirect(route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function deactivate($roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $implementingPartner->delete();
        session()->flash('success', 'Implementing partner deactivated');

        return redirect(route('role.implementingPartners', [$roleCode, $roleId]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function activate($roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $implementingPartner->restore();
        session()->flash('success', 'Implementing partner activated');

        return redirect(route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return StreamedResponse
     */
    public function churchesExport($roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $data = array_merge(
            [['Name', 'Email']],
            $implementingPartner->churches()->select('name', 'initial_enrollment_email')->get()->toArray()
        );

        $name = $implementingPartner->name . ' - Churches';

        return new StreamedResponse(
            function () use ($data) {
                // A resource pointer to the output stream for writing the CSV to
                $handle = fopen('php://output', 'w');
                foreach ($data as $row) {
                    // Loop through the data and write each entry as a new row in the csv
                    fputcsv($handle, $row);
                }

                fclose($handle);
            },
            200,
            [
                'Content-type'        => 'text/csv',
                'Content-Disposition' => 'attachment; filename='.$name.'.csv'
            ]
        );
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return StreamedResponse
     */
    public function regionsExport($roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $data = array_merge(
            [['Name', 'Region Manager']],
            $implementingPartner->regions->map(function($region) {
                return [$region->name, $region->regionalManager->displayName];
            })->toArray()
        );

        $name = $implementingPartner->name . ' - Regions';

        return new StreamedResponse(
            function () use ($data) {
                // A resource pointer to the output stream for writing the CSV to
                $handle = fopen('php://output', 'w');
                foreach ($data as $row) {
                    // Loop through the data and write each entry as a new row in the csv
                    fputcsv($handle, $row);
                }

                fclose($handle);
            },
            200,
            [
                'Content-type'        => 'text/csv',
                'Content-Disposition' => 'attachment; filename='.$name.'.csv'
            ]
        );
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return StreamedResponse
     */
    public function manageRegions($roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $implementingPartner->regions()->sync(request('regions'));
        session()->flash('success', 'Implementing Partner regions updated');
        return redirect(route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return StreamedResponse
     */
    public function regionDetach($roleCode, $roleId, ImplementingPartner $implementingPartner, Region $region)
    {
        $implementingPartner->regions()->detach([$region->id]);
        session()->flash('success', 'Implementing Partner region detached');
        return redirect(route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contactsAdd($roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        return view('category.implementing-partner.contactsModalBody', compact(
            'implementingPartner',
            'roleCode',
            'roleId'
        ));
    }

    /**
     * @param Request $request
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function manageContacts(Request $request, $roleCode, $roleId, ImplementingPartner $implementingPartner)
    {
        $users = [];
        foreach ($request->get('ambassadors') as $id) {
            $users[$id] = ['ambassador' => 1];
        }
        foreach ($request->get('admins') as $id) {
            if(isset($users[$id])) {
                $users[$id]['admin'] = 1;
            } else {
                $users[$id] = ['admin' => 1];
            }

        }
        $implementingPartner->users()->sync($users);

        session()->flash('success', 'Implementing Partner contacts updated');
        return redirect(route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param ImplementingPartner $implementingPartner
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function contactDetach($roleCode, $roleId, ImplementingPartner $implementingPartner, User $user)
    {
        $implementingPartner->users()->detach([$user->id]);
        session()->flash('success', 'Implementing Partner contact detached');
        return redirect(route('implementingPartner.view', [$roleCode, $roleId, $implementingPartner]));
    }
}
