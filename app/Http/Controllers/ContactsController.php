<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class ContactsController
 * @package App\Http\Controllers
 */
class ContactsController extends Controller
{
    public function view($roleCode, $roleId, User $user)
    {
        return view('category.contact.view', compact(
            'user',
            'roleCode',
            'roleId'
        ));
    }

    /**
     * @param Request $request
     * @param string $roleCode
     * @param string $roleId
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $roleCode, $roleId, User $user)
    {
        $validatedInput = $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'image' => 'image'
        ]);

        if($validatedInput['image'] instanceof UploadedFile) {
            $path = $validatedInput['image']->store('public/profile_pictures');
            $link = Storage::url($path);
            $validatedInput['user_photo_url'] = $link;
            unset($validatedInput['image']);
        }

        $user->update($validatedInput);


        session()->flash('success', 'User details updated');
        return redirect(route('contact.view', [$roleCode, $roleId, $user]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function deactivate($roleCode, $roleId, User $user)
    {
        $user->delete();
        session()->flash('success', 'User deactivated');

        return redirect(route('role.contacts', [$roleCode, $roleId]));
    }

    /**
     * @param $roleCode
     * @param $roleId
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function activate($roleCode, $roleId, User $user)
    {
        $user->restore();
        session()->flash('success', 'User activated');

        return redirect(route('contact.view', [$roleCode, $roleId, $user]));
    }
}