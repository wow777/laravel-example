#!/usr/bin/env bash

cd /root

DBPASS1="$(head -c -1 .digitalocean_password | cut -d'=' -f 2)"
DBPASS="$(echo ${DBPASS1} | cut -d'"' -f 2)"

source config.sh

if [[ $# -eq 0 ]];then
    apt -y install php-mbstring php-mcrypt php-xml php-gd zip
    curl -sS https://getcomposer.org/installer | php
    mv composer.phar /usr/local/bin/composer
    chmod +x /usr/local/bin/composer
    a2enmod rewrite
    adduser $USER
    usermod -aG sudo $USER
    cd /home/$USER
    mkdir web
    mkdir web/deploy
    mkdir web/config
    mkdir web/app1
    mkdir web/storage
    mkdir web/storage/app
    mkdir web/storage/app/public
    mkdir web/storage/framework
    mkdir web/storage/framework/cache
    mkdir web/storage/framework/sessions
    mkdir web/storage/framework/views
    mkdir web/storage/logs
    mkdir .ssh
    cp /root/.ssh/authorized_keys .ssh/
    chown -R $USER:$USER .ssh
    cp /root/server.sh web/deploy/
    cp /root/config.sh web/deploy/
    cp /root/.env web/config/
    echo 'Parked Domain' > web/app1/index.html
    ln -s /home/$USER/web/app1 /home/$USER/web/www
    chown -R $USER:$USER /home/$USER/web
    chown -R www-data:www-data /home/$USER/web/storage
fi
mysql -uroot -p$DBPASS -e "create database ${NEWDB}"
mysql -uroot -p$DBPASS -e "grant all on ${NEWDB}.* to ${NEWUSER}@localhost identified by '${NEWPASS}'"
cp /root/000-default.conf /etc/apache2/sites-available/
service apache2 restart
