#!/usr/bin/env bash

DATE=$(date +%s)
DIR="app-${DATE}"

source /home/$USER/web/deploy/config.sh

cd /home/$USER/web
tar -zxf app.tar.gz
mv app $DIR
if [ $? -eq 0 ]; then
    echo "Moved app directory into place"
else
    echo "Failed to get app directory. Exiting."
    exit
fi
rm app.tar.gz
cd $DIR
ln -s ../storage ./
cp ../config/.env ./

# Make sure the vendor folder exists
if [  ! -d "vendor" ]; then
    if [ ! -d "../vendor" ]; then
        composer install
        rsync -a vendor ../
    else
        rsync -a ../vendor ./
    fi
elif [ ! -d "vendor/laravel/src/framework/Illuminate" ]
then
    if [ ! -d "../vendor" ]; then
        rm -rf vendor
        composer install
    else
        rsync -a ../vendor ./
    fi
fi

php artisan cache:clear
composer dump-autoload
composer install
if [ ${MIGRATE} -gt 0 ]; then
    echo "Running migrations"
    php artisan migrate
fi
if [ ${DBSEED} -gt 0 ]; then
    echo "Running database seeds"
    php artisan db:seed
fi

# Symlink the new site root
rm /home/$USER/web/www && ln -s /home/$USER/web/$DIR /home/$USER/web/www

