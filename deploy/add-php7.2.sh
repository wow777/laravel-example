#!/usr/bin/env bash

add-apt-repository ppa:ondrej/php
apt update
apt install php7.2 php7.2-common php7.2-cli php7.2-fpm php7.2-mbstring php7.2-xml php7.2-mysql

#a2enmod proxy_fcgi setenvif
apt purge libapache2-mod-php7.0 libapache2-mod-php
apt install libapache2-mod-php7.2 libapache2-mod-php
a2enmod php7.2
a2enconf php7.2-fpm
service apache2 reload
