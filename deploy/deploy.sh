#!/usr/bin/env bash

if [[ $# -eq 0 ]]; then
    printf "\nYou did not provide a tag to deploy. Here are your currently available tags:\n\n"
    git tag -l
    printf "\n"
    exit
fi

source config.sh

TAG=$1

# Clone the repo
git clone ${REPO} app
if [ $? -eq 0 ]; then
	echo "Cloned ${REPO}"
else
	echo "Failed to clone repo"
	exit
fi

# Checkout the tag
cd app
git fetch --all --tags --prune
git checkout tags/${TAG}

if [ $? -eq 0 ]; then
	echo "Checked out ${TAG}"
else
	echo "Unable to checkout ${TAG}"
	exit
fi
mv src app
rm -rf app/storage
tar -zcf app.tar.gz app
mv app.tar.gz ../
cd ../

# Upload it and run the remote deploy script
UPLOAD_SITE="scp -i ${KEYFILE} app.tar.gz ${USER}@${SERVER}:${SERVER_PATH}/app.tar.gz"
echo "Running ${UPLOAD_SITE}"
${UPLOAD_SITE}
if [ $? -eq 0 ]; then
	echo "File uploaded"
	rm -rf app*
else
	echo "Failed to upload file"
	exit
fi
UPLOAD_DEPLOY="scp -i ${KEYFILE} server.sh config.sh ${USER}@${SERVER}:${SERVER_PATH}/deploy/"
echo "Running ${UPLOAD_DEPLOY}"
${UPLOAD_DEPLOY}
if [ $? -eq 0 ]; then
	echo "File uploaded"
	rm -rf app*
else
	echo "Failed to upload file"
	exit
fi

CMD="ssh -i ${KEYFILE} ${USER}@${SERVER} '/home/ontarget/web/deploy/server.sh'"
${CMD}