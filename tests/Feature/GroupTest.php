<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class GroupTest extends TestCase
{

    use DatabaseTransactions;

    /** @test */
    public function can_post_to_groups()
    {
        $response = $this->post('/api/groups/', [ "name" => "sample name here",
            "address_line1" => "1 happy st",
            "address_line2" => null,
            "city" => "some city",
            "state" => "MO",
            "county" => "Cass",
            "zip_code" => "64701",
            "church_id" => 21,
            "group_size" => "5",
            "country" => "United States of America",
            "initial_enrollment_name" => "pseudo-name",
            "initial_enrollment_email" => "me@test.com",
            "initial_enrollment_phone" => "8675309",
            "questions_comments" => "anything here"]);

        $response->assertSuccessful();
    }


    /** @test */
    public function can_view_all_active_groups()
    {
        $response = $this->get('/api/groups');

        $response->assertSuccessful();

        $response->assertJsonFragment([
            "church_id" => 177,
			"group_size" => "11-20",
            "address_line1" => "9354 E Hillview Circle",
			"address_line2" => "",
			"city" => "Mesa",
			"state_id" => 3,
			"zip_code_id" => 85207,
        ]);

    }
}
