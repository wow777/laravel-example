<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TeamTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function can_get_teams_by_zip_code()
    {
        $response = $this->get('/api/users/team/64137');

        $response->assertStatus(201);

        // State Director
        $response->assertJsonFragment([
            'exists' => true,
            'name' => 'Tate Williams',
            'role' => 'State Director',
            'email' => 'tate@goproject.org',
            'region' => 'MO-1 KC',
            'implementing_partner' => 'Alaska Test',
            'image' => '<img src="https://s3.amazonaws.com/careportal-team/Tate-Williams.jpg" />',
        ]);

        // Regional Manager
        $response->assertJsonFragment([
            'exists' => true,
            'name' => 'Micah Rose Emerson',
            'role' => 'Regional Manager',
            'email' => 'micah@goproject.org',
            'region' => 'MO-1 KC',
            'implementing_partner' => 'The Global Orphan Project',
            'image' => '',
        ]);

        // Coordinator (Community)
        /*  commented out until we figure out coordinator
        $response->assertJsonFragment([
            'exists' => false,
        ]);
        */
    }

    /** @test */
    public function get_error_response_when_supplying_invalid_zip()
    {
        $response = $this->get('/api/users/team/11111');

        $response->assertStatus(422);
    }

    /** @test */
    public function get_all()
    {
        $response = $this->get('/api/users/team/all');

        $response->assertStatus(201);

        $response->assertJsonFragment([
            'name' => 'Abby Fry',
            'title' => ['Regional Manager'],
            'regions' => ['KS-1 NE'],
            'implementing_partner' => 'The Global Orphan Project',
            'email' => 'abby@goproject.org',
            'user_photo_url' => '<img src="https://s3.amazonaws.com/careportal-team/Abby-Fry.jpg" />',
        ]);

        $response->assertJsonFragment([
            'name' => 'Chris Campbell',
            'title' => ['Regional Manager', 'State Director'],
            'regions' => ['OK-1 NW', 'OK-2 SW', 'OK-4 SE', 'OK-6 Tulsa'],
            'implementing_partner' => '111Project Oklahoma',
            //'state_regions' => ["OK-1 NW", "OK-2 SW", "OK-3 OKC", "OK-4 SE", "OK-5 NE", "OK-6 Tulsa"],
            'email' => 'ccampbell@careportal.org',
            'user_photo_url' => '<img src="https://s3.amazonaws.com/careportal-team/Chris-Campbell.jpg" />',
        ]);

        $response->assertJsonFragment([
            "name" => "Katie Mohr",
            "title" => [ "Regional Manager"],
            "regions" => ["MO-3 East"],
            'implementing_partner' => 'The Global Orphan Project',
            //"state_regions" => [],
            "email" => "katie@goproject.org",
            "user_photo_url" => '<img src="https://s3.amazonaws.com/careportal-team/Katie-Mohr.jpg" />',
        ]);


    }
}
