All route are prefixed with /api

Zipcode-

GET /zipcode/{zipcode} returns zipcode, state, city

Users-

GET /users/ show all users
GET /users/{id} show user by id
GET /users/email/{email} show user by email
GET /users/{id}/communities/ show all users communites
POST /users/add-communities adds users to communities
	if user_id is available we look user up by id 
	if email matches an existing user we use that 
	otherwise we create user with the first name last name and email
	required array of communities

	communities[0][id] = 2
	communities[0][join] = false

	communities[1][id] = 2
	communities[1][join] = true
