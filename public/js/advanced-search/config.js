$(function () {
    window.AdvancedSearch = function (option) {
        this.datefields = option.datefields;
        this.search_body = option.search_body;
        this.fields = option.hasOwnProperty('fields') ? option.fields : null;

        this.init = function () {
            switch (option.page) {
                case 'trip': {
                    $(document).ready(_this.createDateFields);
                    break;
                }
                default:
                    break;
            }

            $(document).on('click', option.and_btn, _this.addSearchList);
            $(document).on('click', option.delete_and_list, _this.deleteSearchList);
        };

        this.createDateFields = function () {
            _this.datefields.map(function (date) {
                $(date).datepicker({
                    "format": 'mm/dd/yyyy'
                })
            })
        };

        this.addSearchList = function () {
            var search_btn = $(option.advanced_search_btn),
                last_child = $(option.advanced_last_child),
                data_id = last_child.data('id');
            $(option.search_body).append(_this[option.page + "AdvancedSearchBody"](data_id));
            $(option.advanced_last_child).find(option.and_btn + "[data-id=" + parseInt(data_id + 1) + "]").after(search_btn);
            console.log(1);
        };

        this.deleteSearchList = function () {
            var search_btn = $(option.advanced_search_btn),
                data_id = $(this).data('id');
            if (data_id !== 1) {
                $(this).parents(option.search_body).find("div[data-id=" + data_id + "]").remove();
            }
            $(option.advanced_last_child).find(option.and_btn).after(search_btn);
        };

        this.tripAdvancedSearchBody = function (data_id) {
            var new_data_id = data_id + 1;
            return '<div class="row advanced-search" data-id=' + new_data_id + '>\n' +
                '           <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 text-right advenced-if">IF</div>\n' +
                '                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ml-0">\n' +
                '                        <select class="form-control br-1 field">\n' +
                '                            <option selected>Field</option>\n' +
                '                            <option value="status">Status</option>\n' +
                '                            <option value="type">Type</option>\n' +
                '                        </select>\n' +
                '                    </div>\n' +
                '                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '                        <select class="form-control br-1 value">\n' +
                '                            <option selected>Value</option>\n' +
                '                        </select>\n' +
                '                    </div>\n' +
                '                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 p-0">\n' +
                '                        <button class="btn bg-blue mb-5 trip-advanced-search-and-btn" data-id="' + new_data_id + '">AND</button>\n' +
                '                        <button class="text-red b-0 bg-none advance-delete-btn" data-id="' + new_data_id + '"><i class="fa fa-trash" aria-hidden="true"></i></button>\n' +
                '                    </div>\n' +
                '               </div>';
        };

        this.grantAdvancedSearchBody = function (data_id) {
            var new_data_id = data_id + 1;
            return '<div class="row collapse in mb-1 advanced-search" data-id="' + new_data_id + '">' +
                '  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-lg-offset-2 col-md-offset-1 col-sm-offset-1  text-right advenced-if">IF</div>\n' +
                '    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '      <select class="form-control br-1 field">\n' +
                '         <option selected>Field</option>\n' +
                '         <option value="date_deadline">Deadline</option>\n' +
                '         <option value="status">Status</option>\n' +
                '         <option value="date_next_action">Next Action</option>\n' +
                '         <option value="ministry_division">Ministry</option>\n' +
                '         <option value="foundation_name">Foundation name</option>\n' +
                '         <option value="date_received">Date Received</option>\n' +
                '         <option value="last_contact">Last Contact</option>\n' +
                '         <option value="created_at">Date Written</option>\n' +
                '         <option value="date_submitted">Date Submitted</option>\n' +
                '         <option value="requested_amount">Requested Amount</option>\n' +
                '         <option value="received_amount">Amount Received</option>\n' +
                '       </select>\n' +
                '     </div>\n' +
                '     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '     <select class="form-control br-1 operation">\n' +
                '         <option selected>Operations</option>\n' +
                '         <option><</option>\n' +
                '         <option>></option>\n' +
                '         <option>=</option>\n' +
                '         <option>!=</option>\n' +
                '         <option><=</option>\n' +
                '         <option>>=</option>\n' +
                '         </select>\n' +
                '      </div>\n' +
                '                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '                    <input type="text" class="form-control br-1 value" placeholder="Enter value" aria-describedby="basic-addon1">\n' +
                '                </div>\n' +
                '                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 p-0">\n' +
                '                    <button class="btn bg-blue mb-5 grant-advanced-search-and-btn" data-id="' + new_data_id + '">AND</button>\n' +
                '                    <button class="text-red b-0 bg-none advance-delete-btn" data-id="' + new_data_id + '"><i class="fa fa-trash" aria-hidden="true"></i></button>\n' +
                '   </div>\n' +
                ' </div>\n';
        };


        this.supportersAdvancedSearchBody = function (data_id) {
            return this.universalAdvancedSearchBody(data_id, this.fields, option.and_btn);
        };

        this.foundersAdvancedSearchBody = function (data_id) {
            return this.universalAdvancedSearchBody(data_id);
        };

        this.campaignsAdvancedSearchBody = function (data_id) {
            return this.universalAdvancedSearchBody(data_id);
        };

        this.fundsAdvancedSearchBody = function (data_id) {
            return this.universalAdvancedSearchBody(data_id);
        };


        /**
         * Function to generate new search row based on the specified parameters.
         *
         * option.fields,option.and_btn are required
         *
         * @param data_id
         * @returns {string}
         */
        this.universalAdvancedSearchBody = function (data_id) {
            var options = '';
            var searchableFields = option.fields ? option.fields : null;
            if (searchableFields) {
                for (var k in searchableFields) {
                    if (searchableFields.hasOwnProperty(k)) {
                        options += '<option value="' + k + '">' + searchableFields[k] + '</option>\n'
                    }
                }
            }

            var new_data_id = data_id + 1;

            return '<div class="row collapse in mb-1 advanced-search" data-id="' + new_data_id + '">' +
                '  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1  text-right advenced-if">IF</div>\n' +
                '    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '      <select class="form-control br-1 field">\n' + options + '\n</select>\n' +
                '     </div>\n' +
                '     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '     <select class="form-control br-1 operation">\n' +
                '         <option selected>Operations</option>\n' +
                '         <option><</option>\n' +
                '         <option>></option>\n' +
                '         <option>=</option>\n' +
                '         <option>!=</option>\n' +
                '         <option><=</option>\n' +
                '         <option>>=</option>\n' +
                '         </select>\n' +
                '      </div>\n' +
                '                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
                '                    <input type="text" class="form-control br-1 value" placeholder="Enter value" aria-describedby="basic-addon1">\n' +
                '                </div>\n' +
                '                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 p-0">\n' +
                '                    <button class="btn bg-blue mb-5 ' + (option.and_btn).replace('.', '') + '" data-id="' + new_data_id + '">AND</button>\n' +
                '                    <button class="text-red b-0 bg-none advance-delete-btn" data-id="' + new_data_id + '"><i class="fa fa-trash" aria-hidden="true"></i></button>\n' +
                '   </div>\n' +
                ' </div>\n';
        };

        var _this = this;
        this.init();
        return this;
    };
});