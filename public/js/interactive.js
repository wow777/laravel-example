document.addEventListener('DOMContentLoaded', function () {
    let selector = document.querySelector('select[name="giving_preference"]');
    if (selector)
        selector.onchange = changeEventHandler;
}, false);

function changeEventHandler(event) {

    let e = document.getElementById("giving_preference");
    let selected = e.options[e.selectedIndex].value;


    let list, index;
    list = document.getElementsByClassName("add-inputs");
    for (index = 0; index < list.length; ++index) {
        list[index].setAttribute("hidden", "hidden");
    }

    if (selected == '4') {
        document.getElementById("giv_freq_amount").removeAttribute("hidden");
    }
    else if (selected == '5') {
        document.getElementById("datePic").removeAttribute("hidden");
    }
}

let slides = (document.getElementsByClassName("badge-number"));
for (let i = 0; i < slides.length; i++) {
    let number = (slides.item(i).childNodes[1]);
    let length = number.innerText.length;

    if (length >= 3 && length < 5) {
        number.style.fontSize = '68px';
    }
    else if (length >= 5 && length < 7) {
        number.style.fontSize = '56px';
    }
    else if (length >= 7 && length < 10) {
        number.style.fontSize = '44px';
    }
    else if (length >= 10 && length < 15) {
        number.style.fontSize = '32px';
    }

}

