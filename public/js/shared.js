(function ($) {

	var SharedAssets = function () {
		this.init = function () {
			_this.initDatePickers();
			$(document).on('change', '.file-upload__input', _this.handleProfilePhoto);
			$(document).on('click', '.enable-editing', _this.enableEditing);
			$(document).on('click', '.btn-user-detail', _this.preventDisabled);
			$(document).on('click', '.fund-menu', _this.handleMenu);
			//Disable editing
			_this.disableEditing();
		};
		this.handleMenu = function () {
			var link = $(this).children('a').attr('href');
			window.location.href=link;
        };
		this.readProfileUrl = function ($input, $preview) {
			var inputEl = $input.get(0);
			if (inputEl.files && inputEl.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$preview.attr('src', e.target.result);
				};
				reader.readAsDataURL(inputEl.files[0]);
			}
		};
		this.handleProfilePhoto = function () {
			var $self = $(this);
			var $preview = $self.closest('.file-upload').prev('.photo-preview');
			_this.readProfileUrl($self, $preview);
		};
		this.initDatePickers = function () {
			var $dps = $('.dp-from-format');
			if ($dps.length > 0) {
				$dps.each(function(){
					$(this).datepicker();
				});
			}
		};
		this.disableEditing = function () {
			$('.form-locked').find(':input:not(:disabled)').prop('disabled', true);
			$('.user-detail-save').prop('disabled', true).addClass('disabled');
			$('.user-detail-delete').prop('disabled', true).addClass('disabled');
		};
		this.enableEditing = function () {
			$('.form-locked').find(':input').prop('disabled', false);
			$('.user-detail-save').prop('disabled', false).removeClass('disabled');
			$('.user-detail-delete').prop('disabled', false).removeClass('disabled');
		};
		this.preventDisabled = function (e) {
			var isDisabled = $(this).is(':disabled');
			if (isDisabled) {
				e.preventDefault();
				return false;
			}
		};

		var _this = this;
		this.init();
		return this;
	};

	new SharedAssets();


	//Global functions

    /**
     * Outputs bootstrap alert message
     * @param data
     * @param type
     */
    $.makeAlert = function (data, type) {
        var message = '';
        if( type === 'danger' ) {
            message = '<p>Please fix the following errors:</p>';
            message += '<ul>';
            for(var i = 0; i < data.errors.length; i++) {
                message += '<li>'+data.errors[i]+'</li>';
            }
            message += '</ul>';
        } else {
            message = data;
        }
        message = '<div class="modal-errors alert alert-' + type + '">' + message + '</div>';
        $('.form-status').html(message);
    };


    $.initDatepickers = function() {
        var $dps = $('.datepicker');
        if ($dps.length) {
            $dps.each(function () {
                $(this).datepicker();
            });
        }
	}


})(jQuery);