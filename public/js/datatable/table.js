// $(function () {
//
//     var href = window.location.href;
//     var arr = href.split('/');
//     var type = arr[arr.length - 1];
//
//     $.initDatepickers();
//     new Datatable({
//         page: type,
//         datatable: '#'+ type +'-datatable',
//         length: '.dataTables_length',
//         info: '.dataTables_info',
//         search: '#'+ type +'-search',
//         tbody: '#'+ type +'-datatable tbody',
//         filter: '#filter',
//         start_date: '#start_date',
//         end_date: '#end_date',
//         search_field: '.field',
//         value_field: '.value',
//         select: false,
//         open_advance: '.open-advance',
//         advanced_search: "#advanced-search",
//         search_list: '.advanced-search',
//         advance_search: '#searchAdvenced',
//         top_show_all_enabled: false,
//         top_show_entries_count: true,
//         columns: [
//             null,
//             null,
//             null,
//             null,
//             null,
//             null,
//             null,
//             null
//         ]
//     });
//
//     // new AdvancedSearch({
//     //   page:''+ type +'',
//     //   search_body:'#'+ type +'-search-body',
//     //   and_btn:'.'+ type +'-advanced-search-and-btn',
//     //   advanced_search_btn:'#searchAdvenced',
//     //   advanced_last_child:'.advanced-search:last-child',
//     //   delete_and_list:'.advance-delete-btn',
//     //   fields: $('.open-advance').data('advanced-search-fields')
//     // });
// });

$(document).ready(function() {

    $('#datatable').dataTable( {
        'paging': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'responsive': true,
        "pageLength": 10,
        "dom": 'B<"top"i>rt<"bottom"flp><"clear">',
        "buttons": [
            {
                extend: 'csv',
                text: 'Export'
            }

        ],
        "pagingType": "input",
        "processing": true,
        "language": {
            "paginate": {
                "sFirst": "FIRST",
                "sLast": "LAST",
                "sNext": "NEXT",
                "sPrevious": "PREV"
            },
            "lengthMenu": 'Show &nbsp; <select>' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="50">100</option>' +
            '<option value="-1">All</option>' +
            '</select>'
        }
    } );
} );