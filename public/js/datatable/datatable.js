$(function () {

  window.Datatable = function (option) {

    this.items = [];
    this.options = option;
    this.datatable_html = $(option.datatable);
    this.datatable = $(option.datatable);
    this.tbody = $(option.tbody);
    this.search_body = $(option.search_body);
    this.advanced_search = $(option.advanced_search);
    this.top_body = $(option.top_body);
    this.top_show_all_enabled = option.hasOwnProperty('top_show_all_enabled') ? option.top_show_all_enabled : false;
    this.top_show_entries_count = option.hasOwnProperty('top_show_entries_count') ? option.top_show_entries_count : false;
    this.field_values = {
      'value_fields': ['Public', 'Private'],
      'status_field': ['New','Pending','Posted','Completed','Deactivated']
    };
    this.datatable_processing = '.dataTables_processing';
    this.badges = option.badges;
    this.init = function () {
      $(option.info).remove();

      _this.datatable = _this.table();

      $(option.length).addClass('pull-right');

      $(document).on('click', '.showall', _this.handleShowAll);

      if (option.page === 'trip') {
        $(document).on('click', option.filter, _this.filterByDate);
        $(document).on('change', option.search_field, _this.changeField);
        $(document).on('click', option.advance_search, _this.advanceSearch);
        this.badges.map(function (badge) {
          $(document).on('click', "#" + badge, _this.tripBadgeCollection);
        });
      } else if (option.page === 'grant') {
        $(document).on('click', option.advance_search, _this.advanceSearch);
        $(document).on('change', option.search_field, _this.fieldChange);
        $(document).on('click', "#" + this.badges.first, _this.grantBadgeCollection);
        $(document).on('click', "#" + this.badges.second, _this.grantBadgeCollection);
        $(document).on('click', "#" + this.badges.third, _this.grantBadgeCollection);
      } else if (option.page === 'supporters' || option.page === 'founders') {
        $(document).on('click', option.filter, _this.filterByDate);
        $(document).on('change', option.search_field, _this.changeField);
        $(document).on('click', option.advance_search, _this.advanceSearch);
      } else if (option.page === 'campaigns') {
        $(document).on('click', option.filter, _this.filterByDate);
        $(document).on('change', option.search_field, _this.changeField);
        $(document).on('click', option.advance_search, _this.advanceSearch);
      } else if (option.page === 'trip-leader') {
        //
      } else if (option.page === 'funds') {
        $(document).on('click', option.filter, _this.filterByDate);
        $(document).on('change', option.search_field, _this.changeField);
        $(document).on('click', option.advance_search, _this.advanceSearch);
      }

      $(document).on('keyup', option.search, _this.search);
      $(option.search).on('search', _this.datatableCleanSearch);
      _this.tbody.on('click', 'tr', _this.edit);
      $(document).on('click', option.open_advance, _this.advanceOpen);
    };

    this.table = function () {
      var table = _this.datatable.DataTable({
        'paging': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'responsive': true,
        'columns': option.columns,
        "dom": 'B<"top"i>rt<"bottom"flp><"clear">',
        "buttons": [

          {
            extend: 'csv',
            text: 'Export'
          }

        ],
        "pagingType": "input",
        "processing": true,
        "language": {
          "paginate": {
            "sFirst": "FIRST",
            "sLast": "LAST",
            "sNext": "NEXT",
            "sPrevious": "PREV"
          },
          "lengthMenu": 'Show &nbsp; <select>' +
          '<option value="10">10</option>' +
          '<option value="20">20</option>' +
          '<option value="30">30</option>' +
          '<option value="40">40</option>' +
          '<option value="50">50</option>' +
          '<option value="50">100</option>' +
          '<option value="-1">All</option>' +
          '</select>'
        }
      });

      table.buttons().container().appendTo('#export-dest');

      var $top = $('div.top');

      $top.prepend('<div class="top-left"></div>');
      if (_this.top_show_all_enabled) {
        $top.find('.top-left').prepend('<button class="showall">Show all</button>');
      }
      if (!_this.top_show_entries_count) {
        $top.find('.dataTables_info').hide();
      }

      return table;
    };



    this.changeField = function () {
      var value = $(this).closest(option.search_list).find(option.value_field).html('');

      if (this.value === 'type') {
        _this.field_values.value_fields.map(function (item) {

          if (item === 'Public') {
            value.append('<option value=' + 2 + '>' + item + '</option>');
          } else if (item === 'Private') {
            value.append('<option value=' + 1 + '>' + item + '</option>');
          }
        });
      } else if (this.value === 'status') {
        _this.field_values.status_field.map(function (item) {
          value.append('<option>' + item + '</option>');
        });
      }
    };

    this.handleShowAll = function () {
      var $lengthSelector = $(this).closest('.dataTables_wrapper').find('.dataTables_length select');
      if ($lengthSelector.length > 0) {
        $lengthSelector.find("option[value='-1']").attr('selected', 'selected');
      }

      $(_this.datatable_processing).show();

      $.ajax({
        url: option.page + '/all',
        method: "GET",
        success: function (res) {
          if (res.status === 'success') {
            $(_this.datatable_processing).hide();
            switch (option.page) {
              case 'trip':_this.tripsDatatableCleanAndDraw(res.trips);break;
              case 'grant':_this.grantsDatatableCleanAndDraw(res.grants);break;
              case 'supporters':_this.supportersDatatableCleanAndDraw(res.supporters);break;
              case 'founders':_this.foundersDatatableCleanAndDraw(res.founders);break;
              case 'campaigns':_this.campaignsDatatableCleanAndDraw(res.campaigns);break;
              case 'funds':_this.fundsDatatableCleanAndDraw(res.funds);break;
              default:break;
            }

            _this.datatable.page.len(-1).draw();
          }
        },
        error: function (res) {

        }
      });
    };

    this.datatableCleanSearch = function () {
      _this.datatable.search($(this).val()).draw();
    };

    this.search = function () {
      $('#' + option.page + '-datatable_paginate').show();
      _this.datatable.search(this.value).draw();
    };

    this.edit = function () {
      var data_id = $(this).data('id')
      if(typeof data_id !== 'undefined'){
        if(option.page === 'regions') {
          window.open('/rm/' + data_id, '_blank');
          return;
        }
      }
    };

    this.filterByDate = function () {

      var start_date = $(option.start_date);
      var end_date = $(option.end_date);

      if (start_date.val() !== '' && end_date.val() !== '') {
        start_date.removeClass('b-error');
        end_date.removeClass('b-error');
        $(_this.datatable_processing).show();

        $.ajax({
          url: option.page + '/filter',
          method: 'GET',
          data: {
            start_date: start_date.val().trim(),
            end_date: end_date.val().trim()
          },
          success: function (res) {
            if (res.status === 'success') {
              $(_this.datatable_processing).hide();
              switch (option.page){
                case 'supporters': _this.supportersDatatableCleanAndDraw(res.supporters); break;
                case 'trip': _this.tripsDatatableCleanAndDraw(res.trips); break;
                case 'campaigns': _this.campaignsDatatableCleanAndDraw(res.campaigns); break;
                case 'founders': _this.foundersDatatableCleanAndDraw(res.founders); break;
                case 'funds': _this.fundsDatatableCleanAndDraw(res.funds); break;
                default: break;
              }
            }
          }
        })
      } else {
        start_date.addClass('b-error');
        end_date.addClass('b-error');
      }
    };

    this.advanceSearch = function () {

      this.search_list = $(option.search_list);
      var collapsed = $(this).parent().find('#open-advance');

      var operation;
      var operation_val;

      if (!collapsed.hasClass('closed')) {
        var info_ok = true;

        for (var i = 0; i < this.search_list.length; i++) {
          var field = this.search_list.eq(i).find(option.search_field),
            value = this.search_list.eq(i).find(option.value_field),
            field_val = field.val().trim(),
            value_val = value.val().trim();

          if (option.page === 'trip') {
            if (field_val !== 'Field' && value_val !== 'Value') {

              field.removeClass('b-error');
              value.removeClass('b-error');

              _this.items.push({
                'field': field_val,
                'value': value_val
              });
            } else {
              info_ok = false;
              field.addClass('b-error');
              value.addClass('b-error');
            }

          } else if ($.inArray(option.page, ['grant', 'supporters', 'founders', 'funds', 'campaigns']) > -1) {

            operation = this.search_list.eq(i).find('.operation');
            operation_val = operation.val().trim();

            if (field_val !== 'Field' || field_val !== '' && operation_val !== 'Operations' || operation_val !== '' && value_val !== '') {
              field.removeClass('b-error');
              operation.removeClass('b-error');
              value.removeClass('b-error');

              _this.items.push({
                'field': field_val,
                'operator': operation_val,
                'value': value_val
              });

            } else {
              info_ok = false;
              field.addClass('b-error');
              operation.addClass('b-error');
              value.addClass('b-error');
              break;
            }

          }
        }

        if (info_ok) {
          _this.sendForSearch();
        }
      } else {
        _this.search_body.find('.alert-danger').remove();
      }
    };

    this.fieldChange = function () {
      var val = this.value,
        operation = $(this).parent().parent().find(option.operation);
      if (val === 'status' || val === 'ministry_division' || val === 'foundation_name') {
        operation.prop('disabled', true).val('=');
      } else {
        operation.prop('disabled', false);
      }
    };

    this.advanceOpen = function () {
      var collapse_two = _this.advanced_search.find(option.open_advance),
        search_field = _this.advanced_search.find(option.search_field),
        value_field = _this.advanced_search.find(option.value_field);

      if ($(this).hasClass('closed') && option.page === 'grant') {
        search_field.val('Field');
        _this.advanced_search.find(option.operation).prop('disabled', false).val('Operations');
        value_field.val('');
        collapse_two.removeClass('closed');
        $(this).removeClass('closed');
      } else if ($(this).hasClass('closed') && option.page === 'trip') {
        search_field.val('Field');
        value_field.val('Value');
        collapse_two.removeClass('closed');
        $(this).removeClass('closed');
      } else {
        var list = $(option.search_list);
        if (list.length > 1) {
          for (var i = 0; i < list.length; i++) {
            if (i > 0) list.eq(i).remove();
          }
        }
        $(this).addClass('closed');
        collapse_two.addClass('closed');
      }
    };

    this.sendForSearch = function () {
      $(_this.datatable_processing).show();

      if (!$.isEmptyObject(_this.items)) {
        $.ajax({
          url: option.page + '/search',
          method: "GET",
          data: {
            terms: _this.items
          },
          success: function (res) {
            if (res.status === 'success') {
              $(_this.datatable_processing).show();
              _this.items = [];
              switch (option.page) {
                case 'trip':_this.tripsDatatableCleanAndDraw(res.trips);break;
                case 'grant':_this.grantsDatatableCleanAndDraw(res.grants);break;
                case 'supporters':_this.supportersDatatableCleanAndDraw(res.supporters);break;
                case 'founders':_this.foundersDatatableCleanAndDraw(res.founders);break;
                case 'campaigns':_this.campaignsDatatableCleanAndDraw(res.campaigns);break;
                case 'funds':_this.fundsDatatableCleanAndDraw(res.funds);break;
                default:break;
              }
            }
          },
          error: function (res) {

          }
        });
      }
    };

    this.grantBadgeCollection = function () {
      this.number = $(this).find('.badge-number').find('h3').text().trim();

      if (this.number > 0) {
        if ($(this).hasClass(_this.badges.first)) {
          this.select_from = 'date_deadline';
        } else if ($(this).hasClass(_this.badges.second)) {
          this.select_from = 'date_received';
        } else if ($(this).hasClass(_this.badges.third)) {
          this.select_from = 'status';
        }
        _this.selectFrom(this.select_from);
      }
    };

    this.tripBadgeCollection = function () {
      var badge = $(this);
      this.number = $(this).find('.badge-number').find('h3').text().trim();

      if (this.number > 0) {
        var type = _this.badges.filter(function (item) {
          return badge.hasClass(item) ? item : '';
        });

        _this.selectFrom(type);
      }
    };

    this.selectFrom = function (select_from) {
      $(_this.datatable_processing).show();

      $.ajax({
        url: option.page + '/select-badges',
        method: 'GET',
        data: {field: select_from+""},
        success: function (res) {
          if (res.status === 'success') {
            $(_this.datatable_processing).hide();
            if (option.page === 'grant')
              _this.grantsDatatableCleanAndDraw(res.grants);
            else if (option.page === 'trip')
              _this.tripsDatatableCleanAndDraw(res.trips);
          }
        },
        error: function (res) {

        }
      });
    };

    this.tripsDatatableCleanAndDraw = function (data) {
      var tr = '';
      if(data.length){
        console.log(data);
        tr += data.map(function (list) {
          var location_country = list.location_country !== null ? list.location_country : '',
            max_travelers = list.max_travelers !== null ? list.max_travelers : 0,
            location_city = list.location_city !== null ? list.location_city : '',
            leader_firstname = list.leader !== null ? list.leader.user.first_name : '',
            leader_lastname = list.leader !== null ? list.leader.user.last_name : '',
            total_cost = list.total_cost !== null ? list.total_cost : '',
            missing_info = list.missing_info !== null ? list.missing_info : '';
          var td = '<td>' + list.created_at + '</td>' +
            '<td class="text-center">' + list.status + '</td>' +
            '<td class="text-center">' + list.type.name + '</td>' +
            '<td>' + list.name + '</td>' +
            '<td>' + location_city + '</td>' +
            '<td>' + location_country + '</td>' +
            '<td>' + leader_firstname + ' ' + leader_lastname + '</td>' +
            '<td class="text-center">' + total_cost + '</td>' +
            '<td>' + max_travelers + '</td>' +
            '<td class="text-center">' + missing_info + '</td>';

          return '<tr data-id=' + list.id + '>' + td + '</tr>';
        });
      }

      var $lengthSelector = _this.datatable_html.closest('.dataTables_wrapper').find('.dataTables_length select');
      if ($lengthSelector.length) {
        $lengthSelector.find("option[value='-1']").attr('selected', 'selected');
      }

      _this.datatable.clear().draw();
      _this.datatable.rows.add( $(tr) ).draw();
    };

    this.supportersDatatableCleanAndDraw = function (data) {
      if (data.length > 0) {
        var tr = '';
        data.map(function (user) {
          var city = (!user.address) ? '' : user.address.city;
          var state = (!user.address) ? '' : user.address.state;
          var country = (!user.address) ? '' : user.address.country;
          var td = '<td>' + user.first_name + '</td>' +
            '<td>' + user.last_name + '</td>' +
            '<td>' + user.spouse_name + '</td>' +
            '<td>' + city + '</td>' +
            '<td>' + state + '</td>' +
            '<td>' + country + '</td>' +
            '<td>' + '$' + user.total_direct_donations + '</td>' +
            '<td>' + user.total_trips + '</td>' +
            '<td>' + user.total_campaigns + '</td>' +
            '<td>' + '$' + user.total_donations + '</td>' +
            '<td>' + user.total_logins + '</td>' +
            '<td>' + 'Not set' + '</td>' +
            '<td>' + 'Not set' + '</td>' +
            '<td>' + 'Not set' + '</td>';
          tr += '<tr data-id=' + user.id + '>' + td + '</tr>';
        });
      }
      _this.datatable.clear().draw();
      _this.datatable.rows.add($(tr)).draw();
    };

    this.foundersDatatableCleanAndDraw = function (data) {
      if (data.length > 0) {
        var tr = '';
        data.map(function (user) {
          var city = (!user.address) ? '' : user.address.city;
          var state = (!user.address) ? '' : user.address.state;
          var td = '<td>' + user.first_name + '</td>' +
            '<td>' + user.last_name + '</td>' +
            '<td>' + city + '</td>' +
            '<td>' + state + '</td>' +
            '<td>' + '$' + user.total_direct_donations + '</td>' +
            '<td>' + user.total_trips + '</td>' +
            '<td>' + user.total_campaigns + '</td>' +
            '<td>' + '$' + user.total_donations + '</td>' +
            '<td>' + user.total_logins + '</td>' +
            '<td>' + 'Not set' + '</td>' +
            '<td>' + 'Not set' + '</td>' +
            '<td>' + 'Not set' + '</td>';
          tr += '<tr data-id=' + user.founder.id + '>' + td + '</tr>';
        });
      }
      _this.datatable.clear().draw();
      _this.datatable.rows.add($(tr)).draw();
    };

    this.fundsDatatableCleanAndDraw = function (data) {
      if (data.length > 0) {
        var tr = '';
        data.map(function (list) {
          var td = '<td>' + list.created_at + '</td>' +
            '<td>' + list.status + '</td>' +
            '<td>' + list.name + '</td>' +
            '<td>' + list.L1 + '</td>' +
            '<td>' + list.L2 + '</td>' +
            '<td>' + list.L3 + '</td>' +
            '<td>' + list.L4 + '</td>' +
            '<td>' + list.L5 + '</td>' +
            '<td>' + list.dollars_donated + '</td>' +
            '<td>' + list.id + '</td>' +
            '<td width="15%">' + list.description + '</td>';
          tr += '<tr data-id=' + list.id + '>' + td + '</tr>';
        });
      }
      _this.datatable.clear().draw();
      _this.datatable.rows.add($(tr)).draw();
    };

    this.campaignsDatatableCleanAndDraw = function (data) {
      if (data.length > 0) {
        var tr = '';
        data.map(function (list) {
          var city = (!list.user.address) ? '' : list.user.address.city;
          var td =
            '<td>' + list.end_date + '</td>' +
            '<td>' + list.user.first_name + '</td>' +
            '<td>' + list.user.last_name + '</td>' +
            '<td>' + city + '</td>' +
            '<td>' + list.goal + '</td>' +
            '<td>' + list.total_raised + '</td>' +
            '<td>' + list.L1 + '</td>' +
            '<td>' + list.L2 + '</td>' +
            '<td>' + list.L3 + '</td>' +
            '<td>' + list.L4 + '</td>' +
            '<td>' + list.L5 + '</td>';
          tr += '<tr data-id=' + list.id + '>' + td + '</tr>';
        });
      }
      _this.datatable.clear().draw();
      _this.datatable.rows.add($(tr)).draw();
    };

    this.grantsDatatableCleanAndDraw = function (data) {
      if(data.length){
        var tr = '';
        data.map(function (list) {
          var date_deadline = list.date_deadline ? list.date_deadline : 'None',
            date_next_action = list.date_next_action ? list.date_next_action : 'None',
            date_submitted = list.date_submitted ? list.date_submitted : 'None',
            requested_amount = list.requested_amount ? list.requested_amount : 'None',
            received_amount = list.received_amount ? list.received_amount : 'None';

          var td = '<td>' + date_deadline + '</td>' +
            '<td>' + list.status + '</td>' +
            '<td>' + date_next_action + '</td>' +
            '<td>' + list.ministry_division + '</td>' +
            '<td>' + list.foundation_name + '</td>' +
            '<td>' + list.date_received + '</td>' +
            '<td>' + list.last_contact + '</td>' +
            '<td>' + list.created_at + '</td>' +
            '<td>' + date_submitted + '</td>' +
            '<td>' + requested_amount + '</td>' +
            '<td>' + received_amount + '</td>';

          tr += '<tr data-id=' + list.id + '>' + td + '</tr>';
        });

        _this.datatable.clear().draw();
        _this.datatable.rows.add( $(tr) ).draw();
      }
    };

    this.get_val = function (object) {
      var def = '';
      if (object === 0) {
        return def;
      }
      return object;
    };


    var _this = this;
    this.init();
    return this;
  };

});