
$(document).ready(function () {

    //onclick for each button
    $("#calendar").click(function () {
        getEmail();
    });


    $("#functionList").click(function () {
        getData();
        $('#functionListModal').modal('toggle');
    });




    // click red envelope to add email addresses
    $("#mailto").click(function() {
        var emails = $("#datatable tbody tr").filter(function() {
            return $(this).find("input[type=checkbox]").prop('checked');
        }).map(function() {
            return $( this ).find( "td:eq(12)" ).text();
        }).get();

        window.location.href = 'mailto:' + emails;
    });
});


function getData() {
    var $data = [];

    $("#datatable tbody tr").each(function () {

        var x = $(this);
        var cells = x.find('td');
        var checkbox = $('#datatable :checkbox:checked').closest('tr');

        $(cells).each(function () {
            var $d = $("input[type=checkbox]:checked", this).val() || $(this).text();
            $data.push($d);
        });
        //console.log($data);

    });
    return $data;
}


function getEmail() {
    var emails = $("#datatable tbody tr").filter(function() {
        return $(this).find("input[type=checkbox]").prop('checked');
    }).map(function() {
        return $( this ).find( "td:eq(12)" ).text();
    }).get();
    //console.log(emails);
}