/** Start Resource code **/

function addCategoriesDialog(){
	$('#addNextCategoryPart').hide();
	$('#addNextCategoryButton').show();
	$('#addCategoriesDialog').modal('show');
}

function deleteCategory(id){
	if(id != ''){
		var $confirm = $("#deleteResourcesDialog");
		$confirm.modal('show');
		$("#lblTitleConfirmYesNo").html('Confirmation');
		$("#lblMsgConfirmYesNo").html('Do you really want to delete?');
		$("#btnYesConfirmYesNo").off('click').click(function () {
			$confirm.modal("hide");
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/settings/deleteCategory',
				data     : { "id" : id, '_token' : token },
				cache    : false,
				success  : function(data) {
					if(data == 'true'){
						$('.table.table-striped.deleteCategory tr#deleteCategory_'+id).remove();
					}
				}
			});
		});
		$("#btnNoConfirmYesNo").off('click').click(function () {
			$confirm.modal("hide");
		});
	}
}
function addNextCategory(){
	$('#addNextCategoryButton').hide();
	$('#addNextCategoryPart').show();
}
function addNextCategorySubmit(){
	var valid;	
	valid 					= addNextCategoryForm();
	var categoryId 			= $('.table.table-striped.deleteCategory tr:last').attr('id');
	if(categoryId == undefined){
		var lastAddedId 		= 0;
	}else{
		var splitId 			= categoryId.split('_');
		var lastAddedId 		= splitId[1];
	}
	var category_hidden_id 	= $('#category_hidden_id').val();
	if(category_hidden_id == ''){
		$('#category_hidden_id').val(lastAddedId);
		var lastId 			= $('#category_hidden_id').val();
		var lastCategoryId 	= parseInt(lastId) + parseInt(1);
		$('#category_hidden_id').val(lastCategoryId);
	}else{
		var lastId 			= $('#category_hidden_id').val();
		var lastCategoryId 	= parseInt(lastId)+parseInt(1);
		$('#category_hidden_id').val(lastCategoryId);
	}
	
	if(valid) {
		var categoryname 	= $('#categoryname').val();
		var token 			= $('input[name="_token"]').val();
		$('.table.table-striped.deleteCategory').append('<tr id="deleteCategory_'+lastCategoryId+'"><td>'+categoryname+'</td><td><a href="javascript:void(0);" onclick="deleteCategory('+lastCategoryId+');"><span class="glyphicon glyphicon-trash pull-right" aria-hidden="true" style="color:red;"></span></a><td></tr>');
	}
}

function addNextCategoryForm(){
	var valid 			= true;	
	var categoryname 	= $('#categoryname').val();
	if(categoryname == '' || categoryname == undefined){
		$("#categoryname").css('border', '1px solid #A94442');
		$("#categoryname").focus();
		valid = false;
	}else{
		$("#categoryname").css('border', '1px solid green');
	}
	return valid;
}

function addResourcesDialog(){
	
	$('#addResourcesDialog').modal('show');
	$('#inputContentPannel').show();
	$("#resource_id").val('');
	$("#title").val('');
	$("#category_name").val('');
	$("#resource_type").val('');
	$("#resource_link").val('');
	$('input:checkbox').prop('checked',false);
	$('button.btn.btn-danger').hide();
	$("button.btn.btn-danger").removeAttr("onclick");
	$('#model_title').html('Add Resources');
	$("#title").css('border', '1px solid #d2d6de');
	$("#category_name").css('border', '1px solid #d2d6de');
	$("#resource_type").css('border', '1px solid #d2d6de');
	$("#resource_link").css('border', '1px solid #d2d6de');
	$(".checkbox input.roles_checkbox").css('outline-color', '#d2d6de');
	$(".checkbox input.roles_checkbox").css('outline-style', 'solid');
	$(".checkbox input.roles_checkbox").css('outline-width', 'thin');
	$('#successMeg').html('');
	$('button.btn.btn-success').show();
	$('button.btn.btn-default').html('Cancel');
	$('button.btn.btn-success').html('Save');
	
}

function addEditResources(){
	var valid;	
	valid 					= addResourcesForm();
	if(valid) {
		var token 			= $('input[name="_token"]').val();
		var resource_id 	= $('#resource_id').val();
		var title 			= $('#title').val();
		var category_name 	= $('#category_name').val();
		var resource_type 	= $('#resource_type').val();
		var resource_link 	= $('#resource_link').val();
		var roles = "";
		var roles = $('input[name="roles[]"]:checked').map(function(){
			return this.value;
		}).get();
		
		$.ajax({
			type     : "POST",
			url      : $('form#resources-form').attr('action'),
			data     : { 'id' : resource_id, "title" : title, "category_name" : category_name, "resource_type" : resource_type, "resource_link" : resource_link, "roles" : roles, '_token' : token },
			cache    : false,
			success  : function(data) {
				if(data.id != '' && data.id != undefined){
					$('#successMeg').html('<div class="alert alert-success" role="alert">Resource has been successfully edited!</div>');
				}else{
					$('#successMeg').html('<div class="alert alert-success" role="alert">Resource has been successfully added!</div>');
				}
				$('#inputContentPannel').hide();
				$('button.btn.btn-danger').hide();
				$('button.btn.btn-success').hide();
				$('button.btn.btn-default').html('Close');
			}
		});
	}
}

function addResourcesForm(){
	var valid 			= true;	
	var title 			= $('#title').val();
	var category_name 	= $('#category_name').val();
	var resource_type 	= $('#resource_type').val();
	var resource_link 	= $('#resource_link').val();
	var roles 			= $("input[name='roles[]']:checked").length;
	var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
    
	if(title == '' || title == undefined){
		$("#title").css('border', '1px solid #A94442');
		$("#title").focus();
		valid = false;
	}else{
		$("#title").css('border', '1px solid green');
	}
	if(category_name == '' || category_name == undefined){
		$("#category_name").css('border', '1px solid #A94442');
		$("#category_name").focus();
		valid = false;
	}else{
		$("#category_name").css('border', '1px solid green');
	}
	if(resource_type == '' || resource_type == undefined){
		$("#resource_type").css('border', '1px solid #A94442');
		$("#resource_type").focus();
		valid = false;
	}else{
		$("#resource_type").css('border', '1px solid green');
	}
	if(resource_link == '' || resource_link == undefined){
		$("#resource_link").css('border', '1px solid #A94442');
		$("#resource_link").focus();
		$("p#resource_link_msg").hide();
		valid = false;
	}else if(!/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(resource_link)){
		$("#resource_link").css('border', '1px solid #A94442');
		$("#resource_link").focus();
		$("p#resource_link_msg").show();
		$("p#resource_link_msg").css('margin', '0px');
		$("p#resource_link_msg").html('Please enter a valid URL. Protocol is required (http://example.com or http://www.example.com');
		valid = false;
	}else{
		$("#resource_link").css('border', '1px solid green');
		$("p#resource_link_msg").hide();
	}
	if(roles == 0 || roles == undefined){
		$(".checkbox input.roles_checkbox").css('outline-color', 'red');
		$(".checkbox input.roles_checkbox").css('outline-style', 'solid');
		$(".checkbox input.roles_checkbox").css('outline-width', 'thin');
		$(".checkbox input.roles_checkbox").focus();
		valid = false;
	}else{
		$(".checkbox input.roles_checkbox").css('outline-color', 'green');
		$(".checkbox input.roles_checkbox").css('outline-style', 'solid');
		$(".checkbox input.roles_checkbox").css('outline-width', 'thin');
	}
	return valid;
}

function editResource(id){
	if(id != ''){
		var token		= $('input[name="_token"]').val();
		$.ajax({
			type     : "POST",
			url      : base_url+'/settings/editResources',
			data     : { "id" : id, '_token' : token },
			cache    : false,
			success  : function(data) {
				if ( data.length != 0 ) {
					$('#addResourcesDialog').modal('show');
					$('#inputContentPannel').show();
					$("#title").css('border', '1px solid #d2d6de');
					$("#category_name").css('border', '1px solid #d2d6de');
					$("#resource_type").css('border', '1px solid #d2d6de');
					$("#resource_link").css('border', '1px solid #d2d6de');
					$(".checkbox input.roles_checkbox").css('outline-color', '#d2d6de');
					$(".checkbox input.roles_checkbox").css('outline-style', 'solid');
					$(".checkbox input.roles_checkbox").css('outline-width', 'thin');
					$('#successMeg').html('');
					$('button.btn.btn-danger').show();
					$("button.btn.btn-danger").attr("onclick","deleteResource('"+data.id+"')");
					
					$('#model_title').html('Edit Resources');
					$('#resource_id').val(data.id);
					$('#title').val(data.title);
					$('#category_name').val(data.category);
					$('#resource_type').val(data.resource_type);
					$('#resource_link').val(data.resource_link);
					$('input:checkbox').prop('checked',false);
					var roles = data.role;
					var rolesArray = [];
					rolesArray = roles.split(',');
					
					for (i = 0; i < rolesArray.length; i++) {
						$('input[value="'+$.trim(rolesArray[i])+'"]').prop('checked', 'checked');
					}
					$('button.btn.btn-success').html('Update');
					$('button.btn.btn-danger').show();
					$('button.btn.btn-success').show();
					$('button.btn.btn-default').html('Cancel');
				}
			}
		});
	}
}

function deleteResource(id){ 
	if(id != ''){
		//$('#deleteResourcesDialog').modal('show');
		var $confirm = $("#deleteResourcesDialog");
		$confirm.modal('show');
		$("#lblTitleConfirmYesNo").html('Confirmation');
		$("#lblMsgConfirmYesNo").html('Do you really want to delete?');
		$("#btnYesConfirmYesNo").off('click').click(function () {
			$confirm.modal("hide");
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/settings/deleteResources',
				data     : { "id" : id, '_token' : token },
				cache    : false,
				success  : function(data) {
					if(data == 'true'){
						$('.table.table-bordered.table-striped.mainTable tr#resources'+id).remove();
						$('#successMeg').html('<div class="alert alert-success" role="alert">Resource has been successfully deleted!</div>');
						$('#model_title').html('Delete Resource');
						$('#inputContentPannel').hide();
						$('button.btn.btn-danger').hide();
						$('button.btn.btn-success').hide();
						$('button.btn.btn-default').html('Close');
					}
				}
			});
		});
		$("#btnNoConfirmYesNo").off('click').click(function () {
			$confirm.modal("hide");
		});
	}
}

$(document).ready(function(){
	$('#check-deactivate').click(function() {
		var checked = $(this).prop('checked');
		if(checked){
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/settings/deactivatedResources',
				data     : { "deactivate" : 'Deactivated', '_token' : token },
				cache    : false,
				success  : function(data) {
					$('.table.table-bordered.table-striped.mainTable tbody tr').remove();
					if(data != ''){
						for (i = 0; i < data.length; i++) {
							if(data[i].resource_type == 'PDF'){
								var getImage = '<img src="/img/pdf-icons.png" title="PDF Image" alt="PDF Image" width="22" />';
								var onClickMethod = "editResource('"+data[i].id+"')";
							}else if(data[i].resource_type == 'PowerPoint'){
								var getImage = '<img src="/img/powerPoint-icon.png" title="PowerPoint Image" alt="PowerPoint Image" width="22"/>';
								var onClickMethod = "editResource('"+data[i].id+"')";
							}else if(data[i].resource_type == 'Video'){
								var resourceLinkArr = (data[i].resource_link).split('=');
								var getImage = '<img src="https://img.youtube.com/vi/'+resourceLinkArr[1]+'/0.jpg" title="Click Here Get Video First Screen" alt="Video Image" width="22" />';
								var onClickMethod = "openVideosImage('"+data[i].resource_link+"')";
							}
							
							$('.table.table-bordered.table-striped.mainTable').append('<tr data-id="'+data[i].id+'" id="resources'+data[i].id+'"><td onClick="editResource('+data[i].id+');">'+data[i].added_date+'</td><td onClick="editResource('+data[i].id+');">'+data[i].status+'</td><td onClick="editResource('+data[i].id+');">'+data[i].title+'</td><td onClick="editResource('+data[i].id+');">'+data[i].category+'</td><td onClick="'+onClickMethod+'">'+getImage+' '+data[i].role+'</td></tr>');
						}
					}
				}
			});
		}else{
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/settings/deactivatedResources',
				data     : { "deactivate" : 'All', '_token' : token },
				cache    : false,
				success  : function(data) {
					$('.table.table-bordered.table-striped.mainTable tbody tr').remove();
					if(data != ''){
						for (i = 0; i < data.length; i++) {
							if(data[i].resource_type == 'PDF'){
								var getImage = '<img src="/img/pdf-icons.png" title="PDF Image" alt="PDF Image" width="22" />';
								var onClickMethod = "editResource('"+data[i].id+"')";
							}else if(data[i].resource_type == 'PowerPoint'){
								var getImage = '<img src="/img/powerPoint-icon.png" title="PowerPoint Image" alt="PowerPoint Image" width="22"/>';
								var onClickMethod = "editResource('"+data[i].id+"')";
							}else if(data[i].resource_type == 'Video'){
								var resourceLinkArr = (data[i].resource_link).split('=');
								var getImage = '<img src="https://img.youtube.com/vi/'+resourceLinkArr[1]+'/0.jpg" title="Click Here Get Video First Screen" alt="Video Image" width="22" />';
								var onClickMethod = "openVideosImage('"+data[i].resource_link+"')";
							}
							$('.table.table-bordered.table-striped.mainTable').append('<tr data-id="'+data[i].id+'" id="resources'+data[i].id+'"><td onClick="editResource('+data[i].id+');">'+data[i].added_date+'</td><td onClick="editResource('+data[i].id+');">'+data[i].status+'</td><td onClick="editResource('+data[i].id+');">'+data[i].title+'</td><td onClick="editResource('+data[i].id+');">'+data[i].category+'</td><td onClick="'+onClickMethod+'">'+getImage+' '+data[i].role+'</td></tr>');
						}
					}
				}
			});
		}
	});
});

function showAllResources(){
	var token		= $('input[name="_token"]').val();
	$('input:checkbox').prop('checked',false);
	$.ajax({
		type     : "POST",
		url      : base_url+'/settings/deactivatedResources',
		data     : { "deactivate" : 'All', '_token' : token },
		cache    : false,
		success  : function(data) {
			$('.table.table-bordered.table-striped.mainTable tbody tr').remove();
			if(data != ''){
				for (i = 0; i < data.length; i++) {
					if(data[i].resource_type == 'PDF'){
						var getImage = '<img src="/img/pdf-icons.png" title="PDF Image" alt="PDF Image" width="22" />';
						var onClickMethod = "editResource('"+data[i].id+"')";
					}else if(data[i].resource_type == 'PowerPoint'){
						var getImage = '<img src="/img/powerPoint-icon.png" title="PowerPoint Image" alt="PowerPoint Image" width="22"/>';
						var onClickMethod = "editResource('"+data[i].id+"')";
					}else if(data[i].resource_type == 'Video'){
						var resourceLinkArr = (data[i].resource_link).split('=');
						var getImage = '<img src="https://img.youtube.com/vi/'+resourceLinkArr[1]+'/0.jpg" title="Click Here Get Video First Screen" alt="Video Image" width="22" />';
						var onClickMethod = "openVideosImage('"+data[i].resource_link+"')";
					}
					$('.table.table-bordered.table-striped.mainTable').append('<tr data-id="'+data[i].id+'" id="resources'+data[i].id+'"><td onClick="editResource('+data[i].id+');">'+data[i].added_date+'</td><td onClick="editResource('+data[i].id+');">'+data[i].status+'</td><td onClick="editResource('+data[i].id+');">'+data[i].title+'</td><td onClick="editResource('+data[i].id+');">'+data[i].category+'</td><td onClick="'+onClickMethod+'">'+getImage+' '+data[i].role+'</td></tr>');
				}
			}
		}
	});
}

function openVideosImage(resource_link){
	var resourceLinkArr = resource_link.split('=');
	var resourceLink = 'https://img.youtube.com/vi/'+resourceLinkArr[1]+'/0.jpg';
	$('#videoScreenDialog').modal('show');
	$('#videoScreenImage').html('');
	$('#videoScreenImage').html('<iframe src="https://www.youtube.com/embed/'+resourceLinkArr[1]+'" width="80%" height="350"></iframe>');
	
	// if video autoplay then use this code //
	
	//$('#videoScreenImage').html('<iframe src="https://www.youtube.com/embed/'+resourceLinkArr[1]+'?autoplay=1" width="80%" height="350"></iframe>');
}	

/** End Resource code **/


/** Start Agency code **/

$(document).ready(function() {
    var addAgencyTier1_max_fields_limit = 100;
    var addAgencyTier1_x = 0;
    $('button.addAgencyTier1').click(function(e){
        e.preventDefault();
		$('#addRepTier1Dialog').modal('hide');
		var text = $(this).text();
        
		$("#addNewAgencyTier1").children("div").each(function(){ 
			var addNewAgencyTier1 = this.id;
			var arr = addNewAgencyTier1.split('_');
			addAgencyTier1_x = parseInt(arr[1])+parseInt(1);
		});
		
        if(addAgencyTier1_x < addAgencyTier1_max_fields_limit){
            
			if(text == 'Yes'){
				var token		= $('input[name="_token"]').val();
				$.ajax({
					type     : "POST",
					url      : base_url+'/agencies/autoPopulateEmailCounty',
					data     : { '_token' : token },
					cache    : false,
					success  : function(data) {
						$('#addNewAgencyTier1').append('<div class="row col-xs-12" id="addNewRowAgencyTier1_'+addAgencyTier1_x+'"><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label><input type="email" name="agency_rep_email_tier1[]" class="form-control" id="agency_rep_email_tier1_'+addAgencyTier1_x+'" title="Agency Rep. Email" placeholder="" value="'+data+'"></div><div class="col-sm-4"><label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label><div class="form-group"><div class="col-sm-6"><input type="text" name="first_name_tier1[]" class="form-control" id="first_name_tier1_'+addAgencyTier1_x+'" title="First Name" placeholder="First Name"></div><div class="col-sm-6"><input type="text" name="last_name_tier1[]" class="form-control" id="last_name_tier1_'+addAgencyTier1_x+'", title="Last Name" placeholder="Last Name"></div></div></div><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label><input type="text" name="phone_number_tier1[]" class="form-control" id="phone_number_tier1_'+addAgencyTier1_x+'" title="Agency Rep. Phone" placeholder=""></div><div class="col-sm-2"><label class="control-label">&nbsp;</label><div class="col-sm-12"><a href="javascript:void(0);" onClick="deleteAgencyTier1(`addNewRowAgencyTier1_'+addAgencyTier1_x+'`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a></div></div></div>');
						addAgencyTier1_x++;
					}
				});
			}else{
				$('#addNewAgencyTier1').append('<div class="row col-xs-12" id="addNewRowAgencyTier1_'+addAgencyTier1_x+'"><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label><input type="email" name="agency_rep_email_tier1[]" class="form-control" id="agency_rep_email_tier1_'+addAgencyTier1_x+'" title="Agency Rep. Email" placeholder=""></div><div class="col-sm-4"><label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label><div class="form-group"><div class="col-sm-6"><input type="text" name="first_name_tier1[]" class="form-control" id="first_name_tier1_'+addAgencyTier1_x+'" title="First Name" placeholder="First Name"></div><div class="col-sm-6"><input type="text" name="last_name_tier1[]" class="form-control" id="last_name_tier1_'+addAgencyTier1_x+'", title="Last Name" placeholder="Last Name"></div></div></div><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label><input type="text" name="phone_number_tier1[]" class="form-control" id="phone_number_tier1_'+addAgencyTier1_x+'" title="Agency Rep. Phone" placeholder=""></div><div class="col-sm-2"><label class="control-label">&nbsp;</label><div class="col-sm-12"><a href="javascript:void(0);" onClick="deleteAgencyTier1(`addNewRowAgencyTier1_'+addAgencyTier1_x+'`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a></div></div></div>');
				addAgencyTier1_x++;
			}
        }
    });
});

function deleteAgencyTier1(id){
	if(id != ''){
		var $confirm = $("#deleteAgencyTier1Dialog");
		$confirm.modal('show');
		$("#lblTitleConfirmYesNoTier1").html('Confirmation');
		$("#lblMsgConfirmYesNoTier1").html('Do you really want to delete?');
		$("#btnYesConfirmYesNoTier1").off('click').click(function () {
			$confirm.modal("hide");
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deleteAgencyTier1',
				data     : { "id" : id, '_token' : token },
				cache    : false,
				success  : function(data) {
					if(data == 'true'){
						$('#'+id).remove();
						//$('.table.table-striped.deleteCategory tr#deleteCategory_'+id).remove();
					}
				}
			});
		});
		$("#btnNoConfirmYesNoTier1").off('click').click(function () {
			$confirm.modal("hide");
		});
	}
}

$(document).ready(function() {
    var addAgencyTier2_max_fields_limit = 100;
    var addAgencyTier2_x = 0;
    $('button.addAgencyTier2').click(function(e){
        e.preventDefault();
		$('#addRepTier2Dialog').modal('hide');
		var text = $(this).text();
		
		$("#addNewAgencyTier2").children("div").each(function(){ 
			var addNewAgencyTier2 = this.id;
			var arr = addNewAgencyTier2.split('_');
			addAgencyTier2_x = parseInt(arr[1])+parseInt(1);
		});
		
        if(addAgencyTier2_x < addAgencyTier2_max_fields_limit){
            
			if(text == 'Yes'){
				var token		= $('input[name="_token"]').val();
				$.ajax({
					type     : "POST",
					url      : base_url+'/agencies/autoPopulateEmailCounty',
					data     : { '_token' : token },
					cache    : false,
					success  : function(data) {
						$('#addNewAgencyTier2').append('<div class="row col-xs-12" id="addNewRowAgencyTier2_'+addAgencyTier2_x+'"><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label><input type="email" name="agency_rep_email_tier2[]" class="form-control" id="agency_rep_email_tier2_'+addAgencyTier2_x+'" title="Agency Rep. Email" placeholder="" value="'+data+'"></div><div class="col-sm-4"><label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label><div class="form-group"><div class="col-sm-6"><input type="text" name="first_name_tier2[]" class="form-control" id="first_name_tier2_'+addAgencyTier2_x+'" title="First Name" placeholder="First Name"></div><div class="col-sm-6"><input type="text" name="last_name_tier2[]" class="form-control" id="last_name_tier2_'+addAgencyTier2_x+'", title="Last Name" placeholder="Last Name"></div></div></div><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label><input type="text" name="phone_number_tier2[]" class="form-control" id="phone_number_tier2_'+addAgencyTier2_x+'" title="Agency Rep. Phone" placeholder=""></div><div class="col-sm-2"><label class="control-label">&nbsp;</label><div class="col-sm-12"><a href="javascript:void(0);" onClick="deleteAgencyTier2(`addNewRowAgencyTier2_'+addAgencyTier2_x+'`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a></div></div></div>');
						addAgencyTier2_x++;
					}
				});
			}else{
				$('#addNewAgencyTier2').append('<div class="row col-xs-12" id="addNewRowAgencyTier2_'+addAgencyTier2_x+'"><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label><input type="email" name="agency_rep_email_tier2[]" class="form-control" id="agency_rep_email_tier2_'+addAgencyTier2_x+'" title="Agency Rep. Email" placeholder=""></div><div class="col-sm-4"><label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label><div class="form-group"><div class="col-sm-6"><input type="text" name="first_name_tier2[]" class="form-control" id="first_name_tier2_'+addAgencyTier2_x+'" title="First Name" placeholder="First Name"></div><div class="col-sm-6"><input type="text" name="last_name_tier2[]" class="form-control" id="last_name_tier2_'+addAgencyTier2_x+'", title="Last Name" placeholder="Last Name"></div></div></div><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label><input type="text" name="phone_number_tier2[]" class="form-control" id="phone_number_tier2_'+addAgencyTier2_x+'" title="Agency Rep. Phone" placeholder=""></div><div class="col-sm-2"><label class="control-label">&nbsp;</label><div class="col-sm-12"><a href="javascript:void(0);" onClick="deleteAgencyTier2(`addNewRowAgencyTier2_'+addAgencyTier2_x+'`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a></div></div></div>');
				addAgencyTier2_x++;
			}
        }
    });
});

function deleteAgencyTier2(id){
	if(id != ''){
		var $confirm = $("#deleteAgencyTier2Dialog");
		$confirm.modal('show');
		$("#lblTitleConfirmYesNoTier2").html('Confirmation');
		$("#lblMsgConfirmYesNoTier2").html('Do you really want to delete?');
		$("#btnYesConfirmYesNoTier2").off('click').click(function () {
			$confirm.modal("hide");
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deleteAgencyTier2',
				data     : { "id" : id, '_token' : token },
				cache    : false,
				success  : function(data) {
					if(data == 'true'){
						$('#'+id).remove();
						//$('.table.table-striped.deleteCategory tr#deleteCategory_'+id).remove();
					}
				}
			});
		});
		$("#btnNoConfirmYesNoTier2").off('click').click(function () {
			$confirm.modal("hide");
		});
	}
}

$(document).ready(function() {
    var addAgencyTier3_max_fields_limit = 100;
	var addAgencyTier3_x = 0;
    $('button.addAgencyTier3').click(function(e){
        e.preventDefault();
		$('#addRepTier3Dialog').modal('hide');
		var text = $(this).text();
		
		$("#addNewAgencyTier3").children("div").each(function(){ 
			var addNewAgencyTier3 = this.id;
			var arr = addNewAgencyTier3.split('_');
			addAgencyTier3_x = parseInt(arr[1])+parseInt(1);
		});
		
        if(addAgencyTier3_x < addAgencyTier3_max_fields_limit){
            
			if(text == 'Yes'){
				var token		= $('input[name="_token"]').val();
				$.ajax({
					type     : "POST",
					url      : base_url+'/agencies/autoPopulateEmailCounty',
					data     : { '_token' : token },
					cache    : false,
					success  : function(data) {
						$('#addNewAgencyTier3').append('<div class="row col-xs-12" id="addNewRowAgencyTier3_'+addAgencyTier3_x+'"><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label><input type="email" name="agency_rep_email_tier3[]" class="form-control" id="agency_rep_email_tier3_'+addAgencyTier3_x+'" title="Agency Rep. Email" placeholder="" value="'+data+'"></div><div class="col-sm-4"><label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label><div class="form-group"><div class="col-sm-6"><input type="text" name="first_name_tier3[]" class="form-control" id="first_name_tier3_'+addAgencyTier3_x+'" title="First Name" placeholder="First Name"></div><div class="col-sm-6"><input type="text" name="last_name_tier3[]" class="form-control" id="last_name_tier3_'+addAgencyTier3_x+'", title="Last Name" placeholder="Last Name"></div></div></div><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label><input type="text" name="phone_number_tier3[]" class="form-control" id="phone_number_tier3_'+addAgencyTier3_x+'" title="Agency Rep. Phone" placeholder=""></div><div class="col-sm-2"><label class="control-label">&nbsp;</label><div class="col-sm-12"><a href="javascript:void(0);" onClick="deleteAgencyTier3(`addNewRowAgencyTier3_'+addAgencyTier3_x+'`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a></div></div></div>');
						addAgencyTier3_x++;
					}
				});
			}else{
				$('#addNewAgencyTier3').append('<div class="row col-xs-12" id="addNewRowAgencyTier3_'+addAgencyTier3_x+'"><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Email<span style="color:red;"> *</span></span></label><input type="email" name="agency_rep_email_tier3[]" class="form-control" id="agency_rep_email_tier3_'+addAgencyTier3_x+'" title="Agency Rep. Email" placeholder=""></div><div class="col-sm-4"><label class="control-label"><span>Agency Rep. Name<span style="color:red;"> *</span></span></label><div class="form-group"><div class="col-sm-6"><input type="text" name="first_name_tier3[]" class="form-control" id="first_name_tier3_'+addAgencyTier3_x+'" title="First Name" placeholder="First Name"></div><div class="col-sm-6"><input type="text" name="last_name_tier3[]" class="form-control" id="last_name_tier3_'+addAgencyTier3_x+'", title="Last Name" placeholder="Last Name"></div></div></div><div class="col-sm-3"><label class="control-label"><span>Agency Rep. Phone<span style="color:red;"> *</span></span></label><input type="text" name="phone_number_tier3[]" class="form-control" id="phone_number_tier3_'+addAgencyTier3_x+'" title="Agency Rep. Phone" placeholder=""></div><div class="col-sm-2"><label class="control-label">&nbsp;</label><div class="col-sm-12"><a href="javascript:void(0);" onClick="deleteAgencyTier3(`addNewRowAgencyTier3_'+addAgencyTier3_x+'`)" title="Delete" class="remove_field"><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red; font-size: 25px;"></span></a></div></div></div>');
				addAgencyTier3_x++;
			}
        }
    });
});

function deleteAgencyTier3(id){
	if(id != ''){
		var $confirm = $("#deleteAgencyTier3Dialog");
		$confirm.modal('show');
		$("#lblTitleConfirmYesNoTier3").html('Confirmation');
		$("#lblMsgConfirmYesNoTier3").html('Do you really want to delete?');
		$("#btnYesConfirmYesNoTier3").off('click').click(function () {
			$confirm.modal("hide");
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deleteAgencyTier3',
				data     : { "id" : id, '_token' : token },
				cache    : false,
				success  : function(data) {
					if(data == 'true'){
						$('#'+id).remove();
						//$('.table.table-striped.deleteCategory tr#deleteCategory_'+id).remove();
					}
				}
			});
		});
		$("#btnNoConfirmYesNoTier3").off('click').click(function () {
			$confirm.modal("hide");
		});
	}
}

function addRepTier1Dialog(id){
	$('#addRepTier1Dialog').modal('show');
}


function addRepTier2Dialog(id){
	$('#addRepTier2Dialog').modal('show');
}


function addRepTier3Dialog(id){
	$('#addRepTier3Dialog').modal('show');
}

$(document).ready(function() {
	var checkbox_val = $("input[name='background_agency_yes_no_tier2']:checked").val();
	if(checkbox_val == 'Background Agency Yes'){
		$('#background_agency_yes_no_tier2').show();
	}else if(checkbox_val == 'Background Agency No'){
		$('#background_agency_yes_no_tier2').hide();
	}
    $("input[name='background_agency_yes_no_tier2']").on('change', function() {
        $("input[name='background_agency_yes_no_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='background_agency_yes_no_tier2']:checked").val();
		if(checkbox_val == 'Background Agency Yes'){
			$('#background_agency_yes_no_tier2').show();
		}else if(checkbox_val == 'Background Agency No'){
			$('#background_agency_yes_no_tier2').hide();
		}
    });
});

$(document).ready(function() {
    $("input[name='agencies_yes_no_tier2']").on('change', function() {
        $("input[name='agencies_yes_no_tier2']").not(this).prop('checked', false);  
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='training_agency_yes_no_tier2']:checked").val();
	if(checkbox_val == 'Training Agency Yes'){
		$('#training_agency_yes_no_tier2').show();
	}else if(checkbox_val == 'Training Agency No'){
		$('#training_agency_yes_no_tier2').hide();
	}
    $("input[name='training_agency_yes_no_tier2']").on('change', function() {
        $("input[name='training_agency_yes_no_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='training_agency_yes_no_tier2']:checked").val();
		if(checkbox_val == 'Training Agency Yes'){
			$('#training_agency_yes_no_tier2').show();
		}else if(checkbox_val == 'Training Agency No'){
			$('#training_agency_yes_no_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='childcare_agency_yes_no_tier2']:checked").val();
	if(checkbox_val == 'Background Agency Yes'){
		$('#childcare_agency_yes_no_tier2').show();
	}else if(checkbox_val == 'Background Agency No'){
		$('#childcare_agency_yes_no_tier2').hide();
	}
    $("input[name='childcare_agency_yes_no_tier2']").on('change', function() {
        $("input[name='childcare_agency_yes_no_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='childcare_agency_yes_no_tier2']:checked").val();
		if(checkbox_val == 'Background Agency Yes'){
			$('#childcare_agency_yes_no_tier2').show();
		}else if(checkbox_val == 'Background Agency No'){
			$('#childcare_agency_yes_no_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='childcare_training_agency_yes_no_tier2']:checked").val();
	if(checkbox_val == 'Training Agency Yes'){
		$('#childcare_training_agency_yes_no_tier2').show();
	}else if(checkbox_val == 'Training Agency No'){
		$('#childcare_training_agency_yes_no_tier2').hide();
	}
    $("input[name='childcare_training_agency_yes_no_tier2']").on('change', function() {
        $("input[name='childcare_training_agency_yes_no_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='childcare_training_agency_yes_no_tier2']:checked").val();
		if(checkbox_val == 'Training Agency Yes'){
			$('#childcare_training_agency_yes_no_tier2').show();
		}else if(checkbox_val == 'Training Agency No'){
			$('#childcare_training_agency_yes_no_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='transportation_agency_yes_no_tier2']:checked").val();
	if(checkbox_val == 'Background Agency Yes'){
		$('#transportation_agency_yes_no_tier2').show();
	}else if(checkbox_val == 'Background Agency No'){
		$('#transportation_agency_yes_no_tier2').hide();
	}
    $("input[name='transportation_agency_yes_no_tier2']").on('change', function() {
        $("input[name='transportation_agency_yes_no_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='transportation_agency_yes_no_tier2']:checked").val();
		if(checkbox_val == 'Background Agency Yes'){
			$('#transportation_agency_yes_no_tier2').show();
		}else if(checkbox_val == 'Background Agency No'){
			$('#transportation_agency_yes_no_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='transportation_training_agency_yes_no_tier2']:checked").val();
	if(checkbox_val == 'Training Agency Yes'){
		$('#transportation_training_agency_yes_no_tier2').show();
	}else if(checkbox_val == 'Training Agency No'){
		$('#transportation_training_agency_yes_no_tier2').hide();
	}
    $("input[name='transportation_training_agency_yes_no_tier2']").on('change', function() {
        $("input[name='transportation_training_agency_yes_no_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='transportation_training_agency_yes_no_tier2']:checked").val();
		if(checkbox_val == 'Training Agency Yes'){
			$('#transportation_training_agency_yes_no_tier2').show();
		}else if(checkbox_val == 'Training Agency No'){
			$('#transportation_training_agency_yes_no_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='background_agency_yes_no_tier3']:checked").val();
	if(checkbox_val == 'Background Agency Yes'){
		$('#background_agency_yes_no_tier3').show();
	}else if(checkbox_val == 'Background Agency No'){
		$('#background_agency_yes_no_tier3').hide();
	}
    $("input[name='background_agency_yes_no_tier3']").on('change', function() {
        $("input[name='background_agency_yes_no_tier3']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='background_agency_yes_no_tier3']:checked").val();
		if(checkbox_val == 'Background Agency Yes'){
			$('#background_agency_yes_no_tier3').show();
		}else if(checkbox_val == 'Background Agency No'){
			$('#background_agency_yes_no_tier3').hide();
		}
    });
});

$(document).ready(function() {
    $("input[name='agencies_yes_no_tier3']").on('change', function() {
        $("input[name='agencies_yes_no_tier3']").not(this).prop('checked', false);  
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='walk_through_agency_yes_no_tier3']:checked").val();
	if(checkbox_val == 'Walk-Through Agency Yes'){
		$('#walk_through_agency_yes_no_tier3').show();
	}else if(checkbox_val == 'Walk-Through Agency No'){
		$('#walk_through_agency_yes_no_tier3').hide();
	}
    $("input[name='walk_through_agency_yes_no_tier3']").on('change', function() {
        $("input[name='walk_through_agency_yes_no_tier3']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='walk_through_agency_yes_no_tier3']:checked").val();
		if(checkbox_val == 'Walk-Through Agency Yes'){
			$('#walk_through_agency_yes_no_tier3').show();
		}else if(checkbox_val == 'Walk-Through Agency No'){
			$('#walk_through_agency_yes_no_tier3').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='training_agency_yes_no_tier3']:checked").val();
	if(checkbox_val == 'Training Agency Yes'){
		$('#training_agency_yes_no_tier3').show();
	}else if(checkbox_val == 'Training Agency No'){
		$('#training_agency_yes_no_tier3').hide();
	}
    $("input[name='training_agency_yes_no_tier3']").on('change', function() {
        $("input[name='training_agency_yes_no_tier3']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='training_agency_yes_no_tier3']:checked").val();
		if(checkbox_val == 'Training Agency Yes'){
			$('#training_agency_yes_no_tier3').show();
		}else if(checkbox_val == 'Training Agency No'){
			$('#training_agency_yes_no_tier3').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='family_team_meeting_tier2']:checked").val();
	if(checkbox_val == 'Family Social'){
		$('#family_team_meeting_part_tier2').show();
	}else{
		$('#family_team_meeting_part_tier2').hide();
	}
    $("input[name='family_team_meeting_tier2']").on('change', function() {
        $("input[name='family_team_meeting_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='family_team_meeting_tier2']:checked").val();
		if(checkbox_val == 'Family Social'){
			$('#family_team_meeting_part_tier2').show();
		}else{
			$('#family_team_meeting_part_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='child_care_tier2']:checked").val();
	if(checkbox_val == 'Child Care'){
		$('#child_care_part_tier2').show();
	}else{
		$('#child_care_part_tier2').hide();
	}
    $("input[name='child_care_tier2']").on('change', function() {
        $("input[name='child_care_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='child_care_tier2']:checked").val();
		if(checkbox_val == 'Child Care'){
			$('#child_care_part_tier2').show();
		}else{
			$('#child_care_part_tier2').hide();
		}
    });
});

$(document).ready(function() {
	var checkbox_val = $("input[name='transportation_tier2']:checked").val();
	if(checkbox_val == 'Transportation'){
		$('#transportation_part_tier2').show();
	}else{
		$('#transportation_part_tier2').hide();
	}
    $("input[name='transportation_tier2']").on('change', function() {
        $("input[name='transportation_tier2']").not(this).prop('checked', false);  
		var checkbox_val = $("input[name='transportation_tier2']:checked").val();
		if(checkbox_val == 'Transportation'){
			$('#transportation_part_tier2').show();
		}else{
			$('#transportation_part_tier2').hide();
		}
    });
});

function goBack() {
    window.history.back();
}

function showAllAgency(){
	var token		= $('input[name="_token"]').val();
	$('input:checkbox').prop('checked',false);
	$.ajax({
		type     : "POST",
		url      : base_url+'/agencies/deactivatedAgency',
		data     : { "deactivate" : 'All', '_token' : token },
		cache    : false,
		success  : function(data) {
			$('.table.table-bordered.table-striped tbody tr').remove();
			if(data != ''){
				for (i = 0; i < data.length; i++) {
					$('.table.table-bordered.table-striped').append('<tr data-id="'+data[i].id+'" id="resources'+data[i].id+'" onClick="editAgency('+data[i].id+');"><td>'+data[i].agency_name+'</td><td>'+data[i].city+'</td><td>'+data[i].state+'</td><td>'+data[i].phone_number+'</td><td>'+data[i].tier_approval+'</td><td>'+data[i].approval_option+'</td><td>'+data[i].open_requests+'</td><td>'+data[i].requests_made+'</td></tr>');
				}
			}
		}
	});
}

$(document).ready(function(){
	$('#agency-check-deactivate').click(function() {
		var checked = $(this).prop('checked');
		if(checked){
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deactivatedAgency',
				data     : { "deactivate" : 'Deactivated', '_token' : token },
				cache    : false,
				success  : function(data) {
					$('.table.table-bordered.table-striped tbody tr').remove();
					if(data != ''){
						for (i = 0; i < data.length; i++) {
							$('.table.table-bordered.table-striped').append('<tr data-id="'+data[i].id+'" id="resources'+data[i].id+'" onClick="editAgency('+data[i].id+');"><td>'+data[i].agency_name+'</td><td>'+data[i].city+'</td><td>'+data[i].state+'</td><td>'+data[i].phone_number+'</td><td>'+data[i].tier_approval+'</td><td>'+data[i].approval_option+'</td><td>'+data[i].open_requests+'</td><td>'+data[i].requests_made+'</td></tr>');
						}
					}
				}
			});
		}else{
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deactivatedAgency',
				data     : { "deactivate" : 'All', '_token' : token },
				cache    : false,
				success  : function(data) {
					$('.table.table-bordered.table-striped tbody tr').remove();
					if(data != ''){
						for (i = 0; i < data.length; i++) {
							$('.table.table-bordered.table-striped').append('<tr data-id="'+data[i].id+'" id="resources'+data[i].id+'" onClick="editAgency('+data[i].id+');"><td>'+data[i].agency_name+'</td><td>'+data[i].city+'</td><td>'+data[i].state+'</td><td>'+data[i].phone_number+'</td><td>'+data[i].tier_approval+'</td><td>'+data[i].approval_option+'</td><td>'+data[i].open_requests+'</td><td>'+data[i].requests_made+'</td></tr>');
						}
					}
				}
			});
		}
	});
});

function addEditAgency(){
	var valid;	
	valid = addEditAgencyForm();
	if(valid) {
		$('form#agencies-form').submit();
	}
}

function addEditAgencyForm(){
	var valid 						= true;	
	var agency_name 				= $('#agency_name').val();
	var agency_status 				= $('#agency_status').val();
	var designated_agency_email 	= $('#designated_agency_email').val();
	
	var agency_website 				= $('#agency_website').val();
	var agency_email_domain 		= $('#agency_email_domain').val();
	var first_name 					= $('#first_name').val();
	
	var last_name 					= $('#last_name').val();
	var agency_title 				= $('#agency_title').val();
	var phone_number 				= $('#phone_number').val();
	
	var director_email 				= $('#director_email').val();
	var address1 					= $('#address1').val();
	var city 						= $('#city').val();
	var state 						= $('#state').val();
	var zip 						= $('#zip').val();
	var agencies_approval_option 	= $('#agencies_approval_option').val();
	
	var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);

	if(agency_name == '' || agency_name == undefined){
		$("#agency_name").css('border', '1px solid #A94442');
		$("#agency_name").focus();
		valid = false;
	}else{
		$("#agency_name").css('border', '1px solid green');
	}
	if(agency_status == '' || agency_status == undefined){
		$("#agency_status").css('border', '1px solid #A94442');
		$("#agency_status").focus();
		valid = false;
	}else{
		$("#agency_status").css('border', '1px solid green');
	}
	if(designated_agency_email == '' || designated_agency_email == undefined){
		$("#designated_agency_email").css('border', '1px solid #A94442');
		$("#designated_agency_email").focus();
		valid = false;
	}else if(!pattern.test(designated_agency_email)){
		$("#designated_agency_email").css('border', '1px solid #A94442');
		$("#designated_agency_email").focus();
		valid = false;
	}else{
		$("#designated_agency_email").css('border', '1px solid green');
	}
	if(agency_website == '' || agency_website == undefined){
		$("#agency_website").css('border', '1px solid #A94442');
		$("#agency_website").focus();
		$("p#resource_link_msg").hide();
		valid = false;
	}else if(!/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(agency_website)){
		$("#agency_website").css('border', '1px solid #A94442');
		$("#agency_website").focus();
		$("p#resource_link_msg").show();
		$("p#resource_link_msg").css('margin', '0px');
		$("p#resource_link_msg").html('Please enter a valid URL. Protocol is required (http://example.com or http://www.example.com)');
		valid = false;
	}else{
		$("#agency_website").css('border', '1px solid green');
		$("p#resource_link_msg").hide();
	}
	if(agency_email_domain == '' || agency_email_domain == undefined){
		$("#agency_email_domain").css('border', '1px solid #A94442');
		$("#agency_email_domain").focus();
		valid = false;
	}else if(!pattern.test(agency_email_domain)){
		$("#agency_email_domain").css('border', '1px solid #A94442');
		$("#agency_email_domain").focus();
		valid = false;
	}else{
		$("#agency_email_domain").css('border', '1px solid green');
	}
	if(first_name == '' || first_name == undefined){
		$("#first_name").css('border', '1px solid #A94442');
		$("#first_name").focus();
		valid = false;
	}else{
		$("#first_name").css('border', '1px solid green');
	}
	if(last_name == '' || last_name == undefined){
		$("#last_name").css('border', '1px solid #A94442');
		$("#last_name").focus();
		valid = false;
	}else{
		$("#last_name").css('border', '1px solid green');
	}
	if(agency_title == '' || agency_title == undefined){
		$("#agency_title").css('border', '1px solid #A94442');
		$("#agency_title").focus();
		valid = false;
	}else{
		$("#agency_title").css('border', '1px solid green');
	}
	if(phone_number == '' || phone_number == undefined){
		$("#phone_number").css('border', '1px solid #A94442');
		$("#phone_number").focus();
		valid = false;
	}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(phone_number)){
		$("#phone_number").css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
		$("#phone_number").focus();
		valid = false;
	}else{
		$("#phone_number").css('border', '1px solid green');
	}
	if(director_email == '' || director_email == undefined){
		$("#director_email").css('border', '1px solid #A94442');
		$("#director_email").focus();
		valid = false;
	}else if(!pattern.test(director_email)){
		$("#director_email").css('border', '1px solid #A94442');
		$("#director_email").focus();
		valid = false;
	}else{
		$("#director_email").css('border', '1px solid green');
	}
	if(address1 == '' || address1 == undefined){
		$("#address1").css('border', '1px solid #A94442');
		$("#address1").focus();
		valid = false;
	}else{
		$("#address1").css('border', '1px solid green');
	}
	if(city == '' || city == undefined){
		$("#city").css('border', '1px solid #A94442');
		$("#city").focus();
		valid = false;
	}else{
		$("#city").css('border', '1px solid green');
	}
	if(state == '' || state == undefined){
		$("#state").css('border', '1px solid #A94442');
		$("#state").focus();
		valid = false;
	}else{
		$("#state").css('border', '1px solid green');
	}
	if(zip == '' || zip == undefined){
		$("#zip").css('border', '1px solid #A94442');
		$("#zip").focus();
		valid = false;
	}else{
		$("#zip").css('border', '1px solid green');
	}
	if(agencies_approval_option == '' || agencies_approval_option == undefined){
		$("#agencies_approval_option").css('border', '1px solid #A94442');
		$("#agencies_approval_option").focus();
		valid = false;
	}else{
		$("#agencies_approval_option").css('border', '1px solid green');
	}
	return valid;
}

function editAgency(id){
	if(id != ''){
		var token		= $('input[name="_token"]').val();
		$.ajax({
			type     : "POST",
			url      : base_url+'/agencies/editAgency',
			data     : { "id" : id, '_token' : token },
			cache    : false,
			success  : function(data) {
				if(data != ''){
					window.location.href = base_url+'/agencies/agency-details?agency_id='+data;
				}
			}
		});
	}
}

function agencyEditable(){
	$("form#agencies-form :input").prop("disabled", false);
}

function deactivateAgency(){
	var agency_id = $('#agency_id').val();
	if(agency_id){
		window.location.href = base_url+'/agencies?agency_id='+agency_id;
	}
}

$(document).ready(function(){
	var date_input1=$('input[name="estimated_launch_date_tier1"]'); //our date input has the name "date"
	date_input1.datepicker({
		format: 'mm/dd/yyyy',
		todayHighlight: true,
		autoclose: true,
	});

	var date_input2=$('input[name="estimated_launch_date_tier2"]'); //our date input has the name "date"
	date_input2.datepicker({
		format: 'mm/dd/yyyy',
		todayHighlight: true,
		autoclose: true,
	});

	var date_input3=$('input[name="estimated_launch_date_tier3"]'); //our date input has the name "date"
	date_input3.datepicker({
		format: 'mm/dd/yyyy',
		todayHighlight: true,
		autoclose: true,
	});
});

function addEditCounty(){
	var valid;	
	valid = addEditCountyForm();
	if(valid) {
		$('form#county-form').submit();
	}
}

function addEditCountyForm(){
	var valid 						= true;	
	
	var state 						= $('#state').val();
	var county_name 				= $('#county_name').val();
	var pattern 					= new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	
	if(state == '' || state == undefined){
		$("#state").css('border', '1px solid #A94442');
		$("#state").focus();
		valid = false;
	}else{
		$("#state").css('border', '1px solid green');
	}
	
	if(county_name == '' || county_name == undefined){
		$("#county_name").css('border', '1px solid #A94442');
		$("#county_name").focus();
		valid = false;
	}else{
		$("#county_name").css('border', '1px solid green');
	}
	
	// Start Tier 1 Approved Fields //
	
	var approved_tier1 					= $('#approved_tier1').val();
	var estimated_launch_date_tier1 	= $('#estimated_launch_date_tier1').val();
	var agency_rep_email_tier1 			= $('input[name="agency_rep_email_tier1[]"]').val();
	var first_name_tier1 				= $('input[name="first_name_tier1[]"]').val();
	var last_name_tier1 				= $('input[name="last_name_tier1[]"]').val();
	var phone_number_tier1 				= $('input[name="phone_number_tier1[]"]').val();
	
	if(approved_tier1 == '' || approved_tier1 == undefined){
		$("#approved_tier1").css('border', '1px solid #A94442');
		$("#approved_tier1").focus();
		valid = false;
	}else{
		$("#approved_tier1").css('border', '1px solid green');
		//if(approved_tier1 == 'Yes'){}
	}
	
	var count_email_tier1 = 0;
	$("input[name='agency_rep_email_tier1[]']").each(function () {
		if($(this).val() == '' || $(this).val() == undefined){
			$('#agency_rep_email_tier1_'+count_email_tier1).css('border', '1px solid #A94442');
			$('#agency_rep_email_tier1_'+count_email_tier1).focus();
			valid = false;
		}else if(!pattern.test($(this).val())){
			$('#agency_rep_email_tier1_'+count_email_tier1).css('border', '1px solid #A94442');
			$('#agency_rep_email_tier1_'+count_email_tier1).focus();
			valid = false;
		}else{
			$('#agency_rep_email_tier1_'+count_email_tier1).css('border', '1px solid green');
		}
		count_email_tier1++;
	});
	
	var count_first_name_tier1 = 0;
	$("input[name='first_name_tier1[]']").each(function () {
		if($(this).val() == '' || $(this).val() == undefined){
			$('#first_name_tier1_'+count_first_name_tier1).css('border', '1px solid #A94442');
			$('#first_name_tier1_'+count_first_name_tier1).focus();
			valid = false;
		}else{
			$('#first_name_tier1_'+count_first_name_tier1).css('border', '1px solid green');
		}
		count_first_name_tier1++;
	});
	
	var count_last_name_tier1 = 0;
	$("input[name='last_name_tier1[]']").each(function () {
		if($(this).val() == '' || $(this).val() == undefined){
			$('#last_name_tier1_'+count_last_name_tier1).css('border', '1px solid #A94442');
			$('#last_name_tier1_'+count_last_name_tier1).focus();
			valid = false;
		}else{
			$('#last_name_tier1_'+count_last_name_tier1).css('border', '1px solid green');
		}
		count_last_name_tier1++;
	});
	
	var count_phone_number_tier1 = 0;
	$("input[name='phone_number_tier1[]']").each(function () {
		if($(this).val() == '' || $(this).val() == undefined){
			$('#phone_number_tier1_'+count_phone_number_tier1).css('border', '1px solid #A94442');
			$('#phone_number_tier1_'+count_phone_number_tier1).focus();
			valid = false;
		}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test($(this).val())){
			$('#phone_number_tier1_'+count_phone_number_tier1).css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
			$('#phone_number_tier1_'+count_phone_number_tier1).focus();
			valid = false;
		}else{
			$('#phone_number_tier1_'+count_phone_number_tier1).css('border', '1px solid green');
		}
		count_phone_number_tier1++;
	});
	
	if(estimated_launch_date_tier1 == '' || estimated_launch_date_tier1 == undefined){
		$("#estimated_launch_date_tier1").css('border', '1px solid #A94442');
		//$("#estimated_launch_date_tier1").focus();
		valid = false;
	}else{
		$("#estimated_launch_date_tier1").css('border', '1px solid green');
	}
	
	// End Tier 1 Approved Fields //
	
	// Start Tier 2 Approved Fields //
		
		var approved_tier2 					= $('#approved_tier2').val();
		var estimated_launch_date_tier2 	= $('#estimated_launch_date_tier2').val();
		var agency_rep_email_tier2 			= $('input[name="agency_rep_email_tier2[]"]').val();
		var first_name_tier2 				= $('input[name="first_name_tier2[]"]').val();
		var last_name_tier2 				= $('input[name="last_name_tier2[]"]').val();
		var phone_number_tier2 				= $('input[name="phone_number_tier2[]"]').val();
		
		if(approved_tier2 == '' || approved_tier2 == undefined){
			$("#approved_tier2").css('border', '1px solid #A94442');
			$("#approved_tier2").focus();
			valid = false;
		}else{
			$("#approved_tier2").css('border', '1px solid green');
			//if(approved_tier2 == 'Yes'){}
		}
		
		var count_email_tier2 = 0;
		$("input[name='agency_rep_email_tier2[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#agency_rep_email_tier2_'+count_email_tier2).css('border', '1px solid #A94442');
				$('#agency_rep_email_tier2_'+count_email_tier2).focus();
				valid = false;
			}else if(!pattern.test($(this).val())){
				$('#agency_rep_email_tier2_'+count_email_tier2).css('border', '1px solid #A94442');
				$('#agency_rep_email_tier2_'+count_email_tier2).focus();
				valid = false;
			}else{
				$('#agency_rep_email_tier2_'+count_email_tier2).css('border', '1px solid green');
			}
			count_email_tier2++;
		});
		
		var count_first_name_tier2 = 0;
		$("input[name='first_name_tier2[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#first_name_tier2_'+count_first_name_tier2).css('border', '1px solid #A94442');
				$('#first_name_tier2_'+count_first_name_tier2).focus();
				valid = false;
			}else{
				$('#first_name_tier2_'+count_first_name_tier2).css('border', '1px solid green');
			}
			count_first_name_tier2++;
		});
		
		var count_last_name_tier2 = 0;
		$("input[name='last_name_tier2[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#last_name_tier2_'+count_last_name_tier2).css('border', '1px solid #A94442');
				$('#last_name_tier2_'+count_last_name_tier2).focus();
				valid = false;
			}else{
				$('#last_name_tier2_'+count_last_name_tier2).css('border', '1px solid green');
			}
			count_last_name_tier2++;
		});
		
		var count_phone_number_tier2 = 0;
		$("input[name='phone_number_tier2[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#phone_number_tier2_'+count_phone_number_tier2).css('border', '1px solid #A94442');
				$('#phone_number_tier2_'+count_phone_number_tier2).focus();
				valid = false;
			}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test($(this).val())){
				$('#phone_number_tier2_'+count_phone_number_tier2).css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
				$('#phone_number_tier2_'+count_phone_number_tier2).focus();
				valid = false;
			}else{
				$('#phone_number_tier2_'+count_phone_number_tier2).css('border', '1px solid green');
			}
			count_phone_number_tier2++;
		});
		
		if(estimated_launch_date_tier2 == '' || estimated_launch_date_tier2 == undefined){
			$("#estimated_launch_date_tier2").css('border', '1px solid #A94442');
			//$("#estimated_launch_date_tier2").focus();
			valid = false;
		}else{
			$("#estimated_launch_date_tier2").css('border', '1px solid green');
		}
		
		// family or social part //
		var family_team_meeting_tier2  					= $('#family_team_meeting_tier2').prop('checked');
		
		var background_agency_yes_no_tier2 				= $("input[name='background_agency_yes_no_tier2']:checked").val();
		var background_contact_email_tier2 				= $('#background_contact_email_tier2').val();
		var background_contact_first_name_tier2 		= $('#background_contact_first_name_tier2').val();
		var background_contact_last_name_tier2 			= $('#background_contact_last_name_tier2').val();
		var background_contact_phone_number_tier2 		= $('#background_contact_phone_number_tier2').val();
		
		var agencies_yes_no_tier2 						= $("#agencies_yes_no_tier2").prop('checked');
		
		var training_agency_yes_no_tier2 				= $("input[name='training_agency_yes_no_tier2']:checked").val();
		var training_contact_email_tier2 				= $('#training_contact_email_tier2').val();
		var training_contact_first_name_tier2 			= $('#training_contact_first_name_tier2').val();
		var training_contact_last_name_tier2 			= $('#training_contact_last_name_tier2').val();
		var training_contact_phone_number_tier2 		= $('#training_contact_phone_number_tier2').val();
		
		// child_care part //
		var child_care_tier2 						= $("#child_care_tier2").prop('checked');
		
		var childcare_agency_yes_no_tier2			= $("input[name='childcare_agency_yes_no_tier2']:checked").val();
		var childcare_contact_email_tier2 			= $('#childcare_contact_email_tier2').val();
		var childcare_contact_first_name_tier2 		= $('#childcare_contact_first_name_tier2').val();
		var childcare_contact_last_name_tier2 		= $('#childcare_contact_last_name_tier2').val();
		var childcare_contact_phone_number_tier2 	= $('#childcare_contact_phone_number_tier2').val();
		
		var childcare_agencies_yes_no_tier2 		= $("input[name='childcare_agencies_yes_no_tier2']:checked").val();
		
		var childcare_training_agency_yes_no_tier2 	= $("input[name='childcare_training_agency_yes_no_tier2']:checked").val();
		var childcare_training_contact_email_tier2 	= $('#childcare_training_contact_email_tier2').val();
		var childcare_training_contact_first_name_tier2 = $('#childcare_training_contact_first_name_tier2').val();
		var childcare_training_contact_last_name_tier2 = $('#childcare_training_contact_last_name_tier2').val();
		var childcare_training_contact_phone_number_tier2 = $('#childcare_training_contact_phone_number_tier2').val();
		
		// transportation part //
		var transportation_tier2 						= $('#transportation_tier2').prop('checked');
		
		var transportation_agency_yes_no_tier2			= $("input[name='transportation_agency_yes_no_tier2']:checked").val();
		var transportation_contact_email_tier2 			= $('#transportation_contact_email_tier2').val();
		var transportation_contact_first_name_tier2 	= $('#transportation_contact_first_name_tier2').val();
		var transportation_contact_last_name_tier2 		= $('#transportation_contact_last_name_tier2').val();
		var transportation_contact_phone_number_tier2 	= $('#transportation_contact_phone_number_tier2').val();
		
		var transportation_agencies_yes_no_tier2 		= $("input[name='transportation_agencies_yes_no_tier2']:checked").val();
		
		var transportation_training_agency_yes_no_tier2 = $("input[name='transportation_training_agency_yes_no_tier2']:checked").val();
		var transportation_training_contact_email_tier2 = $('#transportation_training_contact_email_tier2').val();
		var transportation_training_contact_first_name_tier2 = $('#transportation_training_contact_first_name_tier2').val();
		var transportation_training_contact_last_name_tier2 = $('#transportation_training_contact_last_name_tier2').val();
		var transportation_training_contact_phone_number_tier2 = $('#transportation_training_contact_phone_number_tier2').val();
		
		if(approved_tier2 == 'Yes'){
			if(family_team_meeting_tier2){
				$("#family_team_meeting_tier2").css('outline-color', 'green');
				$("#family_team_meeting_tier2").css('outline-style', 'solid');
				$("#family_team_meeting_tier2").css('outline-width', 'thin');
				
				$("#child_care_tier2").css('outline-color', 'green');
				$("#child_care_tier2").css('outline-style', 'solid');
				$("#child_care_tier2").css('outline-width', 'thin');
				
				$("#transportation_tier2").css('outline-color', 'green');
				$("#transportation_tier2").css('outline-style', 'solid');
				$("#transportation_tier2").css('outline-width', 'thin');
				if(background_agency_yes_no_tier2 == 'Background Agency Yes'){
					if(background_contact_email_tier2 == '' || background_contact_email_tier2 == undefined){
						$('input[name="background_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_email_tier2"]').focus();
						valid = false;
					}else if(!pattern.test(background_contact_email_tier2)){
						$('input[name="background_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_email_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_email_tier2"]').css('border', '1px solid green');
					}
					if(background_contact_first_name_tier2 == '' || background_contact_first_name_tier2 == undefined){
						$('input[name="background_contact_first_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_first_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_first_name_tier2"]').css('border', '1px solid green');
					}
					if(background_contact_last_name_tier2 == '' || background_contact_last_name_tier2 == undefined){
						$('input[name="background_contact_last_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_last_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_last_name_tier2"]').css('border', '1px solid green');
					}
					if(background_contact_phone_number_tier2 == '' || background_contact_phone_number_tier2 == undefined){
						$('input[name="background_contact_phone_number_tier2"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_phone_number_tier2"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(background_contact_phone_number_tier2)){
						$('input[name="background_contact_phone_number_tier2"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="background_contact_phone_number_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_phone_number_tier2"]').css('border', '1px solid green');
					}
				}
				if(training_agency_yes_no_tier2 == 'Training Agency Yes'){
					if(training_contact_email_tier2 == '' || training_contact_email_tier2 == undefined){
						$('input[name="training_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_email_tier2"]').focus();
						valid = false;
					}else if(!pattern.test(training_contact_email_tier2)){
						$('input[name="training_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_email_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_email_tier2"]').css('border', '1px solid green');
					}
					if(training_contact_first_name_tier2 == '' || training_contact_first_name_tier2 == undefined){
						$('input[name="training_contact_first_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_first_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_first_name_tier2"]').css('border', '1px solid green');
					}
					if(training_contact_last_name_tier2 == '' || training_contact_last_name_tier2 == undefined){
						$('input[name="training_contact_last_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_last_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_last_name_tier2"]').css('border', '1px solid green');
					}
					if(training_contact_phone_number_tier2 == '' || training_contact_phone_number_tier2 == undefined){
						$('input[name="training_contact_phone_number_tier2"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_phone_number_tier2"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(training_contact_phone_number_tier2)){
						$('input[name="training_contact_phone_number_tier2"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="training_contact_phone_number_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_phone_number_tier2"]').css('border', '1px solid green');
					}
				}
			}
			if(child_care_tier2){
				$("#family_team_meeting_tier2").css('outline-color', 'green');
				$("#family_team_meeting_tier2").css('outline-style', 'solid');
				$("#family_team_meeting_tier2").css('outline-width', 'thin');
				
				$("#child_care_tier2").css('outline-color', 'green');
				$("#child_care_tier2").css('outline-style', 'solid');
				$("#child_care_tier2").css('outline-width', 'thin');
				
				$("#transportation_tier2").css('outline-color', 'green');
				$("#transportation_tier2").css('outline-style', 'solid');
				$("#transportation_tier2").css('outline-width', 'thin');
				if(childcare_agency_yes_no_tier2 == 'Background Agency Yes'){
					if(childcare_contact_email_tier2 == '' || childcare_contact_email_tier2 == undefined){
						$('input[name="childcare_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_contact_email_tier2"]').focus();
						valid = false;
					}else if(!pattern.test(childcare_contact_email_tier2)){
						$('input[name="childcare_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_contact_email_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_contact_email_tier2"]').css('border', '1px solid green');
					}
					if(childcare_contact_first_name_tier2 == '' || childcare_contact_first_name_tier2 == undefined){
						$('input[name="childcare_contact_first_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_contact_first_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_contact_first_name_tier2"]').css('border', '1px solid green');
					}
					if(childcare_contact_last_name_tier2 == '' || childcare_contact_last_name_tier2 == undefined){
						$('input[name="childcare_contact_last_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_contact_last_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_contact_last_name_tier2"]').css('border', '1px solid green');
					}
					if(childcare_contact_phone_number_tier2 == '' || childcare_contact_phone_number_tier2 == undefined){
						$('input[name="childcare_contact_phone_number_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_contact_phone_number_tier2"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(childcare_contact_phone_number_tier2)){
						$('input[name="childcare_contact_phone_number_tier2"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="childcare_contact_phone_number_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_contact_phone_number_tier2"]').css('border', '1px solid green');
					}
				}
				if(childcare_training_agency_yes_no_tier2 == 'Training Agency Yes'){
					if(childcare_training_contact_email_tier2 == '' || childcare_training_contact_email_tier2 == undefined){
						$('input[name="childcare_training_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_training_contact_email_tier2"]').focus();
						valid = false;
					}else if(!pattern.test(childcare_training_contact_email_tier2)){
						$('input[name="childcare_training_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_training_contact_email_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_training_contact_email_tier2"]').css('border', '1px solid green');
					}
					if(childcare_training_contact_first_name_tier2 == '' || childcare_training_contact_first_name_tier2 == undefined){
						$('input[name="childcare_training_contact_first_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_training_contact_first_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_training_contact_first_name_tier2"]').css('border', '1px solid green');
					}
					if(childcare_training_contact_last_name_tier2 == '' || childcare_training_contact_last_name_tier2 == undefined){
						$('input[name="childcare_training_contact_last_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_training_contact_last_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_training_contact_last_name_tier2"]').css('border', '1px solid green');
					}
					if(childcare_training_contact_phone_number_tier2 == '' || childcare_training_contact_phone_number_tier2 == undefined){
						$('input[name="childcare_training_contact_phone_number_tier2"]').css('border', '1px solid #A94442');
						$('input[name="childcare_training_contact_phone_number_tier2"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(childcare_training_contact_phone_number_tier2)){
						$('input[name="childcare_training_contact_phone_number_tier2"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="childcare_training_contact_phone_number_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="childcare_training_contact_phone_number_tier2"]').css('border', '1px solid green');
					}
				}
			}
			if(transportation_tier2){
				$("#family_team_meeting_tier2").css('outline-color', 'green');
				$("#family_team_meeting_tier2").css('outline-style', 'solid');
				$("#family_team_meeting_tier2").css('outline-width', 'thin');
				
				$("#child_care_tier2").css('outline-color', 'green');
				$("#child_care_tier2").css('outline-style', 'solid');
				$("#child_care_tier2").css('outline-width', 'thin');
				
				$("#transportation_tier2").css('outline-color', 'green');
				$("#transportation_tier2").css('outline-style', 'solid');
				$("#transportation_tier2").css('outline-width', 'thin');
				if(transportation_agency_yes_no_tier2 == 'Background Agency Yes'){
					if(transportation_contact_email_tier2 == '' || transportation_contact_email_tier2 == undefined){
						$('input[name="transportation_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_contact_email_tier2"]').focus();
						valid = false;
					}else if(!pattern.test(transportation_contact_email_tier2)){
						$('input[name="transportation_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_contact_email_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_contact_email_tier2"]').css('border', '1px solid green');
					}
					if(transportation_contact_first_name_tier2 == '' || transportation_contact_first_name_tier2 == undefined){
						$('input[name="transportation_contact_first_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_contact_first_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_contact_first_name_tier2"]').css('border', '1px solid green');
					}
					if(transportation_contact_last_name_tier2 == '' || transportation_contact_last_name_tier2 == undefined){
						$('input[name="transportation_contact_last_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_contact_last_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_contact_last_name_tier2"]').css('border', '1px solid green');
					}
					if(transportation_contact_phone_number_tier2 == '' || transportation_contact_phone_number_tier2 == undefined){
						$('input[name="transportation_contact_phone_number_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_contact_phone_number_tier2"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(transportation_contact_phone_number_tier2)){
						$('input[name="transportation_contact_phone_number_tier2"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="transportation_contact_phone_number_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_contact_phone_number_tier2"]').css('border', '1px solid green');
					}
				}
				if(transportation_training_agency_yes_no_tier2 == 'Training Agency Yes'){
					if(transportation_training_contact_email_tier2 == '' || transportation_training_contact_email_tier2 == undefined){
						$('input[name="transportation_training_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_training_contact_email_tier2"]').focus();
						valid = false;
					}else if(!pattern.test(transportation_training_contact_email_tier2)){
						$('input[name="transportation_training_contact_email_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_training_contact_email_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_training_contact_email_tier2"]').css('border', '1px solid green');
					}
					if(transportation_training_contact_first_name_tier2 == '' || transportation_training_contact_first_name_tier2 == undefined){
						$('input[name="transportation_training_contact_first_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_training_contact_first_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_training_contact_first_name_tier2"]').css('border', '1px solid green');
					}
					if(transportation_training_contact_last_name_tier2 == '' || transportation_training_contact_last_name_tier2 == undefined){
						$('input[name="transportation_training_contact_last_name_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_training_contact_last_name_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_training_contact_last_name_tier2"]').css('border', '1px solid green');
					}
					if(transportation_training_contact_phone_number_tier2 == '' || transportation_training_contact_phone_number_tier2 == undefined){
						$('input[name="transportation_training_contact_phone_number_tier2"]').css('border', '1px solid #A94442');
						$('input[name="transportation_training_contact_phone_number_tier2"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(transportation_training_contact_phone_number_tier2)){
						$('input[name="transportation_training_contact_phone_number_tier2"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="transportation_training_contact_phone_number_tier2"]').focus();
						valid = false;
					}else{
						$('input[name="transportation_training_contact_phone_number_tier2"]').css('border', '1px solid green');
					}
				}
			}
			if(transportation_tier2 == false && child_care_tier2 == false && family_team_meeting_tier2 == false){
				$("#family_team_meeting_tier2").css('outline-color', 'red');
				$("#family_team_meeting_tier2").css('outline-style', 'solid');
				$("#family_team_meeting_tier2").css('outline-width', 'thin');
				
				$("#child_care_tier2").css('outline-color', 'red');
				$("#child_care_tier2").css('outline-style', 'solid');
				$("#child_care_tier2").css('outline-width', 'thin');
				
				$("#transportation_tier2").css('outline-color', 'red');
				$("#transportation_tier2").css('outline-style', 'solid');
				$("#transportation_tier2").css('outline-width', 'thin');
				valid = false;
			}
		}
	// End Tier 2 Approved Fields //
	
	// Start Tier 3 Approved Fields //
		
		var approved_tier3 					= $('#approved_tier3').val();
		var estimated_launch_date_tier3 	= $('#estimated_launch_date_tier3').val();
		var agency_rep_email_tier3 			= $('input[name="agency_rep_email_tier3[]"]').val();
		var first_name_tier3 				= $('input[name="first_name_tier3[]"]').val();
		var last_name_tier3 				= $('input[name="last_name_tier3[]"]').val();
		var phone_number_tier3 				= $('input[name="phone_number_tier3[]"]').val();
		
		if(approved_tier3 == '' || approved_tier3 == undefined){
			$("#approved_tier3").css('border', '1px solid #A94442');
			$("#approved_tier3").focus();
			valid = false;
		}else{
			$("#approved_tier3").css('border', '1px solid green');
			//if(approved_tier3 == 'Yes'){}
		}
		
		
		var count_email_tier3 = 0;
		$("input[name='agency_rep_email_tier3[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#agency_rep_email_tier3_'+count_email_tier3).css('border', '1px solid #A94442');
				$('#agency_rep_email_tier3_'+count_email_tier3).focus();
				valid = false;
			}else if(!pattern.test($(this).val())){
				$('#agency_rep_email_tier3_'+count_email_tier3).css('border', '1px solid #A94442');
				$('#agency_rep_email_tier3_'+count_email_tier3).focus();
				valid = false;
			}else{
				$('#agency_rep_email_tier3_'+count_email_tier3).css('border', '1px solid green');
			}
			count_email_tier3++;
		});
		
		var count_first_name_tier3 = 0;
		$("input[name='first_name_tier3[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#first_name_tier3_'+count_first_name_tier3).css('border', '1px solid #A94442');
				$('#first_name_tier3_'+count_first_name_tier3).focus();
				valid = false;
			}else{
				$('#first_name_tier3_'+count_first_name_tier3).css('border', '1px solid green');
			}
			count_first_name_tier3++;
		});
		
		var count_last_name_tier3 = 0;
		$("input[name='last_name_tier3[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#last_name_tier3_'+count_last_name_tier3).css('border', '1px solid #A94442');
				$('#last_name_tier3_'+count_last_name_tier3).focus();
				valid = false;
			}else{
				$('#last_name_tier3_'+count_last_name_tier3).css('border', '1px solid green');
			}
			count_last_name_tier3++;
		});
		
		var count_phone_number_tier3 = 0;
		$("input[name='phone_number_tier3[]']").each(function () {
			if($(this).val() == '' || $(this).val() == undefined){
				$('#phone_number_tier3_'+count_phone_number_tier3).css('border', '1px solid #A94442');
				$('#phone_number_tier3_'+count_phone_number_tier3).focus();
				valid = false;
			}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test($(this).val())){
				$('#phone_number_tier3_'+count_phone_number_tier3).css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
				$('#phone_number_tier3_'+count_phone_number_tier3).focus();
				valid = false;
			}else{
				$('#phone_number_tier3_'+count_phone_number_tier3).css('border', '1px solid green');
			}
			count_phone_number_tier3++;
		});
		
		if(estimated_launch_date_tier3 == '' || estimated_launch_date_tier3 == undefined){
			$("#estimated_launch_date_tier3").css('border', '1px solid #A94442');
			//$("#estimated_launch_date_tier3").focus();
			valid = false;
		}else{
			$("#estimated_launch_date_tier3").css('border', '1px solid green');
		}
		
		// Non-licensed Home //
		var non_licensed_home_tier3 				= $('#non_licensed_home_tier3').prop('checked');
		
		var background_agency_yes_no_tier3			= $("input[name='background_agency_yes_no_tier3']:checked").val();
		var background_contact_email_tier3 			= $('#background_contact_email_tier3').val();
		var background_contact_first_name_tier3 	= $('#background_contact_first_name_tier3').val();
		var background_contact_last_name_tier3 		= $('#background_contact_last_name_tier3').val();
		var background_contact_phone_number_tier3 	= $('#background_contact_phone_number_tier3').val();
		
		var agencies_yes_no_tier3 					= $("input[name='agencies_yes_no_tier3']:checked").val();
		
		var walk_through_agency_yes_no_tier3 		= $("input[name='walk_through_agency_yes_no_tier3']:checked").val();
		
		var walk_through_contact_email_tier3 		= $('#walk_through_contact_email_tier3').val();
		var walk_through_contact_first_name_tier3 	= $('#walk_through_contact_first_name_tier3').val();
		var walk_through_contact_last_name_tier3 	= $('#walk_through_contact_last_name_tier3').val();
		var walk_through_contact_phone_number_tier3 = $('#walk_through_contact_phone_number_tier3').val();
		
		var training_agency_yes_no_tier3 			= $("input[name='training_agency_yes_no_tier3']:checked").val();
		
		var training_contact_email_tier3 			= $('#training_contact_email_tier3').val();
		var training_contact_first_name_tier3 		= $('#training_contact_first_name_tier3').val();
		var training_contact_last_name_tier3 		= $('#training_contact_last_name_tier3').val();
		var training_contact_phone_number_tier3 	= $('#training_contact_phone_number_tier3').val();
		
		var walk_through_agency_contact_tier3 		= $('#walk_through_agency_contact_tier3').val();
		
		if(approved_tier3 == 'Yes'){
			if(non_licensed_home_tier3){
				$("#non_licensed_home_tier3").css('outline-color', 'green');
				$("#non_licensed_home_tier3").css('outline-style', 'solid');
				$("#non_licensed_home_tier3").css('outline-width', 'thin');
				if(background_agency_yes_no_tier3 == 'Background Agency Yes'){
					if(background_contact_email_tier3 == '' || background_contact_email_tier3 == undefined){
						$('input[name="background_contact_email_tier3"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_email_tier3"]').focus();
						valid = false;
					}else if(!pattern.test(background_contact_email_tier3)){
						$('input[name="background_contact_email_tier3"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_email_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_email_tier3"]').css('border', '1px solid green');
					}
					if(background_contact_first_name_tier3 == '' || background_contact_first_name_tier3 == undefined){
						$('input[name="background_contact_first_name_tier3"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_first_name_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_first_name_tier3"]').css('border', '1px solid green');
					}
					if(background_contact_last_name_tier3 == '' || background_contact_last_name_tier3 == undefined){
						$('input[name="background_contact_last_name_tier3"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_last_name_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_last_name_tier3"]').css('border', '1px solid green');
					}
					if(background_contact_phone_number_tier3 == '' || background_contact_phone_number_tier3 == undefined){
						$('input[name="background_contact_phone_number_tier3"]').css('border', '1px solid #A94442');
						$('input[name="background_contact_phone_number_tier3"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(background_contact_phone_number_tier3)){
						$('input[name="background_contact_phone_number_tier3"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="background_contact_phone_number_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="background_contact_phone_number_tier3"]').css('border', '1px solid green');
					}
				}
				if(walk_through_agency_yes_no_tier3 == 'Walk-Through Agency Yes'){
					if(walk_through_contact_email_tier3 == '' || walk_through_contact_email_tier3 == undefined){
						$('input[name="walk_through_contact_email_tier3"]').css('border', '1px solid #A94442');
						$('input[name="walk_through_contact_email_tier3"]').focus();
						valid = false;
					}else if(!pattern.test(walk_through_contact_email_tier3)){
						$('input[name="walk_through_contact_email_tier3"]').css('border', '1px solid #A94442');
						$('input[name="walk_through_contact_email_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="walk_through_contact_email_tier3"]').css('border', '1px solid green');
					}
					if(walk_through_contact_first_name_tier3 == '' || walk_through_contact_first_name_tier3 == undefined){
						$('input[name="walk_through_contact_first_name_tier3"]').css('border', '1px solid #A94442');
						$('input[name="walk_through_contact_first_name_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="walk_through_contact_first_name_tier3"]').css('border', '1px solid green');
					}
					if(walk_through_contact_last_name_tier3 == '' || walk_through_contact_last_name_tier3 == undefined){
						$('input[name="walk_through_contact_last_name_tier3"]').css('border', '1px solid #A94442');
						$('input[name="walk_through_contact_last_name_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="walk_through_contact_last_name_tier3"]').css('border', '1px solid green');
					}
					if(walk_through_contact_phone_number_tier3 == '' || walk_through_contact_phone_number_tier3 == undefined){
						$('input[name="walk_through_contact_phone_number_tier3"]').css('border', '1px solid #A94442');
						$('input[name="walk_through_contact_phone_number_tier3"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(walk_through_contact_phone_number_tier3)){
						$('input[name="walk_through_contact_phone_number_tier3"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="walk_through_contact_phone_number_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="walk_through_contact_phone_number_tier3"]').css('border', '1px solid green');
					}
				}
				if(training_agency_yes_no_tier3 == 'Training Agency Yes'){
					if(training_contact_email_tier3 == '' || training_contact_email_tier3 == undefined){
						$('input[name="training_contact_email_tier3"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_email_tier3"]').focus();
						valid = false;
					}else if(!pattern.test(training_contact_email_tier3)){
						$('input[name="training_contact_email_tier3"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_email_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_email_tier3"]').css('border', '1px solid green');
					}
					if(training_contact_first_name_tier3 == '' || training_contact_first_name_tier3 == undefined){
						$('input[name="training_contact_first_name_tier3"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_first_name_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_first_name_tier3"]').css('border', '1px solid green');
					}
					if(training_contact_last_name_tier3 == '' || training_contact_last_name_tier3 == undefined){
						$('input[name="training_contact_last_name_tier3"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_last_name_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_last_name_tier3"]').css('border', '1px solid green');
					}
					if(training_contact_phone_number_tier3 == '' || training_contact_phone_number_tier3 == undefined){
						$('input[name="training_contact_phone_number_tier3"]').css('border', '1px solid #A94442');
						$('input[name="training_contact_phone_number_tier3"]').focus();
						valid = false;
					}else if(!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(training_contact_phone_number_tier3)){
						$('input[name="training_contact_phone_number_tier3"]').css('border', '1px solid #A94442'); //(123) 456 7899, (123).456.7899, (123)-456-7899, 123-456-7899, 123 456 7899, 1234567899
						$('input[name="training_contact_phone_number_tier3"]').focus();
						valid = false;
					}else{
						$('input[name="training_contact_phone_number_tier3"]').css('border', '1px solid green');
					}
				}
			}else{
				$("#non_licensed_home_tier3").css('outline-color', 'red');
				$("#non_licensed_home_tier3").css('outline-style', 'solid');
				$("#non_licensed_home_tier3").css('outline-width', 'thin');
				valid = false;
			}
			
			if(walk_through_agency_contact_tier3 == '' || walk_through_agency_contact_tier3 == undefined){
				$('input[name="walk_through_agency_contact_tier3"]').css('border', '1px solid #A94442');
				$('input[name="walk_through_agency_contact_tier3"]').focus();
				valid = false;
			}else{
				$('input[name="walk_through_agency_contact_tier3"]').css('border', '1px solid green');
			}
		}
	// End Tier 3 Approved Fields //
	
	return valid;
}

$(document).ready(function(){
	$('#county-check-deactivate').click(function() {
		var checked = $(this).prop('checked');
		var agency_id		= $('#agency_id').val();
		if(checked){
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deactivatedCounty',
				data     : { "deactivate" : 'Deactivated', '_token' : token },
				cache    : false,
				success  : function(data) {
					$('.table.table-bordered.table-striped tbody tr').remove();
					if(data != ''){
						for (i = 0; i < data.length; i++) {
							$('.table.table-bordered.table-striped').append('<tr data-id="'+data[i].id+'" id="county'+data[i].id+'" onClick="editCounty('+data[i].id+','+agency_id+');"><td>'+data[i].county_name+'</td><td>'+data[i].status+'</td><td>'+data[i].state+'</td><td>'+data[i].approved_tier1+'</td><td>'+data[i].approved_tier2+'</td><td>'+data[i].approved_tier3+'</td><td>'+data[i].rep_tier1[0].first_name_tier1+' '+data[i].rep_tier1[0].last_name_tier1+'</td><td>'+data[i].rep_tier1[0].agency_rep_email_tier1+'</td><td>'+data[i].rep_tier1[0].phone_number_tier1+'</td><td>'+data[i].activation_date+'</td></tr>');
						}
					}
				}
			});
		}else{
			var token		= $('input[name="_token"]').val();
			$.ajax({
				type     : "POST",
				url      : base_url+'/agencies/deactivatedCounty',
				data     : { "deactivate" : 'All', '_token' : token },
				cache    : false,
				success  : function(data) {
					$('.table.table-bordered.table-striped tbody tr').remove();
					if(data != ''){
						for (i = 0; i < data.length; i++) {
							$('.table.table-bordered.table-striped').append('<tr data-id="'+data[i].id+'" id="county'+data[i].id+'" onClick="editCounty('+data[i].id+', '+agency_id+');"><td>'+data[i].county_name+'</td><td>'+data[i].status+'</td><td>'+data[i].state+'</td><td>'+data[i].approved_tier1+'</td><td>'+data[i].approved_tier2+'</td><td>'+data[i].approved_tier3+'</td><td>'+data[i].rep_tier1[0].first_name_tier1+' '+data[i].rep_tier1[0].last_name_tier1+'</td><td>'+data[i].rep_tier1[0].agency_rep_email_tier1+'</td><td>'+data[i].rep_tier1[0].phone_number_tier1+'</td><td>'+data[i].activation_date+'</td></tr>');
						}
					}
				}
			});
		}
	});
});

function editCounty(id){
	var arr = id.split(',');
	var county_id = arr[0];
	var agency_id = arr[1];
	if(id != ''){
		var token		= $('input[name="_token"]').val();
		$.ajax({
			type     : "POST",
			url      : base_url+'/agencies/editCounty',
			data     : { "agency_id" : agency_id, "county_id" : county_id, '_token' : token },
			cache    : false,
			success  : function(data) {
				if(data != ''){
					window.location.href = base_url+'/agencies/county-details?county_id='+data.county_id+'&agency_id='+data.agency_id;
				}
			}
		});
	}
}

/** End Agency code **/