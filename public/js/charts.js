window.onload = function () {
    charts = charts || [];
    while (charts.length) {
        charts.shift().call();
    }
}

$(document).ready(function () {
    $(document).on('click', '.chart-print-btn', function (e) {
        e.preventDefault();

        var canvas = $(this).closest('.chart-container').find('canvas').get(0);

        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()">' +
            '<style>@page{size:landscape;}</style>' +
            '<h3>'+$(this).closest('.chart-container').find('h3').text()+'</h3>' +
            '<img src="'+canvas.toDataURL("image/jpg")+'" alt=""/>' +
            '</body></html>');
        newWin.document.close();

        setTimeout(function(){newWin.close();},10);
    })
});