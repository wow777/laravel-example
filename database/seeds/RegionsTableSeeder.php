<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'regions.json';
        if (file_exists($filename)) {
            $guidToID = GUIDToID::instance();

            $region = new App\Models\Region;
            $loadInFileHelper = new LoadInFileHelper($region);

            $ipRegion = new App\Models\ImplementingPartnerRegion;
            $loadInFileHelperIP = new LoadInFileHelper($ipRegion);

            $idx = 0;
            $handle = fopen($filename, 'r');
                while (($buffer = fgets($handle, 200000)) !== false) {
                $record = json_decode($buffer);

                // TEMP TEMP TEMP - ask Derek about this Region - why does it exist
                if ($record->field_271 === 'Central Office') {
                    continue;
                }

                // Skip international for now
                if ($record->field_273_raw[0]->identifier === 'INTERNATIONAL') {
                    continue;
                }

                $region->id = ++$idx;
                $region->guid = $record->id;
                $region->name = $record->field_271;
                $region->state_id = $guidToID->get($record->field_273_raw[0]->id, "\N");

                $userID = empty($record->field_1633_raw) ? "\N" : $guidToID->get($record->field_1633_raw->email, "\N");
                $region->regional_manager_id = $userID;

                if(!empty($record->field_2586_raw)) {
                    foreach($record->field_2586_raw as $ip) {
                        $ipRegion->implementing_partner_id = $guidToID->get($ip->id, -1);
                        $ipRegion->region_id = $region->id;

                        if ($ipRegion->implementing_partner_id !== -1) {
                            $loadInFileHelperIP->fputcsv($ipRegion);
                        }
                    }
                }

                $loadInFileHelper->fputcsv($region);
                $guidToID->store($region->guid, $region->id);
            }
            fclose($handle);
        }

        $loadInFileHelper->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
            array('@created_at', '@update_at'));

        $loadInFileHelperIP->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
            array('@created_at', '@update_at'));
    }
}
