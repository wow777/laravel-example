<?php

use Illuminate\Database\Seeder;

class AddRequestIDtoResponsesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $cnt = DB::select(DB::raw('select count(*) as count from responses where request_id is not null') )[0]->count ;
        $this->command->info("$cnt responses have request_id");

        $this->command->info('Adding request_id ');
        DB::insert('update responses 
                    join request_recipients on request_recipients.id = responses.request_recipient_id
                    set responses.request_id = request_recipients.request_id;');

        $cnt = DB::select(DB::raw('select count(*) as count from responses where request_id is not null') )[0]->count - $cnt;
        $this->command->info('Added '. $cnt. ' request_id ');
    }
}
