<?php

use Illuminate\Database\Seeder;

class ImplementingPartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'implementing_partners.json';
        if (file_exists($filename)) {
            $guidToID = GUIDToID::instance();
            $ip = new App\Models\ImplementingPartner;
            $loadInFileHelper = new LoadInFileHelper($ip);

            $idx = 0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                $ip->id = ++$idx;
                $ip->guid = $record->id;
                $ip->name = $record->field_2537;
                $ip->logo_url = $record->field_2539;
                $ip->giving_percent = $record->field_2633_raw * 100.0;
                $ip->national_percent = $record->field_2632_raw * 100.0;
                $ip->state_director_percent = $record->field_2546_raw * 100.0;
                $ip->rm_percent = $record->field_2545_raw * 100.0;
                $ip->ambassador_percent = $record->field_2547_raw * 100.0;

                $ip->giving_option = $record->field_2540[0] === '1' ? 1 : 2;

                $ip->address_line1 = $record->field_2640;
                $ip->city = $record->field_2643;
                $ip->state = $record->field_2641_raw[0]->identifier ?? "\N";
                $ip->zip_code = $record->field_2642_raw[0]->identifier ?? "\N";
                $ip->website_url = $record->field_2644;
                $ip->phone = $record->field_2645_raw->full ?? "\N";

                $loadInFileHelper->fputcsv($ip);
                $guidToID->store($ip->guid, $ip->id);
            }
            fclose($handle);

            $loadInFileHelper->saveAndLoad();
        }
    }
}
