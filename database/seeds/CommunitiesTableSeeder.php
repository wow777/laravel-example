<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CommunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting CommunitiesTableSeeder: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'communities.json';
        if (file_exists($filename)) {
            $idx = 0;
            $community = new App\Models\Community;
            $loadInFileHelper = new LoadInFileHelper($community);
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                try {
                    $community->id = ++$idx;
                    $community->name = $record->field_884;
                    $community->guid = $record->id;
                    $community->area_of_critical_need = $record->field_2513_raw ?? false;
                    $community->coordinator_id = $guidToID->get($record->field_2517, "\N");

                    $loadInFileHelper->fputcsv($community);
                    $guidToID->store($community->guid, $community->id);
                }
                catch(Exception $e) {
                    echo "Exception caught: " . $e->getMessage() . PHP_EOL;
                    echo $buffer . PHP_EOL;
                }
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad();

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Communities: {$dt} added {$insertCnt}" . PHP_EOL;
        }




        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'partners.json';
        if (file_exists($filename)) {
            $dt = Carbon::now()->toDateTimeString();
            echo "Starting CommunityUserTableSeeder: {$dt}" . PHP_EOL;

            $cu = new App\Models\CommunityUser;
            $loadInFileHelper = new LoadInFileHelper($cu);

            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                try {
                    foreach($record->field_2516_raw as $community) {
                        $cu->user_id = $guidToID->get($record->field_1226_raw->email ?? -1, -1);
                        $cu->community_id = $guidToID->get($community->id, -1);

                        if($cu->user_id !== -1 && $cu->community_id !== -1) {
                            $loadInFileHelper->fputcsv($cu);
                        }
                    }
                }
                catch(Exception $e) {
                    echo "Exception caught: " . $e->getMessage() . PHP_EOL;
                    echo $buffer . PHP_EOL;
                }
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad();

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished CommunityUser: {$dt} added {$insertCnt}" . PHP_EOL;
        }
    }
}
