<?php

use Illuminate\Database\Seeder;

class AddAgencyIDToRepCountySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Adding agency_id to rep_counties');
        DB::update('update rep_counties
            join agency_worker on agency_worker.id = rep_counties.rep_id
             set rep_counties.agency_id = agency_worker.agency_id ');
    }
}
