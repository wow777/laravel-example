<?php

use Illuminate\Database\Seeder;

class GmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'first_name' => 'Test',
            'last_name' => 'Test',
            'email' => 'testgms@www.com',
            'password' => bcrypt('123'),
        ]);

        App\Models\Gms::create(['user_id' => $user->id]);

        \App\Models\Region::whereNull('regional_manager_id')
            ->orWhere('regional_manager_id', 9846)
            ->update(['regional_manager_id' => $user->id ]);
        \App\Models\State::whereNull('state_director_id')
           ->update(['state_director_id' => $user->id]);
        \App\Models\Community::whereNull('coordinator_id')->update(['coordinator_id' => $user->id]);

    }
}
