<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($x = 0; $x < 10; $x++){

            $event = factory(App\Models\Event::class)->create();
            $invite_date = $event->start->subDays(30);
            factory(\App\Models\EventUser::class, 20,['event_id' => $event->id, $invite_date])->create();

        }

    }
}
