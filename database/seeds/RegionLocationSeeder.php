<?php

use Illuminate\Database\Seeder;

class RegionLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \App\Models\RegionLocation::query()->truncate();

        $cnt = DB::select(DB::raw('select count(*) as count from region_locations') )[0]->count ;

        $this->command->info("$cnt current entries for region_locations");
        $this->command->info('Adding zipcodes ');
        DB::insert('insert into region_locations (county_id, zip_code_id, region_id, created_at, updated_at) 
                    select `knack_county_id`, id, region_id , NOW() , NOW() 
                    from zip_codes where region_id is not null');

        $this->command->info('Adding counties');
        DB::insert('insert into region_locations (county_id, zip_code_id, region_id, created_at, updated_at)  
                    select counties.id, cz.zip_code_id, counties.region_id, NOW() , NOW()  
                    from counties
                    join county_zip_code as cz on cz.county_id = counties.id 
                    where region_id is not null');

        $cnt = DB::select(DB::raw('select count(*) as count from region_locations') )[0]->count - $cnt;
        $this->command->info('Added '. $cnt);


    }
}
