<?php

use Illuminate\Database\Seeder;

class TestNeedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add fake needs to requests for testing
        $requests = \App\Models\Request::where('status', 'Open')->inRandomOrder()->limit(10)->get();

        foreach($requests as $request) {
            \App\Models\RequestNeed::create([
                'name' => 'Twin Bed',
                'value' => 100,
                'description' => 'Twin size bed with frame',
                'request_id' => $request->id,
                'met' => false,
            ]);
            \App\Models\RequestNeed::create([
                'name' => 'Bike',
                'value' => 50,
                'description' => '24" bike for teenage boy',
                'request_id' => $request->id,
                'met' => false,
            ]);
            \App\Models\RequestNeed::create([
                'name' => 'Automotive repair',
                'value' => 800,
                'description' => 'Need an alternator fixed on 2002 Toyota Camry',
                'request_id' => $request->id,
                'met' => false,
            ]);
        }
    }
}
