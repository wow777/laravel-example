<?php

class LoadInFileHelper
{
    // $dates = array of dates that might need setting to NULL with a special value of '0001-01-01'
    public function __construct($model, $dates = array())
    {
        $this->reset($model, $dates);
    }

    public function reset($model, $dates = array())
    {
        $this->first = true;
        $this->dates = $dates;
        $this->tableName = $model->getTable();

        $outFileName = realpath(dirname(__FILE__)) . '/exports/gen_' . $this->tableName . '.csv';
        $this->outFileName = str_replace('\\', '/', $outFileName);

        $this->outFile = fopen($this->outFileName, 'w');
    }

    public function fputcsv($model, $delimiter = ",", $enclosure = '"', $escape_char = "\\")
    {
        // Save the fields to the file
        if($this->first) {
            $this->first = false;

            $this->fields = array_keys($model->getAttributes());
            fputcsv($this->outFile, $this->fields, $delimiter, $enclosure, $escape_char);
        }
        fputcsv($this->outFile, $model->getAttributes(), $delimiter, $enclosure, $escape_char);
    }

    public function saveAndLoad($set = '', $extraFields = array())
    {
        fclose($this->outFile);

        $strFields = implode(',', array_merge($this->fields, $extraFields));

        $query = "LOAD DATA LOCAL INFILE '{$this->outFileName}'
            INTO TABLE {$this->tableName} FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'
             IGNORE 1 LINES 
            ({$strFields}) {$set}";

//        DB::connection()->getpdo()->exec('set unique_checks=0');
//        DB::connection()->getpdo()->exec('set foreign_key_checks=0');
//        DB::connection()->getpdo()->exec('set sql_log_bin=0');
        $ret = DB::connection()->getpdo()->exec($query);
        foreach($this->dates as $date) {
            DB::connection()->getpdo()->exec("update {$this->tableName} set {$date}=NULL where DATE({$date})<\"1000-01-01\"");
        }

        echo "SEED:loaded {$ret} rows into {$this->tableName}" . PHP_EOL;

        return $ret;
    }

    private $tableName='';
    private $fields='';
    private $first=true;
    private $outFile = null;
    private $outFileName;
    private $dates = array();
}