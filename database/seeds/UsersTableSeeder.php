<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'all_users.json';
        if (file_exists($filename)) {
            $dt = Carbon::now()->toDateTimeString();
            echo "Starting All Users: {$dt}" . PHP_EOL;

            $guidToID = GUIDToID::instance();
            $user = new App\User;
            $loadInFileHelper = new LoadInFileHelper($user, array('deleted_at'));

            $bcryptPassword = bcrypt(123);
            $idx = 0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                $user->id = ++$idx;
                $user->guid = $record->id;
                $user->first_name = $record->field_117_raw->first ?? "\N";
                $user->last_name = $record->field_117_raw->last ?? "\N";
                $user->email = $record->field_118_raw->email;
                $user->phone = empty($record->field_302_raw) ? '' : $record->field_302_raw->full;
                $user->password = $bcryptPassword;
                $user->user_photo_url = $record->field_671;
                $user->title = $record->field_2272;
                $user->send_email = $record->field_682_raw;
                $user->deleted_at = empty($record->field_2185_raw) ? "0001-01-01" : Carbon::parse($record->field_2185_raw->timestamp);

                $guidToID->store($user->guid, $idx);
                $guidToID->store($user->email, $idx);

                $loadInFileHelper->fputcsv($user);
            }
            fclose($handle);

            $loadInFileHelper->saveAndLoad('SET created_at=NOW()', array('@created_at'));

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished All Users: {$dt}" . PHP_EOL;
        }

        // For now we'll make every SD and RM an IP Admin
        $ipUsers = array();

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'regional_managers.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (!empty($record->field_2551_raw)) {
                    $ipID = $guidToID->get($record->field_2551_raw[0]->id, -1);
                    if ($ipID !== -1) {
                        $userID = $guidToID->get($record->field_591_raw->email, -1);
                        if ($userID !== -1) {
                            $ipUsers[$userID] = array('id' => $ipID, 'admin' => true, 'ambassador' => false);
                        }
                    }
                }
            }
            fclose($handle);
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'state_directors.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (!empty($record->field_2550_raw)) {
                    $ipID = $guidToID->get($record->field_2550_raw[0]->id, -1);
                    if ($ipID !== -1) {
                        $userID = $guidToID->get($record->field_581_raw->email, -1);
                        if($userID !== -1) {
                            // Doesn't matter if we've already added it - it will get overwritten with the same value
                            $ipUsers[$userID] = array('id' => $ipID, 'admin' => true, 'ambassador' => false);
                        }
                    }
                }
            }
            fclose($handle);
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'ip_ambassadors.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (!empty($record->field_2583_raw)) {
                    $ipID = $guidToID->get($record->field_2583_raw[0]->id, -1);
                    if ($ipID !== -1) {
                        $userID = $guidToID->get($record->field_2572_raw->email, -1);
                        if($userID !== -1) {
                            $isAdmin = key_exists($userID, $ipUsers);
                            $ipUsers[$userID] = array('id' => $ipID, 'admin' => $isAdmin, 'ambassador' => true);
                        }
                    }
                }
            }
            fclose($handle);
        }

        $ipUser = new App\Models\ImplementingPartnerUser;
        $loadInFileHelper->reset($ipUser);

        foreach($ipUsers as $key => $val) {
            $ipUser->implementing_partner_id = $val['id'];
            $ipUser->user_id = $key;

            $ipUser->admin = $val['admin'] ? '1' : '0';
            $ipUser->ambassador = $val['ambassador'] ? '1' : '0';
            $loadInFileHelper->fputcsv($ipUser);
        }

        $loadInFileHelper->saveAndLoad('SET created_at=NOW()', array('@created_at'));

        $dt = Carbon::now()->toDateTimeString();
        echo "Finished ip users: {$dt}" . PHP_EOL;

        $gmsUser = new App\Models\Gms;
        $loadInFileHelper->reset($gmsUser);

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'gms_users.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                $userID = $guidToID->get($record->field_228_raw->email, -1);
                if ($userID !== -1) {
                    $gmsUser = new App\Models\Gms;
                    $gmsUser->user_id = $userID;

                    $loadInFileHelper->fputcsv($gmsUser);
                }
            }
            fclose($handle);
        }

        $loadInFileHelper->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
            array('@created_at', '@update_at'));

        $dt = Carbon::now()->toDateTimeString();
        echo "Finished GMS Users: {$dt}" . PHP_EOL;
    }
}
