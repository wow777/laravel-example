<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CountiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting Counties: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();
        $countyToRegion = array();

        // Convert some regions from zip code based to county
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'zip_codes.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                // Skip the international records
                if ($record->field_32_raw[0]->identifier === 'INTERNATIONAL') {
                    continue;
                }

                if (!empty($record->field_1036_raw) && in_array($record->field_1036_raw[0]->id,
                        $guidToID->regionsToConvert())) {
                    $countyToRegion[$record->field_33_raw[0]->id] = $guidToID->get($record->field_1036_raw[0]->id,
                        null);
                }
            }
            fclose($handle);
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'counties.json';
        if (file_exists($filename)) {
            $county = new App\Models\County;
            $loadInFileHelper = new LoadInFileHelper($county, array('launch_date'));
            $idx = 0;
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 100000)) !== false) {
                $record = json_decode($buffer);

                // Skip the international records
                if ($record->field_29_raw[0]->identifier === 'INTERNATIONAL') {
                    continue;
                }

                $county->id = ++$idx;
                $county->name = $record->field_28;
                $county->guid = $record->id;
                $county->status = $record->field_1023;
                $county->polygon_data = $record->field_30;
                $county->launch_date = empty($record->field_2276) ? "0001-01-01" : Carbon::parse($record->field_2276);
                $county->set_radius_to_community = $record->field_2506_raw ? '1' : '0';
                $county->max_churches_in_radius = $record->field_2507;
                $county->max_radius = $record->field_2508;
                $county->esc_max_churches_in_radius = $record->field_2509;
                $county->esc_max_radius = $record->field_2510;
                $county->state_id = empty($record->field_29_raw) ? "\N" : $guidToID->get($record->field_29_raw[0]->id,
                    "\N");

                if (array_key_exists($record->id, $countyToRegion)) {
                    $county->region_id = $countyToRegion[$record->id];
                } else {
                    $county->region_id = "\N";
                }

                $guidToID->store($county->guid, $county->id);
                $guidToID->store($county->name . ':' . $county->state_id, $county->id);
                $loadInFileHelper->fputcsv($county);
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                array('@created_at', '@update_at'));

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Counties: {$dt} : {$insertCnt}" . PHP_EOL;
        }
    }
}
