<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ZipCodesTableSeeder extends Seeder
{
    // The national_county.txt county names don't always match up with the Knack county names
    // we'll try to find what we need.
    private function findCounty($guidToID, $stateID, $countyName) {
        $countyNameIn = $countyName;
        $countyID = $guidToID->get($countyName . ':' . $stateID, -1);
        if($countyID !== -1) {
            return $countyID;
        }

        $thingsToTry = array(
            'St.' => 'Saint',
            'city' => 'City',
            ' Parish' => '',
            ' Census Area' => '',
            ' City and Borough' => '',
            ' Borough' => '',
            ' Municipality' => '',
            "'s" => 's',
            'Saint' => 'St',
            '-' => ' ',
            ' City' => '',
            'Ste.' => 'Sainte',
            'Wade Hampton' => 'Kusilvak', // name changed
            'Shannon' => 'Oglala Lakota'  // name changed
        );
        foreach($thingsToTry as $key => $val) {
            if(strpos($countyName, $key) >= 0) {
                $countyName = str_replace($key, $val, $countyName);
                $countyID = $guidToID->get($countyName . ':' . $stateID, -1);
                if($countyID !== -1) {
                    return $countyID;
                }
            }
        }

        echo "  {$countyNameIn} : {$countyName} : {$stateID} didn't work" . PHP_EOL;

        return -1;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting ZipCodes: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();

        $statesToIgnore = array('AS', 'GU', 'MP', 'PR', 'UM', 'VI');
        $statesFIPSToAbbr = array();
        $fipsToCountyID = array();

        // This file is from:
        // https://www2.census.gov/geo/docs/reference/codes/files/national_county.txt
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'national_county.txt';
        if (file_exists($filename)) {
            if (($handle = fopen($filename, "r")) !== false) {
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $stateAbbr = $data[0];
                    $stateFIPS = $data[1];

                    $statesFIPSToAbbr[$stateFIPS] = $stateAbbr;

                    if (in_array($stateAbbr, $statesToIgnore)) {
                        continue;
                    }

                    $countyFIPS = $data[2];
                    $countyName = preg_replace('/(.*) County/', '$1', $data[3]);

                    $stateID = $guidToID->get($stateAbbr, -1);
                    $countyID = $this->findCounty($guidToID, $stateID, $countyName);
                    if ($countyID === -1) {
                        echo "{$stateAbbr} : {$countyName}, not found" . PHP_EOL;
                    } else {
                        $fipsToCountyID[$stateFIPS . ':' . $countyFIPS] = $countyID;
                    }
                }
            }
            fclose($handle);
        }

        $zipCodeToID = array();
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'zip_codes.json';
        if (file_exists($filename)) {
            $zipCode = new App\Models\ZipCode;
            $loadInFileHelper = new LoadInFileHelper($zipCode);

            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                // Skip the international records
                if ($record->field_32_raw[0]->identifier === 'INTERNATIONAL') {
                    continue;
                }

                $zipCode->id = $record->field_31;
                $zipCode->code = $record->field_31;
                $zipCode->city = $record->field_633; // TEMP TEMP TEMP - not sure we need city on Zip???
                $zipCode->lon = empty($record->field_379_raw->longitude) ? "\N" : $record->field_379_raw->longitude;
                $zipCode->lat = empty($record->field_379_raw->latitude) ? "\N" : $record->field_379_raw->latitude;
                $zipCode->guid = $record->id;
                $zipCode->community_id = empty($record->field_1269_raw) ? "\N" : $guidToID->get($record->field_1269_raw[0]->id,
                    "\N");
                $zipCode->knack_state_id = empty($record->field_32_raw) ? "\N" : $guidToID->get($record->field_32_raw[0]->id,
                    "\N");
                $zipCode->knack_county_id = empty($record->field_33_raw) ? "\N" : $guidToID->get($record->field_33_raw[0]->id,
                    "\N");

                // Convert some regions to county based
                $region_guid = empty($record->field_1036_raw) ? -1 : $record->field_1036_raw[0]->id;
                if (in_array($region_guid, $guidToID->regionsToConvert())) {
                    $zipCode->region_id = "\N";
                } else {
                    $zipCode->region_id = $guidToID->get($region_guid, "\N");
                }

                $guidToID->store($zipCode->guid, $zipCode->id);
                $guidToID->storeZipCounty($zipCode->guid, $zipCode->knack_county_id);
                $zipCodeToID[$zipCode->code] = $zipCode->id;
                $loadInFileHelper->fputcsv($zipCode);
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                array('@created_at', '@update_at'));

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Zip Codes: {$dt} : {$insertCnt}" . PHP_EOL;
        }


        // This file is from:
        // https://www2.census.gov/geo/docs/maps-data/data/rel/zcta_county_rel_10.txt
        // And described at:
        // https://www.census.gov/geo/maps-data/data/zcta_rel_layout.html
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'zcta_county_rel_10.txt';
        $cnt = 0;
        if (file_exists($filename)) {
            if (($handle = fopen($filename, "r")) !== false) {
                $idx = 0;
                $countyZipCode = new App\Models\CountyZipCode;
                $loadInFileHelper = new LoadInFileHelper($countyZipCode);

                fgetcsv($handle, 1000, ","); // Skip the first line
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $zcta = $data[0];
                    $stateFIPS = $data[1];
                    $countyFIPS = $data[2];

                    if (in_array($statesFIPSToAbbr[$stateFIPS], $statesToIgnore)) {
                        continue;
                    }

                    $key = $stateFIPS . ':' . $countyFIPS;
                    $countyID = array_key_exists($key, $fipsToCountyID) ? $fipsToCountyID[$key] : -1;
                    $zipCodeID = array_key_exists($zcta, $zipCodeToID) ? $zipCodeToID[$zcta] : -1;
                    if ($countyID !== -1 && $zipCodeID !== -1) {
                        $countyZipCode->id = ++$idx;
                        $countyZipCode->county_id = $countyID;
                        $countyZipCode->zip_code_id = $zipCodeID;

                        $countyZipCode->zpoppct = $data[16];
                        $countyZipCode->zhupct = $data[17];
                        $countyZipCode->zareapct = $data[18];
                        $countyZipCode->zarealandpct = $data[19];
                        $countyZipCode->copoppct = $data[20];
                        $countyZipCode->cohupct = $data[21];
                        $countyZipCode->coareapct = $data[22];
                        $countyZipCode->coarealandpct = $data[23];

                        $loadInFileHelper->fputcsv($countyZipCode);
                    } else {
                        $cnt++;
                        echo "{$cnt}: {$zcta} {$data[1]} {$countyFIPS} {$countyID} {$zipCodeID}  {$key} not found" . PHP_EOL;
                    }
                }

                fclose($handle);

                $insertCnt = $loadInFileHelper->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                    array('@created_at', '@update_at'));

                $dt = Carbon::now()->toDateTimeString();
                echo "Finished County to Zip Code mapping: {$dt} : {$insertCnt}" . PHP_EOL;
            }
        }
    }
}