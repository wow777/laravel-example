<?php

class GUIDToID
{
    /**
     * Call this method to get singleton
     */
    public static function instance()
    {
        static $instance = false;
        if ($instance === false) {
            // Late static binding (PHP 5.3+)
            $instance = new static();
        }

        return $instance;
    }

    /**
     * Make constructor private, so nobody can call "new Class".
     */
    private function __construct()
    {
        $this->regionsToConvert = array(
            '5a5cda4fded64d593bcdad45', /*KS- 7 Central*/
            '5730e97df806ac8b044bdd13', /*KS- Unassigned*/
            '5730e97df806ac8b044bdd0d', /*KS-1 NE*/
            '5730e97df806ac8b044bdd0f', /*KS-2 South Central*/
            '5730e97df806ac8b044bdd11', /*KS-3 SE*/
            '57be0558b1ca8588225313be', /*KS-5 North Central*/
            '5a53a5af2d08605570009147'  /*KS-6 West*/
            );
    }

    /**
     * Make clone magic method private, so nobody can clone instance.
     */
    private function __clone()
    {
    }

    /**
     * Make sleep magic method private, so nobody can serialize instance.
     */
    private function __sleep()
    {
    }

    /**
     * Make wakeup magic method private, so nobody can unserialize instance.
     */
    private function __wakeup()
    {
    }

    public function store($guid, $id)
    {
        $this->guidToID[$guid] = $id;
    }

    public function get($guid, $default)
    {
        return $this->guidToID[$guid] ?? $default;
    }

    public function regionsToConvert()
    {
        return $this->regionsToConvert;
    }

    public function getCount() {
        return count($this->guidToID);
    }

    public function genFiles() {
        return true;
    }

    public function storeGroup($groupGUID, $groupID, $churchID) {
        $this->groups[$groupGUID] = array('id' => $groupID, 'church_id' => $churchID);
    }

    public function getGroup($groupGUID, $default) {
        return $this->groups[$groupGUID] ?? $default;
    }

    public function storeZipCounty($zipGUID, $countyID) {
        $this->zipCounty[$zipGUID] = $countyID;
    }

    public function getZipCounty($zipGUID) {
        return $this->zipCounty[$zipGUID];
    }

    private $guidToID = array();
    private $groups = array();
    private $zipCounty = array();
    private $regionsToConvert = array();

}