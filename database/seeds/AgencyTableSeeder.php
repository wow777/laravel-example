<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AgencyTableSeeder extends Seeder
{
    public function runTier2ServiceTypes()
    {
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'tier2_service_types.json';
        if (file_exists($filename)) {
            $guidToID = GUIDToID::instance();

            $t2st = new App\Models\Tier2ServiceType;
            $loadInFileHelper = new LoadInFileHelper($t2st);

            $idx=0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                $t2st->id = ++$idx;
                $t2st->name = $record->field_2464;
                $t2st->guid = $record->id;

                $loadInFileHelper->fputcsv($t2st);
                $guidToID->store($t2st->guid, $t2st->id);
            }
            fclose($handle);

            $loadInFileHelper->saveAndLoad();
        }
    }

    public function runTier3ServiceTypes()
    {
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'tier3_service_types.json';
        if (file_exists($filename)) {
            $guidToID = GUIDToID::instance();

            $t3st = new App\Models\Tier3ServiceType;
            $loadInFileHelper = new LoadInFileHelper($t3st);

            $idx=0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                $t3st->id = ++$idx;
                $t3st->name = $record->field_2465;
                $t3st->guid = $record->id;

                $loadInFileHelper->fputcsv($t3st);
                $guidToID->store($t3st->guid, $t3st->id);
            }
            fclose($handle);

            $loadInFileHelper->saveAndLoad();
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting Agencies: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();

        $this->runTier2ServiceTypes();
        $this->runTier3ServiceTypes();

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'agencies.json';
        if (file_exists($filename)) {
            $agency = new App\Models\Agency;
            $loadInFileHelper = new LoadInFileHelper($agency);

            $agencyCounty = new App\Models\AgencyCounty;
            $loadInFileHelperAC = new LoadInFileHelper($agencyCounty);

            $agencyTier2ST = new App\Models\AgencyTier2ServiceType;
            $loadInFileHelperTier2st = new LoadInFileHelper($agencyTier2ST);

            $agencyTier3ST = new App\Models\AgencyTier3ServiceType;
            $loadInFileHelperTier3st = new LoadInFileHelper($agencyTier3ST);

            $idx=0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                $agency->id = ++$idx;
                $agency->name = $record->field_814;
                $agency->name2 = $record->field_1787;
                $agency->global_email = $record->field_1675_raw->email ?? "\N";
                $agency->email_domain = $record->field_674;
                $agency->guid = $record->id;
                $agency->address_line1 = $record->field_1193;
                $agency->address_line2 = "\N"; // None for now
                $agency->country = 'United States of America'; //$record->field_474;
                $agency->city = "\N"; // TEMP TEMP TEMP - what are we doing with city where we used to pull from zip

                $agency->state_id = $guidToID->get($record->field_1192_raw[0]->id, "\N");
                $agency->zip_code_id = $guidToID->get($record->field_1194_raw[0]->id, "\N");

                $agency->tier_1_approval = '0';
                $agency->tier_2_approval = '0';
                $agency->tier_3_approval = '0';
                if (!empty($record->field_825_raw)) {
                    foreach ($record->field_825_raw as $tier) {
                        if (strpos($tier->identifier, 'Tier 1') === 0) {
                            $agency->tier_1_approval = '1';
                        } else {
                            if (strpos($tier->identifier, 'Tier 2') === 0) {
                                $agency->tier_2_approval = '1';
                            } else {
                                if (strpos($tier->identifier, 'Tier 3') === 0) {
                                    $agency->tier_3_approval = '1';
                                }
                            }
                        }
                    }
                }

                foreach ($record->field_809_raw as $county) {
                    $agencyCounty->tier = 1;
                    $agencyCounty->agency_id = $agency->id;
                    $agencyCounty->county_id = $guidToID->get($county->id, "\N");

                    if($agencyCounty->county_id !== "\N") {
                        $loadInFileHelperAC->fputcsv($agencyCounty);
                    }
                }

                foreach ($record->field_2238_raw as $county) {
                    $agencyCounty->tier = 2;
                    $agencyCounty->agency_id = $agency->id;
                    $agencyCounty->county_id = $guidToID->get($county->id, "\N");

                    $loadInFileHelperAC->fputcsv($agencyCounty);
                }

                foreach ($record->field_2239_raw as $county) {
                    $agencyCounty->tier = 3;
                    $agencyCounty->agency_id = $agency->id;
                    $agencyCounty->county_id = $guidToID->get($county->id, "\N");

                    $loadInFileHelperAC->fputcsv($agencyCounty);
                }

                foreach($record->field_2466_raw as $t2st) {
                    $agencyTier2ST->agency_id = $agency->id;
                    $agencyTier2ST->tier2_service_type_id = $guidToID->get($t2st->id, "\N");

                    $loadInFileHelperTier2st->fputcsv($agencyTier2ST);
                }

                foreach($record->field_2467_raw as $t3st) {
                    $agencyTier3ST->agency_id = $agency->id;
                    $agencyTier3ST->tier3_service_type_id = $guidToID->get($t3st->id, "\N");

                    $loadInFileHelperTier3st->fputcsv($agencyTier3ST);
                }

                $agency->about_text = $record->field_1846;
                $agency->website = $record->field_1847_raw->url ?? "\N";
                $agency->logo_url = $record->field_1851;
                $agency->request_submission_option = empty($record->field_2315) ? "\N" : $record->field_2315[0];

                $guidToID->store($agency->guid, $agency->id);
                $loadInFileHelper->fputcsv($agency);
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                array('@created_at', '@update_at'));

            $loadInFileHelperAC->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                array('@created_at', '@update_at'));

            $loadInFileHelperTier2st->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                array('@created_at', '@update_at'));

            $loadInFileHelperTier3st->saveAndLoad('SET created_at=NOW(),updated_at=NOW()',
                array('@created_at', '@update_at'));

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Agencies: {$dt} : {$insertCnt}" . PHP_EOL;
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'agency_workers.json';
        if (file_exists($filename)) {
            $agencyWorker = new App\Models\AgencyWorker;
            $loadInFileHelper = new LoadInFileHelper($agencyWorker);

            $repCounty = new App\Models\RepCounty;
            $loadInFileHelperRC = new LoadInFileHelper($repCounty);

            $idx = 0;
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                $agencyWorker->id = ++$idx;
                $agencyWorker->user_id = $guidToID->get($record->field_560_raw->email ?? -1, "\N");
                $agencyWorker->agency_id = $guidToID->get($record->field_564_raw[0]->id ?? -1, "\N");

                $agencyWorker->agency_rep = $record->field_688_raw ?? false;
                $agencyWorker->agency_executive = $record->field_2251_raw ?? false;

                if($agencyWorker->user_id !== "\N" && $agencyWorker->agency_id !== "\N") {
                    $loadInFileHelper->fputcsv($agencyWorker);

                    foreach($record->field_689_raw as $county) {
                        $repCounty->rep_id = $agencyWorker->id;
                        $repCounty->county_id = $guidToID->get($county->id, "\N");
                        $repCounty->tier = 1;
                        $loadInFileHelperRC->fputcsv($repCounty);
                    }

                    foreach($record->field_2627_raw as $county) {
                        $repCounty->rep_id = $agencyWorker->id;
                        $repCounty->county_id = $guidToID->get($county->id, "\N");
                        $repCounty->tier = 2;
                        $loadInFileHelperRC->fputcsv($repCounty);
                    }

                    foreach($record->field_2628_raw as $county) {
                        $repCounty->rep_id = $agencyWorker->id;
                        $repCounty->county_id = $guidToID->get($county->id, "\N");
                        $repCounty->tier = 3;
                        $loadInFileHelperRC->fputcsv($repCounty);
                    }
                }
                else {
                    echo "ERROR: agency_worker: {$record->id}" . PHP_EOL;
                }
            }
            fclose($handle);

            $loadInFileHelper->saveAndLoad();

            $loadInFileHelperRC->saveAndLoad();
        }
    }
}