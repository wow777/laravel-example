<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guidToID = GUIDToID::instance();
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting Requests: {$dt}" . PHP_EOL;

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'requests.json';
        if (file_exists($filename)) {

            $request = new App\Models\Request;
            $loadInFileHelper = new LoadInFileHelper($request, array('date_closed', 'reviewed_at', 'next_auto_step_ts'));

            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 300000000)) !== false) {
                $record = json_decode($buffer);
                // Skip the international records
                if ($record->field_321_raw[0]->identifier === 'INTERNATIONAL') {
                    continue;
                }

                $request->id = $record->field_1112;
                $request->created_at = Carbon::parse($record->field_605_raw->timestamp);
                $request->updated_at = empty($record->field_2169_raw->timestamp) ? $request->created_at : Carbon::parse($record->field_2169_raw->timestamp);
                $request->status = $record->field_604_raw ? 'Open' : 'Closed';
                $request->guid = $record->field_402;
                $request->tier = substr($record->field_332_raw[0]->identifier, 5, 1);
                $request->agency_case_id = $record->field_875;
                $request->role_of_worker = $record->field_331;
                $request->grid_category = $record->field_336;
                switch($record->field_334[0]) {
                    case 'N': $request->level_of_urgency = 0; break;
                    case 'L': $request->level_of_urgency = 0; break;
                    case 'H': $request->level_of_urgency = 1; break;
                    case 'C': $request->level_of_urgency = 2; break;
                    default:
                        $request->level_of_urgency = 0;
                        echo "{$record->id} has unknown urgency: {$record->field_334}";
                        break;
                }
                $request->purpose_of_request = $record->field_1137;
                $request->case_description = $record->field_339;
                $request->children_served = $record->field_340;
                $request->strictly_financial = $record->field_2248_raw ?? false;
                $request->estimated_value = empty($record->field_341_raw) ? "\N" : $record->field_341_raw;
                $request->cost_avoidance = empty($record->field_2270_raw) ? "\N" : $record->field_2270_raw;
                $request->service_hours_hours_per = $record->field_2243_raw ?? "\N";
                $request->service_hours_times_per_week = $record->field_2244_raw ?? "\N";
                $request->service_hours_total_weeks = $record->field_2245_raw ?? "\N";

                $request->reviewed_by = $record->field_2316_raw->email ?? "\N";
                $request->reviewed_at = empty($record->field_2317_raw->timestamp) ? "0001-01-01" : Carbon::parse($record->field_2317_raw->timestamp);
                $request->reason_closed = $record->field_343;
                $request->responding_churches_not_listed = $record->field_1599_raw ?? "\N";
                $request->date_closed = empty($record->field_1496_raw->timestamp) ? "0001-01-01" : Carbon::parse($record->field_1496_raw->timestamp);
                $request->rating = $record->field_345_raw ?? "\N";
                $request->reason_for_rating = $record->field_346;
                $request->worker_story = $record->field_347;
                $request->next_auto_step = $record->field_2527;
                $request->next_auto_step_ts = empty($record->field_2528_raw->timestamp) ? "0001-01-01" : Carbon::parse($record->field_2528_raw->timestamp);
                $request->zip_code_id = $guidToID->get($record->field_323_raw[0]->id, "\N");
                $request->agency_id = $guidToID->get($record->field_326_raw[0]->id, "\N");
                $request->county_id = $guidToID->get($record->field_324_raw[0]->id ?? -1, "\N");
                $request->state_id = $guidToID->get($record->field_321_raw[0]->id ?? -1, "\N");
                $request->worker_county_id = $guidToID->get($record->field_2630_raw[0]->id ?? -1, "\N");

                $request->tier2_service_type_id = $guidToID->get($record->field_2476_raw[0]->id ?? -1, "\N");
                $request->tier3_service_type_id = $guidToID->get($record->field_2477_raw[0]->id ?? -1, "\N");

                // TEMP TEMP TEMP - this should use the Agency Worker field, but that role hasn't been exported/imported yet
                // so just use the email field for now
                $request->worker_id = $guidToID->get($record->field_890_raw->email ?? -1, "\N");

                $guidToID->store($request->guid, $request->id);
                $loadInFileHelper->fputcsv($request);
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad();

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Requests: {$dt} : {$insertCnt}" . PHP_EOL;
        }

        $dt = Carbon::now()->toDateTimeString();
        echo "Starting RequestRecipients: {$dt}" . PHP_EOL;

        $rrIDs = array();

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'church_in_radius.json';
        if (file_exists($filename)) {
            $rr = new App\Models\RequestRecipient;
            $loadInFileHelperRR = new LoadInFileHelper($rr);

            $cr = new App\Models\ChurchReview;
            $loadInFileHelperCR = new LoadInFileHelper($cr);

            $idx=0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                $requestID = $guidToID->get($record->field_359_raw[0]->id ?? -1, -1);
                $groupVal = $guidToID->getGroup($record->field_362_raw[0]->id ?? -1, -1);
                if($groupVal === -1) {
                    $groupID = "\N";
                    $churchID = $guidToID->get($record->field_362_raw[0]->id ?? -1, -1);
                }
                else {
                    $groupID = $groupVal['id'];
                    $churchID = $groupVal['church_id'];
                }

                if($requestID === -1 || $churchID === -1) {
                    continue;
                }

                $rr->id = ++$idx;
                $rr->created_at = Carbon::parse($record->field_858);
                $rr->guid = $record->id;
                $rr->request_id = $requestID;
                $rr->church_id = $churchID;
                $rr->group_id = $groupID;
                $rr->helped_meet_the_need = $record->field_1495_raw ?? false;

                $rrIDs[$record->id] = array('id' => $rr->id, 'church_id' => $churchID, 'group_id' => $groupID);

                $loadInFileHelperRR->fputcsv($rr);

                // This church helped close the request
                if($rr->helped_meet_the_need && $record->field_1539_raw) {
                    $cr->guid = $record->id;
                    $cr->created_at = Carbon::parse($record->field_858);
                    $cr->request_response_id = $rr->id;
                    $cr->church_id = $churchID;
                    $cr->group_id = $groupID;
                    $cr->verify_did_you_help = true;
                    $cr->comments_questions = $record->field_1535;
                    $cr->your_story = $record->field_1540;
                    $cr->rating = $record->field_1537_raw ?? "\N";
                    $cr->user_id = $guidToID->get($record->field_1534_raw[0]->id ?? -1, "\N");

                    $loadInFileHelperCR->fputcsv($cr);
                }
            }
            fclose($handle);

            $loadInFileHelperRR->saveAndLoad();
            $loadInFileHelperCR->saveAndLoad();
        }

        $dt = Carbon::now()->toDateTimeString();
        echo "Starting RequestRecipients: {$dt}" . PHP_EOL;

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'responses.json';
        if (file_exists($filename)) {
            $response = new App\Models\Response;
            $loadInFileHelperResponse = new LoadInFileHelper($response);

            $idx=0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if(!array_key_exists($record->field_2120_raw[0]->id, $rrIDs)) {
                    continue;
                }

                $rrVal = $rrIDs[$record->field_2120_raw[0]->id];

                $response->id = ++$idx;
                $response->request_recipient_id = $rrVal['id'];
                $response->created_at = Carbon::parse($record->field_2122);
                $response->guid = $record->id;
                $response->message = $record->field_2124;
                $response->church_id = $rrVal['church_id'];
                $response->group_id = $rrVal['group_id'];
                $response->user_id = $guidToID->get($record->field_2121_raw[0]->id, "\N");

                $loadInFileHelperResponse->fputcsv($response);
            }
            fclose($handle);

            $loadInFileHelperResponse->saveAndLoad();
        }
    }
}
