<?php

use Illuminate\Database\Seeder;

class AgencyReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Library\AgencyReviewSeedingService::migrateAgencyReviews();
    }
}
