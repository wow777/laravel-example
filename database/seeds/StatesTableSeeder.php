<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;


class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting States: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();

        $sdEMailToIPGuid = array();
        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'state_directors.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                $sdEMailToIPGuid[$record->field_581_raw->email] = $record->field_2550_raw[0]->id;
            }
            fclose($handle);
        }

        $state = new App\Models\State;
        $loadInFileHelper = new LoadInFileHelper($state);

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'states.json';

        if (file_exists($filename)) {
            $idx = 0;
            $handle = fopen($filename, 'r');
            while (($buffer = fgets($handle, 4000000)) !== false) {
                $record = json_decode($buffer);

                // Skip International
                if ($record->field_25 === 'INTERNATIONAL') {
                    continue;
                }

                $state->id = ++$idx;
                $state->abbreviation = $record->field_26;
                $state->name = $record->field_25;
                $state->guid = $record->id;
                $state->status = $record->field_1056;
                $state->polygon_data = $record->field_2552;
                $state->state_director_id = "\N";

                if (!empty($record->field_2522_raw)) {
                    $userID = $guidToID->get($record->field_2522_raw->email, null);
                    if ($userID) {
                        $state->state_director_id = $userID;
                    }
                }

                $loadInFileHelper->fputcsv($state);
                $guidToID->store($state->guid, $state->id);
                $guidToID->store($state->abbreviation, $state->id);
            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad();
            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Requests: {$dt} : {$insertCnt}" . PHP_EOL;
        }
    }
}
