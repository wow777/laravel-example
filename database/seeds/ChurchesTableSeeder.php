<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ChurchesTableSeeder extends Seeder
{

    private function initRoles($userID, $churchID, &$roles)
    {
        if (!array_key_exists($userID, $roles)) {
            $roles[$userID] = new App\Models\ChurchUser;
            $role = &$roles[$userID];
            $role->church_id = $churchID;
            $role->user_id = $userID;
            $role->pastor = '0';
            $role->t1_point_person = '0';
            $role->t2_point_person = '0';
            $role->t3_point_person = '0';
            $role->primary_point_person = '0';
            $role->finance_person = '0';
            $role->small_group_admin = '0';
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting Churches: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'churches.json';
        if (file_exists($filename)) {
            $church = new App\Models\Church;
            $loadInFileHelper = new LoadInFileHelper($church,
                array('tier_1_enrollment_complete_date', 'deleted_at', 'special_offering_date', 'orientation_date',
                    'giving_info_last_updated', 'last_special_offering_date', 'match_date', 'last_invoice_date'));

            $idx = 0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 20000)) !== false) {
                $record = json_decode($buffer);

                try {
                    $church->id = ++$idx;
                    $church->created_at = Carbon::parse($record->field_668_raw->timestamp);
                    $church->updated_at = Carbon::parse($record->field_1357_raw->timestamp);
                    $church->deleted_at = empty($record->field_2128) ?
                        "0001-01-01" : Carbon::parse($record->field_2128);
                    $church->name = $record->field_147;
                    $church->has_enrolled = $record->field_1034_raw ? '1' : '0';
                    $church->affiliation = $record->field_148;
                    $church->average_church_attendance = $record->field_383;
                    $church->economic_conditions = $record->field_385;
                    $church->location_type = $record->field_384;
                    $church->how_did_you_hear = $record->field_387;
                    $church->giving_options = $record->field_389;
                    $church->giving_amount = $record->field_2189_raw ?? "\N";
                    $church->giving_frequency = $record->field_2190 ?? "\N";
                    $church->collect_special_offering = $record->field_2519_raw ?? "\N";
                    $church->special_offering_date = empty($record->field_2520) ?
                        "0001-01-01" : Carbon::parse($record->field_2520);
                    $church->giving_info_last_updated = empty($record->field_2521) ?
                        "0001-01-01" : Carbon::parse($record->field_2521);
                    $church->last_special_offering_date = empty($record->field_2524) ?
                        "0001-01-01" : Carbon::parse($record->field_2524);
                    $church->last_special_offering_amount = $record->field_2525_raw ?? "\N";
                    $church->match_amount = $record->field_2526_raw ?? "\N";
                    $church->match_date = empty($record->field_2598) ?
                        "0001-01-01" : Carbon::parse($record->field_2598);
                    $church->match_notes = $record->field_2635;
                    $church->questions_comments = $record->field_166;

                    // Convert Pay Immediately to a more correct send_invoice
                    $send_invoice = "\N";
                    $giving_amount = $record->field_2189_raw ?? 0;
                    if($record->field_2226 === 'No' && $giving_amount > 0 && $record->field_729_raw) {
                        $send_invoice = '1';
                        $giving_options = strtolower($church->giving_options);
                        if(strpos($giving_options, 'unable') !== FALSE ||
                            strpos($giving_options, 'we plan to only collect a special offering') !== FALSE ||
                            strpos($giving_options, 'giving via implementing partner') !== FALSE) {
                            $send_invoice = '0';
                        }
                    }

                    $church->send_invoice = $send_invoice;
                    $church->last_invoice_date = empty($record->field_2240) ?
                        "0001-01-01" : Carbon::parse($record->field_2240);
                    $church->guid = $record->id;
                    $church->activation_step_1_complete = $record->field_843_raw ? '1' : '0';
                    $church->activation_step_2_complete = $record->field_844_raw ? '1' : '0';
                    $church->activation_step_3_complete = $record->field_845_raw ? '1' : '0';
                    $church->tier_1_enrollment_complete = $record->field_729_raw ? '1' : '0';
                    $church->tier_2_enrollment_complete = $record->field_956_raw ? '1' : '0';
                    $church->tier_3_enrollment_complete = $record->field_957_raw ? '1' : '0';
                    $church->orientation_date = empty($record->field_417) ?
                        "0001-01-01" : Carbon::parse($record->field_417);
                    $church->tier_1_enrollment_complete_date = empty($record->field_1154) ?
                            "0001-01-01" : Carbon::parse($record->field_1154);
                    $church->address_line1 = $record->field_155;
                    $church->address_line2 = $record->field_156;
                    $church->country = 'United States of America'; //$record->field_474;
                    $church->city = $record->field_1477;
                    $church->lon = empty($record->field_386_raw->longitude) ? "\N" : $record->field_386_raw->longitude;
                    $church->lat = empty($record->field_386_raw->latitude) ? "\N" : $record->field_386_raw->latitude;
                    $church->implementing_partner_id = $guidToID->get($record->field_2544_raw[0]->id ?? -1, "\N");
                    $church->ambassador_id = $guidToID->get($record->field_2585 ?? -1, "\N");
                    $church->engage_congregation = $record->field_422;
                    $church->engage_share_your_ideas = $record->field_423;
                    $church->how_will_you_share_requests = $record->field_424;
                    $church->share_needs_share_your_ideas = $record->field_425;
                    $church->activate_small_groups = $record->field_458 === 'Yes';
                    $church->initial_enrollment_name = $record->field_656;
                    $church->initial_enrollment_email = $record->field_657_raw->email ?? "\N";
                    $church->initial_enrollment_phone = $record->field_1676_raw->full ?? "\N";

                    $church->non_cp_contact_name = $record->field_1197;
                    $church->non_cp_phone = $record->field_1198_raw->full ?? "\N";

                    try {
                        $church->state_id = $guidToID->get($record->field_475_raw[0]->id, "\N");
                        $church->zip_code_id = $guidToID->get($record->field_157_raw[0]->id, "\N");

                        // Some churches were imported without county, figure it out now from what was in knack
                        if(empty($record->field_473_raw[0]->id)) {
                            $church->county_id = $guidToID->getZipCounty($record->field_157_raw[0]->id);
                        }
                        else {
                            $church->county_id = $guidToID->get($record->field_473_raw[0]->id, -1);

                            if($church->county_id === -1) {
                                echo "Church doesn't have county set: {$church->guid}" . PHP_EOL;
                                continue;
                            }
                        }
                    }
                    catch(Exception $e) {
                        $state_id = $record->field_475_raw[0]->id ?? '!state';
                        $zip_id = $record->field_157_raw[0]->id ?? '!zip';
                        $county_id = $record->field_473_raw[0]->id ?? '!county';
                        echo "Church doesn't have proper geo connections: {$church->guid} {$state_id} {$zip_id} {$county_id}" . PHP_EOL;
                        continue;
                    }

                    // TEMP TEMP TEMP - a church that has an address should have a lat/lon
                    if (empty($record->field_386_raw->longitude) || empty($record->field_386_raw->latitude)) {
                        echo "Church has no lat/lon: {$church->name}: {$church->guid} {$church->tier_1_enrollment_complete}" . PHP_EOL;
                    }

                    $loadInFileHelper->fputcsv($church);
                    $guidToID->store($church->guid, $church->id);
                }
                catch(Exception $e) {
                    echo "Exception caught: " . $e->getMessage() . PHP_EOL;
                    echo $buffer . PHP_EOL;
                }
            }
            fclose($handle);
            $insertCnt = $loadInFileHelper->saveAndLoad();

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Churches: {$dt} added {$insertCnt}" . PHP_EOL;
        }

        $roles = array();

        $dt = Carbon::now()->toDateTimeString();
        echo "Starting Church Roles: {$dt}" . PHP_EOL;

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'pastors.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (empty($record->field_498_raw) || count($record->field_498_raw) === 0) {
                    //echo "Pastor with no church: {$record->field_494_raw->email}" . PHP_EOL;
                    continue;
                }

                $churchGUID = $record->field_498_raw[0]->id;

                $userID = $guidToID->get($record->field_494_raw->email, null);
                $churchID = $guidToID->get($churchGUID, null);
                if ($userID === null || $churchID === null) {
                    //echo "Skipping: {$record->field_494_raw->email}: {$churchGUID}" .PHP_EOL;
                    continue;
                }

                $this->initRoles($userID, $churchID, $roles);
                $role = &$roles[$userID];
                $role->pastor = '1';
            }
            fclose($handle);
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 't1_point_people.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (empty($record->field_509_raw) || count($record->field_509_raw) === 0) {
                    //echo "T1 Point Person with no church: {$record->field_505_raw->email}" . PHP_EOL;
                    continue;
                }

                $churchGUID = $record->field_509_raw[0]->id;

                $userID = $guidToID->get($record->field_505_raw->email, null);
                $churchID = $guidToID->get($churchGUID, null);
                if ($userID === null || $churchID === null) {
                    //echo "Skipping: {$record->field_505_raw->email}: {$churchGUID}" .PHP_EOL;
                    continue;
                }

                $this->initRoles($userID, $churchID, $roles);
                $role = &$roles[$userID];

                $role->t1_point_person = true ? '1' : '0';
                $role->primary_point_person = !empty($record->field_1624_raw) && $record->field_1624_raw ? '1' : '0';
            }
            fclose($handle);
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'finance_people.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (empty($record->field_2212_raw) || count($record->field_2212_raw) === 0) {
                    //echo "T1 Point Person with no church: {$record->field_505_raw->email}" . PHP_EOL;
                    continue;
                }

                $churchGUID = $record->field_2212_raw[0]->id;

                $userID = $guidToID->get($record->field_2208_raw->email, null);
                $churchID = $guidToID->get($churchGUID, null);
                if ($userID === null || $churchID === null) {
                    //echo "Skipping: {$record->field_505_raw->email}: {$churchGUID}" .PHP_EOL;
                    continue;
                }

                $this->initRoles($userID, $churchID, $roles);
                $role = &$roles[$userID];

                $role->finance_person = '1';
            }
            fclose($handle);
        }

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'small_group_admins.json';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (empty($record->field_487_raw) || count($record->field_487_raw) === 0) {
                    //echo "SGA with no church: {$record->field_483_raw->email}" . PHP_EOL;
                    continue;
                }

                $churchGUID = $record->field_487_raw[0]->id;

                $userID = $guidToID->get($record->field_483_raw->email, null);
                $churchID = $guidToID->get($churchGUID, null);
                if ($userID === null || $churchID === null) {
                    //echo "Skipping: {$record->field_505_raw->email}: {$churchGUID}" .PHP_EOL;
                    continue;
                }

                $this->initRoles($userID, $churchID, $roles);
                $role = &$roles[$userID];

                $role->small_group_admin = '1';
            }
            fclose($handle);
        }

        $loadInFileHelper = new LoadInFileHelper(new App\Models\ChurchUser);
        foreach ($roles as $role) {
            $loadInFileHelper->fputcsv($role);
        }

        $insertCnt = $loadInFileHelper->saveAndLoad( 'SET updated_at=NOW(), created_at=NOW()',
            array('@created_at', '@updated_at'));

        $dt = Carbon::now()->toDateTimeString();
        echo "Finished Church Roles: {$dt}: {$insertCnt}" . PHP_EOL;
    }
}
