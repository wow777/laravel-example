<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    private function loadFile($tableName, $dates=array())
    {
        $genFileName = realpath(dirname(__FILE__)) . '/exports/gen_' . $tableName . '.csv';
        $genFileName = str_replace('\\', '/', $genFileName);

        if(file_exists($genFileName)) {
            $line = fgets(fopen($genFileName, 'r'));
            $fields = explode(',', $line);

            $strFields = implode(',', $fields);

            $query = "LOAD DATA LOCAL INFILE '{$genFileName}'
            INTO TABLE {$tableName} FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'
             IGNORE 1 LINES 
            ({$strFields})";

            $ret = DB::connection()->getpdo()->exec($query);
            foreach($dates as $date) {
                DB::connection()->getpdo()->exec("update {$tableName} set {$date}=NULL where year({$date})<\"1000\"");
            }

            echo "SEED:loaded {$ret} rows into {$tableName}" . PHP_EOL;
        }
        else {
            echo "ERROR: file not found: {$genFileName}" . PHP_EOL;
        }
    }

    private function importKnackRecords()
    {
        $this->loadFile('implementing_partners');
        $this->loadFile('users', array('deleted_at'));
        $this->loadFile('implementing_partner_user');
        $this->loadFile('gms');
        $this->loadFile('states');
        $this->loadFile('regions');
        $this->loadFile('implementing_partner_region');
        $this->loadFile('communities');
        $this->loadFile('community_user');
        $this->loadFile('counties', array('launch_date'));
        $this->loadFile('zip_codes');
        $this->loadFile('county_zip_code');
        $this->loadFile('tier2_service_types');
        $this->loadFile('tier3_service_types');
        $this->loadFile('agencies');
        $this->loadFile('agency_worker');
        $this->loadFile('agency_counties');
        $this->loadFile('rep_counties');
        $this->loadFile('agency_tier2_service_types');
        $this->loadFile('agency_tier3_service_types');
        $this->loadFile('churches', array('tier_1_enrollment_complete_date', 'deleted_at', 'special_offering_date', 'orientation_date',
            'giving_info_last_updated', 'last_special_offering_date', 'match_date', 'last_invoice_date'));
        $this->loadFile('church_user');
        $this->loadFile('groups', array('tier_1_enrollment_complete_date', 'deleted_at'));
        $this->loadFile('group_user');
        $this->loadFile('requests', array('date_closed', 'reviewed_at', 'next_auto_step_ts'));
        $this->loadFile('request_recipients');
        $this->loadFile('church_reviews');
        $this->loadFile('responses');
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('America/Chicago');

        $dtStart = Carbon::now();
        echo "Starting Seeding: {$dtStart->toDateTimeString()}" . PHP_EOL;

        $this->importKnackRecords();

        $this->call([
            GmsSeeder::class,
            TestNeedsSeeder::class,
            RegionLocationSeeder::class,
            CommunityLocationSeeder::class,
            AddRequestIDtoResponsesSeeder::class,
            EventSeeder::class,
            AgencyReviewsSeeder::class,
            AddAgencyIDToRepCountySeeder::class,
        ]);

        $dtEnd = Carbon::now();
        $diff = $dtEnd->diffInSeconds($dtStart);
        echo "Finished Seeding: {$dtEnd->toDateTimeString()} (took {$diff} seconds)" . PHP_EOL;
    }
}
