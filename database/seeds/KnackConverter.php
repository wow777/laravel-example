<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class KnackConverter extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('America/Chicago');

        ini_set('memory_limit', '2G');

        $dtStart = Carbon::now();
        echo "Starting conversion: {$dtStart->toDateTimeString()}" . PHP_EOL;

        $this->call([
            ImplementingPartnersTableSeeder::class,
            UsersTableSeeder::class,
            StatesTableSeeder::class,
            RegionsTableSeeder::class,
            CommunitiesTableSeeder::class,
            CountiesTableSeeder::class,
            ZipCodesTableSeeder::class,
            AgencyTableSeeder::class,
            ChurchesTableSeeder::class,
            GroupsTableSeeder::class,
            RequestTableSeeder::class
        ]);

        $dtEnd = Carbon::now();
        $diff = $dtEnd->diffInSeconds($dtStart);
        echo "Finished conversion: {$dtEnd->toDateTimeString()} (took {$diff} seconds)" . PHP_EOL;
    }
}
