<?php

use Illuminate\Database\Seeder;

class CommunityLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CommunityLocation::query()->truncate();

        $cnt = DB::select(DB::raw('select count(*) as count from community_locations') )[0]->count ;

        $this->command->info("$cnt current entries for community_locations");
        $this->command->info('Adding zipcodes ');
        DB::insert('insert into community_locations (county_id, zip_code_id, community_id, created_at, updated_at) 
                    select `knack_county_id`, id, community_id , NOW() , NOW() 
                    from zip_codes where community_id is not null');

        $this->command->info('Adding counties');
        DB::insert('insert into community_locations (county_id, zip_code_id, community_id, created_at, updated_at)  
                    select counties.id, cz.zip_code_id, counties.community_id, NOW() , NOW()  
                    from counties
                    join county_zip_code as cz on cz.county_id = counties.id 
                    where community_id is not null');

        $cnt = DB::select(DB::raw('select count(*) as count from community_locations') )[0]->count - $cnt;
        $this->command->info('Added '. $cnt);

    }
}
