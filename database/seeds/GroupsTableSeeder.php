<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    private function initRoles($userID, $churchID, $groupID, &$roles)
    {
        if (!array_key_exists($userID, $roles)) {
            $roles[$userID] = new App\Models\GroupUser;
            $role = &$roles[$userID];
            $role->church_id = $churchID;
            $role->group_id = $groupID;
            $role->user_id = $userID;
            $role->group_leader = '0';
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now()->toDateTimeString();
        echo "Starting Groups: {$dt}" . PHP_EOL;

        $guidToID = GUIDToID::instance();

        $filename = realpath(dirname(__FILE__)) . '/exports/' . 'groups.json';
        if (file_exists($filename)) {
            $group = new App\Models\Group;
            $loadInFileHelper = new LoadInFileHelper($group, array('tier_1_enrollment_complete_date', 'deleted_at'));
            $idx=0;
            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);

                $group->id = ++$idx;
                $group->created_at = Carbon::parse($record->field_668_raw->timestamp);
                $group->updated_at = Carbon::parse($record->field_1357_raw->timestamp);
                $group->deleted_at = empty($record->field_2128) ?
                    "0001-01-01" : Carbon::parse($record->field_2128);
                $group->name = $record->field_147;
                $group->group_size = $record->field_454;
                $group->how_did_you_hear = $record->field_387;
                $group->questions_comments = $record->field_166;
                $group->guid = $record->id;
                $group->tier_1_enrollment_complete = $record->field_729_raw ? '1' : '0';
                $group->tier_2_enrollment_complete = $record->field_956_raw ? '1' : '0';
                $group->tier_3_enrollment_complete = $record->field_957_raw ? '1' : '0';
                $group->tier_1_enrollment_complete_date =
                    empty($record->field_1154) ?
                    "0001-01-01" : Carbon::parse($record->field_1154);
                $group->address_line1 = $record->field_155;
                $group->address_line2 = $record->field_156;
                $group->country = 'United States of America'; //$record->field_474;
                $group->city = $record->field_1477;
                $group->lon = empty($record->field_386_raw->longitude) ? "\N" : $record->field_386_raw->longitude;
                $group->lat = empty($record->field_386_raw->latitude) ? "\N" : $record->field_386_raw->latitude;

                if(empty($record->field_457_raw[0]->id)) {
                    echo "Group has no parent church id: {$group->guid}" . PHP_EOL;
                    continue;
                }

                $group->church_id = $guidToID->get($record->field_457_raw[0]->id, "\N");
                $group->state_id = $guidToID->get($record->field_475_raw[0]->id, "\N");
                $group->zip_code_id = $guidToID->get($record->field_157_raw[0]->id, "\N");
                $group->county_id = $guidToID->get($record->field_473_raw[0]->id, "\N");

                // TEMP TEMP TEMP - a church that has an address should have a lat/lon
                if (empty($record->field_386_raw->longitude) || empty($record->field_386_raw->latitude)) {
                    echo "Group has no lat/lon: {$group->name}: {$group->guid} {$group->tier_1_enrollment_complete}" . PHP_EOL;
                }

                $loadInFileHelper->fputcsv($group);
                $guidToID->store($group->guid, $group->id);

                $guidToID->storeGroup($group->guid, $group->id, $group->church_id);

            }
            fclose($handle);

            $insertCnt = $loadInFileHelper->saveAndLoad();

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Groups: {$dt} added {$insertCnt}" . PHP_EOL;
        }


        $filename = realpath(dirname(__FILE__)) . '/exports/' . 't1_point_people.json';
        if (file_exists($filename)) {
            $roles = array();

            $dt = Carbon::now()->toDateTimeString();
            echo "Starting Group Roles: {$dt}" . PHP_EOL;

            $handle = fopen($filename, 'r');
            while(($buffer = fgets($handle, 16384)) !== false) {
                $record = json_decode($buffer);
                if (empty($record->field_509_raw) || count($record->field_509_raw) === 0) {
                    //echo "T1 Point Person with no church: {$record->field_505_raw->email}" . PHP_EOL;
                    continue;
                }

                $groupGUID = $record->field_509_raw[0]->id;

                $userID = $guidToID->get($record->field_505_raw->email, null);
                $groupVal = $guidToID->getGroup($groupGUID, false);
                if ($userID === null || $groupVal === false) {
                    //echo "Skipping: {$record->field_505_raw->email}: {$groupGUID}" .PHP_EOL;
                    continue;
                }

                $groupID = $groupVal['id'];
                $churchID = $groupVal['church_id'];

                if($churchID === "\N") {
                    continue;
                }

                $this->initRoles($userID, $churchID, $groupID, $roles);
                $role = &$roles[$userID];
                $role->group_leader = true ? '1' : '0';
            }
            fclose($handle);

            $loadInFileHelper = new LoadInFileHelper(new App\Models\GroupUser);
            foreach ($roles as $role) {
                $loadInFileHelper->fputcsv($role);
            }

            $insertCnt = $loadInFileHelper->saveAndLoad( 'SET updated_at=NOW(), created_at=NOW()',
                array('@created_at', '@updated_at'));

            $dt = Carbon::now()->toDateTimeString();
            echo "Finished Group Roles: {$dt}: {$insertCnt}" . PHP_EOL;
        }
    }
}
