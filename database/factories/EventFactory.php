<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Event::class, function (Faker $faker) {
    $types = ['Meeting', 'Fund Raiser'];
    return [
        'name' => $faker->name,
        'type' => $types[rand(0,1)],
        'start' => $start = \Carbon\Carbon::now()->addMonths(rand(1,12)),
        'end' => (clone $start)->addHours(rand(1,48)),
        'capacity' => rand(20,2000),
        'cost' => rand(10,300),
        'link' => $faker->url,
        'description' => $faker->text,
        'venue' => $faker->company,
        'address1' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
    ];
});

$factory->define(\App\Models\EventUser::class, function(Faker $faker){
   return [
       'date_invited' => \Carbon\Carbon::now(),
       'first_name' => $faker->firstName,
       'last_name' => $faker->lastName,
       'email' => $faker->email,
       'event_id' => \App\Models\Event::inRandomOrder()->first()->id,
   ];
});


