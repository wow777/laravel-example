<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_notes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('group_id')->index();
            $table->foreign('group_id')->references('id')->on('groups');

            // User that creates note
            $table->unsignedInteger('added_by')->index()->nullable();
            $table->foreign('added_by')->references('id')->on('users');

            $table->string('note_type')->nullable();
            $table->string('body', 500);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_notes');
    }
}
