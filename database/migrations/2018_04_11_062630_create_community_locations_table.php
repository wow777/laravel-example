<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_locations', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('community_id')->index();
            $table->foreign('community_id')->references('id')->on('communities');

            $table->unsignedInteger('zip_code_id')->index();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');

            $table->unsignedInteger('county_id')->index();
            $table->foreign('county_id')->references('id')->on('counties');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_locations');
    }
}
