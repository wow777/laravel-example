<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();

            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('guid')->nullable();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->string('user_photo_url')->nullable();
            $table->string('title')->nullable();
            $table->string('organization')->nullable();

            $table->boolean('send_email')->default(true);

            $table->date('date_of_birth')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('last_login')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
