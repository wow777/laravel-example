<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChurchToChurchContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('church_to_church_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('guid')->nullable(); // Not sure if this is need for import

            // to church id
            $table->unsignedInteger('to')->index();
            $table->foreign('to')->references('id')->on('churches');

            // from church id
            $table->unsignedInteger('from')->index();
            $table->foreign('from')->references('id')->on('churches');

            $table->unsignedInteger('request_id')->index();
            $table->foreign('request_id')->references('id')->on('requests');

            // From user
            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('church_to_church_contacts');
    }
}
