<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_counties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            // TEMP TEMP TEMP - not sure this is the way you'll want to do this
            // We need a list of supported counties for T1, T2 and T3
            $table->integer('tier');

            $table->unsignedInteger('agency_id')->index();
            $table->foreign('agency_id')->references('id')->on('agencies');

            $table->unsignedInteger('county_id')->index();
            $table->foreign('county_id')->references('id')->on('counties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_counties');
    }
}
