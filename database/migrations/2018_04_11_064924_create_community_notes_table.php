<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_notes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('community_id')->index();
            $table->foreign('community_id')->references('id')->on('communities');

            // User that creates note
            $table->unsignedInteger('added_by')->index()->nullable();
            $table->foreign('added_by')->references('id')->on('users');

            $table->string('note_type')->nullable();
            $table->string('body', 500);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_notes');
    }
}
