<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChurchReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('church_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('guid');

            $table->unsignedInteger('request_response_id')->index();
            $table->foreign('request_response_id')->references('id')->on('request_recipients');

            $table->unsignedInteger('church_id')->index();
            $table->foreign('church_id')->references('id')->on('churches');

            $table->unsignedInteger('group_id')->index()->nullable();
            $table->foreign('group_id')->references('id')->on('groups');

            $table->boolean('verify_did_you_help');
            $table->string('comments_questions');
            $table->string('your_story')->nullable();
            $table->integer('rating');

            $table->unsignedInteger('user_id')->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('church_reviews');
    }
}
