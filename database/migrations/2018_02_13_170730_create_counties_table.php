<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('guid');
            $table->string('status');
            $table->longtext('polygon_data');
            $table->date('launch_date')->nullable();
            $table->boolean('set_radius_to_community');
            $table->unsignedInteger('max_churches_in_radius');
            $table->unsignedInteger('max_radius');
            $table->unsignedInteger('esc_max_churches_in_radius');
            $table->unsignedInteger('esc_max_radius');

            $table->unsignedInteger('state_id')->index();
            $table->foreign('state_id')->references('id')->on('states');

            // A region can be made of counties OR zip codes.  A single county can only be in 1 region
            $table->unsignedInteger('region_id')->index()->nullable();
            $table->foreign('region_id')->references('id')->on('regions');

            // Active Community is defined just like a region
            $table->unsignedInteger('community_id')->index()->nullable();
            $table->foreign('community_id')->references('id')->on('communities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counties');
    }
}
