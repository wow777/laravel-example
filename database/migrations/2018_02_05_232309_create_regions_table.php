<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('guid')->nullable();
            $table->string('name')->nullable();

            $table->unsignedInteger('regional_manager_id')->index()->nullable();
            $table->foreign('regional_manager_id')->references('id')->on('users');

            // A region can only be in 1 state
            // If it's made up of zip codes, this will tell us which state the region is in
            // as a zip code could fall into multiple states
            $table->unsignedInteger('state_id')->index();
            $table->foreign('state_id')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
