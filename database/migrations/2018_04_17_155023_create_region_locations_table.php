<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region_locations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('region_id')->index();
            $table->foreign('region_id')->references('id')->on('regions');

            $table->unsignedInteger('zip_code_id')->index();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');

            $table->unsignedInteger('county_id')->index();
            $table->foreign('county_id')->references('id')->on('counties');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region_locations');
    }
}
