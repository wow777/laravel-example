<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollment_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 120);
            $table->string('last_name', 120);
            $table->string('email', 120);
            $table->string('hear_about', 120);
            $table->string('ministry_implement', 120);
            $table->string('phone_number', 30);
            $table->mediumText('comments')->nullable();

            // Keys
            $table->unsignedInteger('church_id');
            $table->foreign('church_id')
                ->references('id')->on('churches')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrollment_requests');
    }
}
