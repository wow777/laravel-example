<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            // NOTE: This table needs the connections to county OR zip codes to define an area
            // just like Regions does
            $table->string('name');
            $table->string('guid')->nullable();

            $table->boolean('area_of_critical_need')->nullable();

            $table->unsignedInteger('coordinator_id')->index()->nullable();
            $table->foreign('coordinator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Keep until everyone has run this migration, this table name was changed
        Schema::dropIfExists('active_communities');

        Schema::dropIfExists('communities');
    }
}
