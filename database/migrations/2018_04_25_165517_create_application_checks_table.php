<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_checks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('background_check')->nullable();
            $table->date('date_checked')->nullable();

            $table->string('training')->nullable();
            $table->date('date_trained')->nullable();

            $table->string('home_walkthrough')->nullable();
            $table->date('date_walkthrough')->nullable();

            $table->unsignedInteger('application_id')->index();
            $table->foreign('application_id')->references('id')->on('applications');

            $table->string('service_type'); // This should connect to service type id but currently the structure is incompatible

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_checks');
    }
}
