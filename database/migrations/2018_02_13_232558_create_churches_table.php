<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChurchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('churches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('guid')->nullable();
            $table->string('name');
            $table->boolean('has_enrolled')->default(false);
            $table->string('affiliation')->nullable();

            // Multiple choice with values:
            /*
                Less than 100
                100-250
                1001-2500
                251-600
                601-1000
                Over 2500
            */
            $table->string('average_church_attendance')->nullable();

            // Multiple choice
            /*
                Concentrated Poverty
                High Income
                Low Income
                Middle Income
            */
            $table->string('economic_conditions')->nullable();

            // Multiple choice
            /*
                Rural
                Suburban
                Urban
             */
            $table->string('location_type')->nullable();

            $table->text('questions_comments')->nullable();

            // Multiple choice
            /*
                A Different Way
                Another Church
                Another Ministry
                CarePortal Team
                Child Welfare Worker
                Someone in My Church
            */
            $table->string('how_did_you_hear')->nullable();

            // Multiple choice
            /* The ** values might be in the database, but shouldn't be available on the enrollment form
                **Our church is unable to give financially at this time but plans to actively participate in CarePortal
                **Our church commits to giving the recommended $100/month
                **Our church commits to giving $1200 annually
                **Our church commits to giving another amount
                We will give $100/month
                We will give $1,200/year
                We will give more than $100/month to provide access to financially under-resourced churches.
                We need to contribute at a lower amount this year.
                We plan to only collect a special offering for now
                **We plan to continue with our current recurring giving amount
                **Our church commits to giving another amount beginning at the time of our county’s launch of CarePortal
                Giving via Implementing Partner
             */
            $table->string('giving_options')->nullable();
            $table->decimal('giving_amount')->nullable();

            // Not sure we actually need both of these in the database
            // The boolean was needed for the knack form, but I don't believe it's needed here
            // basically if the flag is true, the date has to be there
            // if the flag is false, the date shouldn't be there
            $table->boolean('collect_special_offering')->nullable();
            $table->date('special_offering_date')->nullable();

            $table->date('giving_info_last_updated')->nullable();

            $table->date('last_special_offering_date')->nullable();
            $table->decimal('last_special_offering_amount')->nullable();
            $table->decimal('match_amount')->nullable();
            $table->date('match_date')->nullable();
            $table->text('match_notes')->nullable();

            // TEMP TEMP TEMP: The giving_notes on the 2.0 church record should move into the general NOTES for a church
            // $table->text('giving_notes')->nullable();

            // Multiple Choice
            /*
                Monthly
                Quarterly
                Annually
                One-time
             */
            $table->string('giving_frequency')->nullable();
            $table->boolean('send_invoice')->nullable();

            $table->timestamp('last_invoice_date')->nullable();

            // Address
            $table->string('address_line1');
            $table->string('address_line2')->nullable();
            $table->string('country');
            $table->string('city');

            // TEMP TEMP TEMP - remove nullable: a church that has an address should have a lat/lon
            $table->decimal('lon', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();

            $table->date('orientation_date')->nullable();

            // Multiple choice
            /*
                A different way
                Pastor gives a vision-casting sermon that incorporates the Care Portal video and an individual sign up process
                Send email to congregation with the video link and opportunity to sign up
                Send email to your small group with the video link and ask who wants in
                Show video and provide a sign up process
                Show video at a meeting and ask who wants to sign up
             */
            $table->string('engage_congregation')->nullable();

            $table->text('engage_share_your_ideas')->nullable();

            // Multiple choice
            /*
                A different Way
                Call people you think could help
                Forward a portion of the emails and have them reply to the Point Person if they can help
                Forward the emails that come and let them respond directly
                Make the need known via Facebook or other social media
                Meet the need when you can using church resources
                Needs go out through small group leaders
                Send a text about the request
             */
            $table->string('how_will_you_share_requests')->nullable();

            $table->text('share_needs_share_your_ideas')->nullable();

            $table->boolean('activate_small_groups')->default(false);

            // These three fields are for the person initially enrolling the church
            // This person doesn't need to be a "User" this may be the only thing they ever
            // do in the system.
            $table->string('initial_enrollment_name')->nullable();
            $table->string('initial_enrollment_email')->nullable();
            $table->string('initial_enrollment_phone')->nullable();
            $table->boolean('activation_step_1_complete')->nullable();
            $table->boolean('activation_step_2_complete')->nullable();
            $table->boolean('activation_step_3_complete')->nullable();
            $table->boolean('tier_1_enrollment_complete')->nullable();
            $table->boolean('tier_2_enrollment_complete')->nullable();
            $table->boolean('tier_3_enrollment_complete')->nullable();

            $table->date('tier_1_enrollment_complete_date')->nullable();

            // non_cp name and phone are values from a bought database of churches
            $table->string('non_cp_contact_name')->nullable();
            $table->string('non_cp_phone')->nullable();

            $table->unsignedInteger('state_id')->index();
            $table->foreign('state_id')->references('id')->on('states');

            $table->unsignedInteger('county_id')->index();
            $table->foreign('county_id')->references('id')->on('counties');

            $table->unsignedInteger('zip_code_id')->index();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');

            $table->unsignedInteger('implementing_partner_id')->index()->nullable();
            $table->foreign('implementing_partner_id')->references('id')->on('implementing_partners');

            $table->unsignedInteger('ambassador_id')->index()->nullable();
            $table->foreign('ambassador_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('churches');
    }
}
