<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('rating');
            $table->text('comments_questions')->nullable();
            $table->text('your_story')->nullable();

            $table->unsignedInteger('agency_id')->nullable();
            $table->foreign('agency_id')
                ->references('id')->on('agencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedInteger('agency_worker_id')->nullable();
            $table->foreign('agency_worker_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });

        \App\Library\AgencyReviewSeedingService::migrateAgencyReviews();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_reviews');
    }
}
