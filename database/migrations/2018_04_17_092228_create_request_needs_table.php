<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestNeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_needs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('value')->nullable();
            $table->string('description', 500)->nullable();

            $table->unsignedInteger('request_id')->index();
            $table->foreign('request_id')->references('id')->on('requests');

            $table->boolean('met')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_needs');
    }
}
