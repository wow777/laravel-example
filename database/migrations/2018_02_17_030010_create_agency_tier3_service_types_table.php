<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTier3ServiceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_tier3_service_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('agency_id')->index();
            $table->foreign('agency_id')->references('id')->on('agencies');

            $table->unsignedInteger('tier3_service_type_id')->index();
            $table->foreign('tier3_service_type_id')->references('id')->on('tier3_service_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_tier3_service_types');
    }
}
