<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImplementingPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('implementing_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('guid')->nullable();
            $table->string('logo_url');

            // TEMP TEMP TEMP giving_option is a multi choice in 2.0
            // with values, but I think the integer value is more appropriate for the db
            // We think of an IP as option 1 or option 2
            /*
                1) Include giving information during enrollment
                2) Exclude giving information during enrollment
             */
            $table->integer('giving_option')->unsigned();

            $table->integer('giving_percent')->unsigned();
            $table->integer('national_percent')->unsigned();
            $table->integer('state_director_percent')->unsigned();
            $table->integer('rm_percent')->unsigned();
            $table->integer('ambassador_percent')->unsigned();

            $table->string('website_url')->nullable();
            $table->string('phone')->nullable();

            // Address
            $table->string('address_line1')->nullable();
            $table->string('city')->nullable();

            // TEMP TEMP TEMP - these should probably be foreign key's into the states and zip_codes
            // tables but not sure how to do that given those tables aren't created yet
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();

            // Need to add excluded agencies
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('implementing_partners');
    }
}
