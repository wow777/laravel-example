<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('gender')->nullable();
            $table->string('race')->nullable();
            $table->string('marital status')->nullable();

            $table->string('ethnicity')->nullable();
            $table->string('tribal_affiliation')->nullable();

            $table->string('ssn')->nullable();
            $table->string('drivers_license_number')->nullable();
            $table->string('drivers_license_state')->nullable();
            $table->string('occupation')->nullable();
            $table->string('title')->nullable();
            $table->string('employer')->nullable();
            $table->string('employer_phone')->nullable();

            $table->string('best_suited_children')->nullable();
            $table->integer('number_children')->nullable();
            $table->string('home_status')->nullable();
            $table->integer('bedrooms')->nullable();
            $table->boolean('smoker')->nullable();
            $table->text('family_interest')->nullable();
            $table->text('family_hobbies')->nullable();

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}



