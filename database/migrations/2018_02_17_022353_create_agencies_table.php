<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('name2'); // Not sure why there are two names
            $table->string('global_email')->nullable();
            $table->string('email_domain')->nullable();
            $table->string('guid')->nullable();
            $table->string('address_line1');
            $table->string('address_line2')->nullable();
            $table->string('country');
            $table->string('city');

            $table->mediumText('about_text')->nullable();
            $table->string('website')->nullable();
            $table->string('logo_url')->nullable();
            $table->integer('request_submission_option')->nullable();

            // TEMP TEMP TEMP - should these be in a table like Knack???
            $table->boolean('tier_1_approval')->nullable();
            $table->boolean('tier_2_approval')->nullable();
            $table->boolean('tier_3_approval')->nullable();

            $table->unsignedInteger('state_id')->index();
            $table->foreign('state_id')->references('id')->on('states');

            $table->unsignedInteger('zip_code_id')->index();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}
