<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestsTableColumnAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->string('guid')->nullable()->change();
            $table->string('role_of_worker')->nullable()->change();
            $table->string('grid_category')->nullable()->change();
            $table->integer('level_of_urgency')->nullable()->change();
            $table->text('case_description')->nullable()->change();
            $table->integer('children_served')->nullable()->change();
            $table->boolean('strictly_financial')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->string('guid')->nullable(false)->change();
            $table->string('role_of_worker')->nullable(false)->change();
            $table->string('grid_category')->nullable(false)->change();
            $table->integer('level_of_urgency')->nullable(false)->change();
            $table->text('case_description')->nullable(false)->change();
            $table->integer('children_served')->nullable(false)->change();
            $table->boolean('strictly_financial')->nullable(false)->change();
        });
    }
}
