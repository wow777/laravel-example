<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChurchUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('church_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedInteger('church_id')->index();
            $table->foreign('church_id')->references('id')->on('churches');

            $table->boolean('t1_point_person')->default(false);
            $table->boolean('primary_point_person')->default(false);
            $table->boolean('t2_point_person')->default(false);
            $table->boolean('t3_point_person')->default(false);
            $table->boolean('pastor')->default(false);
            $table->boolean('finance_person')->default(false);
            $table->boolean('small_group_admin')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('church_user');
    }
}
