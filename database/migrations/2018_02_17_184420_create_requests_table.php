<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->timestamp('reviewed_at')->nullable();

            // TEMP TEMP TEMP status (Pending, Approved, Declined, Expired) Add Closed and switch Approved to Open???
            $table->string('status');
            $table->string('guid');
            $table->unsignedTinyInteger('tier'); // TEMP TEMP TEMP Tier - int or connection
            $table->string('agency_case_id');

            /*
                Adoption/Post Adopt
                Comprehensive / Generic
                Family Preservation/In Homes
                Foster Care / Kinship Care / Family Worker
                Independent Living/Aging out of Care
                Investigation/CPS
                Other
                Permanency / Conservatorship / Legal / Family Reunification
             */
            $table->string('role_of_worker');

            /*
                Prevention
                Foster Care
                Adoption
                Transition (Aging Out)
             */
            $table->string('grid_category');

            /*
                0 - Normal; Within 7-10 days
                1 - High; Needed Within 72 hrs
                2 - Critical; Needed Within 24 hrs
             */
            $table->integer('level_of_urgency');

            /*
                Help Make a Foster Care or Kinship Placement
                Help Make an Adoptive Placement
                Help Preserve a Foster Care or Kinship Placement
                Help Preserve an Adoptive Placement
                Help Prevent a Child From Entering Foster Care
                Help Reunify a Bio Family
                Help Strengthen a Bio Family
                Help Support  a Youth Aging Out
                Help Improve a Child's Well-being
                Prayer Request
             */
            $table->string('purpose_of_request');

            $table->text('case_description');
            $table->integer('children_served');
            $table->boolean('strictly_financial');
            $table->integer('estimated_value')->default(0);
            $table->integer('cost_avoidance')->default(0);
            $table->integer('service_hours_hours_per')->nullable();
            $table->integer('service_hours_times_per_week')->nullable();
            $table->integer('service_hours_total_weeks')->nullable();

            $table->unsignedInteger('tier2_service_type_id')->index()->nullable();
            $table->foreign('tier2_service_type_id')->references('id')->on('tier2_service_types');

            $table->unsignedInteger('tier3_service_type_id')->index()->nullable();
            $table->foreign('tier3_service_type_id')->references('id')->on('tier3_service_types');

            $table->string('reviewed_by')->nullable();
            $table->text('decline_reason')->nullable();
            $table->string('supervisor_email')->nullable();

            /*
                Church Responded and Met the Need
                Other Resources Came to Meet the Need
                Time Has Passed and No Longer Relevant
                Other Reason
                I Need to Cancel this Request
                Request Expired
             */
            $table->string('reason_closed')->nullable();
            $table->boolean('responding_churches_not_listed')->nullable();
            $table->timestamp('date_closed')->nullable();
            $table->integer('rating')->nullable();
            $table->text('reason_for_rating')->nullable();
            $table->text('worker_story')->nullable();

            $table->string('next_auto_step')->nullable();
            $table->timestamp('next_auto_step_ts')->nullable();

            $table->timestamp('reminder_sent_at')->nullable();

            $table->unsignedInteger('zip_code_id')->index()->nullable();  // TEMP TEMP TEMP - not nullable
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');

            $table->unsignedInteger('agency_id')->index()->nullable();// TEMP TEMP TEMP - not nullable
            $table->foreign('agency_id')->references('id')->on('agencies');

            $table->unsignedInteger('worker_id')->index()->nullable();// TEMP TEMP TEMP - not nullable
            $table->foreign('worker_id')->references('id')->on('users');

            $table->unsignedInteger('county_id')->index()->nullable();// TEMP TEMP TEMP - not nullable
            $table->foreign('county_id')->references('id')->on('counties');

            $table->unsignedInteger('state_id')->index()->nullable();  // TEMP TEMP TEMP - not nullable
            $table->foreign('state_id')->references('id')->on('states');

            $table->unsignedInteger('worker_county_id')->index()->nullable();// TEMP TEMP TEMP - not nullable
            $table->foreign('worker_county_id')->references('id')->on('counties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
