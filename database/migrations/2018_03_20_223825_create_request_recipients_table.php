<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('guid');

            $table->unsignedInteger('request_id')->index();
            $table->foreign('request_id')->references('id')->on('requests');

            $table->unsignedInteger('church_id')->index();
            $table->foreign('church_id')->references('id')->on('churches');

            $table->unsignedInteger('group_id')->index()->nullable();
            $table->foreign('group_id')->references('id')->on('groups');

            $table->unsignedInteger('user_id')->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->boolean('helped_meet_the_need');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_recipients');
    }
}
