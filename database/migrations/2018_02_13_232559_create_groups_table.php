<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('guid')->nullable();
            $table->string('name');

            // Multiple choice with values: Terry has the values
            $table->string('group_size')->nullable();

            $table->text('questions_comments')->nullable();

            // Multiple choice
            /*
                Another Church
                Another Ministry
                CarePortal Team
                Child Welfare Worker
                Someone in My Church
                Another Way
            */
            $table->string('how_did_you_hear')->nullable();

            // TEMP TEMP TEMP - I'm not sure if these fields are needed, if this person is becoming the T1 point person
            // These three fields are for the person initially enrolling the church
            // This person doesn't need to be a "User" this may be the only thing they ever
            // do in the system.
            $table->string('initial_enrollment_name')->nullable();
            $table->string('initial_enrollment_email')->nullable();
            $table->string('initial_enrollment_phone')->nullable();

            $table->boolean('tier_1_enrollment_complete')->nullable();
            $table->boolean('tier_2_enrollment_complete')->nullable();
            $table->boolean('tier_3_enrollment_complete')->nullable();

            $table->date('tier_1_enrollment_complete_date')->nullable();

            // Address
            $table->string('address_line1');
            $table->string('address_line2')->nullable();
            $table->string('country');
            $table->string('city');

            // TEMP TEMP TEMP - remove nullable: a church that has an address should have a lat/lon
            $table->decimal('lon', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();

            $table->unsignedInteger('church_id')->index();
            $table->foreign('church_id')->references('id')->on('churches');

            $table->unsignedInteger('state_id')->index();
            $table->foreign('state_id')->references('id')->on('states');

            $table->unsignedInteger('county_id')->index();
            $table->foreign('county_id')->references('id')->on('counties');

            $table->unsignedInteger('zip_code_id')->index();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
