<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountyZipCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('county_zip_code', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('county_id')->index();
            $table->foreign('county_id')->references('id')->on('counties');

            $table->unsignedInteger('zip_code_id')->index();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes');

            // The Percentage of Total Population of the 2010 ZCTA represented by the record
            $table->decimal('zpoppct');

            // The Percentage of Total Housing Unit Count of the 2010 ZCTA represented by the record
            $table->decimal('zhupct');

            //The Percentage of Total Area of the 2010 ZCTA represented by the record
            $table->decimal('zareapct');

            // The Percentage of Total Land Area of the 2010 ZCTA represented by the record
            $table->decimal('zarealandpct');

            // The Percentage of Total Population of the 2010 County represented by the record
            $table->decimal('copoppct');

            // The Percentage of Total Housing Unit Count of the 2010 County represented by the record
            $table->decimal('cohupct');

            // The Percentage of Total Area of the 2010 County represented by the record
            $table->decimal('coareapct');

            // The Percentage of Total Land Area of the 2010 County represented by the record
            $table->decimal('coarealandpct');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('county_zip_code');
    }
}
