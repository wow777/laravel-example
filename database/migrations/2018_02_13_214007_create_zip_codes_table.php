<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZipCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zip_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('code');
            $table->string('city');
            $table->decimal('lon', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();
            $table->string('guid')->nullable();

            // A region can be made of counties OR zip codes.  A single county can only be in 1 zip code
            $table->unsignedInteger('region_id')->index()->nullable();
            $table->foreign('region_id')->references('id')->on('regions');

            // A community is defined just like a region
            $table->unsignedInteger('community_id')->index()->nullable();
            $table->foreign('community_id')->references('id')->on('communities');

            // TEMP TEMP TEMP - keeping state_id and county_id for now, but they should NOT be used.
            $table->unsignedInteger('knack_state_id')->index();
            $table->foreign('knack_state_id')->references('id')->on('states');

            $table->unsignedInteger('knack_county_id')->index();
            $table->foreign('knack_county_id')->references('id')->on('counties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zip_codes');
    }
}
