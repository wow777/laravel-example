<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('guid');

            $table->unsignedInteger('request_recipient_id')->index();
            $table->foreign('request_recipient_id')->references('id')->on('request_recipients');

            $table->unsignedInteger('request_id')->index()->nullable();
            $table->foreign('request_id')->references('id')->on('requests');

            $table->unsignedInteger('church_id')->index();
            $table->foreign('church_id')->references('id')->on('churches');

            $table->unsignedInteger('group_id')->index()->nullable();
            $table->foreign('group_id')->references('id')->on('groups');

            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
