<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Routes to make
// apiResource routes include store , show , update , destroy
// search user
// user by email
// team/{zipcode} gives all regional managers/ ambassadors/ coordinators/ state directors for zip
// team all show all users of role- regional manager/ ...

Route::group(['namespace' => 'API'], function() {

    Route::group(['prefix' => '/users'], function() {
        Route::get('/team/{zipcode}', 'UserController@team');
        Route::get('/{user}/communities', 'UserController@showCommunities');
        Route::post('/add-communities', 'UserController@addCommunities');
        Route::get('/email', 'UserController@getByEmail');
        Route::get('/{id}', 'UserController@show');
    });

    Route::group(['prefix' => '/requests'], function() {
        Route::get('/validate-email', 'RequestController@validateAgencyEmail');
        Route::post('/step-1', 'RequestController@createRequestStep1');
        Route::post('/step-2', 'RequestController@createRequestStep2');
        Route::post('/need', 'RequestController@createRequestNeed');
    });

// active churches
// list of churches
// list of churches filtered by
    Route::apiResource('/churches', 'ChurchController');
    Route::get('/churches/zipcode/{zipcode}', 'ChurchController@churchZip');

// add small group
    Route::apiResource('groups', 'GroupController');

// sign up for active community
    Route::apiResource('/communities', 'CommunityController');

    Route::apiResource('/zipcode', 'ZipCodeController');

    Route::apiResource('/agency', 'AgencyController');

    Route::get('/home', 'HomeController@index');
    Route::get('/impact', 'HomeController@impact');
});

