<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Middleware\ByRole;
use App\Http\Middleware\RoleAccessUserVerify;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('oauth', function() {
    $client = new GuzzleHttp\Client([
        'base_uri' => 'http://159.203.116.109',
    ]);
    $response = $client->post('/oauth/token', [
        'form_params' => [
            'client_id' => 1,
            'client_secret' => 'y4o6EszE8iY89AS7CdX05LphPkMlfKu4w6GGT2b1',
            'grant_type' => 'client_credentials',
        ]
    ]);

    $data = json_decode($response->getBody()->__toString());

    $response = $client->request('GET', '/api/user', [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '. $data->access_token,
        ],
    ]);
    echo $response->getBody();
    die;
});


Route::group(['prefix' => '/role/{role}/{role_id}', 'middleware' => [ByRole::class, 'auth']], function () {

    Route::get('/dashboard', 'RoleController@dashboard')->name('role.dashboard');

    Route::get('/dashboard/data-export/{dataSource}', 'RoleController@dashboardDataExport')->name('roleDashboardDataExport');

    Route::get('/regions', 'RoleController@regions');

    Route::get('/churches', 'RoleController@churches');

    //Route::get('/agencies', 'RoleController@agencies');
	
    Route::group(['prefix' => '/agencies'], function() {
        Route::get('/', 'AgencyController@index')->name('agency.index');
		Route::get('/agency-details', 'AgencyController@agenciesDetails')->name('agency.agencies-details');
		Route::get('/county-details', 'AgencyController@countyDetails')->name('agency.county-details');
		Route::post('/deactivatedAgency', 'AgencyController@deactivatedAgency');
		Route::post('/addEditAgency', 'AgencyController@addEditAgency');
		Route::post('/editAgency', 'AgencyController@editAgency');
		Route::post('/autoPopulateEmailCounty', 'AgencyController@autoPopulateEmailCounty');
		Route::post('/addEditCounty', 'AgencyController@addEditCounty');
		Route::post('/deactivatedCounty', 'AgencyController@deactivatedCounty');
		Route::post('/editCounty', 'AgencyController@editCounty');
		Route::post('/deleteAgencyTier1', 'AgencyController@deleteAgencyTier1');
		Route::post('/deleteAgencyTier2', 'AgencyController@deleteAgencyTier2');
		Route::post('/deleteAgencyTier3', 'AgencyController@deleteAgencyTier3');
		Route::post('/setDeactivateAgency', 'AgencyController@setDeactivateAgency');
    });
	
    Route::get('/communities', 'RoleController@communities');

    Route::get('/contacts', 'RoleController@contacts')->name('role.contacts');
    Route::group(['prefix' => '/contacts/{user}', 'middleware' => RoleAccessUserVerify::class], function() {
        Route::get('/', 'ContactsController@view')->name('contact.view');
        Route::get('edit', 'ContactsController@edit')->name('contact.edit');
        Route::put('/', 'ContactsController@update')->name('contact.update');
        Route::get('deactivate', 'ContactsController@deactivate')->name('contact.deactivate');
        Route::get('activate', 'ContactsController@activate')->name('contact.activate');
    });
    Route::get('/community/{id}', 'CommunityController@show')->name('communityDetail');;

    Route::get('/events', 'RoleController@events');

    Route::get('/finances', 'RoleController@finances');

    Route::get('/implementing-partners', 'RoleController@implementingPartner')->name('role.implementingPartners');
    Route::group(['prefix' => '/implementing-partners/{implementingPartner}'], function() {
        Route::get('/', 'ImplementingPartnersController@show')->name('implementingPartner.view');
        Route::put('/', 'ImplementingPartnersController@update')->name('implementingPartner.update');
        Route::get('deactivate', 'ImplementingPartnersController@deactivate')->name('implementingPartner.deactivate');
        Route::get('activate', 'ImplementingPartnersController@activate')->name('implementingPartner.activate');
        Route::get('/churches-export', 'ImplementingPartnersController@churchesExport')->name('implementingPartner.churchesExport');
        Route::get('/regions-export', 'ImplementingPartnersController@regionsExport')->name('implementingPartner.regionsExport');
        Route::post('/manage-regions', 'ImplementingPartnersController@manageRegions')->name('implementingPartner.manageRegions');
        Route::post('/manage-contacts', 'ImplementingPartnersController@manageContacts')->name('implementingPartner.manageContacts');
        Route::get('/regions/{region}/detach', 'ImplementingPartnersController@regionDetach')->name('implementingPartner.regionDetach');
        Route::get('/contacts-add', 'ImplementingPartnersController@contactsAdd')->name('implementingPartner.contactsAdd');
        Route::get('/contacts-export', 'ImplementingPartnersController@contactsExport')->name('implementingPartner.contactsExport');
        Route::get('/contacts/{user}/detach', 'ImplementingPartnersController@contactDetach')->name('implementingPartner.contactDetach');
    });

    Route::get('/region', 'RoleController@regions');

    Route::get('/requests', 'RoleController@requests');

    //Route::get('/settings', 'RoleController@settings');
	
    Route::group(['prefix' => '/settings'], function() {
        Route::get('/resources', 'ResourceController@resources')->name('resource.resources');
		Route::post('/addEditResources', 'ResourceController@addEditResources');
		Route::post('/editResources', 'ResourceController@editResources');
		Route::post('/deleteResources', 'ResourceController@deleteResources');
		Route::post('/deactivatedResources', 'ResourceController@deactivatedResources');
		Route::post('/deleteCategory', 'ResourceController@deleteCategory');
    });
    Route::get('/states', 'RoleController@states');

    Route::get('/applications', 'RoleController@applications');
});

Route::group(['prefix' => '/enrollment'], function () {

    Route::get('/orientation/church/{church}', 'ChurchEnrollmentController@orientation');

    Route::get('/activate/church/{church}', 'ChurchEnrollmentController@activate');

    Route::post('/activate', 'ChurchEnrollmentController@storeActivation')->name('storeActivation');

    Route::post('/orientation', 'ChurchEnrollmentController@StoreOrientation')->name('StoreOrientation');

    Route::get('/church-to-church/{request_id}/{church_id}', 'ChurchEnrollmentController@churchToChurch');

    Route::get('/contact-to-church/{request_id}/{church_id}', 'ChurchEnrollmentController@contactToChurch')->name('getContact');

    Route::get('/close-request/{request_id}', 'ChurchEnrollmentController@closeRequest');

    Route::post('/close-request', 'ChurchEnrollmentController@storeCloseRequest')->name('storeCloseRequest');

    Route::get('/respond-to-request/{request_id}/{church_id}', 'ChurchEnrollmentController@responseToRequest');

    Route::get('/verify-participation/{request_id}/{church_id}', 'ChurchEnrollmentController@verifyParticipation');

    Route::get('/tier2-app-bg-check-approval', 'ChurchEnrollmentController@tier2AppBgCheckApproval');

    Route::get('/tier2-app-training-update', 'ChurchEnrollmentController@tier2AppTrainingUpdate');

    Route::get('/tier3-bg-check-approval', 'ChurchEnrollmentController@tier3BgCheckApproval');

});

